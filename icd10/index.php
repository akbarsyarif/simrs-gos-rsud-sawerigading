<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>Master ICD10</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<a href="#modal-add-icd10" data-toggle="modal" class="btn btn-sm btn-primary">Tambah</a>
				<a href="#modal-search-icd10" data-toggle="modal" class="btn btn-sm btn-default">Cari Data ICD</a>
			</div>
			<div class="col-md-6"></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered" id="datatable-icd10">
					<thead>
						<th width="50">Kode</th>
						<th>Nama Penyakit (Latin)</th>
						<th>Nama Penyakit (Indonesia)</th>
						<th>Nama Penyakit (Lokal RS)</th>
						<th>DTD</th>
						<th>No. Urut</th>
						<th width="10"></th>
						<th width="10"></th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modal_search_icd10.php' ?>
<?php require 'includes/modal_add_icd10.php' ?>
<?php require 'includes/modal_edit_icd10.php' ?>

<!-- javascript -->
<?php require 'includes/js/index.js.php' ?>