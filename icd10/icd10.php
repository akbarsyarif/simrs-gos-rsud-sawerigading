<?php

require BASEPATH . 'models/m_icd10.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('datatableIcd10') )
{

	function datatableIcd10()
	{

		$query = array();
		$query['query'] = '';

		// columns index
		$column_index = array('icd.icd_code', 'icd.jenis_penyakit', 'icd.jenis_penyakit_local', 'icd.jenis_penyakit_local_rs', 'icd.dtd', 'icd.nourutlap', 'icd.sebabpenyakit');

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['query'] .= " WHERE " . $column_index[0] . " = '{$search_value}'";

		}
		else {

			$i = 0;

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $i == 0 ) {
						$query['query'] .= " WHERE ";
					}
					else {
						$query['query'] .= " AND ";
					}

					if ( $column_key == 0 ) {
						$query['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
					}
					else {
						$query['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
					}

					$i++;

				}

			}

		}

		$records_total = Models\ICD10\count($query);
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order'] = $column_index[$order_column] . " " . $order_dir;
			}
		}

		// limit
		$query['limit'] = $_GET['length'];
		$query['offset'] = $_GET['start'];

		$data = Models\ICD10\get($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('getIcd10') )
{

	function getIcd10()
	{

		$icd_code = xss_clean($_GET['icd_code']);

		$result = Models\ICD10\row( array (
			'query' => "WHERE icd_code = '{$icd_code}'"
		) );

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "204",
					'message' => "Data tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $result
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('insert') )
{

	function insert()
	{

		$data = array (
			'icd_code' => xss_clean($_POST['icd_code']),
			'jenis_penyakit' => xss_clean($_POST['jenis_penyakit']),
			'jenis_penyakit_local' => xss_clean($_POST['jenis_penyakit_local']),
			'jenis_penyakit_local_rs' => xss_clean($_POST['jenis_penyakit_local_rs']),
			'dtd' => xss_clean($_POST['dtd']),
			'nourutlap' => xss_clean($_POST['nourutlap']),
			'sebabpenyakit' => xss_clean($_POST['sebabpenyakit'])
		);

		$result = Models\ICD10\insert($data);

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "422",
					'message' => "Server error. Gagal menyimpan data."
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $data
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('update') )
{

	function update()
	{

		$pk = xss_clean($_POST['pk']);

		$data = array (
			'icd_code' => xss_clean($_POST['icd_code']),
			'jenis_penyakit' => xss_clean($_POST['jenis_penyakit']),
			'jenis_penyakit_local' => xss_clean($_POST['jenis_penyakit_local']),
			'jenis_penyakit_local_rs' => xss_clean($_POST['jenis_penyakit_local_rs']),
			'dtd' => xss_clean($_POST['dtd']),
			'nourutlap' => xss_clean($_POST['nourutlap']),
			'sebabpenyakit' => xss_clean($_POST['sebabpenyakit'])
		);

		$result = Models\ICD10\update($data, $pk);

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "422",
					'message' => "Server error. Gagal menyimpan data."
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $data
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('delete') )
{

	function delete()
	{

		$icd_code = xss_clean($_GET['icd_code']);

		$result = Models\ICD10\delete($icd_code);

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "422",
					'message' => "Server error. Gagal menghapus data."
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $kode
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------