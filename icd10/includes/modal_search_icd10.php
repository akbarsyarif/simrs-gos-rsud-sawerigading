<div class="modal fade" id="modal-search-icd10">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST" id="form-search-icd10" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-search-plus"></i> Pencarian Lanjutan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Kode</label>
						<input type="text" name="icd_code" class="form-control" data-column="0" />
					</div>
					<div class="form-group">
						<label>Nama Penyakit (Latin)</label>
						<input type="text" name="jenis_penyakit" class="form-control" data-column="1" />
					</div>
					<div class="form-group">
						<label>Nama Penyakit (Indonesia)</label>
						<input type="text" name="jenis_penyakit_local" class="form-control" data-column="2" />
					</div>
					<div class="form-group">
						<label>Nama Penyakit (Lokal RS)</label>
						<input type="text" name="jenis_penyakit_local_rs" class="form-control" data-column="3" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="clear" class="btn btn-warning btn-sm" data-dismiss="modal">Bersihkan</button>
					<button type="submit" class="btn btn-primary btn-sm">Cari</button>
				</div>
			</form>
		</div>
	</div>
</div>