<div class="modal fade" id="modal-add-icd10">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-add-icd10" action="<?= _BASE_ . 'index2.php?link=icd10&c=icd10&a=insert' ?>" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-plus-circle"></i> Tambah Data ICD 9</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Kode</label>
						<input type="text" name="icd_code" class="form-control required" />
					</div>
					<div class="form-group">
						<label>Nama Penyakit (Latin)</label>
						<input type="text" name="jenis_penyakit" class="form-control required" />
					</div>
					<div class="form-group">
						<label>Nama Penyakit (Indonesia)</label>
						<input type="text" name="jenis_penyakit_local" class="form-control" />
					</div>
					<div class="form-group">
						<label>Nama Penyakit (Lokal RS)</label>
						<input type="text" name="jenis_penyakit_local_rs" class="form-control" />
					</div>
					<div class="form-group">
						<label>Sebab Penyakit</label>
						<input type="text" name="sebabpenyakit" class="form-control" />
					</div>
					<div class="form-group">
						<label>DTD</label>
						<input type="text" name="dtd" class="form-control" />
					</div>
					<div class="form-group">
						<label>No. Urut Laporan</label>
						<input type="text" name="nourutlap" class="form-control" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> Sedang memproses...">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>