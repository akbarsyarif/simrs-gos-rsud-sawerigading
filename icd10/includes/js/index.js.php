<script type="text/javascript">
	jQuery(document).ready(function() {
		var addModal = jQuery('#modal-add-icd10');
		var addForm = jQuery('#form-add-icd10');
		var editModal = jQuery('#modal-edit-icd10');
		var editForm = jQuery('#form-edit-icd10');

		var table = jQuery('#datatable-icd10').DataTable({
			displayLength: 10,
			ordering : true,
			order : [[0, 'asc']],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_ . "index2.php?link=icd10&c=icd10&a=datatableIcd10" ?>',
			columns : [
				{
					className : "text-center",
					data : "icd_code",
				},
				{
					data : "jenis_penyakit",
				},
				{
					data : "jenis_penyakit_local",
				},
				{
					data : "jenis_penyakit_local_rs",
				},
				{
					data : "dtd",
				},
				{
					data : "nourutlap",
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button class="edit btn btn-xs btn-block btn-warning"><i class="fa fa-fw fa-pencil"></i></button>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button class="delete btn btn-xs btn-block btn-danger"><i class="fa fa-fw fa-trash"></i></button>';
					}
				},
			],
		});

		// search
		jQuery('#form-search-icd10').on('submit', function() {
			jQuery('#modal-search-icd10').modal('hide');

			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#clear').on('click', function() {
			jQuery('#form-search-icd10').resetForm();
			jQuery('#modal-search-icd10').modal('hide');

			jQuery('#form-search-icd10 .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});

		// submit add icd10 form
		addForm.validate({
			submitHandler : function(form) {
				jQuery(form).ajaxSubmit({
					dataType : 'json',
					beforeSubmit : function(arr, $form, options) {
						addForm.find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( r.metadata.code != "200" ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							swal('Success', 'Data berhasil ditambahkan', 'success');
							addModal.modal('hide');
							table.ajax.reload(null, false);
							$form.resetForm();
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						addForm.find('[type="submit"]').button('reset');
					}
				});
			}
		});

		// edit data icd10
		jQuery('#datatable-icd10 tbody').on('click', '.edit', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url: '<?= _BASE_ . "index2.php?link=icd10&c=icd10&a=getIcd10" ?>',
				method: 'get',
				dataType: 'json',
				data: {icd_code : data.icd_code},
				success: function(r) {
					if ( r.metadata.code != "200" ) {
						swal('Eror', r.metadata.message, 'error');
						console.log(r);
					}
					else {
						var d = r.response;

						editForm.find('[name="pk"]').val(d.icd_code);
						editForm.find('[name="icd_code"]').val(d.icd_code);
						editForm.find('[name="jenis_penyakit"]').val(d.jenis_penyakit);
						editForm.find('[name="jenis_penyakit_local"]').val(d.jenis_penyakit_local);
						editForm.find('[name="jenis_penyakit_local_rs"]').val(d.jenis_penyakit_local_rs);
						editForm.find('[name="dtd"]').val(d.dtd);
						editForm.find('[name="nourutlap"]').val(d.nourutlap);
						editForm.find('[name="sebabpenyakit"]').val(d.sebabpenyakit);

						editModal.modal('show');
					}
				},
				error: function(e) {
					swal('Error', 'Error 400. Bad request', 'error');
					console.log(e);
				}
			});
		});

		// submit edit icd10 form
		editForm.validate({
			submitHandler : function(form) {
				jQuery(form).ajaxSubmit({
					dataType : 'json',
					beforeSubmit : function(arr, $form, options) {
						editForm.find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( r.metadata.code != "200" ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							swal('Success', 'Data berhasil disimpan', 'success');
							editModal.modal('hide');
							table.ajax.reload(null, false);
							$form.resetForm();
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						editForm.find('[type="submit"]').button('reset');
					}
				});
			}
		});

		// delete icd10
		jQuery('#datatable-icd10 tbody').on('click', '.delete', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			swal({
				title: "Hapus Data?",
				text: "Anda akan menghapus data dengan kode " + data.icd_code,
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				closeOnConfirm: false
			},
			function(isConfirm) {
				if ( isConfirm == true ) {
					jQuery.ajax({
						url: '<?= _BASE_ . "index2.php?link=icd10&c=icd10&a=delete" ?>',
						method: 'get',
						dataType: 'json',
						data: {icd_code : data.icd_code},
						success: function(r) {
							if ( r.metadata.code != "200" ) {
								swal('Eror', r.metadata.message, 'error');
								console.log(r);
							}
							else {
								swal('Success', 'Data berhasil dihapus', 'success');
								table.ajax.reload(null, false);
							}
						},
						error: function(e) {
							swal('Error', 'Error 400. Bad request', 'error');
							console.log(e);
						}
					});
				}
			});
		});
	});
</script>