<?php

use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\Printer;

require_once BASEPATH . 'models/m_pendaftaran.php';

function printTracer($idxdaftar=NULL, $return_json=TRUE)
{
	$idxdaftar = (is_null($idxdaftar) && isset($_GET['idxdaftar']) && !empty($_GET['idxdaftar'])) ? xss_clean($_GET['idxdaftar']) : $idxdaftar;

	// get data pendaftaran
	$data = Models\row_pendaftaran( array (
		'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
	) );

	if ( $data == FALSE ) {
		$response = (object) array (
			'metadata' => (object) array (
				'code' => 400,
				'message' => 'Data kunjungan tidak ditemukan'
			)
		);
	} else {
		// check printer connection
		$connected = @fsockopen(RECEIPT_PRINTER_IP, RECEIPT_PRINTER_PORT, $errno, $errstr, SOCKET_TIMEOUT);

		if ( $connected == FALSE ) {
			$response = (object) array (
				'metadata' => (object) array (
					'code' => 400,
					'message' => 'Tidak dapat terhubung ke printer ' . RECEIPT_PRINTER_IP . ' : ' . RECEIPT_PRINTER_PORT
				)
			);
		} else {
			// setup printer
			$connector = new NetworkPrintConnector(RECEIPT_PRINTER_IP, RECEIPT_PRINTER_PORT);
			$printer = new Printer($connector);

			// logo
			$logo = EscposImage::load(BASEPATH . 'assets/img/logo-rsud-tracer.png');
			$printer->setJustification(Printer::JUSTIFY_CENTER);
			$printer->graphics($logo);

			$printer->feed(1);

			// title
			$printer->selectPrintMode(Printer::MODE_EMPHASIZED);
			$printer->text("RSUD SAWERIGADING \n");
			$printer->text("INSTALASI REKAM MEDIS & SIMRS \n");

			$printer->feed(1);

			$printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
			$printer->text("T R A C E R \n");
			$printer->feed(1);
			$printer->text("MR : " . $data['NOMR'] . " (" . $data['JENIS_KUNJUNGAN'] . ")\n");

			$printer->feed(1);

			// registration information
			$printer->setPrintLeftMargin(50);
			$printer->setJustification(Printer::JUSTIFY_LEFT);
			$printer->selectPrintMode(Printer::MODE_FONT_A);
			$printer->text("NAMA PASIEN   : " . $data['NAMA_PASIEN'] . " \n");
			$printer->text("TUJUAN        : " . $data['NAMA_POLY'] . " \n");
			$printer->text("CARA BAYAR    : " . $data['CARABAYAR'] . " \n");
			$printer->text("PETUGAS       : " . $data['NIP'] . " \n");
			$printer->text("TGL.PELAYANAN : " . $data['TGLREG_CAL'] . " \n");

			$printer->feed(1);

			$printer->setJustification(Printer::JUSTIFY_CENTER);
			$printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
			$printer->text("URUT : \n");

			$printer->feed(1);

			$printer->cut(Printer::CUT_PARTIAL, 0);
			$printer->close();

			$response = (object) array (
				'metadata' => (object) array (
					'code' => 200,
					'message' => 'OK'
				),
				'response' => (object) array (
					'idxdaftar' => $data['IDXDAFTAR']
				)
			);
		}
	}

	if ( $return_json == TRUE ) {
		echo json_encode($response);
	}
}