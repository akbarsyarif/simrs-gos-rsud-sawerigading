<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';

// load models
require BASEPATH . 'models/m_pendaftaran.php';

// get query string
$idxdaftar = (isset($_GET['idx'])) ? xss_clean($_GET['idx']) : NULL;

$data = Models\row_pendaftaran(array(
	'query' => " WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
));

$error_msg = array();

if ( $data === FALSE ) {
	$error_msg[] = "Data transaksi tidak ditemukan";
}
?>

<?php if ( count($error_msg) > 0 ) : ?>

	<?php show_error($error_msg); ?>

<?php else : ?>

	<div align="center">
		<div id="frame">
			<div id="frame_title"><h3>Edit Administrasi Pasien</h3></div>
			<form name="myform" id="myform" action="<?= _BASE_ . 'index2.php?link=pendaftaran&c=action_list_kunjungan&a=update' ?>" method="post">
				<input type="hidden" name="IDXDAFTAR" id="idxdaftar" value="<?= $data['IDXDAFTAR'] ?>" />
				<input type="hidden" name="NOMR" id="idxdaftar" value="<?= $data['NOMR'] ?>" />
				<input type="hidden" name="old_carabayar" value="<?= $data['KDCARABAYAR'] ?>" />

				<fieldset class="fieldset">
					<legend>Data Administrasi</legend>
					<table width="100%">
						<tr>
							<td width="70%">
								<table width="100%" border="0" style="background:none;">
									<tr id="tr_nomr">
										<td width="30%">No. Rekam Medis</td>
										<td><strong><?= $data['NOMR'] ?></strong></td>
									</tr>
									<tr>
										<td>Nama Pasien</td>
										<td><strong><?= $data['NAMA_PASIEN'] ?></strong></td>
									</tr>
									<tr>
										<td>Rujukan</td>
										<td>
											<span class="relative-container">
												<?= dinamycDropdown( array (
													'name' => 'KDRUJUK',
													'table' => 'm_rujukan',
													'key' => 'KODE',
													'value' => 'NAMA',
													'selected' => $data['KD_RUJUK'],
													'order_by' => 'ORDERS asc',
													'empty_first' => TRUE,
													'first_value' => '-- Pilih Rujukan --',
													'attr' => 'class="selectbox required text select2-single" title="Tidak boleh kosong"'
												) ); ?>
											</span>
										</td>
									</tr>
									<tr>
										<td>Cara Bayar</td>
										<td>
											<span class="relative-container">
												<?= dinamycDropdown( array (
													'name' => 'KDCARABAYAR',
													'table' => 'm_carabayar',
													'key' => 'KODE',
													'value' => 'NAMA',
													'selected' => $data['KDCARABAYAR'],
													'order_by' => 'ORDERS asc',
													'empty_first' => TRUE,
													'first_value' => '-- Pilih Cara Bayar --',
													'attr' => 'class="selectbox required text select2-single" title="Tidak boleh kosong"'
												) ); ?>
											</span>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<fieldset class="jkn_container carabayar_container bs-style">
												<legend>JKN</legend>
												<div class="row form-horizontal">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-4">No. Peserta</label>
															<div class="col-md-8">	
																<div class="input-group">
																	<input type="text" name="no_kartu" value="<?= $data['NO_KARTU'] ?>" readonly="readonly" class="text form-control" />
																	<span class="input-group-btn">
																		<button type="button" class="text btn-action btn" id="btn-search-peserta" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i>"><i class="fa fa-fw fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4">Jenis Peserta</label>
															<div class="col-md-8">	
																<input type="hidden" name="kd_jenis_peserta" value="<?= $data['KD_JENIS_PESERTA'] ?>" readonly />
																<input type="text" name="nm_jenis_peserta" value="<?= $data['NM_JENIS_PESERTA'] ?>" readonly="readonly" class="text form-control" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4">Kelas Tanggungan</label>
															<div class="col-md-8">	
																<input type="hidden" name="kd_kelas" value="<?= $data['KD_KELAS'] ?>" readonly />
																<input type="text" name="nm_kelas" value="<?= $data['KELAS_LABEL'] ?>" readonly="readonly" class="text form-control" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4">No. Rujukan</label>
															<div class="col-md-8">
																<input type="text" name="no_rujukan" value="<?= $data['NO_RUJUKAN'] ?>" class="text form-control" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4">Tgl. Rujukan</label>
															<div class="col-md-8 container-relative">
																<input type="text" name="tgl_rujukan" value="<?= $data['TIMESTAMP_RUJUKAN'] ?>" class="text datetimepicker form-control" />
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4">Provider Pemberi Rujukan</label>
															<div class="col-md-8">	
																<input type="hidden" name="kd_ppk_rujukan" value="<?= $data['KD_PPK_RUJUKAN'] ?>" readonly />
																<div class="input-group">
																	<input type="text" name="nm_ppk_rujukan" value="<?= $data['NM_PPK_RUJUKAN'] ?>" readonly="readonly" class="text form-control" />
																	<span class="input-group-btn">
																		<button type="button" class="text btn-action btn" id="btn-search-provider-bpjs"><i class="fa fa-fw fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-4">Diagnosa Awal</label>
															<div class="col-md-8">	
																<input type="hidden" name="kd_diagnosa" value="<?= $data['KD_DIAGNOSA'] ?>" readonly />
																<div class="input-group">
																	<input type="text" name="nm_diagnosa" value="<?= $data['NM_DIAGNOSA'] ?>" readonly="readonly" class="text form-control" />
																	<span class="input-group-btn">
																		<button type="button" class="text btn-action btn" id="btn-search-diagnosa"><i class="fa fa-fw fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</fieldset>
										</td>
									</tr>
									<tr>
										<td>Poli / Dokter yang dituju </td>
										<td>
											<span class="relative-container">
												<?= dinamycDropdown( array (
													'name' => 'KDPOLY',
													'table' => 'm_poly',
													'key' => 'kode',
													'value' => 'nama',
													'selected' => $data['KDPOLY'],
													'order_by' => 'nama asc',
													'empty_first' => TRUE,
													'first_value' => '-- Pilih Poliklinik --',
													'attr' => 'id="kdpoly" class="selectbox text select2-single required" title="Tidak boleh kosong"  style="float:left;margin-right:3px"'
												) ); ?>
											</span>
											<span id="listdokter_jaga">
												<?= dinamycDropdown( array (
													'name' => 'KDDOKTER',
													'select' => 'm_dokter_jaga.id, m_dokter.KDDOKTER, m_dokter.NAMADOKTER',
													'table' => 'm_dokter_jaga',
													'join' => 'm_dokter',
													'source_key' => 'kddokter',
													'reference_key' => 'KDDOKTER',
													'key' => 'KDDOKTER',
													'value' => 'NAMADOKTER',
													'selected' => $data['KDDOKTER'],
													'order_by' => 'm_dokter.NAMADOKTER asc',
													'where' => "m_dokter_jaga.KDPOLY = '{$data['KDPOLY']}'",
													'empty_first' => FALSE,
													'first_value' => '-- Pilih Dokter --',
													'attr' => 'class="text required" title="Tidak boleh kosong"'
												) ); ?>
											</span>
										</td>
									</tr>
									<tr>
										<td>Minta Rujukan </td>
										<td><input type="checkbox" name="minta_rujukan" id="minta_rujukan" value="1" <?= ($data['MINTA_RUJUKAN'] == "1") ? 'checked' : '' ?> /></td>
									</tr>
									<tr>
										<td>Tanggal Daftar </td>
										<td>
											<div class="container-relative">
												<input type="text" name="TGLREG" id="TGLREG" class="text datetimepicker required" value="<?= $data['JAMREGRAW'] ?>" size="20"/>
												<input type='hidden' name='start_daftar' id='start_daftar' />
											</div>
										</td>
									</tr>
									<tr>
										<td>Kecelakaan Lalulintas</td>
										<td>
											<select name="LAKALANTAS" class="text required" title="Tidak boleh kosong" >
												<option value="">-- Pilih --</option>
												<option value="1" <?= ( $data['LAKALANTAS'] == "1" ) ? 'selected' : '' ?>>Ya</option>
												<option value="2" <?= ( $data['LAKALANTAS'] != "1" ) ? 'selected' : '' ?>>Tidak</option>
											</select>
											<input type="text" value="<?= ( $data['LAKALANTAS'] == "1" ) ? $data['LOKASI_LAKALANTAS'] : '' ?>" class="text" name="LOKASI_LAKALANTAS" placeholder="Lokasi Lakalantas" title="Lokasi lakalantas wajib diisi" />
										</td>
									</tr>
									<tr>
										<td>Catatan</td>
										<td>
											<textarea class="blank text" rows="3" cols="50" name="CATATAN" value="<?= ( $data['CATATAN'] ) ?>"></textarea>
										</td>
									</tr>
									<tr>
										<td>No. Billing</td>
										<td colspan="2">
											<input type="text" name="nobill" value="" class="text" />
											<p class="help-block">Masukan no billing jika pasien sudah melakukan pembayaran</p>
										</td>
									</tr>
									<tr>
										<td>Username SPV </td>
										<td><input type="text" name="userspv" id="userspv" value="" class="text required"></td>
									</tr>
									<tr>
										<td>Password SPV </td>
										<td><input type="password" name="pwdspv" id="pwdspv" value="" class="text required"></td>
									</tr>
								</table>
							</td>
							<td width="30%">
								<table width="100%" align="right">
									<tr>
										<td width="40%">Shift Petugas Jaga</td>
										<td>
											<div><label class="radio-inline"><input type="radio" id="shift_pagi" name="SHIFT" class="required" title="Tidak boleh kosong" value="1" <?= ( $data['SHIFT'] == "1" ) ? 'Checked' : '' ?> /> Pagi</label></div>
											<div><label class="radio-inline"><input type="radio" id="shift_siang" name="SHIFT" class="required" title="Tidak boleh kosong" value="2" <?= ( $data['SHIFT'] == "2" ) ? 'Checked' : '' ?> /> Siang</label></div>
											<div><label class="radio-inline"><input type="radio" id="shift_sore" name="SHIFT" class="required" title="Tidak boleh kosong" value="3" <?= ( $data['SHIFT'] == "3" ) ? 'Checked' : '' ?> /> Sore</label></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

					<p>
						<button class="btn btn-sm btn-primary" type="submit" name="action" value="update"><i class="fa fa-fw fa-save"></i> Simpan</button>
					</p>
				</fieldset>
			</form>
		</div>
	</div>

<?php endif; ?>

<?php require 'modals/modal_search_peserta_bpjs.php'; ?>
<?php require 'modals/modal_diagnosa_bpjs.php'; ?>
<?php require 'modals/modal_search_provider.php'; ?>

<script type="text/javascript">

	//------------------------------------------------------------------------

	function validateJKN(validate) {
		// .jkn_container selector
		var jknContainer = jQuery('.jkn_container');

		if ( validate ) {
			jknContainer.find('[name="metode_pengisian"]').addClass('required');
			jknContainer.find('[name="no_kartu"]').addClass('required');
			jknContainer.find('[name="kd_jenis_peserta"]').addClass('required');
			jknContainer.find('[name="nm_jenis_peserta"]').addClass('required');
			jknContainer.find('[name="kd_kelas"]').addClass('required');
			jknContainer.find('[name="nm_kelas"]').addClass('required');
			jknContainer.find('[name="no_rujukan"]').addClass('required');
			jknContainer.find('[name="tgl_rujukan"]').addClass('required');
			jknContainer.find('[name="kd_ppk_rujukan"]').addClass('required');
			jknContainer.find('[name="nm_ppk_rujukan"]').addClass('required');
			jknContainer.find('[name="kd_diagnosa"]').addClass('required');
			jknContainer.find('[name="nm_diagnosa"]').addClass('required');
		}
		else {
			jknContainer.find('[name="metode_pengisian"]').removeClass('required');
			jknContainer.find('[name="no_kartu"]').removeClass('required');
			jknContainer.find('[name="kd_jenis_peserta"]').removeClass('required');
			jknContainer.find('[name="nm_jenis_peserta"]').removeClass('required');
			jknContainer.find('[name="kd_kelas"]').removeClass('required');
			jknContainer.find('[name="nm_kelas"]').removeClass('required');
			jknContainer.find('[name="no_rujukan"]').removeClass('required');
			jknContainer.find('[name="tgl_rujukan"]').removeClass('required');
			jknContainer.find('[name="kd_ppk_rujukan"]').removeClass('required');
			jknContainer.find('[name="nm_ppk_rujukan"]').removeClass('required');
			jknContainer.find('[name="kd_diagnosa"]').removeClass('required');
			jknContainer.find('[name="nm_diagnosa"]').removeClass('required');
		}
	}

	//------------------------------------------------------------------------

	// lock field JKN
	function lockFieldJKN()
	{
		// .jkn_container selector
		var jknContainer = jQuery('.jkn_container');

		jknContainer.find('[name="no_kartu"]').attr('readonly', 'readonly');
		jknContainer.find('[name="kd_jenis_peserta"]').attr('readonly', 'readonly');
		jknContainer.find('[name="nm_jenis_peserta"]').attr('readonly', 'readonly');
		jknContainer.find('[name="kd_kelas"]').attr('readonly', 'readonly');
		jknContainer.find('[name="nm_kelas"]').attr('readonly', 'readonly');
		jknContainer.find('[name="no_rujukan"]').attr('readonly', 'readonly');
		jknContainer.find('[name="tgl_rujukan"]').attr('readonly', 'readonly');
		jknContainer.find('[name="kd_ppk_rujukan"]').attr('readonly', 'readonly');
		jknContainer.find('[name="nm_ppk_rujukan"]').attr('readonly', 'readonly');
		jknContainer.find('[name="kd_diagnosa"]').attr('readonly', 'readonly');
		jknContainer.find('[name="nm_diagnosa"]').attr('readonly', 'readonly');

		// action buttons
		jknContainer.find('.btn-action').attr('disabled', 'disabled');
	}

	//------------------------------------------------------------------------

	// unlock field JKN
	function unlockFieldJKN()
	{
		// .jkn_container selector
		var jknContainer = jQuery('.jkn_container');

		// jknContainer.find('[name="no_kartu"]').removeAttr('readonly');
		jknContainer.find('[name="no_rujukan"]').removeAttr('readonly');
		jknContainer.find('[name="tgl_rujukan"]').removeAttr('readonly');

		// action buttons
		jknContainer.find('.btn-action').removeAttr('disabled');
	}

	//------------------------------------------------------------------------

	// empty form JKN
	function emptyFormJKN()
	{
		// .jkn_container selector
		var jknContainer = jQuery('.jkn_container');

		jknContainer.find('[name="no_kartu"]').val('');
		jknContainer.find('[name="kd_jenis_peserta"]').val('');
		jknContainer.find('[name="nm_jenis_peserta"]').val('');
		jknContainer.find('[name="kd_kelas"]').val('');
		jknContainer.find('[name="nm_kelas"]').val('');
		jknContainer.find('[name="no_rujukan"]').val('');
		jknContainer.find('[name="tgl_rujukan"]').val('');
		jknContainer.find('[name="kd_ppk_rujukan"]').val('');
		jknContainer.find('[name="nm_ppk_rujukan"]').val('');
		jknContainer.find('[name="kd_diagnosa"]').val('');
		jknContainer.find('[name="nm_diagnosa"]').val('');
	}

	//------------------------------------------------------------------------

	function toggle_view_container(val) {
		// .jkn_container selector
		var jknContainer = jQuery('.jkn_container');

		if ( val == 1 ) {
			jQuery('.carabayar_container').hide();

			validateJKN(false);
		}
		else if ( val == 2 ) {
			jQuery('.carabayar_container').hide();
			jknContainer.show();

			validateJKN(true);
		}
		else{
			jQuery('.carabayar_container').hide();

			validateJKN(false);
		}
	}

	//------------------------------------------------------------------------

	jQuery(document).ready(function() {
		
		//------------------------------------------------------------------------
		
		jQuery('#myform').validate();

		//------------------------------------------------------------------------
	
		jQuery('#kdpoly').change(function(){
			var val	= jQuery(this).val();
			jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kdpoly:val,load_dokterjaga:'true'},function(data){
				jQuery('#listdokter_jaga').empty().append(data);
			});
		});

		//------------------------------------------------------------------------

		// kolom lakalantas
		jQuery('[name="LAKALANTAS"]').change(function() {
			var lakalantas = jQuery(this).val();
			if ( lakalantas == 1 ) {
				jQuery('[name="LOKASI_LAKALANTAS"]').addClass('required');
			}
			else {
				jQuery('[name="LOKASI_LAKALANTAS"]').removeClass('required');
			}
		});

		//------------------------------------------------------------------------
		
		// menampilkan form detail berdasarkan cara bayar
		toggle_view_container(jQuery('[name="KDCARABAYAR"]').val());
		jQuery('[name="KDCARABAYAR"]').change(function() {
			var val = jQuery(this).val();
			toggle_view_container(val);
		});

		//------------------------------------------------------------------------

		var modal_peserta = jQuery('#modal-search-peserta-bpjs');
		var form_peserta = jQuery('#form-search-peserta-bpjs');

		//------------------------------------------------------------------------

		// menampilkan modal cari peserta bpjs
		jQuery('#btn-search-peserta').on('click', function(e) {
			modal_peserta.modal('show');
		});

		//------------------------------------------------------------------------

		jQuery('#data-peserta-bpjs tbody').on('click', 'a.choose', function() {
			var resultTemp = modal_peserta.find('[name="resultTemp"]').val();
			var d = JSON.parse(resultTemp);

			jQuery('.jkn_container').find('[name="no_kartu"]').val(d.noKartu);
			jQuery('.jkn_container').find('[name="kd_jenis_peserta"]').val(d.jenisPeserta.kdJenisPeserta);
			jQuery('.jkn_container').find('[name="nm_jenis_peserta"]').val(d.jenisPeserta.nmJenisPeserta);
			jQuery('.jkn_container').find('[name="kd_kelas"]').val(d.kelasTanggungan.kdKelas);
			jQuery('.jkn_container').find('[name="nm_kelas"]').val(d.kelasTanggungan.nmKelas);
			jQuery('.jkn_container').find('[name="kd_ppk_rujukan"]').val(d.provUmum.kdProvider);
			jQuery('.jkn_container').find('[name="nm_ppk_rujukan"]').val(d.provUmum.nmProvider);

			modal_peserta.modal('hide');
		});

		//------------------------------------------------------------------------

		modal_peserta.on('hidden.bs.modal', function() {
			// reset form
			form_peserta.trigger('reset');
			modal_peserta.find('[name="resultTemp"]').val('');
		});

		//------------------------------------------------------------------------

		// menampilkan modal cari data rujukan bpjs
		jQuery('#btn-search-rujukan').on('click', function(e) {
			jQuery('#modal-rujukan-bpjs').modal('show');
		});

		//------------------------------------------------------------------------

		var modal_provider = jQuery('#modal-provider-bpjs');

		//------------------------------------------------------------------------

		// menampilkan daftar provider bpjs
		jQuery('#btn-search-provider-bpjs').on('click', function(e) {
			modal_provider.modal('show');
		});

		//------------------------------------------------------------------------

		jQuery('#data-provider tbody').on('click', 'a.choose', function() {
			var kdProvider = jQuery(this).data('kode');
			var nmProvider = jQuery(this).data('nama');
			jQuery('.jkn_container [name="kd_ppk_rujukan"]').val(kdProvider);
			jQuery('.jkn_container [name="nm_ppk_rujukan"]').val(nmProvider);
			modal_provider.modal('hide');
		});

		//------------------------------------------------------------------------

		var modal_diagnosa = jQuery('#modal-diagnosa-bpjs');

		//------------------------------------------------------------------------

		// menampilkan daftar diagnosa
		jQuery('#btn-search-diagnosa').on('click', function(e) {
			modal_diagnosa.modal('show');
		});

		//------------------------------------------------------------------------

		jQuery('#data-diagnosa tbody').on('click', 'a.choose', function() {
			var kdDiagnosa = jQuery(this).data('kode');
			var nmDiagnosa = jQuery(this).data('nama');
			jQuery('.jkn_container [name="kd_diagnosa"]').val(kdDiagnosa);
			jQuery('.jkn_container [name="nm_diagnosa"]').val(nmDiagnosa);
			modal_diagnosa.modal('hide');
		});

		//------------------------------------------------------------------------

	});
</script>