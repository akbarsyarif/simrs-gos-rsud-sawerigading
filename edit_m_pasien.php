<script language="javascript" src="include/cal3.js"></script>
<script language="javascript" src="include/cal_conf3.js"></script>

<?php
$sql	= mysql_query('select * from m_pasien where NOMR = "'.$_REQUEST['NOMR'].'"');
$data	= mysql_fetch_array($sql);
?>
<script>
jQuery(document).ready(function(){
		jQuery('#TGLLAHIR').blur(function(){
		var tgl = jQuery(this).val();						  
		if(tgl == ('0000/00/00') || tgl == ('0000-00-00') || tgl == ('00-00-0000') || tgl == ('00/00/0000')  ){
			alert('Tanggal Lahir Tidak Boleh 00-00-0000');
			jQuery(this).val('');
		}
	});
	
	jQuery('#myform').validate();

	jQuery("#TGLLAHIR").inputmask("99/99/9999");
	
	jQuery("#KDPROVINSI").change(function(){
		var selectValues = jQuery("#KDPROVINSI").val();
		jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kdprov:selectValues,load_kota:'true'},function(data){
			jQuery('#kotapilih').html(data);
			jQuery('#kecamatanpilih').html("<select name=\"KDKECAMATAN\" class=\"text required\" title=\"*\" id=\"KDKECAMATAN\"><option value=\"0\"> --pilih-- </option></select>");
			jQuery('#kelurahanpilih').html("<select name=\"KELURAHAN\" class=\"text required\" title=\"*\" id=\"KELURAHAN\"><option value=\"0\"> --pilih-- </option></select>");
		});
	});
	
	jQuery("#KOTA").change(function(){
		var selectValues = jQuery("#KOTA").val();
		jQuery.post('./include/ajaxload.php',{kdkota:selectValues,load_kecamatan:'true'},function(data){
			jQuery('#kecamatanpilih').html(data);
			jQuery('#kelurahanpilih').html("<select name=\"KELURAHAN\" class=\"text required\" title=\"*\" id=\"KELURAHAN\"><option value=\"0\"> --pilih-- </option></select>");
		});
	});
	
	jQuery("#KDKECAMATAN").change(function(){
		var selectValues = jQuery("#KDKECAMATAN").val();
		jQuery.post('./include/ajaxload.php',{kdkecamatan:selectValues,load_kelurahan:'true'},function(data){
			jQuery('#kelurahanpilih').html(data);
		});
	});
});
</script>
<div align="center">
	<div id="frame">
	<div id="frame_title"><h3 align="left">IDENTITAS PASIEN</h3></div>
	<div id="all">
		<form name="myform" id="myform" action="models/edit_pasien.php?edit=ok" method="post">	
			<div id="list_data"></div>
			<fieldset class="fieldset"><legend>Identitas Pasien</legend>
				<table align="left" width="100%">
					<tr>
						<td width="50%">
							<table width="100%">
								<tr>
									<td>No Rekam Medik</td>
									<td><input class="text" value="<?=$data['NOMR']?>" type="text" name="NOMR" id="NOMR" size="25" >
										<input class="text" value="<?=$data['NOMR']?>" type="hidden" name="NOMRKEY" id="NOMRKEY" >
									</td>
								</tr>
								<tr>
									<td width="25%">Nama Lengkap Pasien</td>
									<td width="36%"><input class="text" type="text" value="<?=$data['NAMA']?>" name="NAMA" size="25" id="NAMA" /></td>
								</tr>
								<tr>
									<td>Tempat Tanggal Lahir</td>
									<td>
										<input type="text" value="<?=$data['TEMPAT']?>" class="text" name="TEMPAT" size="15" placeholder="Tempat" />
										<input type="text" class="text required" value="<?=date('d/m/Y', strtotime($data['TGLLAHIR']))?>" name="TGLLAHIR" size="20" id="TGLLAHIR" onblur="calage1(this.value,'umur');"/>
										<a href="javascript:showCal1('Calendar1')"><button type="button" class="text"><i class="fa fa-fw fa-calendar"></i></button></a>
									</td>
								</tr>
								<tr>
									<td>Umur Pasien</td>
									<td>
										<?php $a = datediff($data['TGLLAHIR'], date("Y-m-d")); ?>
										<input class="text" type="text" value="<?php echo 'umur '.$a[years].' tahun '.$a[months].' bulan '.$a[days].' hari'; ?>" name="umur" id="umur" size="40" />          
									</td>
								</tr>
								<tr>
									<td valign="top">Alamat Pasien</td>
									<td colspan="1"><input name="ALAMAT" type="text" value="<?=$data['ALAMAT']?>" size="45" class="text" /></td>
								</tr>
								<tr>
									<td>Alamat KTP</td>
									<td><input name="ALAMAT_KTP" class="text" type="text" value="<? if($data['ALAMAT_KTP']){ echo $data['ALAMAT_KTP']; } ?>" size="45" /></td>
								</tr>
								<tr>
									<td>Provinsi</td>
									<td>
										<select name="KDPROVINSI" class="text required" title="*" id="KDPROVINSI">
											<option value="0"> --pilih-- </option>
											<?php
											$ss	= mysql_query('select * from m_provinsi order by idprovinsi ASC');
											while($ds = mysql_fetch_array($ss)){
											if($data['KDPROVINSI'] == $ds['idprovinsi']): $sel = "selected=Selected"; else: $sel = ''; endif;
											echo '<option value="'.$ds['idprovinsi'].'" '.$sel.' /> '.$ds['namaprovinsi'].'</option>&nbsp;';
											} ?>
										</select>
										<input class="text" value="" type="hidden" name="KOTAHIDDEN" id="KOTAHIDDEN" >
										<input class="text" value="" type="hidden" name="KECAMATANHIDDEN" id="KECAMATANHIDDEN" >
										<input class="text" value="" type="hidden" name="KELURAHANHIDDEN" id="KELURAHANHIDDEN" >
									</td>
								</tr>
								<tr>
									<td>Kota</td>
									<td>
										<div id="kotapilih">
											<select name="KOTA" class="text required" title="*" id="KOTA">
												<option value="0"> --pilih-- </option>
												<?php
												$ss	= mysql_query('select * from m_kota where idprovinsi = "'.$data['KDPROVINSI'].'" order by idkota ASC');
												while($ds = mysql_fetch_array($ss)){
												if($data['KOTA'] == $ds['idkota']): $sel = "selected=Selected"; else: $sel = ''; endif;
												echo '<option value="'.$ds['idkota'].'" '.$sel.' /> '.$ds['namakota'].'</option>&nbsp;';
												} ?>
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>Kecamatan</td>
									<td>
										<div id="kecamatanpilih">
											<select name="KDKECAMATAN" class="text required" title="*" id="KDKECAMATAN">
												<option value="0"> --pilih-- </option>
												<?php
												$ss	= mysql_query('select * from m_kecamatan where idkota = "'.$data['KOTA'].'" order by idkecamatan ASC');
												while($ds = mysql_fetch_array($ss)){
												if($data['KDKECAMATAN'] == $ds['idkecamatan']): $sel = "selected=Selected"; else: $sel = ''; endif;
												echo '<option value="'.$ds['idkecamatan'].'" '.$sel.' /> '.$ds['namakecamatan'].'</option>&nbsp;';
												} ?>
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>Kelurahan</td>
									<td>
										<div id="kelurahanpilih">
											<select name="KELURAHAN" class="text required" title="*" id="KELURAHAN">
												<option value="0"> --pilih-- </option>
												<?php
												$ss	= mysql_query('select * from m_kelurahan where idkecamatan = "'.$data['KDKECAMATAN'].'" order by idkelurahan ASC');
												while($ds = mysql_fetch_array($ss)){
												if($data['KELURAHAN'] == $ds['idkelurahan']): $sel = "selected=Selected"; else: $sel = ''; endif;
												echo '<option value="'.$ds['idkelurahan'].'" '.$sel.' /> '.$ds['namakelurahan'].'</option>&nbsp;';
												} ?>
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>No Telepon / HP Pasien</td>
									<td><input  class="text" value="<?=$data['NOTELP']?>" type="text" name="NOTELP" size="25" /></td>
								</tr>
								<tr>
									<td>No KTP </td>
									<td><input  class="text" value="<?=$data['NOKTP']?>" type="text" name="NOKTP" size="25" /></td>
								</tr>
								<tr>
									<td>Nama Suami / Orang Tua Pasien</td>
									<td><input class="text" type="text" value="<?=$data['SUAMI_ORTU']?>" name="SUAMI_ORTU" size="25" /></td>
								</tr>
								<tr>
									<td>Pekerjaan Pasien / Orang Tua</td>
									<td><input class="text" type="text" value="<?=$data['PEKERJAAN']?>" name="PEKERJAAN" size="25" /></td>
								</tr>
							</table>
						</td>
						<td width="50%">
							<table width="100%">
								<tr>
									<td>Status Perkawinan</td>
									<td>
										<div><label class="radio-inline"><input type="radio" name="STATUS" value="1" <? if($data['STATUS']=="1")echo "Checked";?>/> Belum Kawin</label></div>
										<div><label class="radio-inline"><input type="radio" name="STATUS" value="2" <? if($data['STATUS']=="2")echo "Checked";?> /> Kawin</label></div>
										<div><label class="radio-inline"><input type="radio" name="STATUS" value="3" <? if($data['STATUS']=="3")echo "Checked";?>/> Janda / Duda</label></div>
									</td>
								</tr>
								<tr>
									<td valign="top">Agama </td>
									<td colspan="4">
										<div><label class="radio-inline"><input type="radio" name="AGAMA" value="1" <? if($data['AGAMA']=="1")echo "Checked";?> /> Islam</label></div>
										<div><label class="radio-inline"><input type="radio" name="AGAMA" value="2" <? if($data['AGAMA']=="2")echo "Checked";?>/> Kristen Protestan</label></div>
										<div><label class="radio-inline"><input type="radio" name="AGAMA" value="3" <? if($data['AGAMA']=="3")echo "Checked";?>/> Katholik</label></div>
										<div><label class="radio-inline"><input type="radio" name="AGAMA" value="4" <? if($data['AGAMA']=="4")echo "Checked";?>/> Hindu</label></div>
										<div><label class="radio-inline"><input type="radio" name="AGAMA" value="5" <? if($data['AGAMA']=="5")echo "Checked";?>/> Budha</label></div>
										<div><label class="radio-inline"><input type="radio" name="AGAMA" value="6" <? if($data['AGAMA']=="6")echo "Checked";?>/> Lain - lain</label></div>
									</td>
								</tr>
								<tr>
									<td valign="top">Pendidikan Terakhir Pasien</td>
									<td colspan="4">
										<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" value="0" <? if($data['PENDIDIKAN']=="0")echo "Checked";?> /> Belum Sekolah</label></div>
										<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" value="1" <? if($data['PENDIDIKAN']=="1")echo "Checked";?> /> SD</label></div>
										<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" value="2" <? if($data['PENDIDIKAN']=="2")echo "Checked";?> /> SLTP</label></div>
										<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" value="3" <? if($data['PENDIDIKAN']=="3")echo "Checked";?> /> SMU</label></div>
										<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" value="4" <? if($data['PENDIDIKAN']=="4")echo "Checked";?> /> D3/Akademik</label></div>
										<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" value="5" <? if($data['PENDIDIKAN']=="5")echo "Checked";?> /> Universitas</label></div>
									</td>
								</tr>
								<tr>
									<td>Awal daftar</td>
									<td><input class="text" type="text" value="<?=$data['TGLDAFTAR']?>" name="awaldaftar" size="25" id="awaldaftar" /></td>
								</tr>
								<tr>
									<td>Jenis Kelamin</td>
									<td>
										<div><label class="radio-inline"><input type="radio" name="JENISKELAMIN" value="L" <? if($data['JENISKELAMIN']=="L" || $data['JENISKELAMIN']=="l")echo "Checked";?>/> Laki-laki</label></div>
										<div><label class="radio-inline"><input type="radio" name="JENISKELAMIN" value="P" <? if($data['JENISKELAMIN']=="P")echo "Checked";?>/> Perempuan</label></div>
									</td>
								</tr>
								<tr>
									<td>Cara Pembayaran</td>
									<td>
										<?php
										$ss	= mysql_query('select * from m_carabayar order by ORDERS ASC');
										while ( $ds = mysql_fetch_array($ss) ) {
											if($data['KDCARABAYAR'] == $ds['KODE']): $sel = "Checked"; else: $sel = ''; endif;
											echo '<div><label class="radio-inline"><input type="radio" name="KDCARABAYAR" value="'.$ds['KODE'].'" '.$sel.' /> '.$ds['NAMA']. '</label></div>';
										} ?>
										<input type="hidden" name="TGLREG" value="<?php echo date("Y-m-d"); ?>" size="10" />            
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<p>
					<a href="<?= _BASE_ ?>/index.php?link=21"><button type="button" class="text"><i class="fa fa-fw fa-refresh"></i> Batal</button></a>
					<button type="submit" class="text"><i class="fa fa-fw fa-save"></i> Simpan</button>
				</p>
			</fieldset>
		</form>
	 </div>
	</div>
</div>