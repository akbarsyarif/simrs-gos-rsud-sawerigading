<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?= ucwords($rstitle) ?></title>

		<!-- css -->
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'js/bootstrap/css/bootstrap.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'js/bootstrap/css/lumen.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/font-awesome/css/font-awesome.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/slick-carousel/slick/slick.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/slick-carousel/slick/slick-theme.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'dq_sirs.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'themes/css/public.css' ?>" />

		<!-- javascript -->
		<script type="text/javascript" src="<?= _BASE_ . 'js/jquery.min.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'js/bootstrap/js/bootstrap.min.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/slick-carousel/slick/slick.min.js' ?>"></script>
	</head>
	<body>
		<?php require BASEPATH . 'switch.php'; ?>
		<?php require BASEPATH . 'themes/js/public.js.php'; ?>
	</body>
</html>