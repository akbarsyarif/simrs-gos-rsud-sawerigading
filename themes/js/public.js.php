<script type="text/javascript">
	$(document).ready(function() {
		$('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			autoplay: true,
			autoplaySpeed: 3000
		});
		// $('.slider-nav').slick({
		// 	slidesToShow: 3,
		// 	slidesToScroll: 1,
		// 	arrows: false,
		// 	asNavFor: '.slider-for',
		// 	centerMode: true,
		// 	focusOnSelect: true
		// });

		// auto refresh halaman informasi tempat tidur
		setTimeout(function() {
			location.reload();
		}, 300000);

		// clock
		setInterval(function(){
			var currentTime = new Date();
			var hours = currentTime.getHours();
			var minutes = currentTime.getMinutes();
			var seconds = currentTime.getSeconds();

			// Add leading zeros
			minutes = (minutes < 10 ? "0" : "") + minutes;
			seconds = (seconds < 10 ? "0" : "") + seconds;
			hours = (hours < 10 ? "0" : "") + hours;

			// Compose the string for display
			var currentTimeString = hours + ":" + minutes + ":" + seconds;
			$(".clock").html(currentTimeString);
		}, 1000);

		// auto scroll table-summary
		$(".content .left-side .table-summary").animate({ scrollTop: $(document).height() }, 40000);
		setTimeout(function() {
			$('.content .left-side .table-summary').animate({ scrollTop: 0 }, 40000);
		}, 40000);

		var scrolltopbottom =  setInterval(function() {
			// 4000 - it will take 4 secound in total from the top of the page to the bottom
			$(".content .left-side .table-summary").animate({ scrollTop: $(document).height() }, 40000);
			setTimeout(function() {
				$('.content .left-side .table-summary').animate({ scrollTop: 0 }, 40000); 
			}, 40000);
		}, 80000);
	});
</script>