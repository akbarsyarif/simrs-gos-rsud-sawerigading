<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?= ucwords($rstitle) ?></title>

		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'js/bootstrap/css/bootstrap.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'js/bootstrap/css/lumen.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'rajal/style.css' ?>"/>
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'include/jquery.autocomplete.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'css/autosuggest_inquisitor.css' ?>" media="screen" charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/font-awesome/css/font-awesome.min.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/select2/dist/css/select2.min.css' ?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/nprogress/nprogress.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/css3-checkbox/css/checkbox.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/waitme/waitMe.min.css' ?>" />
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'dq_sirs.css' ?>" />

		<!-- jquery -->
		<script src="<?= _BASE_ . 'js/jquery.min.js' ?>" language="JavaScript" type="text/javascript"></script>
		<script src="<?= _BASE_ . 'js/jquery.validate.js' ?>" language="JavaScript" type="text/javascript"></script>
		<script src="<?= _BASE_ . 'assets/jquery_form/jquery.form.min.js' ?>" language="JavaScript" type="text/javascript"></script>
		<script src="<?= _BASE_ . 'assets/bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js' ?>" language="JavaScript" type="text/javascript"></script>

		<!-- tb -->
		<script type="text/javascript" src="<?= _BASE_ . 'js/bootstrap/js/bootstrap.min.js' ?>"></script>

		<!-- datatables -->
		<script type="text/javascript" src="<?= _BASE_ . 'assets/datatables/datatables.min.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'assets/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'assets/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js' ?>"></script>

		<!-- select 2 -->
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/select2/dist/js/select2.full.min.js' ?>"></script>

		<!-- nprogress -->
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/nprogress/nprogress.js' ?>"></script>

		<!-- datepicker -->
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/moment/min/moment.min.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/moment/locale/id.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js' ?>"></script>

		<!-- sweet alert -->
		<script src="<?= _BASE_ . 'assets/bower_components/sweetalert2/dist/sweetalert2.min.js' ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/sweetalert2/dist/sweetalert2.min.css' ?>">

		<!-- alertify.js -->
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/alertify.js/themes/alertify.core.css' ?>">
		<link rel="stylesheet" type="text/css" href="<?= _BASE_ . 'assets/bower_components/alertify.js/themes/alertify.default.css' ?>">
		<script src="<?= _BASE_ . 'assets/bower_components/alertify.js/lib/alertify.min.js' ?>"></script>
		
		<script language="javascript" type="text/javascript" src="<?= _BASE_ . 'rajal/functions.js' ?>"></script>
		<script language="javascript" type="text/javascript" src="<?= _BASE_ . 'rajal/xmlhttp.js' ?>"></script>
		<script language="javascript" type="text/javascript" src="<?= _BASE_ . 'include/ajaxrequest.js' ?>"></script>

		<script type="text/javascript" src="<?= _BASE_ . 'include/js.js' ?>"></script>
		<script language="javascript" src="<?= _BASE_ . 'include/cal2.js' ?>"></script>
		<script language="javascript" src="<?= _BASE_ . 'include/cal_conf2.js' ?>"></script>
		
		<script src="<?= _BASE_ . 'js/jqclock_201.js' ?>" language="JavaScript" type="text/javascript"></script>

		<!--Notifikasi-->
		<script src="<?= _BASE_ . 'include/notification.js' ?>" language="JavaScript" type="text/javascript"></script>

		<!--autocomplete-->
		<script type="text/javascript" src="<?= _BASE_ . 'include/jquery.autocomplete.js' ?>"></script>
		<script type="text/javascript" src="<?= _BASE_ . 'rajal/jscripts/nicEdit.js' ?>"></script>

		<!-- accounting.js -->
		<script type="text/javascript" src="<?= _BASE_ . 'assets/bower_components/accounting.js/accounting.min.js' ?>"></script>

		<!-- waitme -->
		<script type="text/javascript" src="<?= _BASE_ . 'assets/waitme/waitMe.min.js' ?>"></script>

		<!-- app.js -->
		<script type="text/javascript" src="<?= _BASE_ . 'include/app.js' ?>"></script>

		<script type="text/javascript">
			jQuery.noConflict();
		</script>

		<script type="text/javascript">
			bkLib.onDomLoaded(function() {
				var textareas = jQuery('textarea:not(.blank)');
				for(var i = 0; i < textareas.length; i++)
				{
					var myNicEditor = new nicEditor();
					myNicEditor.panelInstance(textareas[i]);
				}
			});
		</script>

		<script type="text/javascript">
			function init() {
				var stretchers = document.getElementsByClassName('box');
				var toggles = document.getElementsByClassName('tab');
				var myAccordion = new fx.Accordion(
					toggles, stretchers, {opacity: false, height: true, duration: 600}
				);

				//hash functions
				var found = false;

				toggles.each(function(h3, i){
					var div = Element.find(h3, 'nextSibling');
					if (window.location.href.indexOf(h3.title) > 0) {
						myAccordion.showThisHideOpen(div);
						found = true;
					}
				});

				if (!found) myAccordion.showThisHideOpen(stretchers[0]);
			}

			function jumpTo(link) {
				var new_url=link;
				if (  (new_url != "")  &&  (new_url != null)  )
					window.location=new_url;
			}
		</script>

		<!-- admission pasien-->
		<script type="text/javascript">
			jQuery(document).ready(function() {
				<!-- OK-->
				jQuery("#nomroperasi").autocomplete("<?= _BASE_ . 'operasi/nomroperasi.php' ?>",{width:260});
				<!-- VK-->
				jQuery("#icdv").autocomplete("<?= _BASE_ . 'vk/autocomplete_vk.php' ?>",{width:260});
				<!-- Rawat Inap-->
				jQuery("#namaobat1").autocomplete("<?= _BASE_ . 'ranap/auto_icd.php' ?>",{width:260});
			});
		</script>

		<!--auto refresh jumlah pasien-->
		<script type="text/javascript">
			var auto_refresh = setInterval(
			function() {
				jQuery('#totalpasienhariini').load("<?= _BASE_ . 'admission/jmlpasien.php' ?>").fadeIn("slow");
			}, 5000); // refresh every 10000 milliseconds
		</script>
		<!--auto refresh jumlah pasien-->

		<script type="text/javascript">
			function enter_pressed(e) {
				var keycode;
				if (window.event) keycode = window.event.keyCode;
				else if (e) keycode = e.which;
				else return false;
				return (keycode == 13);
			}
		</script>

		<!-- menampilkan warning pasien yang harus dipulangkan pada halaman admission setiap 1 menit -->
		<?php if ( isset($_SESSION['ROLES']) && $_SESSION['ROLES'] == "17" ) : ?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					var autoRefresh = setInterval(function() {
						jQuery.ajax({
							url: '<?= _BASE_ ?>' + 'daftar_pasien_harus_dipulangkan.php',
							method: 'get',
							data: {limit: 5},
							dataType: 'json',
							success: function(r) {
								if ( r.metadata.code == "200" && r.response.count > 0 ) {
									var list = r.response.list;

									var html = '';
									html += '<strong>Daftar Pasien Rawat Inap Yang Telah Menyelesaikan Pembayaran</strong>';
									html += '<p>'
										html += '<table width="100%">';
											for (var i = 0; i < list.length ; i++) {
												html += '<tr>';
													html += '<td style="padding:0" width="80">' + list[i].nomr + '</td>';
													html += '<td style="padding:0">' + list[i].nama_pasien + '</td>';
												html += '</tr>';
											};
										html += '</table>';
									html += '</p>';

									alertify.log(html, '', 10000);
								}
							},
							error: function(e) {
								console.log(e);
							}
						});
					}, <?= _POPUPTIME_ ?>);
				});
			</script>
		<?php endif; ?>

		<script type="text/javascript" src="<?= _BASE_ . 'js/bsn.AutoSuggest_c_2.0.js' ?>"></script>
		<link rel="stylesheet" href="<?= _BASE_ . 'css/autosuggest_inquisitor.css' ?>" type="text/css" media="screen" charset="utf-8" />
	</head>

	<!-- <body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload=""> -->
	<body>
		<?php include("notification.php"); ?>
		<div id="header">
			<div id="bg_variation">
				<div id="logo"></div>
				<div id="info_header">
					<div id="info_isi">
						<div><?= strtoupper($singrstitl) ?> <?= strtoupper($singhead1) ?></div>
						<div>
							<?php
							if ( $_SESSION['SES_REG'] ) {
								echo '<a href="' . _BASE_ . 'log_out.php">Sign Out</a> | User : <span class="date">'.$_SESSION['NIP'].'</span>';
							}
							else {
								echo '<a href="' . _BASE_ . 'login.php">Sign In</a> | guest';
							} ?>
						</div>
						<div class="date"><?php echo date("l, F Y"); ?></div>
						<div class="date">
							<?php
							$dep = "SELECT * FROM m_login WHERE KDUNIT = '".$_SESSION['KDUNIT']."' AND ROLES = '".$_SESSION['ROLES']."'";
							$qe  = mysql_query($dep) or die(mysql_error());
							if ( $qe ) {
								$deps = mysql_fetch_assoc($qe);
								echo "<div><b>".$deps['DEPARTEMEN']."</b></div>";
								echo "<div style='position:absolute;'>";
									// include("chat/chatroom.php");
								echo "</div>";
							} ?>
						</div>
					</div>
				</div>
			</div>
			<?php require BASEPATH . 'menu.php'; ?>
		</div>
		
		<br />

		<div class="running-text">
			<?php require BASEPATH . 'runtext.php'; ?>
		</div>

		<div>
			<?php require BASEPATH . 'reminder/includes/widget.php'; ?>
		</div>
					
		<div id="container_master_bg">
			<div id="container">
				<?php require BASEPATH . 'switch.php'; ?>
			</div>
		</div>

		<div align="center" id="footer">
			<div class="left"><?= $singrstitl?> <?= strtoupper($singhead1) ?> System Application Development</div>
			<div class="right"><?= ucwords($rstitle)?> &copy; <?php echo date("Y"); ?>
				<?php 
				$timestamp = time();
				echo "<input id='servertime' type='hidden' value='".$timestamp."' />";
				echo "<input id='clockn' type='hidden' />"; ?>
			</div>
		</div>
	</body>
</html>