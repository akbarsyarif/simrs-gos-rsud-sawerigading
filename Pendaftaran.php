<script language="javascript" src="include/cal3.js"></script>
<script language="javascript" src="include/cal_conf3.js"></script>

<script type="text/javascript">
//------------------------------------------------------------------------

function startjam() {
	var d = new Date();
	var curr_hour = d.getHours();
	var curr_min = d.getMinutes();
	var curr_sec = d.getSeconds();
	document.getElementById('start_daftar').value=(curr_hour + ":" + curr_min+ ":" + curr_sec);
}

//------------------------------------------------------------------------

function stopjam(){
	var d = new Date();
	var curr_hour = d.getHours();
	var curr_min = d.getMinutes();
	var curr_sec = d.getSeconds();
	document.getElementById('stop_daftar').value=(curr_hour + ":" + curr_min+ ":" + curr_sec);
}

//------------------------------------------------------------------------

function resetFormPendaftaran() {
	jQuery('#myform').trigger('reset');
	jQuery('#myform').find('select').val('');
	jQuery('#myform').find('#NOMR').val('- otomatis -');
}

//------------------------------------------------------------------------

function validateJKN(validate) {
	// .jkn_container selector
	var jknContainer = jQuery('.jkn_container');

	if ( validate ) {
		jknContainer.find('[name="metode_pengisian"]').addClass('required');
		jknContainer.find('[name="no_kartu"]').addClass('required');
		jknContainer.find('[name="kd_jenis_peserta"]').addClass('required');
		jknContainer.find('[name="nm_jenis_peserta"]').addClass('required');
		jknContainer.find('[name="kd_kelas"]').addClass('required');
		jknContainer.find('[name="nm_kelas"]').addClass('required');
		jknContainer.find('[name="no_rujukan"]').addClass('required');
		jknContainer.find('[name="tgl_rujukan"]').addClass('required');
		jknContainer.find('[name="kd_ppk_rujukan"]').addClass('required');
		jknContainer.find('[name="nm_ppk_rujukan"]').addClass('required');
		jknContainer.find('[name="kd_diagnosa"]').addClass('required');
		jknContainer.find('[name="nm_diagnosa"]').addClass('required');
	}
	else {
		jknContainer.find('[name="metode_pengisian"]').removeClass('required');
		jknContainer.find('[name="no_kartu"]').removeClass('required');
		jknContainer.find('[name="kd_jenis_peserta"]').removeClass('required');
		jknContainer.find('[name="nm_jenis_peserta"]').removeClass('required');
		jknContainer.find('[name="kd_kelas"]').removeClass('required');
		jknContainer.find('[name="nm_kelas"]').removeClass('required');
		jknContainer.find('[name="no_rujukan"]').removeClass('required');
		jknContainer.find('[name="tgl_rujukan"]').removeClass('required');
		jknContainer.find('[name="kd_ppk_rujukan"]').removeClass('required');
		jknContainer.find('[name="nm_ppk_rujukan"]').removeClass('required');
		jknContainer.find('[name="kd_diagnosa"]').removeClass('required');
		jknContainer.find('[name="nm_diagnosa"]').removeClass('required');
	}
}

//------------------------------------------------------------------------

// lock field JKN
function lockFieldJKN()
{
	// .jkn_container selector
	var jknContainer = jQuery('.jkn_container');

	jknContainer.find('[name="no_kartu"]').attr('readonly', 'readonly');
	jknContainer.find('[name="kd_jenis_peserta"]').attr('readonly', 'readonly');
	jknContainer.find('[name="nm_jenis_peserta"]').attr('readonly', 'readonly');
	jknContainer.find('[name="kd_kelas"]').attr('readonly', 'readonly');
	jknContainer.find('[name="nm_kelas"]').attr('readonly', 'readonly');
	jknContainer.find('[name="no_rujukan"]').attr('readonly', 'readonly');
	jknContainer.find('[name="tgl_rujukan"]').attr('readonly', 'readonly');
	jknContainer.find('[name="kd_ppk_rujukan"]').attr('readonly', 'readonly');
	jknContainer.find('[name="nm_ppk_rujukan"]').attr('readonly', 'readonly');
	jknContainer.find('[name="kd_diagnosa"]').attr('readonly', 'readonly');
	jknContainer.find('[name="nm_diagnosa"]').attr('readonly', 'readonly');

	// action buttons
	jknContainer.find('.btn-action').attr('disabled', 'disabled');
}

//------------------------------------------------------------------------

// unlock field JKN
function unlockFieldJKN()
{
	// .jkn_container selector
	var jknContainer = jQuery('.jkn_container');

	// jknContainer.find('[name="no_kartu"]').removeAttr('readonly');
	jknContainer.find('[name="no_rujukan"]').removeAttr('readonly');
	jknContainer.find('[name="tgl_rujukan"]').removeAttr('readonly');

	// action buttons
	jknContainer.find('.btn-action').removeAttr('disabled');
}

//------------------------------------------------------------------------

// empty form JKN
function emptyFormJKN()
{
	// .jkn_container selector
	var jknContainer = jQuery('.jkn_container');

	jknContainer.find('[name="no_kartu"]').val('');
	jknContainer.find('[name="kd_jenis_peserta"]').val('');
	jknContainer.find('[name="nm_jenis_peserta"]').val('');
	jknContainer.find('[name="kd_kelas"]').val('');
	jknContainer.find('[name="nm_kelas"]').val('');
	jknContainer.find('[name="no_rujukan"]').val('');
	jknContainer.find('[name="tgl_rujukan"]').val('');
	jknContainer.find('[name="kd_ppk_rujukan"]').val('');
	jknContainer.find('[name="nm_ppk_rujukan"]').val('');
	jknContainer.find('[name="kd_diagnosa"]').val('');
	jknContainer.find('[name="nm_diagnosa"]').val('');
}

//------------------------------------------------------------------------

function toggle_view_container(val) {
	// .jkn_container selector
	var jknContainer = jQuery('.jkn_container');

	if ( val == 1 ) {
		jQuery('.carabayar_container').hide();

		validateJKN(false);
	}
	else if ( val == 2 ) {
		jQuery('.carabayar_container').hide();
		jknContainer.show();

		validateJKN(true);
	}
	else{
		jQuery('.carabayar_container').hide();

		validateJKN(false);
	}
}

//------------------------------------------------------------------------

/**
 * membaca data rujukan bpjs
 * @param  {[object]}
 * @return {[object]} respon dari server bpjs
 */
function getRujukan(params)
{
	return jQuery.ajax({
		url : '<?= _BASE_ ?>' + 'bridging_proses.php',
		data : {
			reqdata : 'get_rujukan',
			search_by : params.search_by,
			faskes : params.faskes,
			s : params.s
		},
		dataType : 'json'
	});
}

//------------------------------------------------------------------------

/**
 * melakukan mapping poli
 * @param  {[string]} kd_poly_bpjs
 * @return {[object]}
 */
function mappingPoly(kd_poly_bpjs)
{
	return jQuery.ajax({
		url : '<?= _BASE_ ?>' + 'ajaxload.php',
		data : {
			p : 'poly',
			a : 'mappingpoly',
			kd_bpjs : kd_poly_bpjs
		},
		dataType : 'json'
	});
}

//------------------------------------------------------------------------

function formatTglSimrsDirjenBukKemenkesFuck(tgl_lahir)
{
	var result = '';

	if ( tgl_lahir != null || tgl_lahir != '' ) {
		str = tgl_lahir.split("-");
		var Y = str[0];
		var M = str[1];
		var D = str[2];

		result = D + "/" + M + "/" + Y;
	}

	return result;
}

//------------------------------------------------------------------------

// JavaScript Document
jQuery(document).ready(function() {
	// validate myform
	// submit with ajax
	jQuery("#myform").validate({
		submitHandler : function(form) {
			jQuery(form).ajaxSubmit({
				dataType : 'json',
				beforeSubmit : function(att, $form, options) {
					jQuery('#myform').find('[type="submit"]').button('loading');
				},
				success : function(r, statusText, xhr, $form) {
					if ( typeof r.metadata.code == 'undefined' ) {
						swal('Error', 'Tidak ada respon dari server', 'error');
						console.log(r);
					}
					else if ( r.metadata.code != "200" ) {
						swal('Error', r.metadata.message, 'error');
						console.log(r);
					}
					else {
						var data = r.response;

						resetFormPendaftaran();
						$form.find('select').trigger('change');

						modal = jQuery('#modal-pendaftaran-berhasil');

						// set data to modal body
						modal.find('.noMr').html(data.noMr);
						modal.find('.namaPasien').html(data.namaPasien);
						modal.find('a.printKartu').attr('href', 'pdfb/kartupasien.php?idxdaftar='+data.idxDaftar);
						modal.find('a.printFormulir').attr('href', 'pdfb/formulir.php?idxdaftar='+data.idxDaftar);
						modal.find('a.printFormVerifikasi').attr('href', '<?= _BASE_ ?>index2.php?link=atributpasien&c=formverifikasi&a=download&idxdaftar='+data.idxDaftar);
						modal.find('a.printSep').attr('href', '<?= _BASE_ ?>index.php?link=sep&c=create_sep&idxdaftar='+data.idxDaftar);

						modal.modal('show');						
					}
				},
				error : function(e) {
					swal('Error', 'Tidak dapat menghubungi server', 'error');
					console.log(e);
				},
				complete : function() {
					jQuery('#myform').find('[type="submit"]').button('reset');
				}
			});
		}
	});
	
	jQuery('.loader').hide();
	jQuery('#KDCARABAYAR').hide();
	jQuery('#NOKARTU').hide();
	jQuery('#TGL_SEP').hide();
	jQuery('#DIAGNOSA').hide();
	jQuery('#JEPEL').hide();
	jQuery('.jkn_container').hide();

	<?php if ( $_REQUEST['xNOMR'] == '' ) : ?> 
		jQuery('#NOMR').attr('disabled','disabled').val('- otomatis -');
	<?php endif; ?>

	//------------------------------------------------------------------------

	jQuery('.statuspasien').on('change', function() {
		var status_val	= jQuery(this).val();

		if ( status_val == 1 ) {
			resetFormPendaftaran();

			jQuery('#rm-manual-container').show();
			jQuery('#search-pasien-container').hide();
			
			if ( jQuery('#rm_manual').is(':checked') != true ) {
				jQuery('#NOMR').attr('disabled', 'disabled').val('- otomatis -');
			}

			jQuery('#myform select').trigger('change');
		}
		else {
			jQuery('#rm-manual-container').hide();
			jQuery('#search-pasien-container').show();
			jQuery('#NOMR').removeAttr('disabled').val('');
		}
	});

	//------------------------------------------------------------------------

	jQuery('#btn-search-pasien').on('click', function() {
		jQuery('#modal-search-pasien').modal('show');
	});

	//------------------------------------------------------------------------
	
	jQuery('#rm_manual').on('change', function() {
		if ( this.checked ) {
			jQuery('#NOMR').removeAttr('disabled').val('');
		}
		else {
			jQuery('#NOMR').attr('disabled','disabled').val('- otomatis -');
		}
	});

	//------------------------------------------------------------------------

	jQuery('#sep-manual').on('change', function() {
		if ( this.checked ) {
			jQuery('#no_sep').removeAttr('disabled').val('');
		}
		else {
			jQuery('#no_sep').attr('disabled', 'disabled').val('- otomatis -');
		}
	});

	//------------------------------------------------------------------------

	jQuery('#TGLLAHIR').blur(function() {
		var tgl = jQuery(this).val();						  
		if(tgl == ('0000/00/00') || tgl == ('0000-00-00') || tgl == ('00-00-0000') || tgl == ('00/00/0000') ) {
			alert('Tanggal Lahir Tidak Boleh 00-00-0000');
			jQuery(this).val('');
		}
	});

	//------------------------------------------------------------------------

	jQuery('#NOMR').blur(function() {
		var nomr = jQuery(this).val();
		var status_pasien = jQuery('[name="STATUSPASIEN"]:checked').val();

		if ( status_pasien == 0 ) {
			if ( nomr != '' ) {
				jQuery('.loader').show();
				jQuery.ajax({
					url: '<?= _BASE_ ?>ajaxload.php',
					data: {'p' : 'pasien', 'a' : 'get_pasien', 'nomr' : nomr},
					beforeSend: function() {
						// do some form validation stuff here
					},
					success: function(response) {
						var response = eval("("+ response +")");

						if ( response.code == 200 ) {
							var data = response.data[0];

							setTimeout(function() {
								jQuery('#NAMA').val(data.NAMA);
								jQuery('#CALLER').val(data.TITLE);
								jQuery('#TEMPAT').val(data.TEMPAT);
								jQuery('#TGLLAHIR').val(data.TGLLAHIR_CAL);
								jQuery('#ALAMAT').val(data.ALAMAT);
								jQuery('#ALAMAT_KTP').val(data.ALAMAT_KTP);
								jQuery('#KDPROVINSI').val(data.KDPROVINSI);
								jQuery('#notelp').val(data.NOTELP);
								jQuery('#NOKTP').val(data.NOKTP);
								jQuery('#SUAMI_ORTU').val(data.SUAMI_ORTU);
								jQuery('#PEKERJAAN').val(data.PEKERJAAN);
								jQuery('[name="JENISKELAMIN"][value="'+data.JENISKELAMIN+'"]').trigger('click');
								jQuery('[name="STATUS"][value="'+data.STATUS+'"]').trigger('click');
								jQuery('[name="PENDIDIKAN"][value="'+data.PENDIDIKAN+'"]').trigger('click');
								jQuery('[name="AGAMA"][value="'+data.AGAMA+'"]').trigger('click');

								// menampilkan dropdown daerah
								getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : data.KDPROVINSI, target : '#KOTA', selected : data.KOTA });
								getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : data.KOTA, target : '#KDKECAMATAN', selected : data.KDKECAMATAN });
								getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : data.KDKECAMATAN, target : '#KELURAHAN', selected : data.KELURAHAN });

								// menampilkan umur
								calage1(jQuery('#TGLLAHIR').val(), 'umur');
							}, 10);
						}
						else {
							alert('Data tidak ditemukan.');
						}
					},
					error: function(error) {
						console.log(error);
						alert('Terjadi kesalahan. Tidak dapat menghubungi server.');
					},
					complete: function() {
						jQuery('.loader').hide();
					}
				});
			}
		}
	});

	//------------------------------------------------------------------------

	// handle keypress on no.rm field
	jQuery('#NOMR').keypress(function(e) {
		if ( e.keyCode == '13' ) {
			return false;
		}
	});

	//------------------------------------------------------------------------
	
	jQuery('#kdpoly').change(function(){
		var val	= jQuery(this).val();
		jQuery('#loader_namadokter').show();
		jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{kdpoly:val,load_dokterjaga:'true'},function(data){
			jQuery('#listdokter_jaga').empty().append(data);
		});
	});

	//------------------------------------------------------------------------

	// kolom lakalantas
	jQuery('[name="LAKALANTAS"]').change(function() {
		var lakalantas = jQuery(this).val();
		if ( lakalantas == 1 ) {
			jQuery('[name="LOKASI_LAKALANTAS"]').addClass('required');
		}
		else {
			jQuery('[name="LOKASI_LAKALANTAS"]').removeClass('required');
		}
	});

	//------------------------------------------------------------------------
	
	// menampilkan form detail berdasarkan cara bayar
	toggle_view_container(jQuery('[name="KDCARABAYAR"]').val());
	jQuery('[name="KDCARABAYAR"]').change(function() {
		var val = jQuery(this).val();
		toggle_view_container(val);
	});

	//------------------------------------------------------------------------

	// jkn container
	// metode pengisian change
	var btnSearchRujukan = jQuery('.jkn_container #btn-search-rujukan');
	var metode_pengisian = jQuery('.jkn_container [name="metode_pengisian"]');
	metode_pengisian.on('change', function() {
		var metode = jQuery(this).val();

		// kosongkan form
		emptyFormJKN();

		if ( metode == 'manual' ) {
			unlockFieldJKN();
			btnSearchRujukan.hide();
		}
		else if ( metode == 'rujukan' ) {
			lockFieldJKN();
			btnSearchRujukan.show();
		}
		else {
			lockFieldJKN();
			btnSearchRujukan.hide();
		}
	});

	//------------------------------------------------------------------------

	var modal_peserta = jQuery('#modal-search-peserta-bpjs');
	var form_peserta = jQuery('#form-search-peserta-bpjs');

	//------------------------------------------------------------------------

	// menampilkan modal cari peserta bpjs
	jQuery('#btn-search-peserta').on('click', function(e) {
		modal_peserta.modal('show');
	});

	//------------------------------------------------------------------------

	jQuery('#data-peserta-bpjs tbody').on('click', 'a.choose', function() {
		var resultTemp = modal_peserta.find('[name="resultTemp"]').val();
		var d = JSON.parse(resultTemp);

		jQuery('.jkn_container').find('[name="no_kartu"]').val(d.noKartu);
		jQuery('.jkn_container').find('[name="kd_jenis_peserta"]').val(d.jenisPeserta.kdJenisPeserta);
		jQuery('.jkn_container').find('[name="nm_jenis_peserta"]').val(d.jenisPeserta.nmJenisPeserta);
		jQuery('.jkn_container').find('[name="kd_kelas"]').val(d.kelasTanggungan.kdKelas);
		jQuery('.jkn_container').find('[name="nm_kelas"]').val(d.kelasTanggungan.nmKelas);
		jQuery('.jkn_container').find('[name="kd_ppk_rujukan"]').val(d.provUmum.kdProvider);
		jQuery('.jkn_container').find('[name="nm_ppk_rujukan"]').val(d.provUmum.nmProvider);
		jQuery('[name="NAMA"]').val(d.nama);
		jQuery('[name="NOKTP"]').val(d.nik);
		jQuery('[name="JENISKELAMIN"][value="'+d.sex+'"]').trigger('click');

		// merubah format tgl.lahir sesuai
		// format aplikasi simrs (why i should do this thing? f*ck!!!!!)
		var tgl_lahir = formatTglSimrsDirjenBukKemenkesFuck(d.tglLahir);
		jQuery('[name="TGLLAHIR"]').val(tgl_lahir);

		modal_peserta.modal('hide');
	});

	//------------------------------------------------------------------------

	modal_peserta.on('hidden.bs.modal', function() {
		// reset form
		form_peserta.trigger('reset');
		modal_peserta.find('[name="resultTemp"]').val('');
	});

	//------------------------------------------------------------------------

	// menampilkan modal cari data rujukan bpjs
	jQuery('#btn-search-rujukan').on('click', function(e) {
		jQuery('#modal-rujukan-bpjs').modal('show');
	});

	//------------------------------------------------------------------------

	var modal_provider = jQuery('#modal-provider-bpjs');

	//------------------------------------------------------------------------

	// menampilkan daftar provider bpjs
	jQuery('#btn-search-provider-bpjs').on('click', function(e) {
		modal_provider.modal('show');
	});

	//------------------------------------------------------------------------

	jQuery('#data-provider tbody').on('click', 'a.choose', function() {
		var kdProvider = jQuery(this).data('kode');
		var nmProvider = jQuery(this).data('nama');
		jQuery('.jkn_container [name="kd_ppk_rujukan"]').val(kdProvider);
		jQuery('.jkn_container [name="nm_ppk_rujukan"]').val(nmProvider);
		modal_provider.modal('hide');
	});

	//------------------------------------------------------------------------

	var modal_diagnosa = jQuery('#modal-diagnosa-bpjs');

	//------------------------------------------------------------------------

	// menampilkan daftar diagnosa
	jQuery('#btn-search-diagnosa').on('click', function(e) {
		modal_diagnosa.modal('show');
	});

	//------------------------------------------------------------------------

	jQuery('#data-diagnosa tbody').on('click', 'a.choose', function() {
		var kdDiagnosa = jQuery(this).data('kode');
		var nmDiagnosa = jQuery(this).data('nama');
		jQuery('.jkn_container [name="kd_diagnosa"]').val(kdDiagnosa);
		jQuery('.jkn_container [name="nm_diagnosa"]').val(nmDiagnosa);
		modal_diagnosa.modal('hide');
	});

	//------------------------------------------------------------------------
});
</script>

<style type="text/css">
	.loader{background:url(js/loading.gif) no-repeat; width:16px; height:16px; float:right; margin-right:30px;}
	.loader2{background:url(js/loading.gif) no-repeat; width:16px; height:16px; float:right; margin-right:30px;}
	input.error{ border:1px solid #F00;}
	label.error{ color:#F00; font-weight:bold;}
</style>

<div id="frame">
	<div id="frame_title"><h3 align="left">DATA ADMINISTRASI PASIEN</h3></div>
	<form name="myform" id="myform" action="models/pendaftaran.php" method="post">
		<fieldset class="fieldset">
			<legend>Data Administrasi</legend>

			<?php
			unset($_SESSION['register_nomr']);
			unset($_SESSION['register_nama']); ?>

			<table width="100%">
				<tr>
					<td width="70%">
						<table width="100%" border="0" style="background:none;">
							<tr>
								<td width="30%">Status Pasien</td>
								<td>
									<div id="psn" >
										<label class="radio-inline"><input type="radio" name="STATUSPASIEN" id="STATUSPASIEN_1" class="statuspasien" value="1" checked> Pasien Baru</label>
										<label class="radio-inline"><input type="radio" name="STATUSPASIEN" id="STATUSPASIEN_0" class="statuspasien" value="0"> Pasien Lama</label>
									</div>
								</td>
							</tr>
							<tr id="tr_nomr">
								<td>No. Rekam Medis</td>
								<td>
									<input class="text required" title="Tidak boleh kosong"  type="text" name="NOMR" id="NOMR" size="25" value="<?php echo $_REQUEST['xNOMR']; ?>"/>
									<span id="rm-manual-container" class="checkbox-inline"><input type="checkbox" id="rm_manual" name="rm_manual" value="1"><label for="rm_manual"> Manual</label></span>
									<span id="search-pasien-container" style="display:none"><button type="button" id="btn-search-pasien" class="text"><i class="fa fa-fw fa-search"></i> Cari Pasien</button></span>
									<div class="loader"></div>
								</td>
							</tr>
							<tr>
								<td>Rujukan</td>
								<td>
									<span class="relative-container">
										<?= dinamycDropdown( array (
											'name' => 'KDRUJUK',
											'table' => 'm_rujukan',
											'key' => 'KODE',
											'value' => 'NAMA',
											'selected' => (isset($_GET['KDRUJUK'])) ? xss_clean($_GET['KDRUJUK']) : '',
											'order_by' => 'ORDERS asc',
											'empty_first' => TRUE,
											'first_value' => '-- Pilih Rujukan --',
											'attr' => 'class="selectbox required text select2-single" title="Tidak boleh kosong"'
										) ); ?>
									</span>
								</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>
									<span class="relative-container">
										<?= dinamycDropdown( array (
											'name' => 'KDCARABAYAR',
											'table' => 'm_carabayar',
											'key' => 'KODE',
											'value' => 'NAMA',
											'selected' => (isset($_GET['KDCARABAYAR'])) ? xss_clean($_GET['KDCARABAYAR']) : '',
											'order_by' => 'ORDERS asc',
											'empty_first' => TRUE,
											'first_value' => '-- Pilih Cara Bayar --',
											'attr' => 'class="selectbox required text select2-single" title="Tidak boleh kosong"'
										) ); ?>
									</span>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<fieldset class="jkn_container carabayar_container bs-style">
										<legend>JKN</legend>
										<div class="row form-horizontal">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label col-md-4">Metode Pengisian</label>
													<div class="col-md-8">
														<select name="metode_pengisian" class="text form-control">
															<option value="" selected>-- Pilih Metode --</option>
															<option value="rujukan">Data Rujukan</option>
															<option value="manual">Entry Manual</option>
														</select>
														<p class="help-block">
															Pilih metode (manual/rujukan). Pilih rujukan untuk membaca data dari server berdasarkan nomor rujukan. Pilih manual untuk mengisi data secara manual.
														</p>
														<button type="button" class="btn btn-sm btn-info display-none" id="btn-search-rujukan">Cari Data Rujukan</button>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">No. Peserta</label>
													<div class="col-md-8">	
														<div class="input-group">
															<input type="text" name="no_kartu" readonly="readonly" class="text form-control" />
															<span class="input-group-btn">
																<button type="button" class="text btn-action btn" id="btn-search-peserta" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i>" disabled="disabled"><i class="fa fa-fw fa-search"></i></button>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">Jenis Peserta</label>
													<div class="col-md-8">	
														<input type="hidden" name="kd_jenis_peserta" readonly />
														<input type="text" name="nm_jenis_peserta" readonly="readonly" class="text form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">Kelas Tanggungan</label>
													<div class="col-md-8">	
														<input type="hidden" name="kd_kelas" readonly />
														<input type="text" name="nm_kelas" readonly="readonly" class="text form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">No. Rujukan</label>
													<div class="col-md-8">
														<input type="text" name="no_rujukan" readonly="readonly" class="text form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">Tgl. Rujukan</label>
													<div class="col-md-8 container-relative">
														<input type="text" name="tgl_rujukan" readonly="readonly" value="<?= date('Y-m-d H:i') ?>" class="text datetimepicker form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">Provider Pemberi Rujukan</label>
													<div class="col-md-8">	
														<input type="hidden" name="kd_ppk_rujukan" readonly />
														<div class="input-group">
															<input type="text" name="nm_ppk_rujukan" readonly="readonly" class="text form-control" />
															<span class="input-group-btn">
																<button type="button" class="text btn-action btn" id="btn-search-provider-bpjs" disabled="disabled"><i class="fa fa-fw fa-search"></i></button>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-4">Diagnosa Awal</label>
													<div class="col-md-8">	
														<input type="hidden" name="kd_diagnosa" readonly />
														<div class="input-group">
															<input type="text" name="nm_diagnosa" readonly="readonly" class="text form-control" />
															<span class="input-group-btn">
																<button type="button" class="text btn-action btn" id="btn-search-diagnosa" disabled="disabled"><i class="fa fa-fw fa-search"></i></button>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</fieldset>
								</td>
							</tr>
							<tr>
								<td>Poli / Dokter yang dituju </td>
								<td>
									<span class="relative-container">
										<?= dinamycDropdown( array (
											'name' => 'KDPOLY',
											'table' => 'm_poly',
											'key' => 'kode',
											'value' => 'nama',
											'selected' => (isset($_GET['KDPOLY'])) ? xss_clean($_GET['KDPOLY']) : '',
											'order_by' => 'nama asc',
											'empty_first' => TRUE,
											'first_value' => '-- Pilih Poliklinik --',
											'attr' => 'id="kdpoly" class="selectbox text select2-single required" title="Tidak boleh kosong"  style="float:left;margin-right:3px"'
										) ); ?>
									</span>
									<span id="listdokter_jaga">
										<?php
										if ( $_GET['KDPOLY'] != '' ) {
											$sqldokter	= mysql_query('select a.kddokter, b.NAMADOKTER from m_dokter_jaga a join m_dokter b on a.KDDOKTER = b.kddokter where a.kdpoly = "'.$_GET['KDPOLY'].'"');
											if ( mysql_num_rows($sqldokter) > 0 ) {
												echo '<select name="KDDOKTER">';
												while($datadok = mysql_fetch_array($sqldokter)) {
													if ($_GET['KDDOKTER'] == $datadok['kddokter']) : $sel = 'selected="selected"'; else: $sel = ''; endif;
													echo '<option value="'.$datadok['kddokter'].'" '.$sel.'>'.$datadok['NAMADOKTER'].'</option>';
												}
												echo '</select>';	
											}
											else {
												echo 'Tidak ada dokter jaga di poli tersebut';
											}
										} ?>
									</span>
								</td>
							</tr>
							<tr>
								<td>Minta Rujukan </td>
								<td><input type="checkbox" name="minta_rujukan" id="minta_rujukan" value="1" /></td>
							</tr>
							<tr>
								<td>Tanggal Daftar </td>
								<td>
									<div class="container-relative">
										<input type="text" name="TGLREG" id="TGLREG" class="text datetimepicker" value="<?php if(!empty($_GET['TGLREG'])){ echo $_GET['TGLREG']; }else{ echo date("Y-m-d H:i:s"); } ?>"/>
										<input type='hidden' name='start_daftar' id='start_daftar' />
									</div>
								</td>
							</tr>
							<tr>
								<td>Kecelakaan Lalulintas</td>
								<td>
									<select name="LAKALANTAS" class="text required" title="Tidak boleh kosong" >
										<option value="">-- Pilih --</option>
										<option value="1">Ya</option>
										<option value="2">Tidak</option>
									</select>
									<input type="text" class="text" name="LOKASI_LAKALANTAS" placeholder="Lokasi Lakalantas" title="Lokasi lakalantas wajib diisi" />
								</td>
							</tr>
							<tr>
								<td>Catatan</td>
								<td>
									<textarea class="blank text" rows="3" cols="50" name="catatan"></textarea>
								</td>
							</tr>
						</table>
					</td>
					<td width="30%">
						<table width="100%" align="right">
							<tr>
								<td width="40%">Shift Petugas Jaga</td>
								<td>
									<div><label class="radio-inline"><input type="radio" id="shift_pagi" name="SHIFT" class="required" title="Tidak boleh kosong"  value="1" <? if($t_pendaftaran->SHIFT=="1" || $_GET['SHIFT']=="1")echo "Checked";?>/> Pagi</label></div>
									<div><label class="radio-inline"><input type="radio" id="shift_siang" name="SHIFT" class="required" title="Tidak boleh kosong"  value="2" <? if($t_pendaftaran->SHIFT=="2" || $_GET['SHIFT']=="2")echo "Checked";?>/> Siang</label></div>
									<div><label class="radio-inline"><input type="radio" id="shift_sore" name="SHIFT" class="required" title="Tidak boleh kosong"  value="3" <? if($t_pendaftaran->SHIFT=="3" || $_GET['SHIFT']=="3")echo "Checked";?>/> Sore</label></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</fieldset>

		<div id="all">	
			<?php include("include/view_prosess.php");?>
		</div>
	</form>
</div>

<?php require 'modals/modal_search_pasien.php'; ?>
<?php require 'modals/modal_rujukan_bpjs.php'; ?>
<?php require 'modals/modal_search_peserta_bpjs.php'; ?>
<?php require 'modals/modal_diagnosa_bpjs.php'; ?>
<?php require 'modals/modal_search_provider.php'; ?>
<?php require 'modals/modal_pendaftaran_berhasil.php'; ?>