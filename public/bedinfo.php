<?php
require_once BASEPATH . 'models/m_admission.php';
require_once BASEPATH . 'models/m_ruang.php';
require_once BASEPATH . 'models/m_detail_tempat_tidur.php';

// data ruangan
$result = Models\Ruang\get( array (
	'order' => "nama ASC"
) );
?>

<?php if ( $result == FALSE ) : ?>
	<div style="padding: 20px">
		<div class="alert alert-warning">
			Tidak ada data ruangan ditemukan
		</div>
	</div>
<?php else : ?>
	<div class="row no-margin content" style="display:flex">
		<div class="col-sm-4 left-side" style="align-items:stretch">
			<div class="row no-margin top" style="display:flex">
				<div class="col-xs-3 logo" style="align-items:stretch">
					<img src="<?= _BASE_ . 'assets/img/logo-rsud.gif' ?>" class="img-responsive">
				</div>
				<div class="col-xs-9 title" style="align-items:stretch">
					<h3>
						Informasi Ketersediaan <br />
						Tempat Tidur <br />
						RSUD Sawerigading <br />
						Kota Palopo
					</h3>
				</div>
			</div>
			<div class="row no-margin">
				<div class="col-xs-12 table-summary">
					<table class="table table-striped table-horizontal">
						<thead>
							<tr>
								<th style="width:180px !important">Nama</th>
								<th>Kelas</th>
								<th style="width:10px !important;background:#28B62C;color:#FFFFFF"></th>
								<th style="width:10px !important;background:#FF4136;color:#FFFFFF"></th>
								<th style="width:10px !important"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $result as $ruang ) : ?>
								<tr>
									<td>
										<b><?= $ruang['nama'] ?></b>
										<div><i><?= $ruang['ruang'] ?></i></div>
									</td>
									<td><?= $ruang['kelas_rs'] ?></td>
									<td class="text-center" style="color:#28B62C;font-size:20px"><?= $ruang['tersedia'] ?></td>
									<td class="text-center" style="color:#FF4136;font-size:20px"><?= $ruang['kapasitas'] - $ruang['tersedia'] ?></td>
									<td class="text-center" style="font-size:20px"><?= $ruang['kapasitas'] ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-sm-8 right-side" style="align-items:stretch">
			<div class="row no-margin">
				<div class="col-xs-12">
					<div class="slider slider-for">
						<?php foreach ( $result as $ruang ) : ?>
							<div class="item">
								<div class="row no-margin">
									<div class="col-xs-12">
										<table class="table table-striped table-horizontal room-detail">
											<tr>
												<th colspan="3" style="background:#26799B">
													<h3 class="title">
														<?= $ruang['nama'] ?> (<?= $ruang['kelas_rs'] ?>) <br />
														<i style="font-size:14px"><?= $ruang['ruang'] ?></i>
													</h3>
												</th>
											</tr>
											<tr>
												<th width="30%" style="background:#28B62C;color:#FFFFFF">KOSONG</th>
												<th width="30%" style="background:#FF4136;color:#FFFFFF">DIGUNAKAN</th>
												<th style="background:">TOTAL</th>
											</tr>
											<tr>
												<td><h4><?= $ruang['tersedia'] ?></h4></td>
												<td><h4><?= $ruang['kapasitas'] - $ruang['tersedia'] ?></h4></td>
												<td><h4><?= $ruang['kapasitas'] ?></h4></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row no-margin">
									<?php
									// data tempat tidur
									$result2 = Models\TempatTidur\get( array (
										'query' => "WHERE idxruang = '" . $ruang['kd_kamar'] . "'",
										'order' => "no_tt+0 ASC, no_tt ASC"
									) );
									?>
									<?php if ( $result2 == FALSE ) : ?>
										<div class="col-xs-12" style="padding: 20px">
											<div class="alert alert-warning">
												Tidak ada data tempat tidur ditemukan pada ruangan <b><?= $ruang['nama'] ?></b> ruang <b><?= $ruang['kelas_rs'] ?></b>
											</div>
										</div>
									<?php else : ?>
										<?php foreach ( $result2 as $tt ) : ?>
											<?php $color = $tt['terisi'] == FALSE ? '#28B62C' : '#FF4136'; ?>
											<div class="col-xs-2 tt-container">
												<div class="text-center" style="color:<?= $color ?>">
													<div class="icon"><i class="fa fa-fw fa-bed"></i></div>
													<div class="caption"><?= $tt['no_tt'] ?></div>
												</div>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row no-margin footer" style="display:flex">
		<div class="col-xs-2 clock" style="align-items:stretch">
			00:00:00
		</div>
		<div class="col-xs-10 running-text" style="align-items:stretch">
			<marquee direction="left" scrollamount="7" behavior="scroll">
				.:: Selamat datang di Rumah Sakit Umum Daerah Sawerigading Kota Palopo ::.
			</marquee>
		</div>
	</div>
<?php endif; ?>