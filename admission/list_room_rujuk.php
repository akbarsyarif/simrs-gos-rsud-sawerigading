<?php
if ( ! function_exists("GetSQLValueString") ) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;    
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
				break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
}

$query_rsruang = "select * from m_ruang ORDER BY nama ASC";
$rsruang = mysql_query($query_rsruang) or die(mysql_error());
$row_rsruang = mysql_fetch_assoc($rsruang);
$totalRows_rsruang = mysql_num_rows($rsruang);
?>

<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>INFORMASI KAMAR DAN TEMPAT TIDUR</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<table width="80%" border="1" align="center" cellpadding="1" class="table table-striped bed-info" cellspacing="1">
			<tr>
				<th width="78"><div align="center">No</div></th>
				<th width="95"><div align="center">Nama </div></th>
				<th width="94"><div align="center">Kelas</div></th>
				<th width="137"><div align="center">Ruang</div></th>
				<th width="338"><div align="center">Jumlah Tempat Tidur</div></th>
			</tr>
			<?php $n = 1; ?>
			<?php do { ?>
				<tr>
					<td><div align="center"><?php echo $n; ?></div></td>
					<td><div align="center"><?php echo $row_rsruang['nama']; ?></div></td>
					<td><div align="center"><?php echo $row_rsruang['kelas']; ?></div></td>
					<td><div align="center"><?php echo $row_rsruang['ruang']; ?></div></td>
					<td>
						<div align="left">
						<?php
							$idx = $row_rsruang['no'];
							$namarg = $row_rsruang['nama'];
							$detail = "SELECT idxruang, no_tt, tampil FROM m_detail_tempat_tidur WHERE idxruang='{$idx}' AND tampil=1 ORDER BY no_tt ASC";
							$result = mysql_query($detail);
							while ( $brs = @mysql_fetch_array($result) ) {
								$seleksi = "SELECT t_admission.id_admission, t_admission.masukrs, m_pasien.NOMR, m_pasien.NAMA, m_dokter.NAMADOKTER
											FROM t_admission
											JOIN m_pasien ON m_pasien.NOMR = t_admission.nomr
											JOIN m_dokter ON m_dokter.KDDOKTER = t_admission.dokter_penanggungjawab
											WHERE t_admission.noruang='".$idx."' AND t_admission.nott='".$brs['no_tt']."' AND t_admission.keluarrs IS NULL";
								$hasilseleksi = mysql_query($seleksi);
								$jumlahseleksi = mysql_num_rows($hasilseleksi);
								$data_adm = mysql_fetch_assoc($hasilseleksi);
								$link = '';

								if ( $jumlahseleksi > 0 ) {
									$bed_color = 'text-danger';
									$detail_pasien = '<div><strong>'.$data_adm['NAMA'].'</strong></div>';
									$detail_pasien .= '<div>M.R. : '.$data_adm['NOMR'].'</div>';
									$detail_pasien .= '<div>T.M. : '.tgl_indonesia($data_adm['masukrs']).'</div>';
									$detail_pasien .= '<div>DPJP : '.$data_adm['NAMADOKTER'].'</div>';
								}
								else {
									if ( isset($_GET['no']) && !empty($_GET['no']) ) {
										$link = 'onclick="window.location.href=\'?link=17g&idruang='.$brs['idxruang'].'&p=daftar&idx='.$_GET['no'].'&no_tt='.$brs['no_tt'].'&namaruang='.$namarg.'\'"';
									}

									$bed_color = 'text-success';
									$detail_pasien = 'Kosong';
								}
								
								echo '<div class="col-sm-2">';
									echo '<div class="form-group">';
										echo '<a href="javascript:void(0)" 
												 class="btn-bed text-center '.$bed_color.'" 
												 name="button" 
												 id="'.$brs['no_tt'].'" 
												 data-content="<div style=\'font-size:11px\'>'.$detail_pasien.'</div>" 
												 rel="popover" 
												 data-placement="top" 
												 '.$link.'>';
											echo '<div><i class="fa fa-fw fa-3x fa-bed"></i></div>';
											echo '<div>'.$brs['no_tt'].'</div>';
										echo '</a>';
									echo '</div>';
								echo '</div>';
							}
						?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
				<?php $n++; ?>
			<?php } while ($row_rsruang = mysql_fetch_assoc($rsruang)); ?>
		</table>
	</div>
</div>

<?php mysql_free_result($rsruang); ?>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('.btn-bed').popover({
			trigger: "hover",
			html: true
		});
	});
</script>