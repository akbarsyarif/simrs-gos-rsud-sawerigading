<?php require 'includes/css/widget.css.php' ?>
<?php require 'includes/js/widget.js.php' ?>

<div class="widget widget-pendaftaran-iso" id="widget-pendaftaran-iso">
	<a class="float-button" href="javascript:void(0)" onclick="toggleWidgetPendaftaranIso(this)"><i class="fa fa-fw fa-line-chart"></i></a>
	<div class="widget-content hide-widget">
		<div class="widget-content-2">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>
	</div>
</div>