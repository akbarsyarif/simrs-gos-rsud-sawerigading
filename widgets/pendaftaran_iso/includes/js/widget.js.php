<script type="text/javascript">
	function toggleWidgetPendaftaranIso(obj) {
		let hide = jQuery('#widget-pendaftaran-iso .widget-content').hasClass('hide-widget');
		if ( hide ) {
			jQuery('#widget-pendaftaran-iso .widget-content').removeClass('hide-widget').show();
			jQuery(obj).animate({
				right: '300px'
			}, 0);

			// animate loading
			jQuery('#widget-pendaftaran-iso .widget-content-2').waitMe({
				effect: 'stretch',
				text: '',
				bg: 'rgba(255,255,255,0.7)',
				sizeW: '',
				sizeH: '',
				source: '',
				onClose: function() {
				}
			});
		} else {
			jQuery('#widget-pendaftaran-iso .widget-content').addClass('hide-widget').hide();
			jQuery(obj).animate({
				right: '0px'
			}, 0);

			// stop loading anomation
			jQuery('#widget-pendaftaran-iso .widget-content-2').waitMe('hide');
		}
	}
</script>