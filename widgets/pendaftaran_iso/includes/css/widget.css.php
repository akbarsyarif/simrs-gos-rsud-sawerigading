<style type="text/css">
	#widget-pendaftaran-iso {
		position: fixed;
		z-index: 2;
		background-color: #AE0004;
		top: 20px;
		right: 0;
		width: 300px;
	}

	#widget-pendaftaran-iso .float-button {
		position: absolute;
		right: 0;
		display: inline-block;
		background-color: #AE0004;
		color: #FFFFFF;
		padding: 5px 7px;
	}

	#widget-pendaftaran-iso .widget-content {
		display: none;
	}

	#widget-pendaftaran-iso .widget-content-2 {
		margin: 3px;
		background-color: #FFFFFF;
		height: 400px;
		padding: 5px;
	}
</style>