<?php

session_start();

require_once 'include/connect.php';
require_once BASEPATH . 'include/function.php';
require_once BASEPATH . 'include/security_helper.php';
require_once BASEPATH . 'include/error_helper.php';
require_once BASEPATH . 'include/pasien_helper.php';
require_once BASEPATH . 'include/phpMyBorder2.class.php';

// authenticate
$link = xss_clean($_GET['link']);
if ( $link !== 'public' ) {
	if ( ! isset($_SESSION['SES_REG']) ) header("location:login.php");
}

// register phpmyborder
$pmb = new PhpMyBorder(false);

// register themes
require_once BASEPATH . 'include/themes.php';

mysql_close($connect);