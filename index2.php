<?php
session_start();

if ( ! isset($_SESSION['SES_REG']) ) header("location:login.php");

require 'include/connect.php';
require 'include/function.php';
require 'include/security_helper.php';

require BASEPATH . 'vendor/autoload.php';

$module     = xss_clean($_GET['link']);
$controller = xss_clean($_GET['c']);
$action     = xss_clean($_GET['a']);

$filepath = BASEPATH . $module . DIRSEPARATOR . $controller . FILEEXT;

if ( file_exists($filepath) ) {

	require $filepath;

	if ( function_exists($action) ) {
		$action();
		exit();
	}
	else {
		die('Function not found');
	}
}
else {
	die('Page not found');
}