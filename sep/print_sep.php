<?php
require BASEPATH . 'thirdparty/mpdf/mpdf.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'include/bpjs_api.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('__downloadSepPdf') )
{

	function __downloadSepPdf($data_sep, $info_cetak)
	{

		// merubah format tanggal sep
		$tgl_sep = isset($data_sep->tglSep) ? date_format(date_create($data_sep->tglSep), 'd/m/Y') : '';

		// merubah format tanggal lahir
		$tgl_lahir = isset($data_sep->peserta->tglLahir) ? date_format(date_create($data_sep->peserta->tglLahir), 'd/m/Y') : '';

		// catatan
		if ( isset($data_sep->lakaLantas->status) ) {
			if ( $data_sep->lakaLantas->status != "0" ) {
				$catatan = 'Kecelakaan Lalulintas. ' . $data_sep->catatan;
			}
			else {
				$catatan = $data_sep->catatan;
			}
		}

		$jumlah_cetak = isset($info_cetak['jumlah_cetak']) ? $info_cetak['jumlah_cetak'] : '1';
		$tgl_cetak = isset($info_cetak['tgl_cetak']) ? date_format(date_create($info_cetak['tgl_cetak']), 'd/m/Y H:i:s') : date('d/m/Y H:i:s');

		// ob_start();

		// tentukan jenis printer yang digunakan
		// if ( isset($_COOKIE['printer_type']) ) {
		// 	$printer_type = $_COOKIE['printer_type'];
		// }
		// else {
		// 	$printer_type = 'epson_lq-2190';
		// }

		// if ( $printer_type == "epson_lq-2190" ) {
		// 	$mpdf = new mPDF('utf-8', array(210.058,94.996), 0, 'arial', 10,10,10,10,0,0, 'P');
		// }
		// else {
		// 	$mpdf = new mPDF('utf-8', array(210.058,94.996), 0, 'arial', 10,10,0.1,0.1,0,0, 'P');
		// }

		require BASEPATH . 'sep/html/sep.php';

		// $content = ob_get_clean();
		// $mpdf->WriteHTML($content);

		// $mpdf->Output();

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('printSep') )
{

	function printSep()
	{

		if ( ! isset($_GET['idxdaftar']) ) {

			__downloadSepPdf(NULL, NULL);
			exit();

		}

		$idxdaftar = xss_clean($_GET['idxdaftar']);

		// data transaksi
		$transaksi = Models\row_pendaftaran(array(
			'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
		));

		if ( $transaksi === FALSE ) {
			$error_msg[] = 'Index pendaftaran tidak ditemukan.';
		}

		// cek apakah ada error
		if ( isset($error_msg) && count($error_msg) > 0 ) {
			
			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => show_error($error_msg, TRUE)
				),
				'response' => NULL
			);

		}
		else {

			$no_sep = $transaksi['NO_SEP'];

			// cek detail sep di server bpjs
			$result_sep = detail_sep($no_sep);

			if ( $result_sep === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "800",
						'message' => "Tidak dapat menghubungi server BPJS. Silahkan coba beberapa saat lagi."
					),
					'response' => NULL
				);

			}
			else {

				// validasi sep
				if ( $result_sep->metadata->code == "200" ) {

					$data_sep = $result_sep->response;

					// increment kolom cetakan dan tanggal cetakan terakhir
					$info_cetak = array (
						'jumlah_cetak' => $transaksi['JUMLAH_CETAK'],
						'tgl_cetak' => $transaksi['DATETIME_CETAK_TERAKHIR']
					);

					__downloadSepPdf($data_sep, $info_cetak);

				}

			}

		}
		
	}

}

//---------------------------------------------------------------------------------