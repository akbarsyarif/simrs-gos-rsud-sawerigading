<?php
require BASEPATH . 'include/bpjs_api.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('getTransaksi') )
{

	function getTransaksi()
	{

		$idxdaftar = xss_clean($_GET['idxdaftar']);

		if ( is_null($idxdaftar) ) {
			$err_msg[] = 'IDXDAFTAR tidak boleh kosong.';
		}

		if ( isset($err_msg) && count($err_msg) > 0 ) {

			$response = (object) array (
				'metadata' => array (
					'code' => "800",
					'message' => show_error($err_msg, TRUE)
				),
				'response' => NULL
			);

		}
		else {

			$data = Models\row_pendaftaran( array (
				'query' => "WHERE t1.KDCARABAYAR = 2 AND t1.IDXDAFTAR = '{$idxdaftar}'"
			) );

			if ( $data === FALSE ) {

				$response = (object) array (
					'metaData' => (object) array (
						'code' => "201",
						'message' => 'Data tidak ditemukan'
					),
					'response' => NULL
				);

			}
			else {

				$no_kartu = $data['NO_KARTU'];

				$result_bpjs = get_peserta('nokartu', $no_kartu);

				if ( $result_bpjs === FALSE ) {

					$response = (object) array (
						'metaData' => (object) array (
							'code' => "800",
							'message' => "Tidak dapat menghubungi server BPJS"
						),
						'response' => (object) array (
						)
					);

				}
				else {

					$response = (object) array (
						'metaData' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => (object) array (
							'simrs' => $data,
							'bpjs' => $result_bpjs
						)
					);

				}

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('getSep') )
{

	function getSep()
	{

		$idxdaftar = xss_clean($_GET['idxdaftar']);
		$nosep = xss_clean($_GET['nosep']);

		if ( is_null($idxdaftar) ) {
			$err_msg[] = 'IDXDAFTAR tidak boleh kosong.';
		}
		if ( is_null($nosep) ) {
			$err_msg[] = 'NO.SEP tidak boleh kosong.';
		}

		if ( isset($err_msg) && count($err_msg) > 0 ) {

			$response = (object) array (
				'metadata' => array (
					'code' => '201',
					'message' => show_error($err_msg, TRUE)
				),
				'response' => NULL
			);

		}
		else {

			// jika tidak ada error
			$data = Models\row_pendaftaran( array (
				'query' => "WHERE t1.KDCARABAYAR = 2 AND t1.IDXDAFTAR = '{$idxdaftar}' AND t1.NO_SEP = '{$nosep}'"
			) );

			if ( $data === FALSE ) {

				$response = array (
					'metaData' => array (
						'code' => 201,
						'message' => 'Data tidak ditemukan'
					),
					'response' => NULL
				);

			}
			else {

				// get sep from server
				$result_sep = detail_sep(urlencode($nosep));

				if ( $result_sep === FALSE ) {

					$response = array (
						'metaData' => array (
							'code' => 800,
							'message' => 'Tidak dapat menghubungi server BPJS'
						),
						'response' => NULL
					);

				}
				else {

					$response = array (
						'metaData' => array (
							'code' => 200,
							'message' => 'OK'
						),
						'response' => array (
							'simrs' => $data,
							'bpjs' => $result_sep
						)
					);

				}

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('dataTableSep') ) 
{

	function dataTableSep()
	{

		$query = array();
		$query['query'] = '';

		// columns index
		$column_index = array(NULL, 't1.TGLREG', 't1.NO_KARTU', 'm1.NOMR', 'm1.NAMA', 'm1.JENISKELAMIN', 'm2.nama', 'm3.NAMADOKTER', 't1.SHIFT', 't1.NO_SEP', 't1.TGLREG');

		// filter column handler / search
		$columns = $_GET['columns'];

		$query['query'] .= "WHERE t1.KDCARABAYAR = 2";
		$query['query'] .= " AND (m10.ROLES = 1 OR m10.ROLES = NULL)";

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['query'] .= " AND " . $column_index[3] . " LIKE '%$search_value%'";

		}
		else {

			$i = 0;

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					$query['query'] .= " AND ";

					if ( $column_key == 5 || $column_key == 6 ) {
						$query['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
					}
					else if ( $column_key == 1 ) {
						$query['query'] .= "t1.TGLREG >= '" . $column_val['search']['value'] . "'";
					}
					else if ( $column_key == 10 ) {
						$query['query'] .= "t1.TGLREG <= '" . $column_val['search']['value'] . "'";
					}
					else {
						$query['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
					}

					$i++;

				}

			}

		}

		$records_total = Models\count_pendaftaran($query);
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order'] = $column_index[$order_column] . " " . $order_dir;
			}
		}

		// limit
		$query['limit'] = $_GET['length'];
		$query['offset'] = $_GET['start'];

		$data = Models\get_pendaftaran($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('createSep') )
{

	function createSep()
	{
		$error_msg = '';

		// profil RS
		$profil = getProfil();

		$idxdaftar = xss_clean($_POST['idxdaftar']);
		$no_sep = xss_clean($_POST['no_sep']);
		$no_kartu = xss_clean($_POST['no_kartu']);
		$tgl_registrasi = date('Y-m-d');
		$tgl_rujukan = xss_clean($_POST['tgl_rujukan']);
		$no_rujukan = xss_clean($_POST['no_rujukan']);
		$kd_ppk_rujukan = xss_clean($_POST['ppk_rujukan']);
		$kd_ppk_pelayanan = $profil['kdproviderbpjs'];
		$kd_jenis_pelayanan = xss_clean($_POST['kd_jenis_pelayanan']);
		$catatan = xss_clean($_POST['catatan']);
		$kd_diagnosa = xss_clean($_POST['kd_diagnosa']);
		$kd_poli_bpjs = xss_clean($_POST['kd_poli_bpjs']);
		$kd_kelas_rawat = xss_clean($_POST['kls_rawat']);
		$lakalantas = xss_clean($_POST['lakalantas']);
		$lokasi_lakalantas = xss_clean($_POST['lokasi_lakalantas']);
		$user_login = xss_clean($_POST['user_login']);
		$no_mr = xss_clean($_POST['no_mr']);
		$kd_status_peserta = xss_clean($_POST['kd_status_peserta']);

		// validasi
		if ( $no_sep != '' ) {
			$error_msg = "No. SEP sudah dibuat. Jika anda ingin mengedit SEP silahkan klik tombol EDIT SEP";
		}

		if ( $kd_status_peserta != 0 ) {
			$error_msg = "Tidak dapat membuat SEP. Status kepesertaan {$nm_status_peserta}";
		}

		if ( $no_kartu == '' ) {
			$error_msg = "No. kartu tidak boleh kosong";
		}

		// mengecek status peserta
		if ( strlen($error_msg) > 0 ) {
			
			$response = (object) array (
				'metadata' => (object) array (
					'code' => "800",
					'message' => "$error_msg"
				),
				'response' => NULL
			);
		
		}
		else {

			$data_sep = array (
				"no_kartu" => $no_kartu,
				"tgl_sep" => $tgl_registrasi,
				"tgl_rujukan" => $tgl_rujukan,
				"no_rujukan" => $no_rujukan,
				"ppk_rujukan" => $kd_ppk_rujukan,
				"ppk_pelayanan" => $kd_ppk_pelayanan,
				"kd_jns_pelayanan" => $kd_jenis_pelayanan,
				"catatan" => $catatan,
				"kd_diagnosa_awal" => $kd_diagnosa,
				"kd_poli_tujuan" => $kd_poli_bpjs,
				"kd_kelas_rawat" => $kd_kelas_rawat,
				"kd_lakalantas" => $lakalantas,
				"lokasi_lakalantas" => $lokasi_lakalantas,
				"username" => $user_login,
				"no_mr" => $no_mr
			);

			// submit sep to bpjs server
			$result_sep = insert_sep($data_sep);

			if ( $result_sep === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "800",
						'message' => "Tidak dapat menghubungi server BPJS. Silahkan coba kembali beberapa saat lagi."
					),
					'response' => NULL
				);

			}
			else {

				// no sep baru
				$no_sep_baru = $result_sep->response;

				// jika sep berhasil dibuat
				// update sep lokal (SIMRS)
				if ( $no_sep_baru != NULL ) {
					$update_simrs = Models\update_t_pendaftaran( array (
						'NO_SEP' => $no_sep_baru
					), $idxdaftar );
				}

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "200",
						'message' => "OK"
					),
					'response' => $result_sep
				);

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('updateSep') )
{

	function updateSep()
	{

		$put = array();
		parse_str(file_get_contents('php://input'), $put);

		$idxdaftar = xss_clean($put['idxdaftar']);
		$nosep = xss_clean($put['noSep']);

		// get data pelayanan (simrs)
		$result_pelayanan = Models\row_pendaftaran( array (
			'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}' AND t1.NO_SEP = '{$nosep}'"
		) );

		if ( $result_pelayanan === FALSE ) {

			$response = array (
				'metaData' => array (
					'code' => 201,
					'message' => 'Data transaksi tidak ditemukan'
				),
				'response' => NULL
			);

		}
		else {

			$data = array();

			// exit(print_r($put));

			// get profil rumah sakit
			$profil = getProfil();

			if ( isset($put['noSep']) && !empty($put['noSep']) ) { $data['noSep'] = $put['noSep']; }
			if ( isset($put['noKartu']) && !empty($put['noKartu']) ) { $data['noKartu'] = $put['noKartu']; }
			if ( isset($put['tglSep']) && !empty($put['tglSep']) ) { $data['tglSep'] = $put['tglSep']; }
			if ( isset($put['tglRujukan']) && !empty($put['tglRujukan']) ) { $data['tglRujukan'] = $put['tglRujukan']; }
			if ( isset($put['noRujukan']) && !empty($put['noRujukan']) ) { $data['noRujukan'] = $put['noRujukan']; }
			if ( isset($put['ppkRujukan']) && !empty($put['ppkRujukan']) ) { $data['ppkRujukan'] = $put['ppkRujukan']; }
			$data['ppkPelayanan'] = $profil['kdproviderbpjs'];
			if ( isset($put['jnsPelayanan']) && !empty($put['jnsPelayanan']) ) { $data['jnsPelayanan'] = $put['jnsPelayanan']; }
			$data['catatan'] = $put['catatan'];
			if ( isset($put['diagAwal']) && !empty($put['diagAwal']) ) { $data['diagAwal'] = $put['diagAwal']; }
			if ( isset($put['diagAwal']) && !empty($put['diagAwal']) ) { $data['diagAwal'] = $put['diagAwal']; }
			if ( isset($put['poliTujuan']) && !empty($put['poliTujuan']) ) {
				// mapping poli tujuan
				$kd_poly_bpjs = mappingPolyBpjs($put['poliTujuan']);
				$data['poliTujuan'] = $kd_poly_bpjs;
			}
			if ( isset($put['klsRawat']) && !empty($put['klsRawat']) ) { $data['klsRawat'] = $put['klsRawat']; }
			if ( isset($put['lakaLantas']) && !empty($put['lakaLantas']) ) { $data['lakaLantas'] = $put['lakaLantas']; }
			$data['lokasiLaka'] = $put['lokasiLaka'];
			if ( isset($put['user']) && !empty($put['user']) ) { $data['user'] = $put['user']; }
			if ( isset($put['noMr']) && !empty($put['noMr']) ) { $data['noMr'] = $put['noMr']; }

			// print_r($data);
			// exit();

			$result_update_sep = update_sep($data);

			if ( $result_update_sep === FALSE ) {

				$response = array (
					'metaData' => array (
						'code' => 800,
						'message' => 'Tidak dapat menghubungi server BPJS'
					),
					'response' => NULL
				);

			}
			else {

				$response = array (
					'metaData' => array (
						'code' => 200,
						'message' => 'OK'
					),
					'response' => $result_update_sep
				);

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------
// update tanggal pulang

if ( ! function_exists('updateTglPulang') )
{

	function updateTglPulang()
	{

		$put = array();
		parse_str(file_get_contents('php://input'), $put);

		$idxdaftar = xss_clean($put['idxdaftar']);
		$nosep = xss_clean($put['noSep']);

		// get data pelayanan (simrs)
		$result_pelayanan = Models\row_pendaftaran( array (
			'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}' AND t1.NO_SEP = '{$nosep}'"
		) );

		if ( $result_pelayanan === FALSE ) {

			$response = array (
				'metaData' => array (
					'code' => 201,
					'message' => 'Data transaksi tidak ditemukan'
				),
				'response' => NULL
			);

		}
		else {

			$data = array();

			// exit(print_r($put));

			// get profil rumah sakit
			$profil = getProfil();

			if ( isset($put['noSep']) && !empty($put['noSep']) ) { $data['noSep'] = $put['noSep']; }
			if ( isset($put['tglPulang']) && !empty($put['tglPulang']) ) { $data['tglPulang'] = $put['tglPulang']; }
			if ( isset($put['ppkPelayanan']) && !empty($put['ppkPelayanan']) ) { $data['ppkPelayanan'] = $put['ppkPelayanan']; }

			// print_r($data);

			$result_update_tgl_pulang = update_tgl_pulang($data);

			if ( $result_update_tgl_pulang === FALSE ) {

				$response = array (
					'metaData' => array (
						'code' => 800,
						'message' => 'Tidak dapat menghubungi server BPJS'
					),
					'response' => NULL
				);

			}
			else {

				$response = array (
					'metaData' => array (
						'code' => 200,
						'message' => 'OK'
					),
					'response' => $result_update_tgl_pulang
				);

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('deleteSep') )
{

	function deleteSep()
	{

		$params = array();
		parse_str(file_get_contents('php://input'), $params);

		$profil = getProfil();

		$idxdaftar = xss_clean($params['idxdaftar']);
		$nosep = xss_clean($params['nosep']);
		$ppk_pelayanan = xss_clean($profil['kdproviderbpjs']);

		// delete remote sep
		$result_sep = delete_sep( array (
			'no_sep' => $nosep,
			'ppk_pelayanan' => $ppk_pelayanan
		) );

		if ( $result_sep === FALSE ) {

			$response = array (
				'metaData' => array (
					'code' => 800,
					'message' => 'Tidak dapat menghubungi server BPJS'
				),
				'response' => NULL
			);

		}
		else {

			// update lokal sep
			// kosongkan no.sep
			$data = array (
				'NO_SEP' => NULL,
				'JUMLAH_CETAK' => 0,
				'TGL_CETAK_TERAKHIR' => NULL
			);
			$result = Models\update_t_pendaftaran($data, $idxdaftar);

			if ( $result === FALSE ) {

				$response = array (
					'metaData' => array (
						'code' => 800,
						'message' => 'Gagal mengupdate SEP lokal SIMRS'
					),
					'response' => NULL
				);

			}
			else {

				// reset jumlah cetak dan tgl. cetak terakhir
				Models\resetJumlahCetak($idxdaftar);

				$response = array (
					'metaData' => array (
						'code' => 200,
						'message' => 'OK'
					),
					'response' => $result_sep
				);

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('incJumlahCetak') )
{

	function incJumlahCetak()
	{

		$idxdaftar = xss_clean($_GET['idxdaftar']);

		$result = Models\incJumlahCetak($idxdaftar);

		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "Data transaksi tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $result
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('getRiwayatPelayananBPJS') )
{

	function getRiwayatPelayananBPJS()
	{

		$idxdaftar = xss_clean($_GET['idxdaftar']);
		$nokartu = xss_clean($_GET['nokartu']);

		if ( is_null($nokartu) ) {
			$err_msg[] = 'NO.Kartu tidak boleh kosong.';
		}

		if ( isset($err_msg) && count($err_msg) > 0 ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => show_error($err_msg, TRUE)
				),
				'response' => NULL
			);

		}
		else {

			// jika tidak ada error
			$result_simrs = Models\row_pendaftaran( array (
				'query' => "WHERE t1.KDCARABAYAR = 2 AND t1.IDXDAFTAR = '{$idxdaftar}'"
			) );

			if ( $result_simrs === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "201",
						'message' => 'Data tidak ditemukan'
					),
					'response' => NULL
				);

			}
			else {

				// baca riwayat pelayanan peserta dari server bpjs
				$result_bpjs = riwayat_pelayanan_peserta(urlencode($nokartu));

				if ( $result_bpjs === FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "400",
							'message' => 'Tidak dapat menghubungi server BPJS'
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => 'OK'
						),
						'response' => (object) array (
							'result_simrs' => $result_simrs,
							'result_bpjs' => $result_bpjs
						)
					);

				}

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------