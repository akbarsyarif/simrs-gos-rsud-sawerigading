<?php
require BASEPATH . 'include/error_helper.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('getSettings') )
{

	function getSettings()
	{

		// daftar printer yang suport SEP SIMRS
		$printer_list = array (
			'epson_lq-2190',
			'epson_lx-310'
		);

		// pengaturan printer default
		$default_printer = $printer_list[0];

		if ( isset($_COOKIE['printer_type']) ) {

			$printer_type = $_COOKIE['printer_type'];

			if ( in_array($printer_type, $printer_list) ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "200",
						'message' => "OK"
					),
					'response' => (object) array (
						'printer_type' => $printer_type
					)
				);

			}
		}
		else {
			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => (object) array (
					'printer_type' => $default_printer
				)
			);
		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('saveSettings') )
{

	function saveSettings()
	{

		$printer_type = xss_clean($_POST['printer_type']);

		// menyimpan pengaturan ke cookie client
		$result = setCookie('printer_type', $printer_type, time() + (10 * 365 * 24 * 60 * 60));

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "500",
					'message' => "Internal Server Error"
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => (object) array (
					'printer_type' => $printer_type
				)
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------