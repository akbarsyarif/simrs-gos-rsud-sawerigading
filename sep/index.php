<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>SEP (BPJS)</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<a href="<?= _BASE_ . 'index.php?link=sep&c=create_sep' ?>" data-toggle="modal" class="btn btn-sm btn-primary">Buat SEP</a>
				<a href="#modal-search-transaksi" data-toggle="modal" class="btn btn-sm btn-default">Cari Data SEP</a>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-sm btn-default" href="<?= _BASE_ . 'index2.php?link=sep&c=print_sep&a=printSep' ?>" target="_blank">Cetak Blanko SEP</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered" id="datatable-pelayanan">
					<thead>
						<th width="10"></th>
						<th width="80">Tgl.Pelayanan</th>
						<th width="100">No.Peserta</th>
						<th width="50">No.RM</th>
						<th>Nama</th>
						<th width="10">L/P</th>
						<th width="100">Poly</th>
						<th>Dokter</th>
						<th width="20">Shift</th>
						<th width="80">No.SEP</th>
						<th width="80">Riwayat SEP</th>
						<th><!-- EndDate --></th>
						<th width="10">Tgl.Pulang</th>
						<th width="10"></th>
						<th width="10"></th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modal_search_transaksi.php' ?>
<?php require 'includes/modal_update_tgl_pulang.php' ?>
<?php require 'includes/modal_riwayat_pelayanan_bpjs.php' ?>
<?php require 'includes/modal_edit_sep.php' ?>

<!-- javascript -->
<script type="text/javascript">
	function decodeBpjsSepResult(d)
	{
		var html = '';

		html += '<table class="table table-striped table-horizontal" id="table-riwayat-pelayanan-bpjs">';
			html += '<tr>';
				html += '<th>No.SEP</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.noSep+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>No.MR</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.peserta.noMr+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Nama Pasien</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.peserta.nama+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>No.Peserta BPJS</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.peserta.noKartu+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Tgl.SEP</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.tglSep+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Tgl.Rujukan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.tglRujukan+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>No.Rujukan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.noRujukan+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Faskes Rujukan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.provRujukan.nmProvider+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Jenis Pelayanan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.jnsPelayanan+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Catatan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.catatan+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Diagnosa Awal</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.diagAwal.nmDiag+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Poli Tujuan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.poliTujuan.nmPoli+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Lakalantas</th>';
				html += '<td width="10">:</td>';

				var lakalantas_label = 'Bukan';
				var keterangan_lakalantas = '('+d.lakaLantas.keterangan+')';
				
				if ( d.lakaLantas.status == 1 ) {
					lakalantas_label = 'Iya';
				}

				if ( d.lakaLantas.keterangan == '' || d.lakaLantas.keterangan == null ) {
					keterangan_lakalantas = '';
				} 
				
				html += '<td>'+lakalantas_label+' '+keterangan_lakalantas+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Kelas Rawat</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.klsRawat.nmKelas+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Tgl.Pulang</th>';
				html += '<td width="10">:</td>';

				if ( d.tglPulang == null ) {
					tglPulang = '<i class="text-danger">Belum Pulang</i>';
				}
				else {
					tglPulang = '<span class="text-success">'+d.tglPulang+'</span>';
				}

				html += '<td>'+tglPulang+'</td>';
			html += '</tr>';
		html += '</table>';

		return html;
	}

	function decodeSimrsSepResult(d)
	{
		var html = '';

		html += '<table class="table table-striped table-horizontal" style="margin-bottom: 0 !important">';
			html += '<tr>';
				html += '<th>No.SEP</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NO_SEP+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>No.MR</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NOMR+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Nama Pasien</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NAMA_PASIEN+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>No.Peserta BPJS</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NO_KARTU+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Tgl.SEP</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.TGLREG+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Tgl.Rujukan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.TGL_RUJUKAN+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>No.Rujukan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NO_RUJUKAN+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Faskes Rujukan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NM_PPK_RUJUKAN+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Jenis Pelayanan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NM_JENIS_PELAYANAN+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Catatan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.CATATAN+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Diagnosa Awal</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NM_DIAGNOSA+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Poli Tujuan</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.NAMA_POLY+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Lakalantas</th>';
				html += '<td width="10">:</td>';

				var lokasi_lakalantas = '('+d.LOKASI_LAKALANTAS+')';
				if ( d.LOKASI_LAKALANTAS == null ) {
					lokasi_lakalantas = '';
				} 
				
				html += '<td>'+d.LAKALANTAS_LABEL+' '+lokasi_lakalantas+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Kelas Rawat</th>';
				html += '<td width="10">:</td>';
				html += '<td>'+d.KELAS_LABEL+'</td>';
			html += '</tr>';
			html += '<tr>';
				html += '<th>Tgl.Pulang</th>';
				html += '<td width="10">:</td>';

				if ( d.TGL_PULANG == null ) {
					tglPulang = '<i class="text-danger">Belum Pulang</i>';
				}
				else {
					tglPulang = '<span class="text-success">'+d.TGL_PULANG+'</span>';
				}

				html += '<td>'+tglPulang+'</td>';
			html += '</tr>';
		html += '</table>';

		return html;
	}

	function decodeRiwayatPelayanan(list)
	{
		var html = '';

		html += '<table class="table">';
			html += '<thead>';
				html += '<tr>';
					html += '<th>No.SEP</th>';
					html += '<th>Tgl.SEP</th>';
					html += '<th>Poli</th>';
					html += '<th>Pelayanan</th>';
					html += '<th>Diagnosa</th>';
					html += '<th>Biaya Tagihan</th>';
					html += '<th>Tautkan</th>';
				html += '</tr>';
			html += '</thead>';

			html += '<tbody>';
			for (var i = 0; i < list.length; i++) {
				var d = list[i];
				html += '<tr>';
					html += '<td><i>'+d.noSEP+'</i></td>';
					html += '<td>'+moment(d.tglSEP).format('DD/MM/YYYY')+'</td>';
					html += '<td>'+d.poliTujuan.nmPoli+'</td>';
					html += '<td>'+d.jnsPelayanan+'</td>';
					html += '<td>'+d.diagnosa.kodeDiagnosa+' ('+d.diagnosa.namaDiagnosa+') '+'</td>';
					html += '<td>'+accounting.formatMoney(d.biayaTagihan,'Rp. ')+'</td>';
					html += '<td class="text-center"><button class="btn btn-xs btn-primary data-nosep="'+d.noSEP+'" tautkan-sep"><i class="fa fa-fw fa-link"></i></button></td>';
				html += '</tr>';
			};
			html += '</tbody>';

		html += '</table>';

		return html;
	}

	jQuery(document).ready(function() {
		function format ( d ) {
			// `d` is the original data object for the row
			var html = '';

			html += '<div class="row">';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">Tgl. Pelayanan</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+moment(d.JAMREGRAW).format('DD/MM/YYYY')+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Waktu Pelayanan</th>';
							html += '<td>:</td>';
							html += '<td>'+moment(d.JAMREGRAW).format('hh:mm')+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Poli Tujuan</th>';
							html += '<td>:</td>';
							html += '<td>'+d.NAMA_POLY+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Dokter</th>';
							html += '<td>:</td>';
							html += '<td>'+d.NAMADOKTER+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Jenis Pelayanan</th>';
							html += '<td>:</td>';

							if ( d.KD_JENIS_PELAYANAN == "1" ) {
								jenisPelayanan = '<span class="text-success">'+d.NM_JENIS_PELAYANAN+'</span>';
							}
							else {
								jenisPelayanan = '<span class="text-warning">'+d.NM_JENIS_PELAYANAN+'</span>';
							}

							html += '<td>'+jenisPelayanan+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Kelas Rawat</th>';
							html += '<td>:</td>';
							html += '<td>'+d.KELAS_LABEL+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">No. Rujukan</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.NO_RUJUKAN+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Tgl. Rujukan</th>';
							html += '<td>:</td>';
							html += '<td>'+moment(d.TIMESTAMP_RUJUKAN).format('DD/MM/YYYY hh:mm')+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Kode Provider</th>';
							html += '<td>:</td>';
							html += '<td>'+d.KD_PPK_RUJUKAN+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Nama Provider</th>';
							html += '<td>:</td>';
							html += '<td>'+d.NM_PPK_RUJUKAN+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">Kode ICD-10</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.KD_DIAGNOSA+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th valign="top">Nama Diagnosa</th>';
							html += '<td>:</td>';
							html += '<td>'+d.NM_DIAGNOSA+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Lakalantas</th>';
							html += '<td>:</td>';
							html += '<td>'+d.LAKALANTAS_LABEL+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th>Keterangan</th>';
							html += '<td>:</td>';
							html += '<td>'+d.LOKASI_LAKALANTAS+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">Tgl. Pulang</th>';
							html += '<td width="10">:</td>';

							if ( d.KD_JENIS_PELAYANAN == "2" ) {
								tglPulang = '<span class="text-success">'+moment(d.TGLREG+' '+d.KELUARPOLY).format('DD/MM/YYYY hh:mm:ss')+'</span>';
							}
							else {
								if ( d.RANAP_KELUARRS == null || d.RANAP_KELUARRS == "0000-00-00 00:00:00" ) {
									tglPulang = '<i class="text-danger">Belum Pulang</i>';
								}
								else {
									tglPulang = '<span class="text-success">'+moment(d.RANAP_KELUARRS).format('DD/MM/YYYY hh:mm:ss')+'</span>';
								}
							}

							html += '<td>'+tglPulang+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
			html += '</div>';

			return html;
		};

		var table = jQuery('#datatable-pelayanan').DataTable({
			displayLength: 10,
			ordering : true,
			order : [[1, 'desc']],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=datatablesep" ?>',
			columns : [
				{
					className : 'details-control',
					orderable : false,
					data : '',
					defaultContent : '<i class="fa fa-fw fa-plus-circle"></i> <i class="fa fa-fw fa-minus-circle"></i>'
				},
				{
					className : 'text-center',
					data : "TGLREG",
					mRender: function(data, type, row) {
						return moment(row.JAMREGRAW).format('DD/MM/YYYY');
					}
				},
				{
					className : 'text-center',
					data : "NO_KARTU"
				},
				{
					className : 'text-center',
					data : "NOMR"
				},
				{ data : "NAMA_PASIEN" },
				{
					className : 'text-center',
					data : "JENISKELAMIN"
				},
				{
					className : 'text-center',
					data : "NAMA_POLY"
				},
				{ data : "NAMADOKTER" },
				{
					data : "SHIFT",
					className : "text-center",
					mRender : function(data, type, row) {
						return row.SHIFT_LABEL
					}
				},
				{
					data : "NO_SEP",
					className : "text-center",
					mRender : function(data, type, row) {
						if ( row.NO_SEP == '' || row.NO_SEP == null ) {
							return '<span class="text-danger"><i>Belum dibuat</i></span>';
						}
						else {
							return row.NO_SEP;
						}
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button class="sep-history btn btn-xs btn-default btn-block">Lihat Riwayat</button>';
					}
				},
				{
					data : "TGLREG",
					visible : false
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button class="update-tgl-pulang btn btn-xs btn-default btn-block">Update Tgl.Pulang</button>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						if ( row.NO_SEP == '' || row.NO_SEP == null ) {
							return '<a href="<?= _BASE_ . "index.php?link=sep&c=create_sep&idxdaftar='+row.IDXDAFTAR+'" ?>" class="create-sep btn btn-xs btn-block btn-default btn-primary">Buat SEP</a>';
						}
						else {
							return '<button type="button" class="edit-sep btn btn-xs btn-block btn-default btn-warning">Edit SEP</button>';
						}
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
							return '<button class="create-sep btn btn-xs btn-block btn-default btn-default print-sep" data-loading-text="<i class=\'fa fa-fw fa-circle-o-notch fa-spin\'></i>"><i class="fa fa-fw fa-print"></i></button>';
					}
				}
			],
		});

		// search
		jQuery('#form-search-transaksi').on('submit', function() {
			jQuery('#modal-search-transaksi').modal('hide');

			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#clear').on('click', function() {
			jQuery('#form-search-transaksi').resetForm();
			jQuery('#modal-search-transaksi').modal('hide');

			jQuery('#form-search-transaksi .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});

		// Add event listener for opening and closing details
		jQuery('#datatable-pelayanan tbody').on('click', 'td.details-control', function () {
			var tr = jQuery(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		});

		// edit tgl pulang
		jQuery('#datatable-pelayanan tbody').on('click', '.update-tgl-pulang', function() {
			var btn = jQuery(this);
			var modal = jQuery('#modal-update-tgl-pulang');
			var form = jQuery('#form-update-tgl-pulang');
			
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=getSep&idxdaftar='+data.IDXDAFTAR+'" ?>',
				dataType : 'json',
				method : 'get',
				data : {
					nosep : data.NO_SEP
				},
				beforeSend : function() {
					btn.button('loading');
				},
				success : function(r) {
					if ( r.metaData.code !== 200 ) {
						alertify.error(r.metaData.message);
						console.log(r);
					}
					else {
						if ( r.response.bpjs.metadata.code != 200 ) {
							alertify.error(r.response.bpjs.metadata.message);
							console.log(r);
						}
						else {
							// tampilkan warning jika pasien merupakan pasien rawat inap
							// dan dalam status masih dirawat
							if ( r.response.simrs.KD_JENIS_PELAYANAN == "1" && r.response.simrs.TGL_PULANG == null ) {
								modal.find('.messages').html(function() {
									let html = '';

									html += '<div style="padding:0 10px">';
										html += '<div class="alert alert-danger">';
											html += '<i>';
												html += 'Perhatian! Pasien ini masih dalam status dirawat. Mengupdate tgl.pulang SEP akan mengakibatkan data simrs dan BPJS tidak sinkron.'
											html += '</i>';
										html += '</div>';
									html += '</div>';

									return html;
								});
							}

							form.find('[name="idxdaftar"]').val(r.response.simrs.IDXDAFTAR);
							form.find('[name="noSep"]').val(r.response.simrs.NO_SEP);
							form.find('[name="ppkPelayanan"]').val(r.response.simrs.KD_PROV_PELAYANAN);
							form.find('[name="tglReg"]').val(r.response.simrs.JAMREGRAW);
							form.find('[name="tglPulang"]').val(r.response.simrs.TGL_PULANG);

							modal.find('.simrs-result').html(function() { return decodeSimrsSepResult(r.response.simrs) });
							modal.find('.bpjs-result').html(function() { return decodeBpjsSepResult(r.response.bpjs.response) });

							modal.modal('show');
						}
					}
				},
				error : function(e) {
					alertify.error('Error. Tidak dapat menghubungi server.');
					console.log(e);
				},
				complete : function() {
					btn.button('reset');
				}
			});
		});

		// menampilkan riwayat pelayanan (SEP) dari server BPJS
		jQuery('#datatable-pelayanan tbody').on('click', '.sep-history', function() {
			var btn = jQuery(this);
			var modal = jQuery('#modal-riwayat-pelayanan-bpjs');
			
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=getRiwayatPelayananBPJS" ?>',
				dataType : 'json',
				method : 'get',
				data : {
					nokartu : data.NO_KARTU,
					idxdaftar : data.IDXDAFTAR
				},
				beforeSend : function() {
					btn.button('loading');
				},
				success : function(r) {
					if ( r.metadata.code != "200" ) {
						swal('Error', r.metadata.message, 'error');
						console.log(r);
					}
					else {
						if ( r.response.result_bpjs.metadata.code != "200" ) {
							swal('Warning', r.response.result_bpjs.metadata.message, 'warning');
							console.log(r);
						}
						else {
							modal.find('[name="idxdaftar"]').val(r.response.result_simrs.IDXDAFTAR);

							jQuery('#modal-riwayat-pelayanan-bpjs .riwayat-pelayanan').html(function() { return decodeRiwayatPelayanan(r.response.result_bpjs.response.list) });
							modal.modal('show');
						}
					}
				},
				error : function(e) {
					alertify.error('Error. Tidak dapat menghubungi server.');
					console.log(e);
				},
				complete : function() {
					btn.button('reset');
				}
			});
		});

		// mebuat tautan sep
		// proses sinkronisasi data simrs dan bpjs
		jQuery('#modal-riwayat-pelayanan-bpjs .riwayat-pelayanan').on('click', '.tautkan-sep', function() {
			console.log('asdsa');
		});

		// on modal udate tgl pulang hidden
		jQuery('#modal-update-tgl-pulang').on('hidden.bs.modal', function() {
			var modal = jQuery('#modal-update-tgl-pulang');
			var form = jQuery('#form-update-tgl-pulang');

			form.trigger('reset');
			modal.find('.simrs-result').html('');
			modal.find('.bpjs-result').html('');
			modal.find('.messages').html('');
		});

		// submit update tanggal pulang
		jQuery('#form-update-tgl-pulang').ajaxForm({
			url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=updatetglpulang" ?>',
			method : 'PUT',
			dataType : 'json',
			beforeSubmit : function(arr, $form, options) {
				jQuery('#form-update-tgl-pulang').find('[type="submit"]').button('loading');
			},
			success : function(r) {
				if ( r.metaData.code != 200 ) {
					swal('Error', r.metaData.message, 'error');
					console.log(r);
				}
				else{
					if ( r.response.metadata.code != 200 ) {
						swal('Server BPJS Response', r.response.metadata.message, 'error');
						console.log(r);
					}
					else {
						table.ajax.reload(null, false);
						jQuery('#modal-update-tgl-pulang').modal('hide');
						swal('Berhasil', 'SEP berhasil diupdate. ' + r.response.response, 'success');
					}
				}
			},
			error : function(e) {
				swal('Error', 'Tidak dapat menghubungi server', 'error');
				console.log(e);
			},
			complete : function() {
				jQuery('#form-update-tgl-pulang').find('[type="submit"]').button('reset');
			}
		});

		// edit sep
		jQuery('#datatable-pelayanan tbody').on('click', '.edit-sep', function() {
			var btn = jQuery(this);
			var modal = jQuery('#modal-edit-sep');
			
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=getSep&idxdaftar='+data.IDXDAFTAR+'" ?>',
				dataType : 'json',
				method : 'get',
				data : {
					nosep : data.NO_SEP
				},
				beforeSend : function() {
					btn.button('loading');
				},
				success : function(r) {
					if ( r.metaData.code !== 200 ) {
						alertify.error(r.metaData.message);
						console.log(r);
					}
					else {
						if ( r.response.bpjs.metadata.code != 200 ) {
							alertify.error(r.response.bpjs.metadata.message);
							console.log(r);
						}
						else {
							jQuery('#form-edit-sep').find('[name="idxdaftar"]').val(r.response.simrs.IDXDAFTAR);
							jQuery('#form-edit-sep').find('[name="noSep"]').val(r.response.simrs.NO_SEP);
							jQuery('#form-edit-sep').find('[name="noKartu"]').val(r.response.simrs.NO_KARTU);
							jQuery('#form-edit-sep').find('[name="tglSep"]').val(r.response.simrs.TGLREG);
							jQuery('#form-edit-sep').find('[name="tglRujukan"]').val(r.response.simrs.TGL_RUJUKAN);
							jQuery('#form-edit-sep').find('[name="noRujukan"]').val(r.response.simrs.NO_RUJUKAN);
							jQuery('#form-edit-sep').find('[name="ppkRujukan"]').val(r.response.simrs.KD_PPK_RUJUKAN);
							jQuery('#form-edit-sep').find('[name="jnsPelayanan"]').val(r.response.simrs.KD_JENIS_PELAYANAN);
							jQuery('#form-edit-sep').find('[name="catatan"]').val(r.response.simrs.CATATAN);
							jQuery('#form-edit-sep').find('[name="diagAwal"]').val(r.response.simrs.KD_DIAGNOSA);
							jQuery('#form-edit-sep').find('[name="poliTujuan"]').val(r.response.simrs.KDPOLY);
							jQuery('#form-edit-sep').find('[name="klsRawat"]').val(r.response.simrs.KD_KELAS);
							jQuery('#form-edit-sep').find('[name="lakaLantas"]').val(r.response.simrs.LAKALANTAS_BPJS);
							jQuery('#form-edit-sep').find('[name="lokasiLaka"]').val(r.response.simrs.LOKASI_LAKALANTAS);
							jQuery('#form-edit-sep').find('[name="user"]').val(r.response.simrs.NIP);
							jQuery('#form-edit-sep').find('[name="noMr"]').val(r.response.simrs.NOMR);

							jQuery('#modal-edit-sep .simrs-result').html(function() { return decodeSimrsSepResult(r.response.simrs) });
							jQuery('#modal-edit-sep .bpjs-result').html(function() { return decodeBpjsSepResult(r.response.bpjs.response) });

							modal.modal('show');
						}
					}
				},
				error : function(e) {
					alertify.error('Error. Tidak dapat menghubungi server.');
					console.log(e);
				},
				complete : function() {
					btn.button('reset');
				}
			});
		});

		// update sep
		jQuery('#form-edit-sep').ajaxForm({
			url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=updatesep" ?>',
			method : 'PUT',
			dataType : 'json',
			beforeSubmit : function(arr, $form, options) {
				jQuery('#form-edit-sep').find('[type="submit"]').button('loading');
			},
			success : function(r) {
				if ( r.metaData.code != 200 ) {
					swal('Error', r.metaData.message, 'error');
					console.log(r);
				}
				else{
					if ( r.response.metadata.code != 200 ) {
						swal('Server BPJS Response', r.response.metadata.message, 'error');
						console.log(r);
					}
					else {
						table.ajax.reload(null, false);
						jQuery('#modal-edit-sep').modal('hide');
						swal('Berhasil', 'SEP berhasil diupdate. ' + r.response.response, 'success');
					}
				}
			},
			error : function(e) {
				swal('Error', 'Tidak dapat menghubungi server', 'error');
				console.log(e);
			},
			complete : function() {
				jQuery('#form-edit-sep').find('[type="submit"]').button('reset');
			}
		});

		// delete sep
		jQuery('#modal-edit-sep').on('click', '.delete-sep', function() {
			swal({
				title : 'Hapus SEP',
				text : 'Apakah anda yakin ingin menghapus SEP ?',
				type : 'warning',
				showCancelButton : true,
				confirmButtonText : "Ya",
				cancelButtonText : "Tidak",
				closeOnConfirm : false,
				showLoaderOnConfirm : true,
			}).then(function() {
				var modal = jQuery('#modal-edit-sep');
				var idxdaftar = modal.find('[name="idxdaftar"]').val();
				var nosep = modal.find('[name="noSep"]').val();

				jQuery.ajax({
					url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=deletesep" ?>',
					method : 'DELETE',
					dataType : 'json',
					data : {
						idxdaftar : idxdaftar,
						nosep : nosep
					},
					success : function(r) {
						if ( r.metaData.code != 200 ) {
							swal('Error', r.metaData.message, 'error');
							console.log(r);
						}
						else {
							if ( r.response.metadata.code != 200 ) {
								swal('Error', r.response.metadata.message, 'error');
								console.log(r);
							}
							else {
								swal('Berhasil', 'Berhasil menghapus SEP', 'success');
								table.ajax.reload(null, false);
								modal.modal('hide');
							}
						}
					},
					error : function(e) {
						swal('Error', 'Tidak menghubungi server', 'error');
						console.log(e);
					}
				});
			});
		});

		// print sep
		jQuery('#datatable-pelayanan tbody').on('click', '.print-sep', function() {
			var btn = jQuery(this);
			var row = jQuery(this).closest('tr');
			var d = table.row(row).data();

			var idxdaftar = d.IDXDAFTAR;
			var nosep = d.NO_SEP;
			
			jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=getsep" ?>',
				method : 'GET',
				dataType : 'json',
				data : {
					idxdaftar : idxdaftar,
					nosep : nosep
				},
				beforeSend : function() {
					btn.button('loading');
				},
				success : function(r) {
					if ( r.metaData.code != "200" ) {
						swal('Error', r.metaData.message, 'error');
						console.log(r);
					}
					else {
						if ( r.response.bpjs.metadata.code != "200" ) {
							swal('Error', r.response.bpjs.metadata.message, 'error');
							console.log(r);
						}
						else {
							jQuery.ajax({
								url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=incJumlahCetak" ?>',
								method : 'GET',
								dataType : 'json',
								data : {
									idxdaftar : idxdaftar
								}
							})
							.done(function(r) {
								window.open('<?= _BASE_ . "index2.php?link=sep&c=print_sep&a=printSep&idxdaftar=' + idxdaftar + '" ?>');
							})
							.fail(function(e) {
								swal('Error', 'Gagal mengupdate cetakan terakhir', 'error');
								console.log(e);
							});
						}
					}
				},
				error : function(e) {
					swal('Error', 'Tidak dapat menghubungi server', 'error');
					console.log(e);
				},
				complete : function() {
					btn.button('reset');
				}
			});
		});
	});
</script>