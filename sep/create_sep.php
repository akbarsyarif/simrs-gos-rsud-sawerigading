<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>BUAT SEP</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<form id="form-create-sep" method="post" role="form" class="transparent-field">
			<input type="hidden" name="user_login" />
			<div class="row sm-margin">
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<fieldset class="bs-style" id="search-transaksi">
								<legend><i class="fa fa-fw fa-heartbeat"></i> Data Transaksi</legend>
								<div class="form-group">
									<div class="input-group">
										<input type="text" id="idxdaftar" value="<?= (isset($_GET['idxdaftar']) && !empty($_GET['idxdaftar'])) ? xss_clean($_GET['idxdaftar']) : NULL ?>" class="form-control input-lg text-center" style="font-size:16px;height:47px" placeholder="Masukkan nomor transaksi..." />
										<input type="hidden" name="idxdaftar" value="" />
										<div class="input-group-btn">
											<button type="button" id="btn-search" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i>" class="btn btn-info btn-lg"><i class="fa fa-fw fa-search"></i></button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<h4>Cara mencetak SEP</h4>
									<ul>
										<li>Halaman cetak SEP hanya untuk pasien dengan metode pembayaran BPJS/JKN</li>
										<li>Masukkan nomor transaksi pada pada kotak input di atas, lalu klik tombol search (cari) di sebelah kanan, atau cukup tekan ENTER</li>
										<li>Jika informasi pasien/peserta ditemukan, klik tombol BUAT SEP</li>
									</ul>
								</div>
								<div id="action-btn-container"></div>
							</fieldset>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="text-center">
						<h2><i class="fa fa-fw fa-user"></i> Pasien</h2>
					</div>
					<div class="row no-margin">
						<div class="col-md-6">
							<table class="table table-horizontal table-striped">
								<tbody>
									<tr>
										<th>No. SEP</th>
										<td width="10">:</td>
										<td colspan="4">
											<input type="hidden" name="no_sep" value="" />
											<div class="no_sep"></div>
										</td>
									</tr>
									<tr>
										<th>No. Rekam Medik</th>
										<td width="10">:</td>
										<td colspan="4"><input type="text" name="no_mr" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>Jenis Pelayanan</th>
										<td width="10">:</td>
										<td colspan="4">
											<input type="hidden" name="kd_jenis_pelayanan" value="" />
											<input type="text" name="nm_jenis_pelayanan" value="" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<th>No. KTP</th>
										<td width="10">:</td>
										<td colspan="4"><input type="text" name="no_ktp" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>Nama Pasien</th>
										<td>:</td>
										<td colspan="4"><input type="text" name="nm_pasien" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>Tempat Lahir</th>
										<td>:</td>
										<td colspan="4"><input type="text" name="tmp_lahir" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>Tgl. Lahir</th>
										<td>:</td>
										<td colspan="4"><input type="text" name="tgl_lahir" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>L/P</th>
										<td width="10">:</td>
										<td class="no-gap"><input type="text" size="1" style="width:10px" name="jk" value="" readonly="readonly" /></td>
										<th style="width:10px !important">Agama</th>
										<td width="10">:</td>
										<td><input type="text" size="11" name="agama" value="" readonly="readonly" /></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-6">
							<table class="table table-horizontal table-striped">
								<tbody>
									<tr>
										<th>No. Kartu BPJS</th>
										<td width="10">:</td>
										<td colspan="4"><input type="text" name="no_kartu" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>Status Peserta</th>
										<td width="10">:</td>
										<td colspan="4">
											<input type="hidden" name="kd_status_peserta" value="" />
											<div class="nm_status_peserta"></div>
										</td>
									</tr>
									<tr>
										<th>Kelas Rawat</th>
										<td>:</td>
										<td colspan="4">
											<input type="hidden" name="kls_rawat" value="" />
											<input type="text" name="kls_rawat_label" value="" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<th>Alamat</th>
										<td>:</td>
										<td colspan="4"><input type="text" size="30" name="alamat" value="" readonly="readonly" /></td>
									</tr>
									<tr>
										<th>Kelurahan</th>
										<td>:</td>
										<td colspan="4">
											<input type="text" name="kelurahan" value="" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<th>Kecamatan</th>
										<td>:</td>
										<td colspan="4">
											<input type="text" name="kecamatan" value="" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<th>Kota/Kabupaten</th>
										<td>:</td>
										<td colspan="4">
											<input type="text" name="kabupaten" value="" readonly="readonly" />
										</td>
									</tr>
									<tr>
										<th>Catatan</th>
										<td>:</td>
										<td colspan="4">
											<input type="hidden" name="catatan" value="" />
											<div class="catatan"></div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row sm-margin">
				<div class="col-md-4">
					<div class="text-center">
						<h2><i class="fa fa-fw fa-user-md"></i> Kunjungan</h2>
					</div>
					<table class="table table-horizontal table-striped">
						<tbody>
							<tr>
								<th>Tgl. Registrasi</th>
								<td width="10">:</td>
								<td colspan="4"><input type="text" name="tgl_registrasi" value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<th>Jenis Pelayanan</th>
								<td>:</td>
								<td colspan="4">
									<input type="hidden" name="jns_pelayanan" value="" />
									<input type="text" name="jns_pelayanan_label" value="" readonly="readonly" />
								</td>
							</tr>
							<tr>
								<th>Poly Tujuan</th>
								<td>:</td>
								<td colspan="4">
									<input type="hidden" name="kd_poli_bpjs" value="" />
									<input type="text" name="nm_poli" value="" readonly="readonly" />
								</td>
							</tr>
							<tr>
								<th>Dokter</th>
								<td>:</td>
								<td colspan="4"><input type="text" name="nm_dokter" value="" readonly="readonly" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<div class="text-center">
						<h2><i class="fa fa-fw fa-ambulance"></i> Rujukan</h2>
					</div>
					<table class="table table-horizontal table-striped">
						<tbody>
							<tr>
								<th>No. Rujukan</th>
								<td width="10">:</td>
								<td colspan="4"><input type="text" name="no_rujukan" value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<th>Tgl. Rujukan</th>
								<td>:</td>
								<td colspan="4"><input type="text" name="tgl_rujukan" value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<th>Kode Provider</th>
								<td>:</td>
								<td colspan="4"><input type="text" name="ppk_rujukan" value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<th>Nama Provider</th>
								<td>:</td>
								<td colspan="4"><input type="text" name="nm_provider_rujukan" value="" readonly="readonly" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<div class="text-center">
						<h2><i class="fa fa-fw fa-stethoscope"></i> Diagnosa Awal</h2>
					</div>
					<table class="table table-horizontal table-striped">
						<tbody>
							<tr>
								<th>Kode</th>
								<td width="10">:</td>
								<td colspan="4"><input type="text" name="kd_diagnosa" value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<th>Nama Diagnosa</th>
								<td>:</td>
								<td colspan="4"><input type="text" name="nm_diagnosa" value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<th>Lakalantas</th>
								<td>:</td>
								<td colspan="4">
									<input type="hidden" name="lakalantas" value="" />
									<input type="text" name="lakalantas_label" value="" readonly="readonly" />
								</td>
							</tr>
							<tr>
								<th>Keterangan</th>
								<td>:</td>
								<td colspan="4">
									<input type="text" name="lokasi_lakalantas" size="30" value="" readonly="readonly" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		var form = jQuery('#form-create-sep');
		var search = jQuery('#search-transaksi');

		// search data transaksi
		function getTransaksi()
		{
			var idxdaftar = search.find('#idxdaftar').val();

			return jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=getTransaksi" ?>',
				dataType : 'json',
				method : 'GET',
				data : {
					idxdaftar : idxdaftar
				},
				beforeSend : function() {
					search.find('#idxdaftar').attr('disabled', '');
					search.find('#btn-search').button('loading');
				},
				success : function(r) {
					if ( r.metaData.code != 200 ) {
						swal('Error', r.metaData.message, 'error');
						console.log(r);
					}
					else {
						if ( r.response.bpjs.metadata.code != 200 ) {
							swal('Error', r.response.bpjs.metadata.message, 'error');
							console.log(r);
						}
						else {
							var form = jQuery('#form-create-sep');
							var d = r.response.simrs;
							var d_bpjs = r.response.bpjs;

							// no sep
							var no_sep = d.NO_SEP;
							if ( no_sep == '' || no_sep == null ) {
								no_sep_label = '<span class="text-danger"><i>Belum Dibuat</i></span>';
							
								// toggle button
								// mengubah tombol buat sep menjadi cetak sep
								// jika sep sudah dibuat
								togglePrintCreateSepBtn();
							}
							else {
								var no_sep_label = '<span class="text-success">'+no_sep+'</span>';
								
								// toggle button
								// mengubah tombol buat sep menjadi cetak sep
								// jika sep sudah dibuat
								toggleCreatePrintSepBtn();
							}

							// status peserta
							// aktif/tidak aktif
							var kd_status_peserta = d_bpjs.response.peserta.statusPeserta.kode;
							var nm_status_peserta = '<span class="text-success">'+d_bpjs.response.peserta.statusPeserta.keterangan+'</span>';
							if ( kd_status_peserta != 0 ) {
								nm_status_peserta = '<span class="text-danger"><i>'+nm_status_peserta+'</i></span>';
							}

							form.find('[name="user_login"]').val(d.NIP);
							form.find('[name="idxdaftar"]').val(d.IDXDAFTAR);
							form.find('div.no_sep').html(no_sep_label);
							form.find('[name="no_sep"]').val(no_sep);
							form.find('[name="no_mr"]').val(d.NOMR);
							form.find('[name="kd_jenis_pelayanan"]').val(d.KD_JENIS_PELAYANAN);
							form.find('[name="nm_jenis_pelayanan"]').val(d.NM_JENIS_PELAYANAN);
							form.find('[name="no_ktp"]').val(d.NOKTP);
							form.find('[name="nm_pasien"]').val(d.NAMA_PASIEN);
							form.find('[name="tmp_lahir"]').val(d.TMP_LAHIR_PASIEN);
							form.find('[name="tgl_lahir"]').val(d.TGL_LAHIR_PASIEN);
							form.find('[name="jk"]').val(d.JENISKELAMIN);
							form.find('[name="agama"]').val(d.AGAMA_LABEL);
							form.find('[name="no_kartu"]').val(d.NO_KARTU);
							form.find('div.nm_status_peserta').html(nm_status_peserta);
							form.find('[name="kd_status_peserta"]').val(kd_status_peserta);
							form.find('[name="kls_rawat"]').val(d.KD_KELAS);
							form.find('[name="kls_rawat_label"]').val(d.KELAS_LABEL);
							form.find('[name="alamat"]').val(d.ALAMAT);
							form.find('[name="kelurahan"]').val(d.namakelurahan);
							form.find('[name="kecamatan"]').val(d.namakecamatan);
							form.find('[name="kabupaten"]').val(d.namakota);
							form.find('[name="catatan"]').html(d.CATATAN);
							form.find('div.catatan').html(d.CATATAN);
							form.find('[name="tgl_registrasi"]').val(d.TGLREG);
							form.find('[name="jns_pelayanan"]').val('2');
							form.find('[name="jns_pelayanan_label"]').val('Jalan');
							form.find('[name="kd_poli_bpjs"]').val(d.KD_POLY_BPJS);
							form.find('[name="nm_poli"]').val(d.NAMA_POLY);
							form.find('[name="nm_dokter"]').val(d.NAMADOKTER);
							form.find('[name="no_rujukan"]').val(d.NO_RUJUKAN);
							form.find('[name="tgl_rujukan"]').val(d.TGL_RUJUKAN);
							form.find('[name="ppk_rujukan"]').val(d.KD_PPK_RUJUKAN);
							form.find('[name="nm_provider_rujukan"]').val(d.NM_PPK_RUJUKAN);
							form.find('[name="kd_diagnosa"]').val(d.KD_DIAGNOSA);
							form.find('[name="nm_diagnosa"]').val(d.NM_DIAGNOSA);
							form.find('[name="lakalantas"]').val(d.LAKALANTAS_BPJS);
							form.find('[name="lakalantas_label"]').val(d.LAKALANTAS_LABEL);
							form.find('[name="lokasi_lakalantas"]').val(d.LOKASI_LAKALANTAS);
						}
					}
				},
				error : function(e) {
					swal('Error', 'Tidak dapat menghubungi server', 'error');
					console.log(e);
				},
				complete : function() {
					search.find('#idxdaftar').removeAttr('disabled');
					search.find('#btn-search').button('reset');
				}
			});
		}

		function toggleCreatePrintSepBtn(noSep)
		{
			var btnContainer = form.find('#action-btn-container');
			var idxdaftar = form.find('#idxdaftar').val();

			btnContainer.html('<button class="btn btn-lg btn-block btn-default" id="print-sep" type="button" data-loading-text="<i class=\'fa fa-fw fa-circle-o-notch fa-spin\'></i> Tunggu...">Cetak SEP</button>')
		}

		function togglePrintCreateSepBtn()
		{
			var btnContainer = form.find('#action-btn-container');

			btnContainer.html('<button class="btn btn-lg btn-block btn-primary" id="create-sep" type="submit" data-loading-text="<i class=\'fa fa-fw fa-circle-o-notch fa-spin\'></i> Mengirim Data...">Buat SEP</button>');
		}

		search.find('#idxdaftar').on('keypress', function(e) {
			if ( e.which == 13 ) {
				getTransaksi();
				return false;
			}
		});

		search.find('#btn-search').on('click', function(e) {
			getTransaksi();
		});

		// auto submit if idxdaftar not empty
		if ( search.find('#idxdaftar').val() != '' ) {
			getTransaksi();
		}

		// create sep
		form.ajaxForm({
			url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=createsep" ?>',
			dataType : 'json',
			method : 'POST',
			beforeSubmit : function(arr, $form, options) {
				form.find('#create-sep').button('loading');
			},
			success : function(r) {
				if ( r.metadata.code != "200" ) {
					swal('Error', r.metadata.message, 'error');
					console.log(r);
				}
				else {
					if ( r.response.metadata.code != "200" ) {
						swal('Error', r.response.metadata.message, 'error');
						console.log(r);
					}
					else {
						var no_sep = r.response.response;

						swal('Success', 'No. SEP : ' + r.response.response, 'success');
						form.find('[name="no_sep"]').val(no_sep);
						form.find('div.no_sep').html(function() {
							return "<span class='text-success'>" + no_sep + "</span>";
						});

						// mengubah tombol buat sep menjadi cetak sep
						// jika sep berhasil dibuat
						toggleCreatePrintSepBtn();
					}
				}
			},
			error : function(e) {
				swal('Error', 'Tidak dapat menghubungi server', 'error');
				console.log(e);
			},
			complete : function() {
				form.find('#create-sep').button('reset');
			}
		});

		// print sep
		jQuery('#action-btn-container').on('click', '#print-sep', function() {
			var btnPrint = jQuery(this);

			var idxdaftar = form.find('#idxdaftar').val();
			var nosep = form.find('[name="no_sep"]').val();
			
			jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=getsep" ?>',
				method : 'GET',
				dataType : 'json',
				data : {
					idxdaftar : idxdaftar,
					nosep : nosep
				},
				beforeSend : function() {
					btnPrint.button('loading');
				},
				success : function(r) {
					if ( r.metaData.code != "200" ) {
						swal('Error', r.metaData.message, 'error');
						console.log(r);
					}
					else {
						if ( r.response.bpjs.metadata.code != "200" ) {
							swal('Error', r.response.bpjs.metadata.message, 'error');
							console.log(r);
						}
						else {
							jQuery.ajax({
								url : '<?= _BASE_ . "index2.php?link=sep&c=sep&a=incJumlahCetak" ?>',
								method : 'GET',
								dataType : 'json',
								data : {
									idxdaftar : idxdaftar
								}
							})
							.done(function(r) {
								window.open('<?= _BASE_ . "index2.php?link=sep&c=print_sep&a=printSep&idxdaftar=' + idxdaftar + '" ?>');
							})
							.fail(function(e) {
								swal('Error', 'Gagal mengupdate cetakan terakhir', 'error');
								console.log(e);
							});
						}
					}
				},
				error : function(e) {
					swal('Error', 'Tidak dapat menghubungi server', 'error');
					console.log(e);
				},
				complete : function() {
					btnPrint.button('reset');
				}
			});
		});
	});
</script>