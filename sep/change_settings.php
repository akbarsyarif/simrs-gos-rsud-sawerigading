<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>PENGATURAN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form id="form-settings" method="post" role="form">
					<div class="form-group">
						<label>Jenis/Merk Printer</label>
						<select name="printer_type" class="form-control">
							<option value="epson_lq-2190" default>Epson LQ-2190</option>
							<option value="epson_lx-310" default>Epson LX-310</option>
						</select>
						<p>Pilih jenis/merk printer yang anda gunakan untuk mencetak SEP</p>
					</div>
					<button type="submit" class="btn btn-sm btn-primary" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> Sedang memproses">Simpan Pengaturan</button>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		var form = jQuery('#form-settings');

		function getSettings()
		{
			return jQuery.ajax({
				url : '<?= _BASE_ . "index2.php?link=sep&c=settings&a=getSettings" ?>',
				dataType : 'json',
				method : 'GET',
				success : function(r) {
					if ( r.metadata.code != "200" ) {
						swal('Error', r.metadata.message, 'error');
						console.log(r);
					}
					else {
						var d = r.response;

						form.find('[name="printer_type"]').val(d.printer_type)
					}
				},
				error : function(e) {
					swal('Error', 'Tidak dapat menghubungi server', 'error');
					console.log(e);
				}
			})
		}

		getSettings();

		// --------------------------------------------------------------------------

		// submit form settings
		var btnSubmit = form.find('[type="submit"]');
		form.ajaxForm({
			url : '<?= _BASE_ . "index2.php?link=sep&c=settings&a=saveSettings" ?>',
			dataType : 'json',
			method : 'POST',
			beforeSubmit : function(arr, $form, options) {
				btnSubmit.button('loading');
			},
			success : function(r) {
				if ( r.metadata.code != "200" ) {
					swal('Error', r.metadata.message, 'error');
					console.log(r);
				}
				else {
					swal('Success', 'Pengaturan berhasil disimpan', 'success');
				}
			},
			error : function(e) {
				swal('Error', 'Tidak dapat menghubungi server', 'error');
				console.log(e);
			},
			complete : function() {
				btnSubmit.button('reset');
			}
		});

		// --------------------------------------------------------------------------
	})
</script>