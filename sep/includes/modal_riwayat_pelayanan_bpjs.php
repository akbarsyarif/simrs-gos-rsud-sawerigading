<div class="modal fade" id="modal-riwayat-pelayanan-bpjs">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<input type="hidden" name="idxdaftar" />
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Riwayat Pelayanan Pasien</h4>
			</div>
			<div class="modal-body riwayat-pelayanan"></div>
		</div>
	</div>
</div>