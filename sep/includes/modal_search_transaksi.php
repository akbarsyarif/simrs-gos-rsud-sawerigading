<div class="modal fade" id="modal-search-transaksi">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST" id="form-search-transaksi" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-search-plus"></i> Pencarian Lanjutan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<label>No. SEP</label>
								<input type="text" name="NoSep" class="form-control" data-column="9" />
							</div>
							<div class="col-md-6">
								<label>No. Rekam Medik</label>
								<input type="text" name="NoMr" class="form-control" data-column="3" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<label>Nama Pasien</label>
								<input type="text" name="Nama" class="form-control" data-column="4" />
							</div>
							<div class="col-md-6">
								<label>Poli Tujuan</label>
								<?= dinamycDropdown( array (
									'name' => 'PoliTujuan',
									'table' => 'm_poly',
									'key' => 'nama',
									'value' => 'nama',
									'selected' => '',
									'order_by' => 'nama asc',
									'empty_first' => TRUE,
									'first_value' => '-- Semua --',
									'attr' => 'class="form-control" data-column="6"'
								) ) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<label>Mulai Tanggal</label>
								<input type="text" name="StartDate" value="<?= date('Y-m-d') ?>" class="form-control datepicker" data-column="1" />
							</div>
							<div class="col-md-6">
								<label>S/D Tanggal</label>
								<input type="text" name="EndDate" value="<?= date('Y-m-d') ?>" class="form-control datepicker" data-column="10" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="clear" class="btn btn-warning btn-sm" data-dismiss="modal">Bersihkan</button>
					<button type="submit" class="btn btn-primary btn-sm">Cari</button>
				</div>
			</form>
		</div>
	</div>
</div>