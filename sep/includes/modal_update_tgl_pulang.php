<div class="modal fade" id="modal-update-tgl-pulang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="form-update-tgl-pulang" method="post">
				<input type="hidden" name="idxdaftar" />
				<input type="hidden" name="noSep" />
				<input type="hidden" name="ppkPelayanan" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-pencil"></i> Update Tgl.Pulang</h4>
				</div>
				<div class="modal-body" style="padding:0">
					<div class="row no-margin" style="border-bottom: solid 1px #DDDDDD">
						<div class="col-md-6">
							<div class="simrs-result"></div>
						</div>
						<div class="col-md-6" style="border-left:solid 1px #DDDDDD">
							<div class="bpjs-result"></div>
						</div>
					</div>
					<div class="row no-margin">
						<div class="col-md-6" style="padding:10px">
							<label>Tgl.Masuk</label>
							<input type="text" name="tglReg" class="form-control" readonly="true" />
						</div>
						<div class="col-md-6" style="padding:10px">
							<label>Tgl.Keluar</label>
							<input type="text" name="tglPulang" class="form-control datetimepicker" />
						</div>
					</div>
					<div class="row no-margin">
						<div class="col-md-12">
							<div class="messages"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-right">
						<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-check-circle"></i> Update Tgl.Pulang</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>