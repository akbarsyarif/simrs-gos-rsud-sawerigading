<div class="modal fade" id="modal-edit-sep">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="form-edit-sep" method="post">
				<input type="hidden" name="idxdaftar" />
				<input type="hidden" name="noSep" />
				<input type="hidden" name="noKartu" />
				<input type="hidden" name="tglSep" />
				<input type="hidden" name="tglRujukan" />
				<input type="hidden" name="noRujukan" />
				<input type="hidden" name="ppkRujukan" />
				<input type="hidden" name="jnsPelayanan" />
				<input type="hidden" name="catatan" />
				<input type="hidden" name="diagAwal" />
				<input type="hidden" name="poliTujuan" />
				<input type="hidden" name="klsRawat" />
				<input type="hidden" name="lakaLantas" />
				<input type="hidden" name="lokasiLaka" />
				<input type="hidden" name="user" />
				<input type="hidden" name="noMr" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-pencil"></i> Edit SEP</h4>
				</div>
				<div class="modal-body" style="padding:0">
					<div class="row no-margin">
						<div class="col-md-6">
							<div class="simrs-result"></div>
						</div>
						<div class="col-md-6" style="border-left:solid 1px #DDDDDD">
							<div class="bpjs-result"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="pull-left">
						<button type="button" class="delete-sep btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i> Hapus SEP</button>
					</div>
					<div class="pull-right">
						<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-check-circle"></i> Update SEP</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>