<!DOCTYPE html>
<html>
<head>
	<title>SEP - <?= isset($data_sep->noSep) ? $data_sep->noSep : '' ?></title>
	<style type="text/css">
		@media screen {
			.frame {
				margin: 0 auto;
				padding: 20px;
				width: 793.92px;
				height: 351.36px;
				border: solid 1px #000000;
			}
		}
		@media print {
			.hide-print {
				display: none;
			}
		}
		@media all {
			body {
				font-family: 'Calibri';
				font-size: 10pt;
			}
			table {
				border-collapse: collapse;
				width: 100% !important;
			}
			td {
				font-weight: normal;
				text-align: left;
				vertical-align: top;
				white-space: nowrap;
				padding: 1px 0;
				margin: 0;
			}
			h3 {
				font-size: 12pt;
				margin: 0 !important;
			}
			th.wrap-text,
			td.wrap-text {
				white-space: normal;
			}
			small {
				font-size: 8pt;
			}
			hr {
				border: none;
				border-bottom: solid 1px #000000;
			}
		}
	</style>
</head>
<body>
	<div class="frame">
		<table style="margin-bottom: 10px">
			<tr>
				<td width="20%">
					<img src="assets/img/logo-bpjs.png" width="150">
				</td>
				<td width="60%">
					<h3 style="text-align: center">
						SURAT ELEGIBILITAS PESERTA <br />
						SAWERIGADING PALOPO
					</h3>
				</td>
				<td width="20%"></td>
			</tr>
		</table>
		<table>
			<tr>
				<td>
					<table>
						<tr><td width="100">No. SEP</td><td width="10">:</td><td colspan="4"><?= isset($data_sep->noSep) ? $data_sep->noSep : '' ?></td></tr>
						<tr><td>Tgl. SEP</td><td>:</td><td colspan="4"><?= isset($tgl_sep) ? $tgl_sep : '' ?></td></tr>
						<tr>
							<td>No. Kartu</td><td>:</td><td width="130"><?= isset($data_sep->peserta->noKartu) ? $data_sep->peserta->noKartu : '' ?></td>
							<td width="50">No. MR</td><td width="10">:</td><td width="130"><?= isset($data_sep->peserta->noMr) ? $data_sep->peserta->noMr : '' ?></td>
						</tr>
						<tr><td>Nama Peserta</td><td>:</td><td colspan="4"><?= isset($data_sep->peserta->nama) ? $data_sep->peserta->nama : '' ?></td></tr>
						<tr><td>Tgl. Lahir</td><td>:</td><td colspan="4"><?= isset($tgl_lahir) ? $tgl_lahir : '' ?></td></tr>
						<tr><td>Jns. Kelamin</td><td>:</td><td colspan="4"><?= isset($data_sep->peserta->sex) ? $data_sep->peserta->sex : '' ?></td></tr>
						<tr><td>Poli Tujuan</td><td>:</td><td colspan="4"><?= isset($data_sep->poliTujuan->nmPoli) ? $data_sep->poliTujuan->nmPoli : '' ?></td></tr>
						<tr><td>Asal Faskes Tk.I</td><td>:</td><td colspan="4"><?= isset($data_sep->provRujukan->nmProvider) ? $data_sep->provRujukan->nmProvider : '' ?></td></tr>
						<tr><td>Diagnosa Awal</td><td>:</td><td colspan="4" width="200" style="font-size:9pt" class="wrap-text"><?= isset($data_sep->diagAwal->nmDiag) ? $data_sep->diagAwal->nmDiag : '' ?></td></tr>
						<tr><td>Catatan</td><td>:</td><td colspan="4" width="200" style="font-size:9pt" class="wrap-text"><?= isset($data_sep->catatan) ? $data_sep->catatan : '' ?></td></tr>
						<tr>
							<td colspan="8">
								<table>
									<tr><td><div style="margin-bottom: 5px"></div></td></tr>
									<tr><td><small><i>* Saya menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan</i></small></td></tr>
									<tr><td><small><i>* SEP bukan sebagai bukti penjaminan peserta</i></small></td></tr>
									<tr><td><div style="margin-bottom: 5px"></div></td></tr>
									<tr><td><small>Cetakan ke <?= isset($jumlah_cetak) ? $jumlah_cetak : '' ?> - <?= isset($tgl_cetak) ? $tgl_cetak : '' ?></small></td></tr>
								</table>		
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table border=1 style="border-color: transparent">
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr><td width="75">Peserta</td><td width="10">:</td><td><?= isset($data_sep->peserta->jenisPeserta->nmJenisPeserta) ? $data_sep->peserta->jenisPeserta->nmJenisPeserta : '' ?></td></tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr><td>COB</td><td width="10">:</td><td colspan="2"><?= isset($data_sep->statusCOB->namaCOB) ? $data_sep->statusCOB->namaCOB : '' ?></td></tr>
						<tr><td>Jns. Rawat</td><td width="10">:</td><td><?= isset($data_sep->jnsPelayanan) ? $data_sep->jnsPelayanan : '' ?></td></tr>
						<tr><td>Kls. Rawat</td><td width="10">:</td><td><?= isset($data_sep->klsRawat->nmKelas) ? $data_sep->klsRawat->nmKelas : '' ?></td></tr>
						<tr><td colspan="3">&nbsp;</td></tr>
						<tr>
							<td colspan="3">
								<table>
									<tr>
										<td rowspan="2" width="<?= isset($data_sep->noSep) ? '20' : '20' ?>"></td>
										<td height="80">
											Pasien / <br />
											Keluarga Pasien
										</td>
										<td rowspan="2" width="<?= isset($data_sep->noSep) ? '20' : '20' ?>"></td>
										<td>
											Petugas <br />
											BPJS Kesehatan
										</td>
									</tr>
									<tr>
										<td valign="bottom"><hr /></td>
										<td valign="bottom"><hr /></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<p align="center" class="hide-print">
		<button class="print" onclick="window.print()">Cetak SEP</button>
	</p>
</body>
</html>