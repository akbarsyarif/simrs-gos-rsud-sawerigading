<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'thirdparty/mpdf/mpdf.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('download') )
{

	function download()
	{

		$error_msg = array();

		$idxdaftar = (isset($_GET['idxdaftar'])) ? xss_clean($_GET['idxdaftar']) : NULL;

		$transaksi = Models\row_pendaftaran(array(
			'query' => " WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
		));

		// idxdaftar kosong
		if ( empty($idxdaftar) ) $error_msg[] = "IDXDAFTAR tidak boleh kosong.";
		// data transaksi tidak ditemukan
		if ( $transaksi === FALSE ) $error_msg[] = "Data transaksi dengan nomor {$idxdaftar} tidak ditemukan.";

		if ( count($error_msg) > 0 ) {

			show_error($error_msg);

		}
		else {

			$mpdf = new mPDF('', 'A4', 9, 'calibri', 10, 9, 10, 10);

			ob_start();
			require "html/formverifikasi1.php";
			$content = ob_get_clean();
			$mpdf->WriteHTML($content);

			// $mpdf->AddPage();

			// ob_start();
			// require "html/formverifikasi2.php";
			// $content = ob_get_clean();
			// $mpdf->WriteHTML($content);

			$mpdf->Output();

		}

	}

}

//---------------------------------------------------------------------------------