<!DOCTYPE html>
<html>
<head>
	<title>Resume Medis</title>
	<style type="text/css">
		h4 {
			margin: 0;
			text-align: center;
			font-size: 11pt;
		}
		table {
			margin: 0;
			padding: 0;
			border-collapse: collapse;
			width: 100% !important;
		}
		td,th {
			padding: 5px 10px 5px 0;
			margin: 0;
			white-space: nowrap;
			vertical-align: top;
		}
		th.wrap-text,
		td.wrap-text {
			white-space: normal;
		}
		th {
			text-align: center;
			background-color: #DEDEDE;
		}
		td td {
			padding: 0 10px 0 0;
		}
		table.bordered th,
		table.bordered td {
			border: solid 1px #000;
			padding: 2px 4px;
		}
		.box {
			padding: 2px 5px;
			border: solid 1px #000;
			width: 100%;
		}
		.divider {
			margin: 10px;
		}
	</style>
</head>
<body>
	<h4>LEMBAR INFORMASI DAN KLARIFIKASI</h4>
	<h4>VERIFIKASI FASKES TINGKAT LANJUTAN</h4>
	<div class="divider"></div>
	<table width="100%" class="bordered">
		<tr>
			<td width="130">Nama Pasien</td>
			<td>: <?= $transaksi['NAMA_PASIEN'] ?></td>
			<td width="130">Tanggal Pelayanan</td>
			<td>: <?= date_format(date_create($transaksi['TGLREG']), 'd/m/Y') ?> </td>
		</tr>
		<tr>
			<td width="130">No. Rekam Medis</td>
			<td>: <?= $transaksi['NOMR'] ?></td>
			<td width="130">Tanggal Klarifikasi</td>
			<td>:</td>
		</tr>
		<tr>
			<td width="130">Nama RS</td>
			<td>: RSUD Sawerigading Palopo</td>
			<td width="130">Jenis Pelayanan</td>
			<td width="200">:</td>
		</tr>
		<tr>
			<td colspan="3" height="800">Uraian dan Hasil Konfirmasi</td>
			<td>Tanda tangan dan Nama Terang</td>
		</tr>
	</table>
</body>
</html>