<style type="text/css">
	td {
		font-size: 13px;
		font-weight: normal;
		text-align: left;
		vertical-align: top;
		white-space: normal;
		padding: 0;
		margin: 0;
	}
	h3 {
		margin: 0 !important;
	}
</style>

<table>
	<tr>
		<th style="padding-right: 10px">
			<img src="<?= BASEPATH . 'assets/img/logo-rsud.gif' ?>" height="50" />
		</th>
		<td width="200">
			<h3><?= $NAMA_PASIEN ?></h3>
			<div>M.R. : <?= $NOMR ?></div>
			<div><?= date_format(date_create($TGL_LAHIR_PASIEN), "d/m/Y") ?> (<?= $UMUR_PASIEN ?>)</div>
		</td>
		<th>
			<barcode code="<?= $NOMR ?>" type="I25" />
		</th>
	</tr>
</table>