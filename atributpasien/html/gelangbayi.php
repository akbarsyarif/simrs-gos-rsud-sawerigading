<style type="text/css">
	td {
		font-size: 10px;
		text-align: left;
		vertical-align: top;
		white-space: normal;
		padding: 0;
	}
</style>

<table>
	<tr>
		<th rowspan="3">
			<img src="<?= BASEPATH . 'assets/img/logo-rsud.gif' ?>" height="30" />
		</th>
		<td width="100">
			<h3><?= $NAMA_PASIEN ?></h3>
			<div>M.R. : <?= $NOMR ?></div>
			<div><?= date_format(date_create($TGL_LAHIR_PASIEN), "d/m/Y") ?> (<?= $UMUR_PASIEN ?>)</div>
		</td>
	</tr>
</table>