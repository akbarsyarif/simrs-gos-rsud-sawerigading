<!DOCTYPE html>
<html>
<head>
	<title>Resume Medis</title>
	<style type="text/css">
		h4 {
			margin: 0;
			text-align: center;
			font-size: 11pt;
		}
		table {
			margin: 0;
			padding: 0;
			border-collapse: collapse;
			width: 100% !important;
		}
		td,th {
			padding: 3px 5px 3px 0;
			margin: 0;
			white-space: nowrap;
			vertical-align: top;
		}
		th.wrap-text,
		td.wrap-text {
			white-space: normal;
		}
		th {
			text-align: center;
			background-color: #DEDEDE;
		}
		td td {
			padding: 0 5px 0 0;
		}
		table.bordered th,
		table.bordered td {
			border: solid 1px #000;
			padding: 2px 4px;
		}
		tr.md-gap th,
		tr.md-gap td {
			margin-right: 10px;
		}
		.box {
			padding: 2px 5px;
			border: solid 1px #000;
			width: 100%;
		}
		.divider {
			margin: 10px;
		}
	</style>
</head>
<body>
	<h4>FORMULIR VERIFIKASI</h4>
	<h4>SISTEM CASE-MIX / INA-CBG</h4>
	<div class="divider"></div>
	<table width="100%">
		<tr>
			<td width="20">1.</td>
			<td width="130">Nama Rumah Sakit</td>
			<td width="10">:</td>
			<td colspan="2"><strong>RSU Sawerigading Palopo</strong></td>
		</tr>
		<tr>
			<td>2.</td>
			<td>No. Kode RS</td>
			<td width="10">:</td>
			<td width="130"><strong>7373016</strong></td>
			<td>
				<table width="100%">
					<tr>
						<td width="20">3.</td>
						<td width="50">Kelas RS</td>
						<td width="10">:</td>
						<td><strong>B</strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>4.</td>
			<td>Nomor Rekam Medis</td>
			<td width="10">:</td>
			<td colspan="2"><?= $transaksi['NOMR'] ?></td>
		</tr>
		<tr>
			<td>5.</td>
			<td>Nama Pasien</td>
			<td width="10">:</td>
			<td colspan="2"><?= $transaksi['NAMA_PASIEN'] ?></td>
		</tr>
		<tr>
			<td>6.</td>
			<td>Jenis Perawatan</td>
			<td width="10">:</td>
			<td></td>
			<td>
				<table width="100%">
					<tr class="md-gap">
						<td>a. Rawat Inap</td>
						<td>b. Rawat Jalan</td>
						<td>c. IGD</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>7.</td>
			<td>Kelas Perawatan</td>
			<td width="10">:</td>
			<td></td>
			<td>
				<table width="100%">
					<tr>
						<td>a. Kelas I</td>
						<td>b. Kelas II</td>
						<td>c. Kelas III</td>
						<td>d. Kelas VIP</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>8.</td>
			<td>Total Biaya</td>
			<td width="10">:</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>9.</td>
			<td>Tanggal Masuk</td>
			<td width="10">:</td>
			<td><?= date_format(date_create($transaksi['TGLREG']), 'd/m/Y') ?></td>
			<td>
				<table width="100%">
					<tr>
						<td width="20">10.</td>
						<td>Tanggal Keluar</td>
						<td width="10">:</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>11.</td>
			<td>Jumlah Hari Perawatan</td>
			<td width="10">:</td>
			<td></td>
			<td>Tgl. Keluar - Tgl. Masuk + 1</td>
		</tr>
		<tr>
			<td>12.</td>
			<td>Tanggal Lahir</td>
			<td width="10">:</td>
			<td><?= date_format(date_create($transaksi['TGL_LAHIR_PASIEN']), 'd/m/Y') ?></td>
			<td>
				<table width="100%">
					<tr>
						<td width="20">13.</td>
						<td>Usia Dalam Tahun</td>
						<td width="10">:</td>
						<td width="30"></td>
						<td width="20">14.</td>
						<td>Usia Dalam Hari</td>
						<td width="10">:</td>
						<td width="30"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>15.</td>
			<td>Jenis Kelamin</td>
			<td width="10">:</td>
			<td colspan="2"><?= $transaksi['JENISKELAMIN'] ?></td>
		</tr>
		<tr>
			<td>16.</td>
			<td>Cara Pulang</td>
			<td width="10">:</td>
			<td></td>
			<td>
				<table width="100%">
					<tr>
						<td>a. Sembuh</td>
						<td>b. Dirujuk</td>
						<td>c. Pulang Paksa</td>
						<td>d. Meninggal Dunia</td>
						<td>e. Melarikan Diri</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>17.</td>
			<td>Berat Lahir</td>
			<td width="10">:</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>18.</td>
			<td>Diagnosa Utama</td>
			<td width="10">:</td>
			<td colspan="2">
				<table>
					<tr>
						<td width="320" style="border: solid 1px #000"></td>
						<td width="40"></td>
						<td width="190" style="border: solid 1px #000; padding: 2px 5px;"> ICD-10 : </td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>19.</td>
			<td>Diagnosa Sekunder</td>
			<td width="10">:</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="5">
				<table class="bordered" width="100%">
					<tr>
						<th width="20">NO.</th>
						<th width="494">DIAGNOSA</th>
						<th width="190">ICD-10</th>
					</tr>
					<?php for ( $i=1; $i<=10; $i++ ) : ?>
					<tr>
						<td><?= $i ?>.</td>
						<td></td>
						<td></td>
					</tr>
					<?php endfor; ?>
				</table>
			</td>
		</tr>
		<tr>
			<td>20.</td>
			<td width="100">Prosedur / Tindakan</td>
			<td width="10">:</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="20">
				<table class="bordered" width="100%">
					<tr>
						<th width="20">NO.</th>
						<th width="374">TINDAKAN / PROSEDUR</th>
						<th width="120">ICD-9-CM</th>
						<th width="190">TGL. TINDAKAN</th>
					</tr>
					<?php for ( $i=1; $i<=10; $i++ ) : ?>
					<tr>
						<td><?= $i ?>.</td>
						<td colspan="2"></td>
						<td></td>
					</tr>
					<?php endfor; ?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="5" class="wrap-text">
				<table>
					<tr>
						<td width="50">Catatan :</td>
						<td width="654" class="wrap-text">
							Diagnosa diisi oleh dokter yang merawat (yang memeriksa dengan Huruf Cetak)<br />
							Untuk pasien yang dilakukan tindakan, disertakan tanggal dilakukannya tindakan tersebut.
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<br /><br />
				<table>
					<tr>
						<td width="352" align="center"></td>
						<td width="352" align="center">
							<strong>Palopo, Tgl </strong>.....................................
						</td>
					</tr>
					<tr>
						<td align="center">
							DPJP / Dokter Yang Merawat
						</td>
						<td align="center">
							Pasien / Keluarga Pasien
						</td>
					</tr>
					<tr>
						<td align="center"><br /><br /><br /><br />
							..............................................................
						</td>
						<td align="center"><br /><br /><br /><br />
							..............................................................
						</td>
					</tr>
					<tr>
						<td align="center">
							NIP.
							<span style="color:#FFF">.......................................................</span>
						</td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>