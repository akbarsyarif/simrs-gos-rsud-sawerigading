<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'thirdparty/mpdf/mpdf.php';
// require BASEPATH . 'thirdparty/QrCode/src/QrCode.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('download') )
{

	function download()
	{

		$error_msg = array();

		$idxdaftar = (isset($_GET['idxdaftar'])) ? xss_clean($_GET['idxdaftar']) : NULL;
		$type = (isset($_GET['type'])) ? xss_clean($_GET['type']) : NULL;

		$transaksi = Models\row_pendaftaran(array(
			'query' => " WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
		));

		// idxdaftar kosong
		if ( empty($idxdaftar) ) $error_msg[] = "IDXDAFTAR tidak boleh kosong.";
		// data transaksi tidak ditemukan
		if ( $transaksi === FALSE ) $error_msg[] = "Data transaksi dengan nomor {$idxdaftar} tidak ditemukan.";

		if ( count($error_msg) > 0 ) {

			show_error($error_msg);

		}
		else {

			// extract data pasien
			extract($transaksi);

			ob_start();

			// load template gelang
			if ( $type == "dewasa" ) {
				$mpdf = new mPDF('utf-8', array(24.892,288.036), 0, 'arial', 8,5,4,3,0,0, 'L');
				require BASEPATH . 'atributpasien/html/gelangdewasa.php';
			}
			else {
				$mpdf = new mPDF('utf-8', array(24.892,288.036), 0, 'arial', 8,5,5,3,0,0, 'L');
				require BASEPATH . 'atributpasien/html/gelangbayi.php';
			}

			$content = ob_get_clean();
			$mpdf->WriteHTML($content);

			$mpdf->Output();

		}

	}

}

//---------------------------------------------------------------------------------