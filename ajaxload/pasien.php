<?php
namespace Controller;

require 'include/security_helper.php';
require 'models/m_pasien.php';

use Models;

function data_table() {
	$column_index = array('m1.NOMR', 'm1.NAMA', 'm1.TGLLAHIR', 'm1.NOKTP', 'm1.NO_KARTU', null);

	$config['query'] = NULL;
	
	// filter column handler / search
	$columns = $_GET['columns'];

	if ( ! empty($_GET['search']['value']) ) {

		$search_value = xss_clean($_GET['search']['value']);

		$config['query'] .= "WHERE ".$column_index[1]." LIKE '%".$search_value."%'";

	}
	else {

		$i = 0;

		foreach ( $columns as $column_key => $column_val ) {

			if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

				$config['query'] .= ($i === 0) ? " WHERE " : " AND ";

				if ( $column_key == -1 ) {
					$config['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
				}
				else {
					$config['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
				}

				$i++;

			}

		}

	}

	$numrows = Models\count_pasien($config);
	$records_total = $numrows;
	$records_filtered = $numrows;

	// orders
	$order_column = $_GET['order'][0]['column'];
	$order_dir    = $_GET['order'][0]['dir'];

	$config['limit']  = xss_clean($_GET['length']);
	$config['offset'] = xss_clean($_GET['start']);
	$config['order']  = $column_index[$order_column]." ".$order_dir;

	$data = Models\get_pasien($config);

	$response = (object) array (
		'draw' => $_GET['draw'],
		'recordsTotal' => $records_total,
		'recordsFiltered' => $records_filtered,
		'data' => $data
	);

	echo json_encode($response);
}

function get_pasien() {
	$nomr = (isset($_GET['nomr']) && !empty($_GET['nomr'])) ? xss_clean($_GET['nomr']) : NULL;
	
	$config['query'] = NULL;
	if ( !is_null($nomr) ) $config['query'] .= " WHERE NOMR = '".$nomr."'";

	$data = Models\get_pasien($config);
	if ( is_array($data) ) {
		$response = array(
			'code' => 200,
			'message' => NULL,
			'data' => $data
			);
	}
	else {
		$response = array(
			'code' => 404,
			'message' => 'Pasien tidak ditemukan',
			'data' => NULL
			);
	}

	echo json_encode($response);
}