<?php
namespace Controller;

require 'include/security_helper.php';
require 'models/m_poly.php';

use Models;

/**
 * mencari poli berdsarkan kode poli bpjs
 * 
 * @return [int]
 */
function mappingPoly() {
	$kd_bpjs = (isset($_GET['kd_bpjs']) && !empty($_GET['kd_bpjs'])) ? xss_clean($_GET['kd_bpjs']) : NULL;
	
	$config['query'] = NULL;
	if ( !is_null($kd_bpjs) ) $config['query'] .= " WHERE kd_bpjs = '".$kd_bpjs."'";

	$config['limit'] = 50;
	$config['order'] = 'nama ASC';

	$result = Models\Poly\rowPoly($config);

	if ( $result === FALSE ) {
		$response = (object) array (
			'metadata' => (object) array (
				'code' => '201',
				'message' => 'Poli tidak ditemukan'
			),
			'response' => NULL
		);
	}
	else {
		$response = (object) array (
			'metadata' => (object) array (
				'code' => '200',
				'message' => 'OK'
			),
			'response' => $result['kode']
		);
	}

	echo json_encode($response);
}