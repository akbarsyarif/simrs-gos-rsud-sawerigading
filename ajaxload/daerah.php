<?php
namespace Controller;

require 'include/security_helper.php';
require 'models/m_daerah.php';

use Models;

/**
 * [get_kota description]
 * @return [type] [description]
 */
function get_kota() {
	$id_prov = (isset($_GET['id_prov']) && !empty($_GET['id_prov'])) ? xss_clean($_GET['id_prov']) : NULL;
	
	$config['query'] = NULL;
	if ( !is_null($id_prov) ) $config['query'] .= " WHERE idprovinsi = '".$id_prov."'";

	$config['limit'] = 50;
	$config['order'] = 'namakota ASC';

	$data = Models\get_kota($config);
	if ( is_array($data) ) {
		$response = array(
			'code' => 200,
			'message' => NULL,
			'data' => $data
			);
	}
	else {
		$response = array(
			'code' => 404,
			'message' => 'Kota tidak ditemukan',
			'data' => NULL
			);
	}

	echo json_encode($response);
}

/**
 * menampilkan daftar kecamatan berdasarkan kota
 * @return [type] [description]
 */
function get_kecamatan() {
	$id_kota = (isset($_GET['id_kota']) && !empty($_GET['id_kota'])) ? xss_clean($_GET['id_kota']) : NULL;
	
	$config['query'] = NULL;
	if ( !is_null($id_kota) ) $config['query'] .= " WHERE idkota = '".$id_kota."'";

	$config['limit'] = 50;
	$config['order'] = 'namakecamatan ASC';

	$data = Models\get_kecamatan($config);
	if ( is_array($data) ) {
		$response = array(
			'code' => 200,
			'message' => NULL,
			'data' => $data
			);
	}
	else {
		$response = array(
			'code' => 404,
			'message' => 'Kecamatan tidak ditemukan',
			'data' => NULL
			);
	}

	echo json_encode($response);
}

/**
 * menampilkan daftar kelurahan berdasarkan kecamatan
 * @return [type] [description]
 */
function get_kelurahan() {
	$id_kec = (isset($_GET['id_kec']) && !empty($_GET['id_kec'])) ? xss_clean($_GET['id_kec']) : NULL;
	
	$config['query'] = NULL;
	if ( !is_null($id_kec) ) $config['query'] .= " WHERE idkecamatan = '".$id_kec."'";

	$config['limit'] = 50;
	$config['order'] = 'namakelurahan ASC';

	$data = Models\get_kelurahan($config);
	if ( is_array($data) ) {
		$response = array(
			'code' => 200,
			'message' => NULL,
			'data' => $data
			);
	}
	else {
		$response = array(
			'code' => 404,
			'message' => 'Kelurahan tidak ditemukan',
			'data' => NULL
			);
	}

	echo json_encode($response);
}