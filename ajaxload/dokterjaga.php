<?php
namespace Controller;

require 'include/security_helper.php';
require 'models/m_dokterjaga.php';

use Models;

function getDokterJaga() {
	$kd_poly = (isset($_GET['kd_poly']) && !empty($_GET['kd_poly'])) ? xss_clean($_GET['kd_poly']) : NULL;
	
	$config['query'] = NULL;
	if ( !is_null($kd_poly) ) $config['query'] .= " WHERE kdpoly = '".$kd_poly."'";

	$config['order'] = 'kdpoly ASC, namadokter ASC';

	$result = Models\DokterJaga\get($config);

	if ( $result === FALSE ) {
		$response = (object) array (
			'metadata' => (object) array (
				'code' => '201',
				'message' => 'Tidak ada dokter ditemukan'
			),
			'response' => NULL
		);
	}
	else {
		$response = (object) array (
			'metadata' => (object) array (
				'code' => '200',
				'message' => 'OK'
			),
			'response' => $result
		);
	}

	echo json_encode($response);
}