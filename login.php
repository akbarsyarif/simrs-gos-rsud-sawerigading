<?php
require 'include/connect.php';
require BASEPATH . 'include/security_helper.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= ucwords($rstitle)?></title>

	<!-- favicon -->
	<link rel="shortcut icon" href="favicon.ico" />
	
	<!-- Stylesheet -->
	<link rel="stylesheet" type="text/css" href="js/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="js/bootstrap/css/lumen.min.css" />
	<link rel="stylesheet" href="css/fontawesome/css/font-awesome.min.css" type="text/css" media="screen" />
	<link href="dq_sirs.css" type="text/css" rel="stylesheet" />
	<link href="login.css" type="text/css" rel="stylesheet" />

	<!-- jquery -->
	<script src="js/jquery.min.js" language="JavaScript" type="text/javascript"></script>
	<script src="js/jquery.validate.js" language="JavaScript" type="text/javascript"></script>
	<script src="assets/jquery_form/jquery.form.min.js" language="JavaScript" type="text/javascript"></script>

	<!-- tb -->
	<script type="text/javascript" src="js/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="frame login-page">
		<div class="frame_body">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<div class="jumbotron">
							<div class="container">
								<h1>Sistem Informasi Manajemen Rumah Sakit</h1>
								<p>
									Jika butuh bantuan silahkan hubungi Administrator SIMRS di <a href="#">911</a> (local line rsud sawerigading). Untuk saran dan kritik yang berkaitan dengan pengembangan sistem silahkan tinggalkan pesan pada kotak saran.
								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<form name="login_form" action="user_level.php" method="post">
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="username" class="form-control input-lg" placeholder="Masukkan username" />
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control input-lg" placeholder="Masukkan kata sandi" />
							</div>

							<?php if ( isset($_GET['msg']) && !empty($_GET['msg']) ) : ?>
								<div class="alert alert-danger">
									<?= xss_clean($_GET['msg']) ?>
								</div>
							<?php else : ?>
								<p class="help-block" align="center">
									Silahkan gunakan username dan kata sandi anda untuk login ke dalam sistem.
								</p>
							<?php endif; ?>
							
							<button class="btn btn-warning btn-block btn-lg">Login</button>
							<p align="center">
								<a href="#modal-password-reset" data-toggle="modal"><i class="fa fa-fw fa-key"></i> Lupa kata sandi?</a>
								<br />
								<a href="#modal-register" data-toggle="modal"><i class="fa fa-fw fa-user"></i> Belum punya akun?</a>
							</p>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="text-center">
							<?=strtoupper($rstitle)?> OPEN SOURCE <?=strtoupper($singhead1)?> &copy; <?php echo date("Y"); ?> <br />
							Modified and Developed by UNIT SIMRS RSUD Sawerigading Palopo
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- includes -->
	<?php require BASEPATH . 'modals/modal_register.php' ?>
	<?php require BASEPATH . 'modals/modal_password_reset.php' ?>
</body>
</html>