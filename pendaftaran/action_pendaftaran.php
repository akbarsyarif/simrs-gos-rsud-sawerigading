<?php
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'include/pasien_helper.php';
require BASEPATH . 'models/m_auth.php';
require BASEPATH . 'models/m_pasien.php';
require BASEPATH . 'models/m_carabayar.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'models/m_pendaftaran_iso.php';
require BASEPATH . 'models/m_tmp_cartbayar.php';
require BASEPATH . 'models/m_billrajal.php';
require BASEPATH . 'models/m_bayarrajal.php';
require BASEPATH . 'tracer/action_tracer.php';
require BASEPATH . 'thirdparty/GUMP-master/gump.class.php';

//---------------------------------------------------------------------------------

/**
 * save data transaksi (pendaftaran) pasien
 */
if ( ! function_exists('save') )
{
	function save()
	{
		// form validation
		$gump = new GUMP();
		$validation_rules = array ( 
			'shift' => 'required',
			'jenis_kunjungan' => 'required',
			'kd_rujuk' => 'required',
			'kdpoly' => 'required',
			'kddokter' => 'required',
			'minta_rujukan' => 'required',
			'tglreg' => 'required',
			'lakalantas' => 'required',
			'kd_diagnosa' => 'required',
			'nm_diagnosa' => 'required',
			'kdcarabayar' => 'required',
			'nama' => 'required',
			'title' => 'required',
			'tmp_lahir' => 'required',
			'tgl_lahir' => 'required',
			'nik' => 'required',
			'notelp' => 'required',
			'pekerjaan_pasien' => 'required',
			'alamat' => 'required',
			'alamat_ktp' => 'required',
			'id_provinsi' => 'required',
			'id_kota' => 'required',
			'id_kecamatan' => 'required',
			'id_kelurahan' => 'required',
			'nama_ayah' => 'required',
			'pekerjaan_ayah' => 'required',
			'nama_ibu' => 'required',
			'pekerjaan_ibu' => 'required',
			'nama_pasangan' => 'required',
			'pekerjaan_pasangan' => 'required',
			'jenis_kelamin' => 'required',
			'status' => 'required',
			'agama' => 'required',
			'penanggungjawab_nama' => 'required',
			'penanggungjawab_hubungan' => 'required',
			'penanggungjawab_alamat' => 'required',
			'penanggungjawab_phone' => 'required'
		);

		// conditional validation
		// untuk nomor rekam medik manual
		if ( ($_POST['jenis_kunjungan'] == TRUE && isset($_POST['nomr_manual'])) OR ($_POST['jenis_kunjungan'] == FALSE) ) {
			$validation_rules['nomr'] = 'required';
		}

		// conditional validation
		// jika metode pembayaran menggunakan JKN
		if ( $_POST['kdcarabayar'] == 2 ) {
			if ( $_POST['metode_pengisian'] == 'rujukan' || $_POST['metode_pengisian'] == 'manual' ) {
				$validation_rules['metode_pengisian'] = 'required';
				$validation_rules['no_kartu'] = 'required';
				$validation_rules['kd_jenis_peserta'] = 'required';
				$validation_rules['nm_jenis_peserta'] = 'required';
				$validation_rules['kd_kelas'] = 'required';
				$validation_rules['no_rujukan'] = 'required';
				$validation_rules['tgl_rujukan'] = 'required';
				$validation_rules['kd_ppk_rujukan'] = 'required';
				$validation_rules['nm_ppk_rujukan'] = 'required';
			} else if ( $_POST['metode_pengisian'] == 'manual_offline' ) {
				$validation_rules['metode_pengisian'] = 'required';
				$validation_rules['no_kartu'] = 'required';
				$validation_rules['kd_kelas'] = 'required';
				$validation_rules['no_rujukan'] = 'required';
				$validation_rules['tgl_rujukan'] = 'required';
			}
		}

		$gump->validation_rules($validation_rules);

		if ( $gump->run($_POST) === FALSE ) {
			$response = (object) array (
				'metadata' => (object) array (
					'code' => 400,
					'message' => $gump->get_readable_errors(true)
				)
			);
		} else {
			$error_msg = [];
			$pasien_exist = FALSE;
			$user_login = Models\Auth\getUserLogin();
			$ip_address = getRealIpAddr();

			$start_daftar = (isset($_POST['start_daftar']) && ! empty($_POST['start_daftar'])) ? xss_clean($_POST['start_daftar']) : NULL;
			$stop_daftar  = (isset($_POST['stop_daftar']) && ! empty($_POST['stop_daftar'])) ? xss_clean($_POST['stop_daftar']) : NULL;

			$shift                    = (isset($_POST['shift']) && ! empty($_POST['shift'])) ? xss_clean($_POST['shift']) : NULL;
			$jenis_kunjungan          = (isset($_POST['jenis_kunjungan']) && ! empty($_POST['jenis_kunjungan'])) ? xss_clean($_POST['jenis_kunjungan']) : NULL;
			$nomr_manual              = (isset($_POST['nomr_manual']) && ! empty($_POST['nomr_manual'])) ? xss_clean($_POST['nomr_manual']) : FALSE;
			$nomr                     = (isset($_POST['nomr']) && ! empty($_POST['nomr'])) ? xss_clean($_POST['nomr']) : NULL;
			$kd_rujuk                 = (isset($_POST['kd_rujuk']) && ! empty($_POST['kd_rujuk'])) ? xss_clean($_POST['kd_rujuk']) : NULL;
			$kdpoly                   = (isset($_POST['kdpoly']) && ! empty($_POST['kdpoly'])) ? xss_clean($_POST['kdpoly']) : NULL;
			$kddokter                 = (isset($_POST['kddokter']) && ! empty($_POST['kddokter'])) ? xss_clean($_POST['kddokter']) : NULL;
			$minta_rujukan            = (isset($_POST['minta_rujukan']) && ! empty($_POST['minta_rujukan'])) ? xss_clean($_POST['minta_rujukan']) : NULL;
			$tglreg                   = (isset($_POST['tglreg']) && ! empty($_POST['tglreg'])) ? xss_clean($_POST['tglreg']) : NULL;
			$lakalantas               = (isset($_POST['lakalantas']) && ! empty($_POST['lakalantas'])) ? xss_clean($_POST['lakalantas']) : NULL;
			$lokasi_lakalantas        = (isset($_POST['lokasi_lakalantas']) && ! empty($_POST['lokasi_lakalantas'])) ? xss_clean($_POST['lokasi_lakalantas']) : NULL;
			$kd_diagnosa              = (isset($_POST['kd_diagnosa']) && ! empty($_POST['kd_diagnosa'])) ? xss_clean($_POST['kd_diagnosa']) : NULL;
			$nm_diagnosa              = (isset($_POST['nm_diagnosa']) && ! empty($_POST['nm_diagnosa'])) ? xss_clean($_POST['nm_diagnosa']) : NULL;
			$kdcarabayar              = (isset($_POST['kdcarabayar']) && ! empty($_POST['kdcarabayar'])) ? xss_clean($_POST['kdcarabayar']) : NULL;
			$metode_pengisian         = (isset($_POST['metode_pengisian']) && ! empty($_POST['metode_pengisian'])) ? xss_clean($_POST['metode_pengisian']) : NULL;
			$no_kartu                 = (isset($_POST['no_kartu']) && ! empty($_POST['no_kartu'])) ? xss_clean($_POST['no_kartu']) : NULL;
			$kd_jenis_peserta         = (isset($_POST['kd_jenis_peserta']) && ! empty($_POST['kd_jenis_peserta'])) ? xss_clean($_POST['kd_jenis_peserta']) : NULL;
			$nm_jenis_peserta         = (isset($_POST['nm_jenis_peserta']) && ! empty($_POST['nm_jenis_peserta'])) ? xss_clean($_POST['nm_jenis_peserta']) : NULL;
			$kd_kelas                 = (isset($_POST['kd_kelas']) && ! empty($_POST['kd_kelas'])) ? xss_clean($_POST['kd_kelas']) : NULL;
			
			// nama kelas JKN/BPJS
			switch ($kd_kelas) {
				case '1':
					$nm_kelas = 'KELAS I';
					break;
				case '2':
					$nm_kelas = 'KELAS II';
					break;
				case '3':
					$nm_kelas = 'KELAS III';
					break;
				default:
					$nm_kelas = NULL;
					break;
			}
			
			$kd_provider              = (isset($_POST['kd_provider']) && ! empty($_POST['kd_provider'])) ? xss_clean($_POST['kd_provider']) : NULL;
			$nm_provider              = (isset($_POST['nm_provider']) && ! empty($_POST['nm_provider'])) ? xss_clean($_POST['nm_provider']) : NULL;
			$no_rujukan               = (isset($_POST['no_rujukan']) && ! empty($_POST['no_rujukan'])) ? xss_clean($_POST['no_rujukan']) : NULL;
			$tgl_rujukan              = (isset($_POST['tgl_rujukan']) && ! empty($_POST['tgl_rujukan'])) ? xss_clean($_POST['tgl_rujukan']) : NULL;
			$kd_ppk_rujukan           = (isset($_POST['kd_ppk_rujukan']) && ! empty($_POST['kd_ppk_rujukan'])) ? xss_clean($_POST['kd_ppk_rujukan']) : NULL;
			$nm_ppk_rujukan           = (isset($_POST['nm_ppk_rujukan']) && ! empty($_POST['nm_ppk_rujukan'])) ? xss_clean($_POST['nm_ppk_rujukan']) : NULL;
			$catatan                  = (isset($_POST['catatan']) && ! empty($_POST['catatan'])) ? xss_clean($_POST['catatan']) : NULL;
			$nama                     = (isset($_POST['nama']) && ! empty($_POST['nama'])) ? xss_clean($_POST['nama']) : NULL;
			$title                    = (isset($_POST['title']) && ! empty($_POST['title'])) ? xss_clean($_POST['title']) : NULL;
			$tmp_lahir                = (isset($_POST['tmp_lahir']) && ! empty($_POST['tmp_lahir'])) ? xss_clean($_POST['tmp_lahir']) : NULL;
			$tgl_lahir                = (isset($_POST['tgl_lahir']) && ! empty($_POST['tgl_lahir'])) ? xss_clean($_POST['tgl_lahir']) : NULL;
			$nik                      = (isset($_POST['nik']) && ! empty($_POST['nik'])) ? xss_clean($_POST['nik']) : NULL;
			$notelp                   = (isset($_POST['notelp']) && ! empty($_POST['notelp'])) ? xss_clean($_POST['notelp']) : NULL;
			$pekerjaan_pasien         = (isset($_POST['pekerjaan_pasien']) && ! empty($_POST['pekerjaan_pasien'])) ? xss_clean($_POST['pekerjaan_pasien']) : NULL;
			$alamat                   = (isset($_POST['alamat']) && ! empty($_POST['alamat'])) ? xss_clean($_POST['alamat']) : NULL;
			$alamat_ktp               = (isset($_POST['alamat_ktp']) && ! empty($_POST['alamat_ktp'])) ? xss_clean($_POST['alamat_ktp']) : NULL;
			$id_provinsi              = (isset($_POST['id_provinsi']) && ! empty($_POST['id_provinsi'])) ? xss_clean($_POST['id_provinsi']) : NULL;
			$id_kota                  = (isset($_POST['id_kota']) && ! empty($_POST['id_kota'])) ? xss_clean($_POST['id_kota']) : NULL;
			$id_kecamatan             = (isset($_POST['id_kecamatan']) && ! empty($_POST['id_kecamatan'])) ? xss_clean($_POST['id_kecamatan']) : NULL;
			$id_kelurahan             = (isset($_POST['id_kelurahan']) && ! empty($_POST['id_kelurahan'])) ? xss_clean($_POST['id_kelurahan']) : NULL;
			$nama_ayah                = (isset($_POST['nama_ayah']) && ! empty($_POST['nama_ayah'])) ? xss_clean($_POST['nama_ayah']) : NULL;
			$pekerjaan_ayah           = (isset($_POST['pekerjaan_ayah']) && ! empty($_POST['pekerjaan_ayah'])) ? xss_clean($_POST['pekerjaan_ayah']) : NULL;
			$nama_ibu                 = (isset($_POST['nama_ibu']) && ! empty($_POST['nama_ibu'])) ? xss_clean($_POST['nama_ibu']) : NULL;
			$pekerjaan_ibu            = (isset($_POST['pekerjaan_ibu']) && ! empty($_POST['pekerjaan_ibu'])) ? xss_clean($_POST['pekerjaan_ibu']) : NULL;
			$nama_pasangan            = (isset($_POST['nama_pasangan']) && ! empty($_POST['nama_pasangan'])) ? xss_clean($_POST['nama_pasangan']) : NULL;
			$pekerjaan_pasangan       = (isset($_POST['pekerjaan_pasangan']) && ! empty($_POST['pekerjaan_pasangan'])) ? xss_clean($_POST['pekerjaan_pasangan']) : NULL;
			$jenis_kelamin            = (isset($_POST['jenis_kelamin']) && ! empty($_POST['jenis_kelamin'])) ? xss_clean($_POST['jenis_kelamin']) : NULL;
			$status                   = (isset($_POST['status']) && ! empty($_POST['status'])) ? xss_clean($_POST['status']) : NULL;
			$pendidikan               = (isset($_POST['pendidikan']) && ! empty($_POST['pendidikan'])) ? xss_clean($_POST['pendidikan']) : 0;
			$agama                    = (isset($_POST['agama']) && ! empty($_POST['agama'])) ? xss_clean($_POST['agama']) : NULL;
			$penanggungjawab_nama     = (isset($_POST['penanggungjawab_nama']) && ! empty($_POST['penanggungjawab_nama'])) ? xss_clean($_POST['penanggungjawab_nama']) : NULL;
			$penanggungjawab_hubungan = (isset($_POST['penanggungjawab_hubungan']) && ! empty($_POST['penanggungjawab_hubungan'])) ? xss_clean($_POST['penanggungjawab_hubungan']) : NULL;
			$penanggungjawab_alamat   = (isset($_POST['penanggungjawab_alamat']) && ! empty($_POST['penanggungjawab_alamat'])) ? xss_clean($_POST['penanggungjawab_alamat']) : NULL;
			$penanggungjawab_phone    = (isset($_POST['penanggungjawab_phone']) && ! empty($_POST['penanggungjawab_phone'])) ? xss_clean($_POST['penanggungjawab_phone']) : NULL;

			// nama pasien
			$nama_pasien = str_replace(',', ' ', $nama) . ', ' . $title;

			// cek apakah pasien merupakan pasien baru atau pasien lama
			// kode 1 untuk pasien baru, kode 0 untuk pasien lama
			if ( $jenis_kunjungan == 1 ) {
				if ( $nomr_manual == TRUE ) {
					// nomor rekam medik manual
					if ( is_pasien_exist($nomr) ) $error_msg[] = "Nomor {$nomr} sudah digunakan";
				} else {
					// nomor rekam medik otomatis
					$nomr = create_nomr();
					while ( is_pasien_exist($nomr) ) {
						$nomr = create_nomr();
					}
				}
			} else {
				// pasien lama
				// cek apakah nomor rekam medik terdaftar
				if ( is_pasien_exist($nomr) === FALSE ) {
					#$error_msg[] = "Nomor rekam medis pasien {$nomr} tidak ditemukan";
					// fitur validasi pasien lama dinonaktifkan sementara
					// karena terdapat pasien lama yang tidak terdaftar di dalam simrs
					$pasien_exist = TRUE;
				} else {
					$pasien_exist = TRUE;
				}
			}

			if ( count($error_msg) > 0 ) {
				$response = (object) array (
					'metadata' => (object) array (
						'code' => 500,
						'message' => show_error($error_msg, TRUE)
					)
				);
			} else {
				// data pasien
				$data_pasien = array (
					'NOMR'                     => $nomr,
					'NAMA'                     => $nama_pasien,
					'TITLE'                    => $title,
					'TEMPAT'                   => $tmp_lahir,
					'TGLLAHIR'                 => $tgl_lahir,
					'NOKTP'                    => $nik,
					'NOTELP'                   => $notelp,
					'PEKERJAAN'                => $pekerjaan_pasien,
					'ALAMAT'                   => $alamat,
					'ALAMAT_KTP'               => $alamat_ktp,
					'KDPROVINSI'               => $id_provinsi,
					'KOTA'                     => $id_kota,
					'KDKECAMATAN'              => $id_kecamatan,
					'KELURAHAN'                => $id_kelurahan,
					'NAMA_AYAH'                => $nama_ayah,
					'PEKERJAAN_AYAH'           => $pekerjaan_ayah,
					'NAMA_IBU'                 => $nama_ibu,
					'PEKERJAAN_IBU'            => $pekerjaan_ibu,
					'NAMA_PASANGAN'            => $nama_pasangan,
					'PEKERJAAN_PASANGAN'       => $pekerjaan_pasangan,
					'JENISKELAMIN'             => $jenis_kelamin,
					'STATUS'                   => $status,
					'PENDIDIKAN'               => $pendidikan,
					'AGAMA'                    => $agama,
					'PENANGGUNGJAWAB_NAMA'     => $penanggungjawab_nama,
					'PENANGGUNGJAWAB_HUBUNGAN' => $penanggungjawab_hubungan,
					'PENANGGUNGJAWAB_ALAMAT'   => $penanggungjawab_alamat,
					'PENANGGUNGJAWAB_PHONE'    => $penanggungjawab_phone,
					'KDCARABAYAR'              => $kdcarabayar,
					'NIP'                      => $user_login
				);

				// jika metode pembayaran menggunakan JKN
				// baca informasi kepesertaan JKN
				if ( $kdcarabayar == 2 ) {
					$data_pasien['NO_KARTU']         = $no_kartu;
					$data_pasien['KD_JENIS_PESERTA'] = $kd_jenis_peserta;
					$data_pasien['NM_JENIS_PESERTA'] = $nm_jenis_peserta;
					$data_pasien['KD_PROVIDER']      = $kd_provider;
					$data_pasien['NM_PROVIDER']      = $nm_provider;
					$data_pasien['KD_KELAS']         = $kd_kelas;
					$data_pasien['NM_KELAS']         = $nm_kelas;
				}

				// jika pasien adalah pasien baru tambahkan data pasien
				// jika pasien adalah pasien lama, perbahrui informasi data pasien
				if ( $jenis_kunjungan == 1 || is_pasien_exist($nomr) == FALSE ) {
					// kondisi if dirubah untuk mengakomodir pasien lama
					// yang tidak terdaftar di simrs
					try {
						// jika ini adalah kunjungan pertama (baru)
						// simpan informasi tgl daftar
						$data_pasien['TGLDAFTAR'] = $tglreg;

						$result_pasien = Models\insert_pasien($data_pasien);
					} catch (Exception $e) {
						show_exception_error($e);
					}
				} else {
					try {
						$result_pasien = Models\update_pasien($data_pasien, array (
							'NOMR' => $nomr
						) );
					} catch (Exception $e) {
						show_exception_error($e);
					}
				}

				// cek apakah data pasien berhasil disimpan
				if ( $result_pasien == FALSE ) {
					$response = (object) array (
						'metadata' => (object) array (
							'code' => 500,
							'message' => 'Server error. Tidak dapat menyimpan/mengupdate data pasien'
						)
					);
				} else {
					// data pendaftaran
					$data_pendaftaran = array (
						'NOMR' => $nomr,
						'TGLREG' => $tglreg,
						'KDDOKTER' => $kddokter,
						'KDPOLY' => $kdpoly,
						'KDRUJUK' => $kd_rujuk,
						'KDCARABAYAR' => $kdcarabayar,
						'SHIFT' => $shift,
						'JAMREG' => $tglreg,
						'MINTA_RUJUKAN' => $minta_rujukan,
						'STATUS' => 0,
						'PASIENBARU' => $jenis_kunjungan,
						'NIP' => $user_login,
						'PENANGGUNGJAWAB_NAMA' => $penanggungjawab_nama,
						'PENANGGUNGJAWAB_HUBUNGAN' => $penanggungjawab_hubungan,
						'PENANGGUNGJAWAB_ALAMAT' => $penanggungjawab_alamat,
						'PENANGGUNGJAWAB_PHONE' => $penanggungjawab_phone,
						'LAKALANTAS' => $lakalantas,
						'LOKASI_LAKALANTAS' => $lokasi_lakalantas,
						'CATATAN' => $catatan,
						'NO_KARTU' => $no_kartu,
						'KD_KELAS' => $kd_kelas,
						'KD_JENIS_PESERTA' => $kd_jenis_peserta,
						'NM_JENIS_PESERTA' => $nm_jenis_peserta,
						'KD_RUJUK' => $kd_rujuk,
						'NO_RUJUKAN' => $no_rujukan,
						'TGL_RUJUKAN' => $tgl_rujukan,
						'KD_PPK_RUJUKAN' => $kd_ppk_rujukan,
						'NM_PPK_RUJUKAN' => $nm_ppk_rujukan,
						'KD_DIAGNOSA' => $kd_diagnosa,
						'NM_DIAGNOSA' => $nm_diagnosa,
					);

					// additional data untuk pendaftaran untuk IGD dan VK/IGD Ponek
					if ( $kdpoly == 9 OR $kdpoly == 10 ) {
						$data_pendaftaran['MASUKPOLY'] = date('H:i:s');
					}

					try {
						$pendaftaran = Models\insert_pendaftaran($data_pendaftaran);
					} catch (Exception $e) {
						show_exception_error($e);
					}

					// cek apakah data pendaftaran berhasil disimpan
					if ( $pendaftaran == FALSE ) {
						// jika pendaftaran gagal
						// dan pasien adalah pasien baru
						// hapus data pasien yang sudah tersimpan
						if ( $jenis_kunjungan == 1 ) {
							try {
								Models\deletePasien($nomr);
							} catch (Exception $e) {
								show_exception_error($e);
							}
						}

						$response = (object) array (
							'metadata' => (object) array (
								'code' => 500,
								'message' => 'Server error. Tidak dapat menghapus data pendaftaran.'
							)
						);
					} else {
						// get last idxdaftar
						$last_idxdaftar = mysql_insert_id();

						// save pendaftaran iso
						try {
							$pendaftaran_iso = Models\PendaftaranISO\insert( array (
								'idxdaftar' => $last_idxdaftar,
								'NOMR' => $nomr,
								'start_daftar' => $start_daftar,
								'stop_daftar' => $stop_daftar,
							) );
						} catch (Exception $e) {
							show_exception_error($e);
						}

						$kdprofesi    = getProfesiDoktor($kddokter);
						$kodetarif    = getKodePendaftaran($kdpoly, $kdprofesi);
						$tarif_daftar = getTarifPendaftaran($kodetarif);
						$tarif        = getTarif($kodetarif);
						$last_bill    = getLastNoBILL(1);
						$qty          = 1;

						// tmp cartbayar
						try {
							$tmp_cartbayar = Models\TmpCartBayar\insert( array (
								'KODETARIF' => $kodetarif,
								'QTY' => $qty,
								'IP' => $ip_address,
								'ID' => $kodetarif,
								'POLY' => $kdpoly,
								'KDDOKTER' => $kddokter,
								'TARIF' => $tarif['tarif'],
								'TOTTARIF' => $tarif['tarif'],
								'JASA_PELAYANAN' => $tarif['jasa_pelayanan'],
								'JASA_SARANA' => $tarif['jasa_sarana'],
								'UNIT' => $kdpoly
							) );
						} catch (Exception $e) {
							show_exception_error($e);
						}

						// bill rajal
						try {
							$bill_rajal = Models\BillRajal\insert( array (
								'KODETARIF' => $kodetarif,
								'NOMR' => $nomr,
								'KDPOLY' => $kdpoly,
								'TANGGAL' => date('Y-m-d'),
								'SHIFT' => $shift,
								'NIP' => $user_login,
								'QTY' => $qty,
								'IDXDAFTAR' => $last_idxdaftar,
								'NOBILL' => $last_bill,
								'ASKES' => 0,
								'COSTSHARING' => 0,
								'KETERANGAN' => '-',
								'KDDOKTER' => $kddokter,
								'STATUS' => 'SELESAI',
								'CARABAYAR' => $kdcarabayar,
								'APS' => 0,
								'JASA_SARANA' => $tarif_daftar[0],
								'JASA_PELAYANAN' => $tarif_daftar[1],
								'UNIT' => $kdpoly,
								'TARIFRS' => $tarif_daftar[2],
							) );
						} catch (Exception $e) {
							show_exception_error($e);
						}

						// data pembayaran
						$data_bayarrajal = array (
							'NOMR' => $nomr,
							'IDXDAFTAR' => $last_idxdaftar,
							'NOBILL' => $last_bill,
							'TOTTARIFRS' => $tarif_daftar[2] * $qty,
							'UNIT' => $kdpoly,
							'TOTJASA_SARANA' => $tarif_daftar[0] * $qty,
							'TOTJASA_PELAYANAN' => $tarif_daftar[1] * $qty,
							'APS' => 0,
							'CARABAYAR' => $kdcarabayar,
						);

						// jika carabayar menggunakan JKN/Asuransi lainnya
						if ( Models\CaraBayar\isJmks($kdcarabayar) ) {
							$data_bayarrajal['TGLBAYAR'] = date('Y-m-d');
							$data_bayarrajal['JAMBAYAR'] = date('H:i:s');
							$data_bayarrajal['JMBAYAR'] = $tarif_daftar[2] * $qty;
							$data_bayarrajal['JMBAYAR'] = $tarif_daftar[2] * $qty;
							$data_bayarrajal['NIP'] = $user_login;
							$data_bayarrajal['SHIFT'] = $shift;
							$data_bayarrajal['TBP'] = 0;
							$data_bayarrajal['UNIT'] = $kdpoly;
							$data_bayarrajal['LUNAS'] = 1;
							$data_bayarrajal['STATUS'] = 'LUNAS';
						}

						// bayar rajal
						try {
							$bayar_rajal = Models\BayarRajal\insert($data_bayarrajal);
						} catch (Exception $e) {
							show_exception_error($e);
						}

						// jika tmp_cartbayar atau bill_rajal atau bayar_rajal gagal disimpan
						if ( $tmp_cartbayar == FALSE OR $bill_rajal == FALSE OR $bayar_rajal == FALSE ) {
							// hapus data pasien (jika kunjungan baru)
							if ( $jenis_kunjungan == 1 ) {
								try {
									Models\deletePasien($nomr);
								} catch (Exception $e) {
									show_exception_error($e);
								}
							}

							// hapus data pendaftaran
							try {
								Models\deleteKunjungan($last_idxdaftar);
							} catch (Exception $e) {
								show_exception_error($e);
							}

							$response = (object) array (
								'metadata' => (object) array (
									'code' => 500,
									'message' => 'Server error. Menyimpan data bill_rajal dan bayar_rajal.'
								)
							);
						} else {
							// update maxnomr
							if ( $jenis_kunjungan == 1 && $nomr_manual == FALSE ) update_last_nomr($nomr);

							// update maxnobill
							mysql_query("UPDATE m_maxnobill SET nomor = '{$last_bill}'") or die(mysql_error());

							// hitung lama pelayanan (dalam menit)
							$lama_pelayanan = countTimeDiff($start_daftar, $stop_daftar);

							// print tracer
							try {
								printTracer($last_idxdaftar, FALSE);
							} catch (Exception $e) {
								show_exception_error($e);
							}

							$response = (object) array (
								'metadata' => (object) array (
									'code' => 200,
									'message' => 'OK'
								),
								'response' => (object) array (
									'idxDaftar' => $last_idxdaftar,
									'noMr' => $nomr,
									'namaPasien' => $nama_pasien,
									'lamaPelayanan' => $lama_pelayanan
								)
							);
						}
					}
				}
			}
		}

		echo json_encode($response);
	}
}

//---------------------------------------------------------------------------------