<!-- modal search provider bpjs -->
<div class="modal fade" id="modal-search-provider-bpjs">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Cari Provider BPJS</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<form action="" id="form-search-provider">
							<div class="form-group">
								<input type="text" placeholder="Ketik nama provider" name="nama_provider" autocomplete="off" class="form-control">
							</div>
						</form>
						<div class="form-group">
							<div>
								<table class="table result" id="data-provider">
									<thead>
										<tr>
											<th>Kode</th>
											<th>Nama Provider</th>
											<th>Cabang</th>
											<th>Pilih</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="4" class="text-center">Provider tidak ditemukan</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {

		var modal = jQuery('#modal-search-provider-bpjs');
		var form = jQuery('#form-search-provider');
		
		//------------------------------------------------------------------------

		jQuery('#form-search-provider').submit(function(e) {
			var nama_provider = form.find('[name="nama_provider"]').val();
			
			if ( nama_provider.length >= 1 ) {
				jQuery.ajax({
					url : '<?= _BASE_ ?>' + 'bridging_proses.php',
					data : {
						reqdata : 'get_faskes',
						kode : nama_provider
					},
					dataType : 'json',
					beforeSend : function() {
						modal.find('table.result tbody').html('<tr><td colspan="4" class="text-center">Sedang memuat...</td></tr>');
					},
					success : function(response) {
						if ( response.metadata.code != "200" ) {
							swal('Error', response.metadata.message, 'error');
							console.log(response);

							modal.find('table.result tbody').html('<tr><td colspan="4" class="text-center">Provider tidak ditemukan</td></tr>');
						}
						else {
							html = '';
							var list = response.response.list;
							for ( var i = 0; i < list.length; i++ ) {
								var kdProvider = list[i].kdProvider;
								var nmProvider = list[i].nmProvider;
								var nmCabang = list[i].nmCabang;
								nmProvider = nmProvider.replace(/['"]+/g, '');
								html += '<tr>';
									html += '<td>' + kdProvider + '</td>';
									html += '<td>' + nmProvider + '</td>';
									html += '<td>' + nmCabang + '</td>';
									html += '<td class="no-gap"><a href="javascript:void(0)" class="choose btn btn-xs btn-primary" data-kode="' + kdProvider + '" data-nama="' + nmProvider + '"><i class="fa fa-fw fa-hand-pointer-o"></i></a></td>';
								html += '</tr>';

								modal.find('table.result tbody').html(html);
							}
						}
					},
					error : function(error) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(error);
						modal.find('table.result tbody').html('<tr><td colspan="4" class="text-center">Provider tidak ditemukan</td></tr>');
					}
				});
			}

			return false;
		});

		//------------------------------------------------------------------------

	});
</script>