<div class="modal fade" id="modal-search-kunjungan">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-search-kunjungan" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-search-plus"></i> Pencarian Lanjutan</h4>
				</div>
				<div class="modal-body">
					<div class="row row-sm">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Mulai Tanggal</label>
								<input type="text" value="<?= date('Y-m-d') ?>" class="form-control datepicker" data-column="0" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Sampai Tanggal</label>
								<input type="text" value="<?= date('Y-m-d') ?>" class="form-control datepicker" data-column="11" />
							</div>
						</div>
					</div>
					<div class="row row-sm">
						<div class="col-sm-5">
							<div class="form-group">
								<label>No. Rekam Medis</label>
								<input type="text" class="form-control" data-column="1" />
							</div>
						</div>
						<div class="col-sm-5">
							<div class="form-group">
								<label>Nama Pasien</label>
								<input type="text" class="form-control" data-column="2" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>L/P</label>
								<select class="form-control" data-column="3">
									<option value="" selected>-</option>
									<option value="L">L</option>
									<option value="P">P</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row row-sm">
						<div class="col-sm-4">
							<div class="form-group">
								<label>Poli</label>
								<?= dinamycDropdown( array (
									'name' => 'KDPOLY',
									'table' => 'm_poly',
									'key' => 'nama',
									'value' => 'nama',
									'selected' => '',
									'order_by' => 'nama asc',
									'empty_first' => TRUE,
									'first_value' => '-',
									'attr' => 'class="form-control" data-column="4"'
								) ); ?>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label>Cara Bayar</label>
								<?= dinamycDropdown( array (
									'name' => 'CARABAYAR',
									'table' => 'm_carabayar',
									'key' => 'NAMA',
									'value' => 'NAMA',
									'selected' => '',
									'order_by' => 'NAMA asc',
									'empty_first' => TRUE,
									'first_value' => '-',
									'attr' => 'class="form-control" data-column="8"'
								) ); ?>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label>Kunjungan</label>
								<select class="form-control" data-column="10">
									<option value="" selected>-</option>
									<option value="true">Baru</option>
									<option value="false">Lama</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="clear" class="btn btn-warning btn-sm" data-dismiss="modal">Bersihkan</button>
					<button type="submit" class="btn btn-primary btn-sm">Cari</button>
				</div>
			</form>
		</div>
	</div>
</div>