<!-- modal search diagnosa bpjs -->
<div class="modal fade" id="modal-search-diagnosa">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Cari Diagnosa</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<form action="" id="form-search-diagnosa">
							<div class="form-group">
								<label>Kata Kunci</label>
								<input type="text" placeholder="Ketik kode / nama (latin) / nama (lokal rs)" name="s" autocomplete="off" class="form-control">
							</div>
						</form>
						<div class="form-group">
							<div>
								<table class="table table-bordered result" id="data-diagnosa">
									<thead>
										<tr>
											<th width="50">Kode</th>
											<th>Nama Diagnosa</th>
											<th>Nama Lokal</th>
											<th width="200">Nama Lokal RS</th>
											<th class="no-gap">Pilih</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="5" class="text-center">Diagnosa tidak ditemukan</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {

		//------------------------------------------------------------------------

		jQuery('#form-search-diagnosa').submit(function(e) {
			var s = jQuery('#form-search-diagnosa [name="s"]').val();
			
			if ( s.length >= 1 ) {
				jQuery.ajax({
					url : '<?= _BASE_ ?>' + 'bridging_proses.php',
					data : {
						reqdata : 'get_local_diagnosa',
						s : s,
					},
					dataType : 'json',
					beforeSend : function() {
						jQuery('#modal-search-diagnosa table.result tbody').html('<tr><td colspan="5" class="text-center">Sedang memuat . . .</td></tr>');
					},
					success : function(response) {
						if ( response.metadata.code != "200" ) {
							swal('Error', response.metadata.message, 'error');
							console.log(response);

							jQuery('#modal-search-diagnosa table.result tbody').html('<tr><td colspan="5" class="text-center">Diagnosa tidak ditemukan</td></tr>');
						}
						else {
							html = '';
							var list = response.response.list;
							for ( var i = 0; i < list.length; i++ ) {
								var kodeDiagnosa = list[i].icd_code;
								var namaDiagnosa = list[i].jenis_penyakit;
								var namaDiagnosaLokal = list[i].jenis_penyakit_local;
								var namaDiagnosaLokalRs = list[i].jenis_penyakit_local_rs;
								html += '<tr>';
									html += '<td>' + kodeDiagnosa + '</td>';
									html += '<td>' + namaDiagnosa + '</td>';
									html += '<td>' + namaDiagnosaLokal + '</td>';
									html += '<td>' + namaDiagnosaLokalRs + '</td>';
									html += '<td class="no-gap"><a href="javascript:void(0)" class="choose btn btn-xs btn-primary" data-kode="' + kodeDiagnosa + '" data-nama="' + namaDiagnosa + '"><i class="fa fa-fw fa-hand-pointer-o"></i></a></td>';
								html += '</tr>';

								jQuery('#modal-search-diagnosa table.result tbody').html(html);
							}
						}
					},
					error : function(error) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						jQuery('#modal-search-diagnosa table.result tbody').html('<tr><td colspan="5" class="text-center">Diagnosa tidak ditemukan</td></tr>');
						console.log(error);
					}
				});
			}

			return false;
		});

		//------------------------------------------------------------------------

		jQuery('#data-diagnosa tbody').on('click', 'a.choose', function() {
			var kdDiagnosa = jQuery(this).data('kode');
			var nmDiagnosa = jQuery(this).data('nama');
			jQuery('#form-pendaftaran [name="kd_diagnosa"]').val(kdDiagnosa);
			jQuery('#form-pendaftaran [name="nm_diagnosa"]').val(nmDiagnosa);
			jQuery('#modal-search-diagnosa').modal('hide');
		});

		//------------------------------------------------------------------------

	});
</script>