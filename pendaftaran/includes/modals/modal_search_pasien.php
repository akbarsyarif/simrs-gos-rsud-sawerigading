<!-- modal search pasien -->
<div class="modal fade" id="modal-search-pasien">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Cari Pasien</h4>
			</div>
			<div class="modal-body">
				<form id="form-search-pasien">
					<div class="row sm-margin">
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" placeholder="Nomor Rekam Medik" class="form-control" data-column="0">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" placeholder="Nama Pasien" class="form-control" data-column="1">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" placeholder="YYYY-MM-DD" class="form-control" data-column="2">
							</div>
						</div>
					</div>
					<div class="row sm-margin">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" placeholder="NIK" class="form-control" data-column="3">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" placeholder="Nomor Peserta JKN" class="form-control" data-column="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<button type="button" id="clear" class="btn btn-warning">Clear Filter</button> 
							<button type="submit" class="btn btn-primary">Cari Pasien</button>
						</div>
					</div>
				</form>
			</div>
			<hr />
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-bordered" id="data-pasien">
							<thead>
								<tr>
									<th class="no-gap">No.MR</th>
									<th>Nama</th>
									<th>Tempat & Tgl.Lahir</th>
									<th>NIK</th>
									<th>No.JKN</th>
									<th class="no-gap"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		var table = jQuery('#data-pasien').DataTable({
			ordering : true,
			order : [[0, "asc"]],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_."ajaxload.php?p=pasien&a=data_table" ?>',
			columns : [
				{
					data : "NOMR",
					className : 'text-center'
				},
				{
					data : "NAMA",
					mRender: function(data, type, row) {
						if ( row.TITLE == "" || row.TITLE == null ) {
							return data;
						}
						else {
							return data + ', ' + row.TITLE;
						}
					}
				},
				{
					data : "TGLLAHIR",
					mRender : function(data, type, row) {
						var TEMPAT = '';
						if ( row.TEMPAT != '' ) {
							TEMPAT = row.TEMPAT+', ';
						}
						return TEMPAT+row.TGLLAHIR;
					},
					orderable : false
				},
				{ data : "NOKTP" },
				{ data : "NO_KARTU" },
				{
					data : null,
					orderable : false,
					className : 'text-center',
					mRender : function(data, type, row) {
						return '<a class="choose btn btn-xs btn-block btn-primary" href="javascript:void(0)" data-nomr="'+row.NOMR+'"><i class="fa fa-fw fa-hand-pointer-o"></i></a>'
					},
				}
			]
		});

		// Apply the search
		table.columns().every( function () {
			var that = this;

			jQuery( 'input', this.footer() ).on( 'keyup change', function () {
				jQuery( '#data-pasien tfoot input.text' ).each(function() {
					var i = jQuery(this).attr('data-column');

					table.column(i).search(
						jQuery(this).val()
					).draw();
				});
			});
		});

		// klik tombol pilih
		jQuery('#data-pasien tbody').on('click', 'a.choose', function() {
			let data = table.row(jQuery(this).closest('tr')).data();

			// carabayar
			jQuery('#form-pendaftaran [name="kdcarabayar"]').val(data.KDCARABAYAR).trigger('change');
			if (data.KDCARABAYAR == 2) {
				// loading effect on jkn form
				jQuery('#form-pendaftaran .carabayar-2').waitMe({
					effect: 'rotation',
					text: '',
					bg: 'rgba(255,255,255,0.7)',
					sizeW: '',
					sizeH: '',
					source: '',
					onClose: function() {
					}
				});

				emptyFormJKN();
				jQuery('#form-pendaftaran .carabayar-2 [name="metode_pengisian"]').val('manual').trigger('change');
				getPesertaBpjs('nokartu', data.NO_KARTU)
					.done(function(r) {
						if ( r.metadata.code != "200" ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						} else {
							if (r.response.metadata.code != "200") {
								swal('Server BPJS Error', r.response.metadata.message, 'error');
								console.log(r);
							} else {
								let data = r.response.response.peserta;
								jQuery('#form-pendaftaran .carabayar-2 [name="no_kartu"]').val(data.noKartu);
								jQuery('#form-pendaftaran .carabayar-2 [name="kd_jenis_peserta"]').val(data.jenisPeserta.kdJenisPeserta);
								jQuery('#form-pendaftaran .carabayar-2 [name="nm_jenis_peserta"]').val(data.jenisPeserta.nmJenisPeserta);
								jQuery('#form-pendaftaran .carabayar-2 [name="kd_kelas"]').val(data.kelasTanggungan.kdKelas);
								jQuery('#form-pendaftaran .carabayar-2 [name="nm_kelas"]').val(data.kelasTanggungan.nmKelas);
								jQuery('#form-pendaftaran .carabayar-2 [name="kd_provider"]').val(data.provUmum.kdProvider);
								jQuery('#form-pendaftaran .carabayar-2 [name="nm_provider"]').val(data.provUmum.nmProvider);
								jQuery('#form-pendaftaran .carabayar-2 [name="kd_ppk_rujukan"]').val(data.provUmum.kdProvider);
								jQuery('#form-pendaftaran .carabayar-2 [name="nm_ppk_rujukan"]').val(data.provUmum.nmProvider);
							}
						}
					})
					.fail(function(e) {
						swal('Error', r.metadata.message, 'error');
						console.log(e);
					})
					.then(function(xhr) {
						jQuery('#form-pendaftaran .carabayar-2').waitMe('hide');
					});
			}

			jQuery('#form-pendaftaran [name="nomr"]').val(data.NOMR);
			jQuery('#form-pendaftaran [name="nama"]').val(data.NAMA);
			jQuery('#form-pendaftaran [name="title"]').val(data.TITLE);
			jQuery('#form-pendaftaran [name="tmp_lahir"]').val(data.TEMPAT);
			jQuery('#form-pendaftaran [name="tgl_lahir"]').val(data.TGLLAHIR).trigger('change');
			jQuery('#form-pendaftaran [name="nik"]').val(data.NOKTP);
			jQuery('#form-pendaftaran [name="notelp"]').val(data.NOTELP);
			jQuery('#form-pendaftaran [name="pekerjaan_pasien"]').val(data.PEKERJAAN);
			jQuery('#form-pendaftaran [name="alamat"]').val(data.ALAMAT);
			jQuery('#form-pendaftaran [name="alamat_ktp"]').val(data.ALAMAT_KTP);
			jQuery('#form-pendaftaran [name="id_provinsi"]').val(data.KDPROVINSI).trigger('change');

			// menampilkan dropdown daerah
			getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : data.KDPROVINSI, target : '#id_kota', selected : data.KOTA, complete: function() {
				jQuery('#form-pendaftaran [name="id_kota"]').trigger('change');
				getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : data.KOTA, target : '#id_kecamatan', selected : data.KDKECAMATAN, complete: function() {
					jQuery('#form-pendaftaran [name="id_kecamatan"]').trigger('change');
					getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : data.KDKECAMATAN, target : '#id_kelurahan', selected : data.KELURAHAN, complete: function() {
						jQuery('#form-pendaftaran [name="id_kelurahan"]').trigger('change');
					} });
				} });
			} });

			jQuery('#form-pendaftaran [name="nama_ayah"]').val(data.NAMA_AYAH);
			jQuery('#form-pendaftaran [name="pekerjaan_ayah"]').val(data.PEKERJAAN_AYAH);
			jQuery('#form-pendaftaran [name="nama_ibu"]').val(data.NAMA_IBU);
			jQuery('#form-pendaftaran [name="pekerjaan_ibu"]').val(data.PEKERJAAN_IBU);
			jQuery('#form-pendaftaran [name="nama_pasangan"]').val(data.NAMA_PASANGAN);
			jQuery('#form-pendaftaran [name="pekerjaan_pasangan"]').val(data.PEKERJAAN_PASANGAN);
			jQuery('#form-pendaftaran [name="jenis_kelamin"][value="'+data.JENISKELAMIN+'"]').trigger('click');
			jQuery('#form-pendaftaran [name="status"][value="'+data.STATUS+'"]').trigger('click');
			jQuery('#form-pendaftaran [name="pendidikan"][value="'+data.PENDIDIKAN+'"]').trigger('click');
			jQuery('#form-pendaftaran [name="agama"][value="'+data.AGAMA+'"]').trigger('click');
			jQuery('#form-pendaftaran [name="penanggungjawab_nama"]').val(data.PENANGGUNGJAWAB_NAMA);
			jQuery('#form-pendaftaran [name="penanggungjawab_hubungan"]').val(data.PENANGGUNGJAWAB_HUBUNGAN);
			jQuery('#form-pendaftaran [name="penanggungjawab_alamat"]').val(data.PENANGGUNGJAWAB_ALAMAT);
			jQuery('#form-pendaftaran [name="penanggungjawab_phone"]').val(data.PENANGGUNGJAWAB_PHONE);

			// focus to field asal rujukan
			jQuery('#form-pendaftaran [name="kd_rujuk"]').focus();

			// hide modal
			jQuery('#modal-search-pasien').modal('hide');
		});

		// search pasien
		jQuery('#form-search-pasien').on('submit', function() {
			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#form-search-pasien #clear').on('click', function() {
			jQuery('#form-search-pasien').resetForm();

			jQuery('#form-search-pasien .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});
	});
</script>