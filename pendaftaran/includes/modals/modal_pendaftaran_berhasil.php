<div class="modal fade" id="modal-pendaftaran-berhasil" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-check-circle"></i> Pendaftaran Berhasil</h4>
			</div>
			<div class="modal-body text-center">
				<div class="form-group">
					<label>.:: NO. MR ::.</label>
					<h2 class="noMr" style="margin:0;font-weight:bold"></h2>
				</div>
				<div class="form-group">
					<label>.:: NAMA PASIEN ::.</label>
					<h2 class="namaPasien" style="margin:0;font-weight:bold"></h2>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-10 text-left">
						<a href="#" target="_blank" class="printKartu btn btn-sm btn-default">Kartu Berobat</a>
						<a href="#" target="_blank" class="printFormulir btn btn-sm btn-default">Form Pasien</a>
						<a href="#" target="_blank" class="printFormVerifikasi btn btn-sm btn-default">CASE-MIX/INACBG</a>
						<a href="#" target="_blank" class="printSep btn btn-sm btn-default">SEP</a>
					</div>
					<div class="col-sm-2">
						<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>