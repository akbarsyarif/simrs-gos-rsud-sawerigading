<script type="text/javascript">
	//------------------------------------------------------------------------

	/**
	 * membaca data rujukan bpjs
	 * @param  {[object]}
	 * @return {[object]} respon dari server bpjs
	 */
	function getRujukan(params) {
		return jQuery.ajax({
			url : '<?= _BASE_ ?>' + 'bridging_proses.php',
			data : {
				reqdata : 'get_rujukan',
				search_by : params.search_by,
				faskes : params.faskes,
				s : params.s
			},
			dataType : 'json'
		});
	}

	//------------------------------------------------------------------------

	/**
	 * melakukan mapping poli
	 * @param  {[string]} kd_poly_bpjs
	 * @return {[object]}
	 */
	function mappingPoly(kd_poly_bpjs)
	{
		return jQuery.ajax({
			url : '<?= _BASE_ ?>' + 'ajaxload.php',
			data : {
				p : 'poly',
				a : 'mappingpoly',
				kd_bpjs : kd_poly_bpjs
			},
			dataType : 'json'
		});
	}

	//------------------------------------------------------------------------

	function validateJKN(validate) {
		let jknContainer = jQuery('#form-pendaftaran .carabayar-2');

		if ( validate == true ) {
			jknContainer.find('[name="metode_pengisian"]').addClass('required');
			jknContainer.find('[name="no_kartu"]').addClass('required');
			jknContainer.find('[name="kd_jenis_peserta"]').addClass('required');
			jknContainer.find('[name="nm_jenis_peserta"]').addClass('required');
			jknContainer.find('[name="kd_kelas"]').addClass('required');
			jknContainer.find('[name="kd_provider"]').addClass('required');
			jknContainer.find('[name="nm_provider"]').addClass('required');
			jknContainer.find('[name="no_rujukan"]').addClass('required');
			jknContainer.find('[name="tgl_rujukan"]').addClass('required');
			jknContainer.find('[name="kd_ppk_rujukan"]').addClass('required');
			jknContainer.find('[name="nm_ppk_rujukan"]').addClass('required');
		} else if ( validate == 'offline' ) {
			jknContainer.find('[name="metode_pengisian"]').addClass('required');
			jknContainer.find('[name="no_kartu"]').addClass('required');
			jknContainer.find('[name="kd_jenis_peserta"]').removeClass('required');
			jknContainer.find('[name="nm_jenis_peserta"]').removeClass('required');
			jknContainer.find('[name="kd_kelas"]').addClass('required');
			jknContainer.find('[name="kd_provider"]').removeClass('required');
			jknContainer.find('[name="nm_provider"]').removeClass('required');
			jknContainer.find('[name="no_rujukan"]').addClass('required');
			jknContainer.find('[name="tgl_rujukan"]').addClass('required');
			jknContainer.find('[name="kd_ppk_rujukan"]').removeClass('required');
			jknContainer.find('[name="nm_ppk_rujukan"]').removeClass('required');
		} else {
			jknContainer.find('[name="metode_pengisian"]').removeClass('required');
			jknContainer.find('[name="no_kartu"]').removeClass('required');
			jknContainer.find('[name="kd_jenis_peserta"]').removeClass('required');
			jknContainer.find('[name="nm_jenis_peserta"]').removeClass('required');
			jknContainer.find('[name="kd_kelas"]').removeClass('required');
			jknContainer.find('[name="kd_provider"]').removeClass('required');
			jknContainer.find('[name="nm_provider"]').removeClass('required');
			jknContainer.find('[name="no_rujukan"]').removeClass('required');
			jknContainer.find('[name="tgl_rujukan"]').removeClass('required');
			jknContainer.find('[name="kd_ppk_rujukan"]').removeClass('required');
			jknContainer.find('[name="nm_ppk_rujukan"]').removeClass('required');
		}
	}

	//------------------------------------------------------------------------

	// lock field JKN
	function lockFieldJKN()
	{
		let jknContainer = jQuery('#form-pendaftaran .carabayar-2');

		jknContainer.find('[name="no_kartu"]').attr('readonly', '');
		jknContainer.find('[name="kd_jenis_peserta"]').attr('readonly', '');
		jknContainer.find('[name="nm_jenis_peserta"]').attr('readonly', '');
		jknContainer.find('[name="kd_kelas"]').attr('readonly', '');
		jknContainer.find('[name="kd_provider"]').attr('readonly', '');
		jknContainer.find('[name="nm_provider"]').attr('readonly', '');
		jknContainer.find('[name="no_rujukan"]').attr('readonly', '');
		jknContainer.find('[name="tgl_rujukan"]').attr('readonly', '');
		jknContainer.find('[name="kd_ppk_rujukan"]').attr('readonly', '');
		jknContainer.find('[name="nm_ppk_rujukan"]').attr('readonly', '');
		jknContainer.find('[name="catatan"]').attr('readonly', '');

		// action buttons
		jknContainer.find('.btn-action').attr('disabled', '');
	}

	//------------------------------------------------------------------------

	// unlock field JKN
	function unlockFieldJKN()
	{
		let jknContainer = jQuery('#form-pendaftaran .carabayar-2');

		jknContainer.find('[name="kd_kelas"]').removeAttr('readonly');
		jknContainer.find('[name="no_rujukan"]').removeAttr('readonly');
		jknContainer.find('[name="tgl_rujukan"]').removeAttr('readonly');
		jknContainer.find('[name="catatan"]').removeAttr('readonly');

		// action buttons
		jknContainer.find('.btn-action').removeAttr('disabled');
	}

	//------------------------------------------------------------------------

	// unlock field JKN (Offline Mode)
	function unlockFieldJKNOffline()
	{
		let jknContainer = jQuery('#form-pendaftaran .carabayar-2');

		jknContainer.find('[name="no_kartu"]').removeAttr('readonly');
		jknContainer.find('[name="kd_kelas"]').removeAttr('readonly');
		jknContainer.find('[name="no_rujukan"]').removeAttr('readonly');
		jknContainer.find('[name="tgl_rujukan"]').removeAttr('readonly');
		jknContainer.find('[name="catatan"]').removeAttr('readonly');

		// action buttons
		jknContainer.find('.btn-action').removeAttr('disabled');
	}

	//------------------------------------------------------------------------

	// empty form JKN
	function emptyFormJKN()
	{
		var jknContainer = jQuery('#form-pendaftaran .carabayar-2');

		jknContainer.find('[name="no_kartu"]').val('');
		jknContainer.find('[name="kd_jenis_peserta"]').val('');
		jknContainer.find('[name="nm_jenis_peserta"]').val('');
		jknContainer.find('[name="kd_kelas"]').val('');
		jknContainer.find('[name="kd_provider"]').val('');
		jknContainer.find('[name="nm_provider"]').val('');
		jknContainer.find('[name="no_rujukan"]').val('');
		jknContainer.find('[name="tgl_rujukan"]').val('');
		jknContainer.find('[name="kd_ppk_rujukan"]').val('');
		jknContainer.find('[name="nm_ppk_rujukan"]').val('');
		jknContainer.find('[name="catatan"]').val('');
	}

	//------------------------------------------------------------------------

	function doResetPendaftaran() {
		swal({
			title: "Kosongkan Form",
			type: "warning",
			html: "Anda yakin ingin mengosongkan form ?",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak",
		}).then(function(result) {
			resetPendaftaran();
			swal('Berhasil!', 'Form telah dikosongkan', 'success');
		}, function(dismiss) {});
	}

	//------------------------------------------------------------------------

	function resetPendaftaran() {
		jQuery('#form-pendaftaran').trigger('reset');
		jQuery('#form-pendaftaran').find('select,.datepicker,.datetimepicker').trigger('change');
		jQuery('#form-pendaftaran').find('[name="start_daftar"]').val('');
		jQuery('#form-pendaftaran').find('[name="stop_daftar"]').val('');
		jQuery('#form-pendaftaran').find('[type="hidden"]').val('');
		jQuery('#form-pendaftaran').find('[name="tglreg"]').val(moment().format('YYYY-MM-DD HH:mm:ss'));
		jQuery('#form-pendaftaran .carabayar-2').find('[name="tgl_rujukan"]').val(moment().format('YYYY-MM-DD HH:mm:ss'));
	}

	//------------------------------------------------------------------------

	function doChangeStatusPasien(status) {
		let html = '';
		if ( status == false ) {
			html += '<div class="input-group">';
				html += '<input type="text" name="nomr" class="form-control input-sm required" title="Tidak boleh kosong" />';
		        html += '<span class="input-group-btn">';
		   	        html += '<button class="btn btn-sm btn-default" type="button" onclick="showModalSearchPasien(this)"><i class="fa fa-fw fa-search"></i></button>';
		        html += '</span>';
		    html += '</div>';
			jQuery('#nomr').html(html);
		} else {
			html += '<div class="input-group">';
				html += '<input type="text" name="nomr" class="form-control input-sm" title="Tidak boleh kosong" readonly="" />';
		        html += '<span class="input-group-addon">';
		       	    html += '<label class="checkbox-inline">';
		       	        html += '<input type="checkbox" id="nomr-manual" name="nomr_manual" value="true" onchange="doChangeNomrManual(this)"> Manual';
		       	    html += '</label>';
		        html += '</span>';
		    html += '</div>';
			jQuery('#nomr').html(html);
		}
	}

	//------------------------------------------------------------------------

	function doChangeNomrManual(obj) {
		let manual = jQuery(obj).is(':checked');
		jQuery('#nomr').find('[name="nomr"]').val(null);
		if ( manual == true ) {
			jQuery('#nomr').find('[name="nomr"]').removeAttr('readonly');
			jQuery('#nomr').find('[name="nomr"]').addClass('required');
		} else {
			jQuery('#nomr').find('[name="nomr"]').attr('readonly', '');
			jQuery('#nomr').find('[name="nomr"]').removeClass('required');
		}
	}

	//------------------------------------------------------------------------

	function showModalSearchPasien(obj) {
		jQuery('#modal-search-pasien').modal('show');
	}

	//------------------------------------------------------------------------

	function doToggleLokasiLakalantas(obj) {
		let lakaLantas = jQuery(obj).val();
		jQuery('#form-pendaftaran').find('#lokasi-lakalantas').val('');
		if ( lakaLantas == 1 ) {
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').removeAttr('disabled');
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').addClass('required');
		} else {
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').attr('disabled', '');
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').removeClass('required');
		}
	}

	//------------------------------------------------------------------------

	function showModalSearchDiagnosa(obj) {
		jQuery('#modal-search-diagnosa').modal('show');
	}

	//------------------------------------------------------------------------

	function doChangeCaraBayar(obj) {
		let caraBayar = jQuery(obj).val();
		jQuery('.carabayar').slideUp(100);
		jQuery('.carabayar-' + caraBayar).slideDown(100);
		validateJKN(false);
		if (caraBayar == 2) {
			validateJKN(true);
		}
	}

	//------------------------------------------------------------------------

	function doChangeMetodePengisianBpjs(obj) {
		let metode = jQuery(obj).val();
		jQuery('#form-pendaftaran .carabayar-2 .search-rujukan').hide();
		lockFieldJKN();
		validateJKN(true);
		if (metode == "rujukan") {
			jQuery('#form-pendaftaran .carabayar-2 .search-rujukan').show();
			jQuery('#form-pendaftaran .carabayar-2').find('[name="catatan"]').removeAttr('readonly');
		} else if (metode == "manual") {
			unlockFieldJKN();
		} else if (metode == "manual_offline") {
			unlockFieldJKNOffline();
			validateJKN('offline');
		}
	}

	//------------------------------------------------------------------------

	function showModalSearchRujukanBpjs(obj) {
		jQuery('#modal-search-rujukan-bpjs').modal('show');
	}

	//------------------------------------------------------------------------

	function showModalSearchPesertaBpjs(obj) {
		jQuery('#modal-search-peserta-bpjs').modal('show');
	}

	//------------------------------------------------------------------------

	function showModalSearchProviderBpjs(obj) {
		jQuery('#modal-search-provider-bpjs').modal('show');
	}

	//------------------------------------------------------------------------

	function getPesertaBpjs(searchBy, s) {
		return jQuery.ajax({
			url : '<?= _BASE_ ?>' + 'bridging_proses.php',
			type : 'GET',
			dataType : 'json',
			data : {
				reqdata : 'get_peserta',
				search_by : searchBy,
				s : s
			}
		});
	}

	//------------------------------------------------------------------------

	function doChangePoly(obj) {
		let kdpoly = jQuery(obj).val();
		jQuery('#kddokter').html('<option value="">- Pilih Dokter -</option>');
		if (typeof kdpoly != "undefined" && kdpoly != "") {
			getDokterJaga({ url : '<?= _BASE_ ?>ajaxload.php', kd_poly : kdpoly, target : '#kddokter', selected : '' });
		}
	}

	//------------------------------------------------------------------------

	function startDaftar() {
		jQuery('#form-pendaftaran').find('[name="start_daftar"]').val(moment().format('HH:mm:ss'));
	}

	//------------------------------------------------------------------------

	function stopDaftar() {
		jQuery('#form-pendaftaran').find('[name="stop_daftar"]').val(moment().format('HH:mm:ss'));
	}

	//------------------------------------------------------------------------

	jQuery(document).ready(function() {
		let modal_peserta = jQuery('#modal-search-peserta-bpjs');
		let form_peserta = jQuery('#form-search-peserta-bpjs');

		// trigger change status pasien
		doChangeStatusPasien(jQuery('#status-pasien').val());

		// choose data provider bpjs
		jQuery('#data-provider tbody').on('click', 'a.choose', function() {
			var kdProvider = jQuery(this).data('kode');
			var nmProvider = jQuery(this).data('nama');
			jQuery('#form-pendaftaran .carabayar-2').find('[name="kd_ppk_rujukan"]').val(kdProvider);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="nm_ppk_rujukan"]').val(nmProvider);
			jQuery('#modal-search-provider-bpjs').modal('hide');
		});

		// choose data peserta bpjs
		jQuery('#data-peserta-bpjs tbody').on('click', 'a.choose', function() {
			var resultTemp = modal_peserta.find('[name="resultTemp"]').val();
			var d = JSON.parse(resultTemp);

			jQuery('#form-pendaftaran .carabayar-2').find('[name="no_kartu"]').val(d.noKartu);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="kd_jenis_peserta"]').val(d.jenisPeserta.kdJenisPeserta);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="nm_jenis_peserta"]').val(d.jenisPeserta.nmJenisPeserta);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="kd_kelas"]').val(d.kelasTanggungan.kdKelas);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="kd_provider"]').val(d.provUmum.kdProvider);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="nm_provider"]').val(d.provUmum.nmProvider);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="kd_ppk_rujukan"]').val(d.provUmum.kdProvider);
			jQuery('#form-pendaftaran .carabayar-2').find('[name="nm_ppk_rujukan"]').val(d.provUmum.nmProvider);
			jQuery('#form-pendaftaran').find('[name="nama"]').val(d.nama);
			jQuery('#form-pendaftaran').find('[name="nik"]').val(d.nik);
			jQuery('#form-pendaftaran').find('[name="jenis_kelamin"][value="'+d.sex+'"]').trigger('click');
			jQuery('#form-pendaftaran').find('[name="tgl_lahir"]').val(d.tglLahir).trigger('change');

			modal_peserta.modal('hide');
		});

		modal_peserta.on('hidden.bs.modal', function() {
			// reset form
			form_peserta.trigger('reset');
			modal_peserta.find('[name="resultTemp"]').val('');
		});

		// dropdown kota
		jQuery('#form-pendaftaran').on('change', '#id_provinsi', function() {
			var id_prov = jQuery(this).val();
			getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : id_prov, target : '#id_kota', selected : '' });
			jQuery('#id_kota').html('<option value="" selected>- Pilih Kota -</option>');
			jQuery('#id_kecamatan').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kecamatan
		jQuery('#form-pendaftaran').on('change', '#id_kota', function() {
			var id_kota = jQuery(this).val();
			getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : id_kota, target : '#id_kecamatan', selected : '' });
			jQuery('#id_kecamatan').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kelurahan
		jQuery('#form-pendaftaran').on('change', '#id_kecamatan', function() {
			var id_kec = jQuery(this).val();
			getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : id_kec, target : '#id_kelurahan', selected : '' });
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// submit form pendaftaran
		jQuery("#form-pendaftaran").validate({
			submitHandler : function(form) {
				// set stop_daftar time
				stopDaftar();

				jQuery(form).ajaxSubmit({
					url: '<?= _BASE_ . 'index2.php?link=pendaftaran&c=action_pendaftaran&a=save' ?>',
					dataType : 'json',
					beforeSubmit : function(att, $form, options) {
						jQuery('#form-pendaftaran').find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( typeof r.metadata.code == 'undefined' ) {
							swal('Error', r, 'error');
							console.log(r);
						}
						else if ( r.metadata.code != 200 ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							// reset form pendaftaran
							resetPendaftaran();

							let data = r.response;
							let modal = jQuery('#modal-pendaftaran-berhasil');

							// set data to modal body
							modal.find('.noMr').html(data.noMr);
							modal.find('.namaPasien').html(data.namaPasien);
							modal.find('a.printKartu').attr('href', 'pdfb/kartupasien.php?idxdaftar='+data.idxDaftar);
							modal.find('a.printFormulir').attr('href', '<?= _BASE_ ?>index2.php?link=pasien&c=laporan&a=mr1&nomr='+data.noMr);
							modal.find('a.printFormVerifikasi').attr('href', '<?= _BASE_ ?>index2.php?link=atributpasien&c=formverifikasi&a=download&idxdaftar='+data.idxDaftar);
							modal.find('a.printSep').attr('href', '<?= _BASE_ ?>index.php?link=sep&c=create_sep&idxdaftar='+data.idxDaftar);

							modal.modal('show');
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						jQuery('#form-pendaftaran').find('[type="submit"]').button('reset');
					}
				});
			}
		});
	});

	//------------------------------------------------------------------------
</script>