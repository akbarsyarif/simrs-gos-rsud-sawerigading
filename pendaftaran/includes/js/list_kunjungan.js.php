<script type="text/javascript">
	jQuery(document).ready(function() {
		var editKunjunganModal = jQuery('#modal-edit-kunjungan');
		var editKunjunganForm = jQuery('#form-edit-kunjungan');

		var table = jQuery('#datatable-daftar-kunjungan').DataTable({
			displayLength: 10,
			ordering : true,
			order : [[0, 'desc']],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_ . "index2.php?link=pendaftaran&c=action_list_kunjungan&a=datatable" ?>',
			columns : [
				{
					className : 'text-center',
					data : "TGLREG",
					mRender: function(data, type, row) {
						return moment(row.JAMREGRAW).format('DD/MM/YYYY');
					}
				},
				{
					className : 'text-center',
					data : "NOMR"
				},
				{ data : "NAMA_PASIEN" },
				{
					className : 'text-center',
					data : "JENISKELAMIN"
				},
				{
					className : 'text-center',
					data : "NAMA_POLY"
				},
				{ data : "NAMADOKTER" },
				{
					data : "SHIFT",
					className : "text-center",
					mRender : function(data, type, row) {
						return row.SHIFT_LABEL;
					}
				},
				{
					data : "NIP",
					className : "text-center",
				},
				{
					data : "CARABAYAR",
					className : "text-center"
				},
				{
					data : "RUJUKAN",
					className : "text-center"
				},
				{
					data : "PASIENBARU",
					className : "text-center",
					mRender : function(data, type, row) {
						html = 'B';
						if ( data == 0 ) {
							html = 'L';
						}

						return html;
					}
				},
				{
					data : "TGLREG",
					visible : false
				},
				<?php if ( $_SESSION['ROLES'] == 1 ) : ?>
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<a href="<?= _BASE_ ?>index.php?link=28&idx='+ row.IDXDAFTAR +'" class="edit btn btn-xs btn-warning btn-block"><i class="fa fa-fw fa-pencil"></i></a>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button class="delete btn btn-xs btn-danger btn-block"><i class="fa fa-fw fa-trash"></i></button>';
					}
				}
				<?php endif; ?>
			],
		});

		// search
		jQuery('#form-search-kunjungan').on('submit', function() {
			jQuery('#modal-search-kunjungan').modal('hide');

			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#clear').on('click', function() {
			jQuery('#form-search-kunjungan').resetForm();
			jQuery('#modal-search-kunjungan').modal('hide');

			jQuery('#form-search-kunjungan .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});

		// edit kunjungan
		jQuery('#datatable-daftar-kunjungan tbody').on('click', '.edit', function(e) {
			editKunjunganModal.modal('show');
		});

		// delete kunjungan
		jQuery('#datatable-daftar-kunjungan tbody').on('click', '.delete', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			swal({
				title: "Hapus Data?",
				type: "warning",
				html: 
					"Anda akan menghapus data dengan id transaksi <b class='text-danger'>" + data.IDXDAFTAR + "</b> a/n <b class='text-danger'>" + data.NAMA_PASIEN + "</b>" +
					"<p>" +
					"<div class='form-group'><input type='text' id='username-spv' class='form-control input-lg' placeholder='Username SPV' style='text-align:center' /></div>" +
					"<div class='form-group'><input type='password' id='password-spv' class='form-control input-lg' placeholder='Password SPV' style='text-align:center' /></div>" +
					"</p>",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				preConfirm: function() {
					return new Promise(function (resolve, reject) {
						var usernameSpv = jQuery('#username-spv').val();
						var passwordSpv = jQuery('#password-spv').val();

						if ( usernameSpv === "" ) {
							swal.showValidationError('Username SPV tidak boleh kosong');
							reject();
						}
						else if ( passwordSpv === "" ) {
							swal.showValidationError('Password SPV tidak boleh kosong');
							reject();
						}
						else {
							jQuery.ajax({
								url: '<?= _BASE_ . "index2.php?link=pendaftaran&c=action_list_kunjungan&a=delete" ?>',
								method: 'get',
								dataType: 'json',
								data: {
									usernameSpv : usernameSpv,
									passwordSpv : passwordSpv,
									idxdaftar : data.IDXDAFTAR
								},
								success: function(r) {
									if ( r.metadata.code != "200" ) {
										swal('Eror', r.metadata.message, 'error');
										console.log(r);
									}
									else {
										swal('Success', 'Data berhasil dihapus', 'success');
										table.ajax.reload(null, false);
									}
								},
								error: function(e) {
									swal('Error', 'Error 400. Bad request', 'error');
									console.log(e);
								}
							});
						}
					});
				},
			}).then(function(result) {
				swal('Success!', 'Berhasil', 'success');
			}, function(dismiss) {});
		});
	});
</script>