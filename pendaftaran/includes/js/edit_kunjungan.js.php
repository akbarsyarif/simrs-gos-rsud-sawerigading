<script type="text/javascript">
	//------------------------------------------------------------------------

	function doResetPendaftaran() {
		swal({
			title: "Reset Form",
			type: "warning",
			html: "Anda yakin ingin mereset form ?",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak",
		}).then(function(result) {
			resetPendaftaran();
			swal('Berhasil!', 'Form telah direset', 'success');
		}, function(dismiss) {});
	}

	//------------------------------------------------------------------------

	function resetPendaftaran() {
		jQuery('#form-pendaftaran').trigger('reset');
		jQuery('#form-pendaftaran').find('select,.datepicker,.datetimepicker').trigger('change');
	}

	//------------------------------------------------------------------------

	function doToggleLokasiLakalantas(obj) {
		let lakaLantas = jQuery(obj).val();
		jQuery('#form-pendaftaran').find('#lokasi-lakalantas').val('');
		if ( lakaLantas == 1 ) {
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').removeAttr('disabled');
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').addClass('required');
		} else {
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').attr('disabled', '');
			jQuery('#form-pendaftaran').find('#lokasi-lakalantas').removeClass('required');
		}
	}

	//------------------------------------------------------------------------

	function showModalSearchDiagnosa(obj) {
		jQuery('#modal-search-diagnosa').modal('show');
	}

	//------------------------------------------------------------------------

	function doChangePoly(obj, kdDokter) {
		let kdpoly = jQuery(obj).val();
		jQuery('#kddokter').html('<option value="">- Pilih Dokter -</option>');
		if (typeof kdpoly != "undefined" && kdpoly != "") {
			getDokterJaga({ url : '<?= _BASE_ ?>ajaxload.php', kd_poly : kdpoly, target : '#kddokter', selected : kdDokter });
		}
	}

	//------------------------------------------------------------------------

	jQuery(document).ready(function() {
		// toggle lokasi lakalantas
		doToggleLokasiLakalantas(jQuery('#form-pendaftaran [name="lakalantas"]'));

		// dropdown kota
		jQuery('#form-pendaftaran').on('change', '#id_provinsi', function() {
			var id_prov = jQuery(this).val();
			getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : id_prov, target : '#id_kota', selected : '' });
			jQuery('#id_kota').html('<option value="" selected>- Pilih Kota -</option>');
			jQuery('#id_kecamatan').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kecamatan
		jQuery('#form-pendaftaran').on('change', '#id_kota', function() {
			var id_kota = jQuery(this).val();
			getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : id_kota, target : '#id_kecamatan', selected : '' });
			jQuery('#id_kecamatan').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kelurahan
		jQuery('#form-pendaftaran').on('change', '#id_kecamatan', function() {
			var id_kec = jQuery(this).val();
			getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : id_kec, target : '#id_kelurahan', selected : '' });
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// submit form pendaftaran
		jQuery("#form-pendaftaran").validate({
			submitHandler : function(form) {
				jQuery(form).ajaxSubmit({
					url: '<?= _BASE_ . 'index2.php?link=pendaftaran&c=action_pendaftaran&a=update' ?>',
					dataType : 'json',
					beforeSubmit : function(att, $form, options) {
						jQuery('#form-pendaftaran').find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( typeof r.metadata.code == 'undefined' ) {
							swal('Error', 'Tidak dapat menghubungi server', 'error');
							console.log(r);
						}
						else if ( r.metadata.code != 200 ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							// reset form pendaftaran
							resetPendaftaran();

							let data = r.response;
							
							console.log(data);
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						jQuery('#form-pendaftaran').find('[type="submit"]').button('reset');
					}
				});
			}
		});
	});

	//------------------------------------------------------------------------
</script>