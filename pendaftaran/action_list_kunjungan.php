<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'models/m_pasien.php';
require BASEPATH . 'models/m_auth.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('dataTable') ) 
{

	function dataTable()
	{

		$query = array();
		$query['query'] = '';

		// columns index
		$column_index = array('t1.TGLREG', 'm1.NOMR', 'm1.NAMA', 'm1.JENISKELAMIN', 'm2.nama', 'm3.NAMADOKTER', 't1.SHIFT', 't1.NIP', 'm8.NAMA', 'm9.NAMA', 't1.PASIENBARU', 't1.TGLREG');

		// filter column handler / search
		$columns = $_GET['columns'];

		$query['query'] .= "WHERE (m10.ROLES = 1 OR m10.ROLES = NULL)";

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['query'] .= "AND " . $column_index[1] . " LIKE '%$search_value%'";

		}
		else {

			$i = 0;

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					// if ( $i == 0 ) {
					// 	$query['query'] .= " WHERE ";
					// }
					// else {
						$query['query'] .= " AND ";
					// }

					if ( $column_key == 1 || $column_key == 3 || $column_key == 4 || $column_key == 8 ) {
						$query['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
					}
					else if ( $column_key == 10 ) {
						$val = ( $column_val['search']['value'] === "true" ) ? 1 : 0;
						$query['query'] .= $column_index[$column_key]." = {$val}";
					}
					else if ( $column_key == 0 ) {
						$query['query'] .= "t1.TGLREG >= '" . $column_val['search']['value'] . "'";
					}
					else if ( $column_key == 11 ) {
						$query['query'] .= "t1.TGLREG <= '" . $column_val['search']['value'] . "'";
					}
					else {
						$query['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
					}

					$i++;

				}

			}

		}

		$records_total = Models\count_pendaftaran($query);
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order'] = $column_index[$order_column] . " " . $order_dir;
			}
		}

		// limit
		$query['limit'] = $_GET['length'];
		$query['offset'] = $_GET['start'];

		$data = Models\get_pendaftaran($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('delete') )
{

	function delete()
	{

		$usernameSpv = xss_clean($_GET['usernameSpv']);
		$passwordSpv = xss_clean($_GET['passwordSpv']);
		$idxdaftar = xss_clean($_GET['idxdaftar']);

		$is_supervisor = Models\Auth\is_supervisor($usernameSpv, $passwordSpv);

		$error_msg = array();

		if ( is_null($usernameSpv) OR is_null($passwordSpv) OR $is_supervisor === FALSE ) {

			$error_msg[] = 'Kombinasi username dan password salah. Silahkan hubungi supervisor.';
		
		}

		if ( count($error_msg) > 0 ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "202",
					'message' => show_error($error_msg, TRUE)
				),
				'response' => NULL
			);

		}
		else {

			$result = Models\deleteKunjungan($idxdaftar);

			if ( $result == FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "422",
						'message' => "Server error. Gagal menghapus data."
					),
					'response' => NULL
				);

			}
			else {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "200",
						'message' => "OK"
					),
					'response' => $idxdaftar
				);

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('update') )
{

	function update()
	{

		$usernameSpv = xss_clean($_POST['userspv']);
		$passwordSpv = xss_clean($_POST['pwdspv']);

		$is_supervisor = Models\Auth\is_supervisor($usernameSpv, $passwordSpv);

		$error_msg = array();

		if ( is_null($usernameSpv) OR is_null($passwordSpv) OR $is_supervisor === FALSE ) {

			$error_msg[] = 'Kombinasi user dan password salah. Silahkan hubungi penyelia anda.';
		
		}

		if ( count($error_msg) > 0 ) {

			show_error($error_msg);

		}
		else {

			$idxdaftar     = (isset($_POST['IDXDAFTAR'])) ? xss_clean($_POST['IDXDAFTAR']) : NULL;
			$nomr          = (isset($_POST['NOMR'])) ? xss_clean($_POST['NOMR']) : NULL;
			$nobill        = (isset($_POST['nobill'])) ? xss_clean($_POST['nobill']) : NULL;
			$old_carabayar = (isset($_POST['old_carabayar'])) ? xss_clean($_POST['old_carabayar']) : NULL;
			
			// data administrasi baru
			$kdrujuk           = (isset($_POST['KDRUJUK'])) ? xss_clean($_POST['KDRUJUK']) : NULL;
			$kdcarabayar       = (isset($_POST['KDCARABAYAR'])) ? xss_clean($_POST['KDCARABAYAR']) : NULL;
			$kdpoly            = (isset($_POST['KDPOLY'])) ? xss_clean($_POST['KDPOLY']) : NULL;
			$kddokter          = (isset($_POST['KDDOKTER'])) ? xss_clean($_POST['KDDOKTER']) : NULL;
			$minta_rujukan     = (isset($_POST['MINTA_RUJUKAN'])) ? xss_clean($_POST['MINTA_RUJUKAN']) : NULL;
			$tglreg            = (isset($_POST['TGLREG'])) ? xss_clean($_POST['TGLREG']) : NULL;
			$lakalantas        = (isset($_POST['LAKALANTAS'])) ? xss_clean($_POST['LAKALANTAS']) : NULL;
			$lokasi_lakalantas = (isset($_POST['LOKASI_LAKALANTAS'])) ? xss_clean($_POST['LOKASI_LAKALANTAS']) : NULL;
			$catatan           = (isset($_POST['CATATAN'])) ? xss_clean($_POST['CATATAN']) : NULL;
			$shift             = (isset($_POST['SHIFT'])) ? xss_clean($_POST['SHIFT']) : NULL;

			// data peserta bpjs
			$no_kartu         = (isset($_POST['no_kartu'])) ? xss_clean($_POST['no_kartu']) : NULL;
			$kd_jenis_peserta = (isset($_POST['kd_jenis_peserta'])) ? xss_clean($_POST['kd_jenis_peserta']) : NULL;
			$nm_jenis_peserta = (isset($_POST['nm_jenis_peserta'])) ? xss_clean($_POST['nm_jenis_peserta']) : NULL;
			$kd_kelas         = (isset($_POST['kd_kelas'])) ? xss_clean($_POST['kd_kelas']) : NULL;
			$nm_kelas         = (isset($_POST['nm_kelas'])) ? xss_clean($_POST['nm_kelas']) : NULL;
			$no_rujukan       = (isset($_POST['no_rujukan'])) ? xss_clean($_POST['no_rujukan']) : NULL;
			$tgl_rujukan      = (isset($_POST['tgl_rujukan'])) ? xss_clean($_POST['tgl_rujukan']) : NULL;
			$kd_ppk_rujukan   = (isset($_POST['kd_ppk_rujukan'])) ? xss_clean($_POST['kd_ppk_rujukan']) : NULL;
			$nm_ppk_rujukan   = (isset($_POST['nm_ppk_rujukan'])) ? xss_clean($_POST['nm_ppk_rujukan']) : NULL;
			$kd_diagnosa      = (isset($_POST['kd_diagnosa'])) ? xss_clean($_POST['kd_diagnosa']) : NULL;
			$nm_diagnosa      = (isset($_POST['nm_diagnosa'])) ? xss_clean($_POST['nm_diagnosa']) : NULL;

			if ( $nobill != '' ) {
				$ss = mysql_query("SELECT * FROM t_bayarrajal WHERE nobill = '{$nobill}'");
				if ( mysql_num_rows($ss) > 0 ) {
					$val = mysql_fetch_array($ss);
					$status	= $val['status'];
				}
				else{
					show_error('No. Billing tidak terdaftar');
				}				
			}

			$jenispoly = $kdpoly;
			$kdprofesi = getProfesiDoktor($kddokter);
			$kodetarif = getKodePendaftaran($jenispoly, $kdprofesi);
			$tarif     = getTarif($kodetarif);

			$tarifrs            = $tarif['tarif'];
			$tarifjasasarana    = $tarif['jasa_sarana'];
			$tarifjasapelayanan = $tarif['jasa_pelayanan'];

			try {

				// update carabayar pasien
				$update_pasien = Models\update_pasien(
					array ( 'KDCARABAYAR' => $kdcarabayar ),
					array ( 'NOMR' => $nomr )
				);

				$data_pendaftaran = array (
					'KD_RUJUK' => $kdrujuk,
					'KDCARABAYAR' => $kdcarabayar,
					'KDPOLY' => $kdpoly,
					'KDDOKTER' => $kddokter,
					'MINTA_RUJUKAN' => $minta_rujukan,
					'TGLREG' => $tglreg,
					'JAMREG' => $tglreg,
					'LAKALANTAS' => $lakalantas,
					'LOKASI_LAKALANTAS' => $lokasi_lakalantas,
					'CATATAN' => $catatan,
					'SHIFT' => $shift
				);

				// jika carabayar menggunakan JKN/BPJS
				// update data peserta BPJS pada tabel pendaftaran
				if ( $kdcarabayar == "2" ) {

					$data_pendaftaran['NO_KARTU'] = $no_kartu;
					$data_pendaftaran['KD_JENIS_PESERTA'] = $kd_jenis_peserta;
					$data_pendaftaran['NM_JENIS_PESERTA'] = $nm_jenis_peserta;
					$data_pendaftaran['KD_RUJUK'] = $kd_rujuk;
					$data_pendaftaran['NO_RUJUKAN'] = $no_rujukan;
					$data_pendaftaran['TGL_RUJUKAN'] = $tgl_rujukan;
					$data_pendaftaran['KD_PPK_RUJUKAN'] = $kd_ppk_rujukan;
					$data_pendaftaran['NM_PPK_RUJUKAN'] = $nm_ppk_rujukan;
					$data_pendaftaran['KD_DIAGNOSA'] = $kd_diagnosa;
					$data_pendaftaran['NM_DIAGNOSA'] = $nm_diagnosa;
					$data_pendaftaran['KD_KELAS'] = $kd_kelas;

				}

				// update data pendaftaran
				$update_pendaftaran = Models\update_pendaftaran( $data_pendaftaran, $idxdaftar );
			
			} catch (Exception $e) {
			
				show_error($e->getMessage() . '. ' . $e->getFile() . ' on line ' . $e->getLine());

			}

			mysql_query("UPDATE t_bayarrajal
				         SET UNIT = '{$kdpoly}'
				         WHERE IDXDAFTAR = '{$idxdaftar}'
				         AND NOBILL = '{$nobill}'");
			
			mysql_query("UPDATE t_billrajal
				         SET KDPOLY = '{$kdpoly}',
				             UNIT = '{$kdpoly}',
				             KDDOKTER = '{$kddokter}',
				             CARABAYAR = '{$kdcarabayar}',
				             TARIFRS = '{$tarifrs}',
				             JASA_SARANA = '{$tarifjasasarana}',
				             JASA_PELAYANAN = '{$tarifjasapelayanan}'
				         WHERE IDXDAFTAR = '{$idxdaftar}'
				         AND NOBILL = '{$nobill}'");
			
			if ( $kdcarabayar > 1 ) {

				mysql_query("UPDATE t_bayarrajal
					         SET CARABAYAR = '{$kdcarabayar}',
					             UNIT = '{$kdpoly}',
					             JMBAYAR = 0,
					             TGLBAYAR = CURDATE(),
					             JAMBAYAR = CURTIME(),
					             SHIFT = '{$shift}',
					             LUNAS = 1,
					             STATUS = 'LUNAS'
					         WHERE IDXDAFTAR = '{$idxdaftar}'");

			}
			else {

				if ( $old_carabayar > 1 ) {

					mysql_query("UPDATE t_bayarrajal
						         SET UNIT = '{$kdpoly}',
						             CARABAYAR = '{$kdcarabayar}',
						             JMBAYAR = 0,
						             TGLBAYAR = '0000-00-00',
						             JAMBAYAR = '00:00:00',
						             SHIFT = '{$shift}',
						             LUNAS = 0,
						             STATUS = 'TRX',
						             TARIFRS = '{$tarifrs}',
						             JASA_SARANA = '{$tarifjasasarana}',
						             JASA_PELAYANAN = '{$tarifjasapelayanan}'
						         WHERE IDXDAFTAR = '{$idxdaftar}'
						         AND NOBILL = '{$nobill}'");

				}
				else {

					if ( $status == 'LUNAS' ) {
						
						mysql_query("UPDATE t_bayarrajal
							         SET UNIT = '{$kdpoly}',
							             CARABAYAR = '{$kdcarabayar}',
							             JMBAYAR = '{$tarifrs}',
							             TARIFRS = '{$tarifrs}',
							             JASA_SARANA = '{$tarifjasasarana}',
							             JASA_PELAYANAN = '{$tarifjasapelayanan}'
							         WHERE IDXDAFTAR = '{$idxdaftar}'
							         AND NOBILL = '{$nobill}'");
					
					}
					else {

						mysql_query("UPDATE t_bayarrajal
						             SET UNIT = '{$kdpoly}',
						                 CARABAYAR = '{$kdcarabayar}',
						                 JMBAYAR = 0,
						                 TARIFRS = '{$tarifrs}',
						                 JASA_SARANA = '{$tarifjasasarana}',
						                 JASA_PELAYANAN = '{$tarifjasapelayanan}'
						             WHERE IDXDAFTAR = '{$idxdaftar}'
						             AND NOBILL = '{$nobill}'");

					}

				}

			}

		}

		// redirect kembali ke halaman list kunjungan
		redirect(_BASE_ . 'index.php?link=pendaftaran&c=list_kunjungan');

	}

}

//---------------------------------------------------------------------------------