<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>PENDAFTARAN PASIEN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<form id="form-pendaftaran" class="form-horizontal" method="POST">

			<!--
				pencatatan waktu pendaftaran
				untuk menghitung lama pelayanan
			-->
			<input type="hidden" name="start_daftar" value="" />
			<input type="hidden" name="stop_daftar" value="" />

			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-newspaper-o"></i> Data Administrasi</legend>
							<div class="form-group">
								<label class="control-label col-sm-4">Shift Petugas Jaga</label>
								<div class="col-sm-8">
									<?= formDropdown(array(
										'name' => 'shift',
										'options' => array(
											'' => '- Pilih Shift Petugas -',
											1 => 'Pagi',
											2 => 'Siang',
											3 => 'Sore'
										),
										'selected' => '',
										'attr' => 'class="form-control input-sm required" title="Tidak boleh kosong" onchange="startDaftar()"'
									)) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Jenis Kunjungan</label>
								<div class="col-sm-8">
									<?= formDropdown(array(
										'name' => 'jenis_kunjungan',
										'options' => array(
											true => 'Baru',
											false => 'Lama'
										),
										'selected' => true,
										'attr' => 'id="status-pasien" class="form-control input-sm required" onchange="doChangeStatusPasien(jQuery(this).val())"'
									)) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Nomor Rekam Medis</label>
								<div class="col-sm-8">
									<div id="nomr"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Asal Rujukan</label>
								<div class="col-sm-8">
									<?= dinamycDropdown(array(
										'name' => 'kd_rujuk',
										'table' => 'm_rujukan',
										'key' => 'KODE',
										'value' => 'NAMA',
										'selected' => '',
										'empty_first' => TRUE,
										'first_value' => '- Pilih Rujukan -',
										'attr' => 'class="form-control input-sm required" title="Tidak boleh kosong"'
									)) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Poli Tujuan</label>
								<div class="col-sm-8">
									<div class="container-relative">
										<?= dinamycDropdown(array(
											'name' => 'kdpoly',
											'table' => 'm_poly',
											'key' => 'kode',
											'value' => 'nama',
											'selected' => '',
											'order_by' => 'nama asc',
											'empty_first' => TRUE,
											'first_value' => '- Pilih Poliklinik -',
											'attr' => 'id="kdpoly" class="form-control input-sm select2-single required" title="Tidak boleh kosong" onchange="doChangePoly(this)"'
										)); ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Dokter</label>
								<div class="col-sm-8">
									<div class="container-relative">
										<?= formDropdown(array(
											'name' => 'kddokter',
											'options' => array(
												'' => '- Pilih Dokter -'
											),
											'selected' => '',
											'attr' => 'id="kddokter" class="form-control input-sm select2-single required" title="Tidak boleh kosong"'
										)); ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Minta Rujukan</label>
								<div class="col-sm-8">
									<div class="container-relative">
										<?= formDropdown(array(
											'name' => 'minta_rujukan',
											'options' => array(
												'true' => 'Iya',
												'false' => 'Tidak'
											),
											'selected' => 'false',
											'attr' => 'id="kddokter" class="form-control input-sm required" title="Tidak boleh kosong"'
										)); ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">Tanggal Daftar</label>
								<div class="col-sm-8">
									<input type="text" name="tglreg" class="form-control input-sm datetimepicker required" title="Tidak boleh kosong" value="<?= date('Y-m-d H:i:s') ?>" />
								</div>
							</div>
						</fieldset>
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-male"></i> Diagnosa Awal</legend>
							<div class="form-group">
								<label class="control-label col-sm-4">Kecelakaan Lalulintas</label>
								<div class="col-sm-8">
									<?= formDropdown(array(
										'name' => 'lakalantas',
										'options' => array(
											'' => '- Pilih -',
											1 => 'Ya',
											2 => 'Tidak'
										),
										'selected' => '',
										'attr' => 'class="form-control input-sm required" title="Tidak boleh kosong" onchange="doToggleLokasiLakalantas(this)"'
									)) ?>
								</div>
							</div>
							<div class="form-group">
								<label for="lokasi-lakalantas" class="control-label col-sm-4">Lokasi Lakalantas</label>
								<div class="col-sm-8">
									<input type="text" name="lokasi_lakalantas" id="lokasi-lakalantas" class="form-control input-sm" title="Tidak boleh kosong" disabled="" />
									<p class="help-block">Hanya diisi apabila pasien merupakan korban kecelakaan lalulintas.</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4">ICD-10</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="hidden" name="kd_diagnosa" />
										<input type="text" name="nm_diagnosa" readonly="readonly" class="form-control input-sm required" title="Tidak boleh kosong" />
										<span class="input-group-btn">
											<button type="button" class="btn btn-sm btn-default" id="btn-search-diagnosa" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i>" onclick="showModalSearchDiagnosa(this)"><i class="fa fa-fw fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-sm-6">
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-briefcase"></i> Cara Bayar</legend>
							<div class="form-group">
								<label class="control-label col-sm-4">Cara Bayar</label>
								<div class="col-sm-8">
									<?= dinamycDropdown(array(
										'table' => 'm_carabayar',
										'order_by' => 'ORDERS ASC',
										'name' => 'kdcarabayar',
										'key' => 'KODE',
										'value' => 'NAMA',
										'selected' => '',
										'empty_first' => TRUE,
										'first_value' => '- Pilih Cara Bayar -',
										'attr' => 'class="form-control input-sm required" onchange="doChangeCaraBayar(this)"'
									)) ?>
									<p class="help-block">
										Metode pembayaran ini akan digunakan sebagai metode pembayaran default setiap kali pasien berobat (Masih dapat dirubah oleh petugas pendaftaran).
									</p>
								</div>
							</div>
							<div class="carabayar carabayar-2" style="display:none">
								<div class="form-group">
									<label class="control-label col-sm-4">Metode Pengisian</label>
									<div class="col-sm-8">
										<?= formDropdown(array(
											'name' => 'metode_pengisian',
											'options' => array(
												'' => '- Pilih Metode -',
												'rujukan' => 'Data Rujukan (Online)',
												'manual' => 'Entry Manual (Online)',
												'manual_offline' => 'Offline'
											),
											'selected' => '',
											'attr' => 'class="form-control input-sm required" onchange="doChangeMetodePengisianBpjs(this)"'
										)) ?>
										<p class="help-block">
											Pilih rujukan untuk membaca data dari server berdasarkan nomor rujukan. Pilih manual untuk mengisi data secara manual.
										</p>
									</div>
								</div>
								<div class="form-group search-rujukan" style="display:none">
									<div class="col-sm-8 col-sm-offset-4">
										<button type="button" class="btn btn-info btn-sm" onclick="showModalSearchRujukanBpjs(this)">Cari Data Rujukan</button>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">No. Peserta</label>
									<div class="col-sm-8">
										<div class="input-group">
											<input type="text" name="no_kartu" readonly="readonly" class="form-control input-sm required" />
											<span class="input-group-btn">
												<button type="button" class="btn btn-sm btn-default btn-action" id="btn-search-peserta" disabled="" onclick="showModalSearchPesertaBpjs(this)"><i class="fa fa-fw fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Jenis Peserta</label>
									<div class="col-sm-8">
										<input type="hidden" name="kd_jenis_peserta" readonly="" />
										<input type="text" name="nm_jenis_peserta" class="form-control input-sm required" readonly="" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Kelas Tanggungan</label>
									<div class="col-sm-8">
										<!-- <input type="hidden" name="kd_kelas" readonly="" />
										<input type="text" name="nm_kelas" class="form-control input-sm required" readonly="" /> -->
										<?= formDropdown(array(
											'name' => 'kd_kelas',
											'options' => array(
												'' => '- Pilih -',
												'1' => 'KELAS 1',
												'2' => 'KELAS 2',
												'3' => 'KELAS 3'
											),
											'selected' => '',
											'attr' => 'class="form-control input-sm required" readonly=""'
										)) ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Provider</label>
									<div class="col-sm-8">
										<input type="hidden" name="kd_provider" readonly="" />
										<input type="text" name="nm_provider" class="form-control input-sm required" readonly="" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">No. Rujukan</label>
									<div class="col-sm-8">
										<input type="text" name="no_rujukan" class="form-control input-sm required" readonly="" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Tgl. Rujukan</label>
									<div class="col-sm-8">
										<input type="text" name="tgl_rujukan" value="<?= date('Y-m-d H:i') ?>" class="form-control input-sm datetimepicker required" readonly="" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Provider Pemberi Rujukan</label>
									<div class="col-sm-8">
										<input type="hidden" name="kd_ppk_rujukan" readonly="">
										<div class="input-group">
											<input type="text" name="nm_ppk_rujukan" readonly="" class="form-control input-sm required" />
											<span class="input-group-btn">
												<button type="button" class="btn btn-sm btn-default btn-action" id="btn-search-provider-bpjs" disabled="" onclick="showModalSearchProviderBpjs(this)"><i class="fa fa-fw fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Catatan</label>
									<div class="col-sm-8">
										<input type="text" name="catatan" class="form-control input-sm" readonly="" />
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<hr class="separator" />
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-user"></i> Identitas Pasien</legend>
							<div class="form-group">
								<label for="nama" class="control-label col-sm-4">Nama Pasien</label>
								<div class="col-sm-5">
									<input type="text" name="nama" id="nama" class="form-control input-sm required" />
								</div>
								<label for="title" class="control-label col-sm-1">Title</label>
								<div class="col-sm-2">
									<?= formDropdown(array(
										'name' => 'title',
										'options' => array(
											'' => '- Pilih -',
											'Tn' => 'Tn',
											'Ny' => 'Ny',
											'Nn' => 'Nn',
											'An' => 'An',
											'By' => 'By'
										),
										'selected' => '',
										'attr' => 'class="form-control input-sm required" title="Tidak boleh kosong" id="title"'
									)) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="tmp_lahir">Tempat & Tgl. Lahir</label>
								<div class="col-md-4">
									<input type="text" value="" id="tmp_lahir" name="tmp_lahir" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
								<div class="col-md-4">
									<input type="text" value="" id="tgl_lahir" name="tgl_lahir" class="form-control input-sm datepicker required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="nik">No. Induk Kependudukan</label>
								<div class="col-md-8">
									<input type="text" value="" id="nik" name="nik" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="notelp">No. Telephone / HP</label>
								<div class="col-md-8">
									<input type="text" value="" id="notelp" name="notelp" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="pekerjaan_pasien">Pekerjaan Pasien</label>
								<div class="col-md-8">
									<input type="text" value="" id="pekerjaan_pasien" name="pekerjaan_pasien" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
						</fieldset>
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-home"></i> Alamat Pasien</legend>
							<div class="form-group">
								<label class="col-md-4 control-label" for="alamat">Alamat Pasien</label>
								<div class="col-md-8">
									<input type="text" value="" id="alamat" name="alamat" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="alamat_ktp">Alamat <i>(Sesuai KTP)</i></label>
								<div class="col-md-8">
									<input type="text" value="" id="alamat_ktp" name="alamat_ktp" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="id_provinsi">Provinsi</label>
								<div class="col-md-8">
									<div class="container-relative">
										<?= dinamycDropdown(array(
											'name' => 'id_provinsi',
											'table' => 'm_provinsi',
											'key' => 'idprovinsi',
											'value' => 'namaprovinsi',
											'selected' => '',
											'empty_first' => TRUE,
											'first_value' => '- Pilih Provinsi -',
											'order_by' => 'namaprovinsi asc',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_provinsi"',
										)) ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="id_kota">Kabupaten/Kota</label>
								<div class="col-md-8">
									<div class="container-relative">
										<?= formDropdown(array(
											'name' => 'id_kota',
											'options' => array(
												'' => '- Pilih Kota -'
											),
											'selected' => '',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_kota"',
										)) ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="id_kecamatan">Kecamatan</label>
								<div class="col-md-8">
									<div class="container-relative">
										<?= formDropdown(array(
											'name' => 'id_kecamatan',
											'options' => array(
												'' => '- Pilih Kecamatan -'
											),
											'selected' => '',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_kecamatan"',
										)) ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="id_kelurahan">Desa/Kelurahan</label>
								<div class="col-md-8">
									<div class="container-relative">
										<?= formDropdown(array(
											'name' => 'id_kelurahan',
											'options' => array(
												'' => '- Pilih Kelurahan -'
											),
											'selected' => '',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_kelurahan"',
										)) ?>
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-user-plus"></i> Orangtua</legend>
							<div class="form-group">
								<label class="col-md-4 control-label" for="nama_ayah">Nama Ayah</label>
								<div class="col-md-8">
									<input type="text" value="" id="nama_ayah" name="nama_ayah" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="pekerjaan_ayah">Pekerjaan Ayah</label>
								<div class="col-md-8">
									<input type="text" value="" id="pekerjaan_ayah" name="pekerjaan_ayah" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="nama_ibu">Nama Ibu</label>
								<div class="col-md-8">
									<input type="text" value="" id="nama_ibu" name="nama_ibu" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="pekerjaan_ibu">Pekerjaan Ibu</label>
								<div class="col-md-8">
									<input type="text" value="" id="pekerjaan_ibu" name="pekerjaan_ibu" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
						</fieldset>
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-user-plus"></i> Pasangan</legend>
							<div class="form-group">
								<label class="col-md-4 control-label" for="nama_pasangan">Nama Suami/Istri</label>
								<div class="col-md-8">
									<input type="text" value="" id="nama_pasangan" name="nama_pasangan" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="pekerjaan_pasangan">Pekerjaan Suami/Istri</label>
								<div class="col-md-8">
									<input type="text" value="" id="pekerjaan_pasangan" name="pekerjaan_pasangan" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-sm-6">
						<fieldset class="lumen">
							<div class="form-group">
								<label class="col-md-4 control-label">Jenis Kelamin</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'jenis_kelamin',
										'options' => array (
											'L' => 'Laki - Laki',
											'P' => 'Perempuan',
										),
										'selected' => '',
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Status Perkawinan</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'status',
										'options' => array (
											'1' => 'Belum Kawin',
											'2' => 'Kawin',
											'3' => 'Janda / Duda',
										),
										'selected' => '',
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Pendidikan Terakhir</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'pendidikan',
										'options' => array (
											'0' => 'Belum Sekolah',
											'1' => 'SD',
											'2' => 'SLTP',
											'3' => 'SMU',
											'4' => 'D3/Akademik',
											'5' => 'Universitas',
										),
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Agama</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'agama',
										'options' => array (
											'1' => 'Islam',
											'2' => 'Kristen Protestan',
											'3' => 'Khatolik',
											'4' => 'Hindu',
											'5' => 'Budha',
											'6' => 'Lain - Lain',
										),
										'selected' => '',
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
						</fieldset>
						<fieldset class="lumen">
							<legend><i class="fa fa-fw fa-user-plus"></i> Penanggung Jawab</legend>
							<div class="form-group">
								<label class="col-md-4 control-label" for="penanggungjawab_nama">Nama Penanggung Jawab</label>
								<div class="col-md-8">
									<input type="text" value="" id="penanggungjawab_nama" name="penanggungjawab_nama" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="penanggungjawab_hubungan">Hubungan Dengan Pasien</label>
								<div class="col-md-8">
									<input type="text" value="" id="penanggungjawab_hubungan" name="penanggungjawab_hubungan" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="penanggungjawab_alamat">Alamat</label>
								<div class="col-md-8">
									<input type="text" value="" id="penanggungjawab_alamat" name="penanggungjawab_alamat" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="penanggungjawab_phone">No. Telephone / HP</label>
								<div class="col-md-8">
									<input type="text" value="" id="penanggungjawab_phone" name="penanggungjawab_phone" class="form-control input-sm required" title="Tidak boleh kosong" />
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<button type="submit" class="btn btn-sm btn-primary" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> Tunggu">Simpan</button>
							<button type="button" class="btn btn-sm btn-default" onclick="doResetPendaftaran()">Batal</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- includes -->
<?php require 'includes/modals/modal_search_pasien.php' ?>
<?php require 'includes/modals/modal_search_diagnosa.php' ?>
<?php require 'includes/modals/modal_search_rujukan_bpjs.php' ?>
<?php require 'includes/modals/modal_search_peserta_bpjs.php' ?>
<?php require 'includes/modals/modal_search_provider_bpjs.php' ?>
<?php require 'includes/modals/modal_pendaftaran_berhasil.php' ?>

<!-- widgets -->
<?php require BASEPATH . 'widgets/pendaftaran_iso/widget.php' ?>

<!-- javascript -->
<?php require 'includes/js/pendaftaran.js.php' ?>