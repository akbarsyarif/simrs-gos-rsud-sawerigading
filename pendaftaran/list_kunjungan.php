<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>DAFTAR KUNJUNGAN PASIEN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<?php if ( $_SESSION['ROLES'] == 1 ) : ?>
					<a href="<?= _BASE_ . 'index.php?link=pendaftaran&c=pendaftaran' ?>" class="btn btn-sm btn-primary">Pendaftaran Pasien</a>
				<?php endif; ?>
				<button href="#modal-search-kunjungan" data-toggle="modal" class="btn btn-sm btn-default">Cari Data Kunjungan</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered" id="datatable-daftar-kunjungan">
					<thead>
						<th width="60">Tanggal</th>
						<th width="45">No. RM</th>
						<th>Nama</th>
						<th width="10">L/P</th>
						<th width="100">Poly</th>
						<th>Dokter</th>
						<th width="50">Shift</th>
						<th width="80">Petugas</th>
						<th width="100">Cara Bayar</th>
						<th width="100">Rujukan</th>
						<th width="10">B/L</th>
						<th><!-- S/D --></th>
						<?php if ( $_SESSION['ROLES'] == 1 ) : ?>
						<th width="10"></th>
						<th width="10"></th>
						<?php endif; ?>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modals/modal_search_kunjungan.php' ?>

<!-- javascript -->
<?php require 'includes/js/list_kunjungan.js.php' ?>