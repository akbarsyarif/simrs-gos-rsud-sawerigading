<?php
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'include/pasien_helper.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'models/m_lap_rawat_jalan.php';

//---------------------------------------------------------------------------------

/**
 * download laporan dalam format spreadsheet
 */
if ( ! function_exists('download') )
{
	function download()
	{
		$format = (isset($_GET['format']) && !empty($_GET['format'])) ? xss_clean($_GET['format']) : NULL;

		$kd_poly = (isset($_GET['kd_poly']) && !empty($_GET['kd_poly'])) ? xss_clean($_GET['kd_poly']) : NULL;
		$kd_carabayar = (isset($_GET['kd_carabayar']) && !empty($_GET['kd_carabayar'])) ? xss_clean($_GET['kd_carabayar']) : NULL;
		$jenis_kepesertaan = (isset($_GET['jenis_kepesertaan']) && !empty($_GET['jenis_kepesertaan'])) ? xss_clean($_GET['jenis_kepesertaan']) : NULL;
		$begin_date = (isset($_GET['begin_date']) && !empty($_GET['begin_date'])) ? xss_clean($_GET['begin_date']) : date('Y-m-d');
		$end_date = (isset($_GET['end_date']) && !empty($_GET['end_date'])) ? xss_clean($_GET['end_date']) : date('Y-m-d');

		// validation
		$error = array();

		if ( is_null($begin_date) ) {
			$error[] = 'Tanggal awal tidak boleh kosong';
		}

		if ( is_null($end_date) ) {
			$error[] = 'Tanggal akhir tidak boleh kosong';
		}

		if ( count($error) > 0 ) {

			show_error($error);
		
		} else {

			$dataPasien = Models\LapRawatJalan\dataPasien($kd_poly, $kd_carabayar, $jenis_kepesertaan, $begin_date, $end_date);

			if ( $dataPasien == FALSE ) {

				show_error('Tidak ada data pasien ditemukan');

			} else {

				switch ($format) {
					case 'xls':
						download_xls($dataPasien, $kd_poly, $begin_date, $end_date);
						break;
					
					default:
						show_error('Format file tidak dikenali.');
						break;
				}

			}

		}

	}
}

if ( ! function_exists('download_xls') )
{
	function download_xls($dataPasien, $kd_poly, $begin_date, $end_date)
	{

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('Laporan Data Pasien Rawat Jalan');

		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN DATA PASIEN RAWAT JALAN');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'RUMAH SAKIT UMUM DAERAH SAWERIGADING');
		$objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');

		// set column style
		$objPHPExcel->getActiveSheet()->getStyle("A7:H7")->getFont()->setBold(true);

		// settings column width
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(9);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(34);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(34);

		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Dari Tanggal');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Sampai Tanggal');

		$objPHPExcel->getActiveSheet()->setCellValue('C4', date('d/m/Y', strtotime($begin_date)));
		$objPHPExcel->getActiveSheet()->setCellValue('C5', date('d/m/Y', strtotime($end_date)));

		// table header
		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'No');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Tgl. Pelayanan');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'No. RM');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Nama Pasien');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Diagnosa');
		$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Tindakan');
		$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Poli');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Dokter');

		$objPHPExcel->getActiveSheet()->getStyle("A7:H7")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle("A7:H7")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$index_row = 8;
		$no = 1;

		foreach ($dataPasien['rows'] as $row) {

			$objPHPExcel->getActiveSheet()->setCellValue("A{$index_row}", $no);
			$objPHPExcel->getActiveSheet()->setCellValue("B{$index_row}", $row['TGLREG_INDONESIA']);
			$objPHPExcel->getActiveSheet()->setCellValue("C{$index_row}", $row['NOMR']);
			$objPHPExcel->getActiveSheet()->setCellValue("D{$index_row}", strtoupper($row['NAMA_PASIEN']));
			$objPHPExcel->getActiveSheet()->setCellValue("E{$index_row}", strtoupper(clearDiagnosaStr($row['DIAGNOSA'])));
			$objPHPExcel->getActiveSheet()->setCellValue("F{$index_row}", strtoupper(arrTindakanToStr($row['TINDAKAN'])));
			$objPHPExcel->getActiveSheet()->setCellValue("G{$index_row}", $row['NAMA_POLY']);
			$objPHPExcel->getActiveSheet()->setCellValue("H{$index_row}", trim($row['NAMA_DOKTER']));

			$objPHPExcel->getActiveSheet()->getStyle("A$index_row:H$index_row")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A$index_row:H$index_row")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			$objPHPExcel->getActiveSheet()->getStyle("A$index_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("B$index_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("C$index_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("G$index_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:H{$index_row}")->getAlignment()->setWrapText(true);
			
			$index_row++;
			$no++;

		}

		$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B SIMRS - RSUD Sawerigading' . '&R Page &P of &N');

		$filename = 'Laporan Data Pasien Rawat Jalan.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}