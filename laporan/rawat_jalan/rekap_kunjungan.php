<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_poly.php';
require BASEPATH . 'models/m_rekap_kunjungan.php';

// filter
// get query string
$begin_date = (isset($_POST['begin_date']) && !empty($_POST['begin_date'])) ? xss_clean($_POST['begin_date']) : date('Y-m-d');
$end_date = (isset($_POST['end_date']) && !empty($_POST['end_date'])) ? xss_clean($_POST['end_date']) : date('Y-m-d');
?>

<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>LAPORAN REKAP KUNJUNGAN RAWAT JALAN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<button href="#modal-search-laporan-pendaftaran-rawat-jalan" data-toggle="modal" class="btn btn-sm btn-default">Filter Data Kunjungan</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-rekap">
					<tr>
						<th width="200">Periode</th>
						<td class="text-center"><?= is_null($begin_date) ? '<i class="text-danger">Belum dipilih</i>' : "<span class='text-success'>{$begin_date}</span>" ?></td>
						<td class="no-gap">-</td>
						<td class="text-center"><?= is_null($end_date) ? '<i class="text-danger">Belum dipilih</i>' : "<span class='text-success'>{$end_date}</span>" ?></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php
				// poli spesialis
				$query = array('query' => " WHERE jenispoly = 1", 'limit' => 100);
				$poly_spesialis = Models\Poly\getPoly($query);
				$count_poly_spesialis = count($poly_spesialis);

				$kunjungan_poly_spesialis = Models\RekapKunjungan\rawatJalanByPoly(1, $begin_date, $end_date);
				?>

				<?php
				// poli non spesialis
				$query = array('query' => " WHERE jenispoly = 0", 'limit' => 100);
				$poly_non_spesialis = Models\Poly\getPoly($query);
				$count_poly_non_spesialis = count($poly_non_spesialis);

				$kunjungan_poly_non_spesialis = Models\RekapKunjungan\rawatJalanByPoly(0, $begin_date, $end_date);
				?>

				<div class="table-responsive">
					<table class="table table-bordered table-rekap">
						<tr>
							<th colspan="50">Berdasarkan Poly</th>
						</tr>
						<tr>
							<th colspan="<?= $count_poly_spesialis ?>">Spesialis</th>
							<th rowspan="<?= ($count_poly_spesialis > 0) ? "2" : "0" ?>">Total</th>
							<th colspan="<?= $count_poly_non_spesialis ?>">Non Spesialis</th>
							<th rowspan="<?= ($count_poly_non_spesialis > 0) ? "2" : "0" ?>">Total</th>
						</tr>
						<tr>
							<!-- poly spesialis -->
							<?php foreach ( $poly_spesialis AS $poly ) : ?>
								<th><?= $poly['nama'] ?></th>
							<?php endforeach; ?>

							<!-- poly non spesialis -->
							<?php foreach ( $poly_non_spesialis AS $poly ) : ?>
								<th><?= $poly['nama'] ?></th>
							<?php endforeach; ?>
						</tr>
						<tr>
							<!-- rekap kunjungan poly spesialis -->
							<?php foreach ( $kunjungan_poly_spesialis AS $kunjungan ) : ?>
								<td class="text-center"><?= $kunjungan['total_kunjungan'] ?></td>
							<?php endforeach; ?>

							<td class="text-center"><?= $kunjungan_poly_spesialis[0]['total_keseluruhan'] ?></td>

							<!-- rekap kunjungan poly non spesialis -->
							<?php foreach ( $kunjungan_poly_non_spesialis AS $kunjungan ) : ?>
								<td class="text-center"><?= $kunjungan['total_kunjungan'] ?></td>
							<?php endforeach; ?>

							<td class="text-center"><?= $kunjungan_poly_non_spesialis[0]['total_keseluruhan'] ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php
				// jenis kunjungan
				$rekap_jenis_kunjungan = Models\RekapKunjungan\rawatJalanByJenisKunjungan($begin_date, $end_date);
				?>

				<div class="table-responsive">
					<table class="table table-bordered table-rekap">
						<tr>
							<th colspan="50">Berdasarkan Jenis Kunjungan</th>
						</tr>
						<tr>
							<th colspan="2">Jenis Kunjungan</th>
							<th rowspan="2">Total</th>
						</tr>
						<tr>
							<th>Baru</th>
							<th>Lama</th>
						</tr>
						<tr>
							<?php if ( $rekap_jenis_kunjungan == FALSE ) : ?>
								<td class="text-center">0</td>
								<td class="text-center">0</td>
							<?php else : ?>
								<?php foreach ( $rekap_jenis_kunjungan AS $kunjungan ) : ?>
									<td class="text-center"><?= $kunjungan['total_kunjungan'] ?></td>
								<?php endforeach; ?>
							<?php endif; ?>

							<td class="text-center"><?= $rekap_jenis_kunjungan[0]['total_keseluruhan'] ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php
				// kunjungan berdasarkan carabayar
				$rekap_cara_bayar = Models\RekapKunjungan\rawatJalanByCaraBayar($begin_date, $end_date);
				$count_rekap_cara_bayar = count($rekap_cara_bayar);
				?>

				<?php
				// rekap kunjungan JKN
				$rekap_jkn = Models\RekapKunjungan\rekapKunjunganJKN($begin_date, $end_date);
				$count_rekap_jkn = count($rekap_jkn);

				$colspan = $count_rekap_jkn + $count_rekap_cara_bayar - 1;
				?>

				<div class="table-responsive">
					<table class="table table-bordered table-rekap">
						<tr>
							<th colspan="50">Berdasarkan Cara Bayar</th>
						</tr>
						<tr>
							<th colspan="<?= $colspan ?>">Cara Bayar</th>
							<th rowspan="3">Total</th>
						</tr>
						<tr>
							<?php foreach ( $rekap_cara_bayar AS $kunjungan ) : ?>
								<th <?= $kunjungan['kode_carabayar'] != 2 ? 'rowspan="2"' : '' ?> <?= $kunjungan['kode_carabayar'] == 2 ? 'colspan="'.$count_rekap_jkn.'"' : '' ?>><?= $kunjungan['nama'] ?></th>
							<?php endforeach; ?>
						</tr>
						<tr>
							<?php foreach ( $rekap_jkn AS $jkn ) : ?>
								<th><?= $jkn['nama'] ?></th>
							<?php endforeach; ?>
						</tr>
						<tr>
							<?php foreach ( $rekap_cara_bayar AS $kunjungan ) : ?>
								<?php if ($kunjungan['kode_carabayar'] == 2) : ?>
									<?php if ( $rekap_jkn == FALSE ) : ?>
										<td class="text-center">0</td>
									<?php else : ?>
										<?php foreach ( $rekap_jkn AS $jkn ) : ?>
											<td class="text-center"><?= $jkn['total_kunjungan'] ?></td>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php else : ?>
									<td class="text-center"><?= $kunjungan['total_kunjungan'] ?></td>
								<?php endif; ?>
							<?php endforeach; ?>

							<td class="text-center"><?= $rekap_cara_bayar[0]['total_keseluruhan'] ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modals/modal_search_laporan_pendaftaran_rawat_jalan.php' ?>