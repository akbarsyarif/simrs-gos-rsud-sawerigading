<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_lap_rawat_jalan.php';

// filter
// get query string
$kd_poly = (isset($_POST['kd_poly']) && !empty($_POST['kd_poly'])) ? xss_clean($_POST['kd_poly']) : NULL;
$kd_carabayar = (isset($_POST['kd_carabayar']) && !empty($_POST['kd_carabayar'])) ? xss_clean($_POST['kd_carabayar']) : NULL;
$jenis_kepesertaan = (isset($_POST['jenis_kepesertaan']) && !empty($_POST['jenis_kepesertaan'])) ? xss_clean($_POST['jenis_kepesertaan']) : NULL;
$begin_date = (isset($_POST['begin_date']) && !empty($_POST['begin_date'])) ? xss_clean($_POST['begin_date']) : date('Y-m-d');
$end_date = (isset($_POST['end_date']) && !empty($_POST['end_date'])) ? xss_clean($_POST['end_date']) : date('Y-m-d');
?>

<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>LAPORAN PASIEN RAWAT JALAN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<button href="#modal-search-laporan-pendaftaran-rawat-jalan" data-toggle="modal" class="btn btn-sm btn-default">Filter Data Kunjungan</button>
			</div>
			<div class="col-md-6 text-right">
				<a href="<?= _BASE_ . 'index2.php?link=laporan/rawat_jalan&c=action_data_pasien&a=download&format=xls&kd_poly='.$kd_poly.'&kd_carabayar='.$kd_carabayar.'&jenis_kepesertaan='.$jenis_kepesertaan.'&begin_date='.$begin_date.'&end_date='.$end_date ?>" class="btn btn-sm btn-default">Download Excel</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-rekap">
					<tr>
						<th width="200">Poli</th>
						<td class="text-center" colspan="3"><?= is_null($kd_poly) ? '<i class="text-success">Semua Poli</i>' : "<span class='text-success'>". getPoly($kd_poly) ."</span>" ?></td>
					</tr>
					<tr>
						<th width="200">Periode</th>
						<td class="text-center"><?= is_null($begin_date) ? '<i class="text-danger">Belum dipilih</i>' : "<span class='text-success'>{$begin_date}</span>" ?></td>
						<td class="no-gap">-</td>
						<td class="text-center"><?= is_null($end_date) ? '<i class="text-danger">Belum dipilih</i>' : "<span class='text-success'>{$end_date}</span>" ?></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php
				// jenis kunjungan
				$dataPasien = Models\LapRawatJalan\dataPasien($kd_poly, $kd_carabayar, $jenis_kepesertaan, $begin_date, $end_date);
				?>

				<?php if ( $dataPasien == FALSE ) : ?>
					<div class="alert alert-warning text-center">
						Tidak ada data pasien ditemukan
					</div>
				<?php else : ?>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th width="110">Tgl. Pelayanan</th>
								<th width="70">No. RM</th>
								<th width="200">Nama</th>
								<th width="300">Diagnosa</th>
								<th>Tindakan</th>
								<th width="100">Poli</th>
								<th width="200">Dokter</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$poly = '';
								$jumlah_pasien_per_poly = 0;
							?>

							<?php $no = 1; foreach ( $dataPasien['rows'] as $row ) : ?>

								<?php if ( $row['NAMA_POLY'] != $poly ) : ?>
									<?php $poly = $row['NAMA_POLY']; ?>
									<tr style="background:#DEDEDE">
										<th colspan="8">Poli : <?= $row['NAMA_POLY'] ?></th>
									</tr>
									<?php
										$jumlah_pasien_per_poly = count(array_filter($dataPasien['rows'], function($var) use ($poly) {
											return $poly == $var['NAMA_POLY'];
										}));
									?>
									<tr style="background:#B7B7B7">
										<th colspan="8">Jumlah Pasien : <?= $jumlah_pasien_per_poly ?></th>
									</tr>
								<?php endif; ?>

								<tr>
									<td><?= $no; ?></td>
									<td align="center"><?= $row['TGLREG_INDONESIA'] ?></td>
									<td><?= $row['NOMR'] ?></td>
									<td><?= $row['NAMA_PASIEN'] ?></td>
									<td><?= strip_tags(trim($row['DIAGNOSA']), "<br><div><p>") ?></td>
									<td>
										<?php if ( count($row['TINDAKAN']) > 0 ) : ?>
											<ul type="square">
												<?php foreach ( $row['TINDAKAN']['rows'] as $tindakan ) : ?>
													<li><?= $tindakan['NAMA_TINDAKAN'] ?></li>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>
									</td>
									<td><?= $row['NAMA_POLY'] ?></td>
									<td><?= $row['NAMA_DOKTER'] ?></td>
								</tr>

							<?php
								$jumlah_pasien_per_poly++;
								$no++;
								endforeach;
							?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modals/modal_filter_lap_data_pasien.php' ?>