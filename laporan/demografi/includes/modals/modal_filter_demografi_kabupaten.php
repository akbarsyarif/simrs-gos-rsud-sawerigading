<div class="modal fade" id="modal-filter-demografi-kabupaten">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-filter-demografi-kabupaten" method="post" action="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-search-plus"></i> Filter Data Kunjungan</h4>
				</div>
				<div class="modal-body">
					<div class="row row-sm">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Mulai Tanggal</label>
								<input type="text" value="<?= $begin_date ?>" name="begin_date" class="form-control datepicker" data-column="0" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Sampai Tanggal</label>
								<input type="text" value="<?= $end_date ?>" name="end_date" class="form-control datepicker" data-column="11" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="clear" class="btn btn-warning btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Filter</button>
				</div>
			</form>
		</div>
	</div>
</div>