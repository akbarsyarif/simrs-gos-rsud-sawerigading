<script type="text/javascript">
	function dropdownKota(id_prov) {
		getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : id_prov, target : '#idkota', selected : '<?= $idkota ?>' });
		jQuery('#idkota').html('<option value="" selected>- Pilih Kota -</option>');
	}

	jQuery(document).ready(function() {
		// call dropdown kota
		dropdownKota(function() {
			let id_prov = jQuery('#form-filter-demografi-kecamatan').find('#idprovinsi').val();
			return id_prov;
		});

		// dropdown kota
		jQuery('#form-filter-demografi-kecamatan').on('change', '#idprovinsi', function() {
			let id_prov = jQuery(this).val();
			dropdownKota(id_prov);
		});
	});
</script>