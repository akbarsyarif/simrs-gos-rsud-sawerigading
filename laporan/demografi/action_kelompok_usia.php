<?php
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'include/pasien_helper.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'models/m_demografi.php';

//---------------------------------------------------------------------------------

/**
 * download laporan dalam format spreadsheet
 */
if ( ! function_exists('download') )
{
	function download()
	{
		$format = (isset($_GET['format']) && !empty($_GET['format'])) ? xss_clean($_GET['format']) : NULL;

		$begin_date = (isset($_GET['begin_date']) && !empty($_GET['begin_date'])) ? xss_clean($_GET['begin_date']) : NULL;
		$end_date   = (isset($_GET['end_date']) && !empty($_GET['end_date'])) ? xss_clean($_GET['end_date']) : NULL;

		// validation
		$error = array();

		if ( is_null($begin_date) ) {
			$error[] = 'Tanggal awal tidak boleh kosong';
		}

		if ( is_null($end_date) ) {
			$error[] = 'Tanggal akhir tidak boleh kosong';
		}

		if ( count($error) > 0 ) {

			show_error($error);
		
		} else {

			$demografi = Models\Demografi\kelompokUsia($begin_date, $end_date);

			if ( $demografi == FALSE ) {

				show_error('Tidak ada data ditemukan');

			} else {

				switch ($format) {
					case 'xls':
						download_xls($demografi, $begin_date, $end_date);
						break;
					
					default:
						show_error('Format file tidak dikenali.');
						break;
				}

			}

		}

	}
}

if ( ! function_exists('download_xls') )
{
	function download_xls($demografi, $begin_date, $end_date)
	{

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('Berdasarkan Umur');

		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'REKAPITULASI KUNJUNGAN PASIEN BERDASARKAN UMUR');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'RUMAH SAKIT UMUM DAERAH SAWERIGADING');
		$objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');

		// set column style
		$objPHPExcel->getActiveSheet()->getStyle("A7:E7")->getFont()->setBold(true);

		// settings column width
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);

		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Dari Tanggal');
		$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Sampai Tanggal');

		$objPHPExcel->getActiveSheet()->setCellValue('C4', date('d/m/Y', strtotime($begin_date)));
		$objPHPExcel->getActiveSheet()->setCellValue('C5', date('d/m/Y', strtotime($end_date)));

		// table header
		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'No');
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Umur');
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Laki-Laki');
		$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Perempuan');
		$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Total');

		$objPHPExcel->getActiveSheet()->getStyle("A7:E7")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle("A7:E7")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$index_row = 8;
		$no = 1;
		$total_lakilaki = 0;
		$total_perempuan = 0;
		$total_keseluruhan = 0;

		foreach ($demografi['rows'] as $row) {

			$objPHPExcel->getActiveSheet()->setCellValue("A{$index_row}", $no);
			$objPHPExcel->getActiveSheet()->setCellValue("B{$index_row}", $row['KELOMPOK_USIA_1']);
			$objPHPExcel->getActiveSheet()->setCellValue("C{$index_row}", $row['LAKILAKI']);
			$objPHPExcel->getActiveSheet()->setCellValue("D{$index_row}", $row['PEREMPUAN']);
			$objPHPExcel->getActiveSheet()->setCellValue("E{$index_row}", $row['JUMLAH']);

			$objPHPExcel->getActiveSheet()->getStyle("A$index_row:E$index_row")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A$index_row:E$index_row")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

			$objPHPExcel->getActiveSheet()->getStyle("A$index_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("C{$index_row}:E{$index_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getAlignment()->setWrapText(true);
			
			$total_lakilaki += $row['LAKILAKI'];
			$total_perempuan += $row['PEREMPUAN'];
			$total_keseluruhan += $row['JUMLAH'];
			$index_row++;
			$no++;

		}

		// menghitung persentase
		$persentase_lakilaki = round(($total_lakilaki / $total_keseluruhan) * 100, 2);
		$persentase_perempuan = round(($total_perempuan / $total_keseluruhan) * 100, 2);

		// rekap total
		$objPHPExcel->getActiveSheet()->setCellValue("A{$index_row}", 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue("C{$index_row}", $total_lakilaki);
		$objPHPExcel->getActiveSheet()->setCellValue("D{$index_row}", $total_perempuan);
		$objPHPExcel->getActiveSheet()->setCellValue("E{$index_row}", $total_keseluruhan);

		$objPHPExcel->getActiveSheet()->mergeCells("A{$index_row}:B{$index_row}");
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getFont()->setBold(true);

		$index_row++;

		// persentase
		$objPHPExcel->getActiveSheet()->setCellValue("A{$index_row}", 'Persentase');
		$objPHPExcel->getActiveSheet()->setCellValue("C{$index_row}", "{$persentase_lakilaki}%");
		$objPHPExcel->getActiveSheet()->setCellValue("D{$index_row}", "{$persentase_perempuan}%");
		$objPHPExcel->getActiveSheet()->setCellValue("E{$index_row}", "100%");

		$objPHPExcel->getActiveSheet()->mergeCells("A{$index_row}:B{$index_row}");
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$objPHPExcel->getActiveSheet()->getStyle("A{$index_row}:E{$index_row}")->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B SIMRS - RSUD Sawerigading' . '&R Page &P of &N');

		$filename = 'Rekap Kunjungan Pasien Berdasarkan Umur.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  

		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
}