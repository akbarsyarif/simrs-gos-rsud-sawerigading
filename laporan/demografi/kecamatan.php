<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_demografi.php';
require BASEPATH . 'models/m_daerah.php';

// filter
// get query string
$begin_date = (isset($_REQUEST['begin_date']) && !empty($_REQUEST['begin_date'])) ? xss_clean($_REQUEST['begin_date']) : date('Y-m-d');
$end_date = (isset($_REQUEST['end_date']) && !empty($_REQUEST['end_date'])) ? xss_clean($_REQUEST['end_date']) : date('Y-m-d');
$idprovinsi = (isset($_REQUEST['idprovinsi']) && !empty($_REQUEST['idprovinsi'])) ? xss_clean($_REQUEST['idprovinsi']) : NULL;
$idkota = (isset($_REQUEST['idkota']) && !empty($_REQUEST['idkota'])) ? xss_clean($_REQUEST['idkota']) : NULL;

$provinsi = '';
$kota = '';

// get kabupaten/kota
$getKota = Models\get_kota(array(
	'query' => "WHERE idkota = '{$idkota}'",
	'limit' => 1
));

if ( $getKota !== FALSE ) {
	$provinsi = $getKota[0]['namaprovinsi'];
	$kota = $getKota[0]['namakota'];
}
?>

<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>REKAPITULASI KUNJUNGAN PASIEN BERDASARKAN KECAMATAN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<button href="#modal-filter-demografi-kabupaten" data-toggle="modal" class="btn btn-sm btn-default">Filter Data Kunjungan</button>
			</div>
			<div class="col-md-6 text-right">
				<a href="<?= _BASE_ . 'index2.php?link=laporan/demografi&c=action_kecamatan&a=download&format=xls&idkota='.$idkota.'&begin_date='.$begin_date.'&end_date='.$end_date ?>" class="btn btn-sm btn-default">Download Excel</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-rekap">
					<tr>
						<th width="200">Periode</th>
						<td class="text-center"><?= is_null($begin_date) ? '<i class="text-danger">Belum dipilih</i>' : "<span class='text-success'>{$begin_date}</span>" ?></td>
						<td class="no-gap">-</td>
						<td class="text-center"><?= is_null($end_date) ? '<i class="text-danger">Belum dipilih</i>' : "<span class='text-success'>{$end_date}</span>" ?></td>
					</tr>
					<tr>
						<th>PROVINSI</th>
						<td colspan="3"><?= $provinsi ?></td>
					</tr>
					<tr>
						<th>KABUPATEN/KOTA</th>
						<td colspan="3"><?= $kota ?></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php
					$demografi = Models\Demografi\kecamatan($idkota, $begin_date, $end_date);

					if ( $demografi == FALSE ) {
						echo "<div class='alert alert-warning'>";
							echo "Tidak ada data ditemukan";
						echo "</div>";
					} else {
						echo "<table class='table table-bordered'>";
							echo "<thead>";
								echo "<tr>";
									echo "<th width='10'>NO</th>";
									echo "<th>KECAMATAN</th>";
									echo "<th width='150'>LAKI-LAKI</th>";
									echo "<th width='150'>PEREMPUAN</th>";
									echo "<th width='150'>TOTAL</th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								$index = 1;
								$total_lakilaki = 0;
								$total_perempuan = 0;
								$total_keseluruhan = 0;
								
								foreach ( $demografi['rows'] as $row ) {
									echo "<tr>";
										echo "<td>{$index}</td>";
										echo "<td>{$row['NAMA_KECAMATAN_1']}</td>";
										echo "<td>{$row['LAKILAKI']}</td>";
										echo "<td>{$row['PEREMPUAN']}</td>";
										echo "<td>{$row['JUMLAH']}</td>";
									echo "</tr>";

									$total_lakilaki += $row['LAKILAKI'];
									$total_perempuan += $row['PEREMPUAN'];
									$total_keseluruhan += $row['JUMLAH'];
									$index++;
								}

								$persentase_lakilaki = round(($total_lakilaki / $total_keseluruhan) * 100, 2);
								$persentase_perempuan = round(($total_perempuan / $total_keseluruhan) * 100, 2);

								echo "<tr>";
									echo "<th colspan='2'>TOTAL</th>";
									echo "<th>{$total_lakilaki}</th>";
									echo "<th>{$total_perempuan}</th>";
									echo "<th>{$total_keseluruhan}</th>";
								echo "</tr>";
								echo "<tr>";
									echo "<th colspan='2'>PERSENTASE</th>";
									echo "<th>{$persentase_lakilaki}%</th>";
									echo "<th>{$persentase_perempuan}%</th>";
									echo "<th>100%</th>";
								echo "</tr>";
							echo "</tbody>";
						echo "</table>";
					}
				?>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<?php require 'includes/js/kecamatan.js.php' ?>

<!-- modals -->
<?php require 'includes/modals/modal_filter_demografi_kecamatan.php' ?>