<script language="javascript" src="include/cal3.js"></script>
<script language="javascript" src="include/cal_conf3.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#myform").validate();

		jQuery('#parent_nomr').blur(function() {
			var val	= jQuery(this).val();
			jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{loadDetailPasien:true,nomr:val},function(data){
				var d = data.split('|');
				jQuery('#parent_nama').empty().append(d[2]);
			});
		});

		jQuery('.ruang').click(function(){
			var val = jQuery(this).val();
			if(val == 'vk'){
					jQuery.post('<?php echo _BASE_;?>include/ajaxload.php',{load_dokterjaga:true,kdpoly:10},function(data){
					jQuery('#load_dokterjaga').empty().html(data);
				});
			}else{
				jQuery('#load_dokterjaga').empty();
			}
		});

		jQuery('.carabayar').change(function(){
			var val = jQuery(this).val();
			if(val == 5){
				jQuery('#carabayar_lain').show().addClass('required');
			}else{
				jQuery('#carabayar_lain').hide().removeClass('required');
			}
		});

		// dropdown kota
		jQuery('#segment-data-pasien').on('change', '#KDPROVINSI', function() {
			var id_prov = jQuery(this).val();
			getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : id_prov, target : '#KOTA', selected : '' });
			jQuery('#KDKECAMATAN').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#KELURAHAN').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kecamatan
		jQuery('#segment-data-pasien').on('change', '#KOTA', function() {
			var id_kota = jQuery(this).val();
			getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : id_kota, target : '#KDKECAMATAN', selected : '' });
			jQuery('#KELURAHAN').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kelurahan
		jQuery('#segment-data-pasien').on('change', '#KDKECAMATAN', function() {
			var id_kec = jQuery(this).val();
			getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : id_kec, target : '#KELURAHAN', selected : '' });
		});
	});
</script>

<style type="text/css">
	.loader{background:url(js/loading.gif) no-repeat; width:16px; height:16px; float:right; margin-right:30px;}
	input.error{ border:1px solid #F00;}
	label.error{ color:#F00; font-weight:bold;}
</style>

<div align="center">
	<div id="frame">
		<div id="frame_title"><h3 align="left">IDENTITAS PASIEN</h3></div>
		<form action="models/pendaftaran_bayi.php" method="post" id="myform">
			<fieldset class="fieldset" id="segment-data-pasien">
				<legend>Identitas</legend>
				<table width="80%" border="0" cellpadding="0" cellspacing="0" title=" From Ini Berfungsi Sebagai Form Entry Data Bayi Baru." style="float:left;">
					<tr>
						<td valign="top" width="3%" rowspan="22"><img src="img/data.png" /></td>
						<td>Nomr Orang Tua</td>
						<td><input type="text" name="parent_nomr" id="parent_nomr" class="text required" title="*" /><span id="parent_nama" style="font-weight:bold;"></span></td>
					</tr>
					<tr>
						<td>Ruang Lahir</td>
						<td>
							<div class="form-group">
								<label class="radio-inline"><input type="radio" name="ruang" value="ok" class="ruang required" title="*" /> Ruang OK</label>
								<label class="radio-inline"><input type="radio" class="ruang required" name="ruang" value="vk" title="*" /> Ruang VK</label>
							</div>
							<div>
								<div id="load_dokterjaga"></div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Carabayar</td>
						<td>
							<?php
							$ss = mysql_query('select * from m_carabayar order by ORDERS ASC');
							while ( $ds = mysql_fetch_array($ss) ) {
								if($_GET['KDCARABAYAR'] == $ds['KODE']): $sel = "checked"; else: $sel = ''; endif;
								echo '<label class="radio-inline">';
									echo '<input type="radio" name="KDCARABAYAR" id="carabayar_'.$ds['KODE'].'" title="*" class="carabayar required" '.$sel.' value="'.$ds['KODE'].'" /> '.$ds['NAMA'];
								echo '</label>';
							}

							$cssb = ( $_GET['KETBAYAR'] != '' ) ? 'style="display:inline;"' : 'style="display:none;"'; ?>
							<input type="text" name="KETBAYAR" title="*" id="carabayar_lain" <?php echo $cssb;?> value="<?php echo $_REQUEST['KETBAYAR']; ?>" class="text"/>
						</td>
					</tr>
					<tr>
						<td width="23%">Nama Lengkap Bayi</td>
						<td width="54%"><input class="text required" title="*" type="text" name="NAMA" size="30" value="<? if(!empty($_GET['NAMA'])){ echo $_GET['NAMA']; }?>" id="NAMA" /></td>
					</tr>
					<tr>
						<td>Tempat Tanggal Lahir</td>
						<td>
							<input type="text" placeholder="Tempat" value="<? if(!empty($_GET['TEMPAT'])){ echo $_GET['TEMPAT']; } ?>" class="text required" title="*" name="TEMPAT" size="15" id="TEMPAT" />
							<input onblur="calage1(this.value,'umur');" type="text" class="text required" title="*" value="<? if(!empty($_GET['TGLLAHIR'])){ echo date('d/m/Y', strtotime($_GET['TGLLAHIR'])); }else{ echo date('d/m/Y');} ?>" name="TGLLAHIR" id="TGLLAHIR" size="20" />
							<a href="javascript:showCal1('Calendar1')"><button type="button" class="text"><i class="fa fa-fw fa-calendar"></i></button></a>
						</td>
					</tr>
					<tr>
						<td>Umur</td>
						<td>
							<?php 
							if ( $_REQUEST['TGLLAHIR']=="") {
								$a = datediff(date("Y/m/d"), date("Y/m/d")); 
							}
							else {
								$a = datediff($_REQUEST['TGLLAHIR'], date("Y/m/d"));
							} ?>
							<span id="umurc"><input class="text" type="text" value="<?php echo 'umur '.$a[years].' tahun '.$a[months].' bulan '.$a[days].' hari'; ?>" name="umur" id="umur" size="45" /></span>
						</td>
					</tr>
					<tr>
						<td valign="top">Alamat Sekarang</td>
						<td colspan="1"><input name="ALAMAT" id="ALAMAT" class="text required" title="*" type="text" value="<? if(!empty($_GET['ALAMAT'])){ echo $_GET['ALAMAT']; }?>" size="45" /></td>
					</tr>
					<tr>
						<td>Alamat KTP</td>
						<td><input name="ALAMAT_KTP" class="text" type="text" value="<? if(!empty($_GET['ALAMAT_KTP'])){ echo $_GET['ALAMAT_KTP']; }?>" size="45" id="ALAMAT_KTP" /></td>
					</tr>
					<tr>
						<td>Provinsi</td>
						<td>
							<select name="KDPROVINSI" class="text required" title="*" id="KDPROVINSI">
								<option value=""> - Pilih Provinsi - </option>
								<?php
								$ss	= mysql_query('select * from m_provinsi order by namaprovinsi ASC');
								while ( $ds = mysql_fetch_array($ss) ) {
									$sel = ( $_GET['KDPROVINSI'] == $ds['idprovinsi'] ) ? 'selected' : '';
									echo '<option value="'.$ds['idprovinsi'].'" '.$sel.'>'.$ds['namaprovinsi'].'</option>';
								} ?>
							</select>
							<input class="text" value="<?=$_GET['KOTA']?>" type="hidden" name="KOTAHIDDEN" id="KOTAHIDDEN" />
							<input class="text" value="<?=$_GET['KECAMATAN']?>" type="hidden" name="KECAMATANHIDDEN" id="KECAMATANHIDDEN" />
							<input class="text" value="<?=$_GET['KELURAHAN']?>" type="hidden" name="KELURAHANHIDDEN" id="KELURAHANHIDDEN" />
						</td>
					</tr>
					<tr>
						<td>Kabupaten / Kota</td>
						<td>
							<div id="kotapilih">
								<select name="KOTA" class="text required" title="*" id="KOTA">
									<option value="0"> - Pilih Kota - </option>
									<?php
									$ss	= mysql_query('select * from m_kota where idprovinsi = "'.$_GET['KDPROVINSI'].'" order by namakota ASC');
									while ( $ds = mysql_fetch_array($ss) ) {
										$sel = ( $_GET['KOTA'] == $ds['idkota'] ) ? 'selected' : '';
										echo '<option value="'.$ds['idkota'].'" '.$sel.'>'.$ds['namakota'].'</option>';
									} ?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Kecamatan</td>
						<td>
							<div id="kecamatanpilih">
								<select name="KDKECAMATAN" class="text required" title="*" id="KDKECAMATAN">
									<option value="0"> - Pilih Kecamatan - </option>
									<?php
									$ss	= mysql_query('select * from m_kecamatan where idkota = "'.$_GET['KOTA'].'" order by namakecamatan ASC');
									while ( $ds = mysql_fetch_array($ss) ) {
										$sel = ( $_GET['KDKECAMATAN'] == $ds['idkecamatan'] ) ? 'selected' : '';
										echo '<option value="'.$ds['idkecamatan'].'" '.$sel.'>'.$ds['namakecamatan'].'</option>';
									} ?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Kelurahan</td>
						<td>
							<div id="kelurahanpilih">
								<select name="KELURAHAN" class="text required" title="*" id="KELURAHAN">
									<option value="0"> - Pilih Kelurahan - </option>
									<?php
									$id_kec = ( isset($_GET['KDKECAMATAN']) ) ? $_GET['KDKECAMATAN'] : null; 
									$ss	= mysql_query('select * from m_kelurahan where idkecamatan = "'.$id_kec.'" order by namakelurahan ASC');
									while ( $ds = mysql_fetch_array($ss) ) {
										$sel = ( $_GET['KELURAHAN'] == $ds['idkelurahan'] ) ? 'selected' : '';
										echo '<option value="'.$ds['idkelurahan'].'" '.$sel.'>'.$ds['namakelurahan'].'</option>';
									} ?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>No Telepon / HP</td>
						<td><input  class="text" value="<? if(!empty($_GET['NOTELP'])){ echo $_GET['NOTELP']; }?>" type="text" name="NOTELP" size="25" id="notelp" /></td>
					</tr>
					<tr>
						<td>No KTP </td>
						<td><input  class="text" value="<? if(!empty($_GET['NOKTP'])){ echo $_GET['NOKTP']; }?>" type="text" name="NOKTP" id="NOKTP" size="25" /></td>
					</tr>
					<tr>
						<td>Nama Suami / Orang Tua </td>
						<td><input class="text" type="text" value="<? if(!empty($_GET['SUAMI_ORTU'])){ echo $_GET['SUAMI_ORTU']; }?>" name="SUAMI_ORTU" id="SUAMI_ORTU" size="25" /></td>
					</tr>
					<tr valign="top">
						<td height="22" valign="top">Pekerjaan Pasien / Orang Tua</td>
						<td><input class="text" type="text" value="<? if(!empty($_GET['PEKERJAAN'])){ echo $_GET['PEKERJAAN']; }?>" name="PEKERJAAN" size="25" id="PEKERJAAN" /></td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td valign="top">Nama Penanggung Jawab</td>
						<td valign="top"><input class="text" type="text" name="nama_penanggungjawab" size="30" value="<? if(!empty($_GET['nama_penanggungjawab'])){ echo $_GET['nama_penanggungjawab']; } ?>" id="nama_penanggungjawab"  /></td>
					</tr>
					<tr>
						<td valign="top">Hubungan Dengan Pasien</td>
						<td valign="top"><input class="text" type="text" name="hubungan_penanggungjawab" size="30" value="<? if(!empty($_GET['hubungan_penanggungjawab'])){ echo $_GET['hubungan_penanggungjawab']; } ?>" id="hubungan_penanggungjawab" /></td>
					</tr>
					<tr>
						<td valign="top">Alamat</td>
						<td valign="top"><input name="alamat_penanggungjawab" class="text" type="text" size="45" value="<? if(!empty($_GET['alamat_penanggungjawab'])){ echo $_GET['alamat_penanggungjawab']; } ?>" id="alamat_penanggungjawab" /></td>
					</tr>
					<tr>
						<td valign="top">No Telepon / HP</td>
						<td valign="top"><input  class="text" type="text" name="phone_penanggungjawab" size="25" value="<? if(!empty($_GET['phone_penanggungjawab'])){ echo $_GET['phone_penanggungjawab']; } ?>" id="phone_penanggungjawab" /></td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td><input type='hidden' name='stop_daftar' id='stop_daftar' /></td>
					</tr>
				</table>
				<table width="20%" style="float:right;">
					<tr>
						<td>Shift</td>
						<td>
							<div>
								<label class="radio-inline"><input type="radio" name="SHIFT" class="required" title="*" value="1" <? if($_GET['SHIFT']=="1")echo "checked";?>/> Pagi</label>
							</div>
							<div>
								<label class="radio-inline"><input type="radio" name="SHIFT" class="required" title="*" value="2" <? if($_GET['SHIFT']=="2")echo "checked";?>/> Siang</label>
							</div>
							<div>
								<label class="radio-inline"><input type="radio" name="SHIFT" class="required" title="*" value="3" <? if($_GET['SHIFT']=="3")echo "checked";?>/> Sore</label>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top">Jenis Kelamin</td>
						<td>
							<div>
								<label class="radio-inline"><input type="radio" name="JENISKELAMIN" class="required" title="*" id="JENISKELAMIN_L" value="L" <? if(strtoupper($_GET['JENISKELAMIN'])=="L") echo "checked";?>/> Laki-laki</label>
							</div>
							<div>
								<label class="radio-inline"><input type="radio" name="JENISKELAMIN" class="required" title="*" id="JENISKELAMIN_P" value="P" <? if(strtoupper($_GET['JENISKELAMIN'])=="P") echo "checked";?>/> Perempuan</label>
							</div>
						</td>
					</tr>
					<tr>
						<td>Status Perkawinan</td>
						<td>
							<div>
								<label class="radio-inline"><input type="radio" name="STATUS" id="status_1" value="1" <? if($_GET['STATUS']=="1") echo "checked";?>/> Belum Kawin </label>
							</div>
							<div>
								<label class="radio-inline"><input type="radio" name="STATUS" id="status_2" value="2" <? if($_GET['STATUS']=="2") echo "checked";?> /> Kawin </label>
							</div>
							<div>
								<label class="radio-inline"><input type="radio" name="STATUS" id="status_3" value="3" <? if($_GET['STATUS']=="3") echo "checked";?>/> Janda / Duda </label>
							</div>
						</td>
					</tr>
					<tr>
						<td>Pendidikan Terakhir</td>
						<td>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_0" value="0" <? if($_GET['PENDIDIKAN']=="0") echo "checked";?> /> Belum Sekolah</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_1" value="1" <? if($_GET['PENDIDIKAN']=="1") echo "checked";?> /> SD</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_2" value="2" <? if($_GET['PENDIDIKAN']=="2") echo "checked";?> /> SLTP</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_3" value="3" <? if($_GET['PENDIDIKAN']=="3") echo "checked";?> /> SMU</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_4" value="4" <? if($_GET['PENDIDIKAN']=="4") echo "checked";?> /> D3/Akademik</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_5" value="5" <? if($_GET['PENDIDIKAN']=="5") echo "checked";?> /> Universitas</label></div>
						</td>
					</tr>
					<tr>
						<td>Agama</td>
						<td>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_1" value="1" <? if($_GET['AGAMA']=="1") echo "checked";?> /> Islam</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_2" value="2" <? if($_GET['AGAMA']=="2") echo "checked";?>/> Kristen Protestan</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_3" value="3" <? if($_GET['AGAMA']=="3") echo "checked";?>/> Katholik</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_4" value="4" <? if($_GET['AGAMA']=="4") echo "checked";?>/> Hindu</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_5" value="5" <? if($_GET['AGAMA']=="5") echo "checked";?>/> Budha</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_6" value="6" <? if($_GET['AGAMA']=="6") echo "checked";?>/> Lain - lain</label></div>
						</td>
					</tr>
				</table>
				<br clear="all" />
				<table width="100%">
					<tr>
						<td align="center">
							<button type="submit" name="daftar" class="text"><i class="fa fa-fw fa-save"></i> Simpan</button>
						</td>
					</tr>
				</table>
				<input type="text" id="msgid" name="msgid" style="border:1px #FFF solid; width:0px; height:0px;">
			</fieldset>
		</form>
	</div>
</div>