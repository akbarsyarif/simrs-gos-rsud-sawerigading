<?php
include 'include/connect.php';

include BASEPATH . 'include/bpjs_api.php';
include BASEPATH . 'include/function.php';
include BASEPATH . 'include/security_helper.php';
include BASEPATH . 'models/m_icd10.php';

$reqdata = xss_clean($_GET['reqdata']);

switch ( $reqdata ) {
	case 'get_poli':
		$result = get_poli();
		if ( $result == false ) {
			echo 'error';
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'get_local_diagnosa':
		$s = ( (isset($_GET['s'])) && (! empty($_GET['s'])) ) ? xss_clean($_GET['s']) : null;

		$config['query'] = "WHERE icd_code LIKE '%{$s}%'
		                    OR jenis_penyakit LIKE '%{$s}%'
		                    OR jenis_penyakit_local LIKE '%{$s}%'
		                    OR jenis_penyakit_local_rs LIKE '%{$s}%'";
		$config['order'] = "CASE
		                        WHEN icd_code LIKE '{$s}' THEN 1
		                        WHEN jenis_penyakit LIKE '{$s}' THEN 1
		                        WHEN jenis_penyakit_local LIKE '{$s}' THEN 1
		                        WHEN jenis_penyakit_local_rs LIKE '{$s}' THEN 1
		                        WHEN icd_code LIKE '{$s}%' THEN 2
		                        WHEN jenis_penyakit LIKE '{$s}%' THEN 2
		                        WHEN jenis_penyakit_local LIKE '{$s}%' THEN 2
		                        WHEN jenis_penyakit_local_rs LIKE '{$s}%' THEN 2
		                        WHEN icd_code LIKE '%{$s}' THEN 3
		                        WHEN jenis_penyakit LIKE '%{$s}' THEN 3
		                        WHEN jenis_penyakit_local LIKE '%{$s}' THEN 3
		                        WHEN jenis_penyakit_local_rs LIKE '%{$s}' THEN 3
		                        ELSE icd_code
		                    END";
		$config['limit'] = 50;
		
		$result = Models\ICD10\get($config);
		if ( $result == false ) {
			echo json_encode((object) array(
				'metadata' => (object) array(
					'code' => "201",
					'message' => "Diagnosa tidak ditemukan"
				),
				'response' => NULL
			));
		}
		else {
			echo json_encode((object) array(
				'metadata' => (object) array(
					'code' => "200",
					'message' => "OK"
				),
				'response' => (object) array(
					'list' => $result
				)
			));
		}
	break;

	case 'get_diagnosa':
		$kode = ( (isset($_GET['kode'])) && (! empty($_GET['kode'])) ) ? urlencode(xss_clean($_GET['kode'])) : null;
		
		$result = get_diagnosa($kode);
		if ( $result == false ) {
			echo 'error';
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'get_faskes':
		$kode = ( (isset($_GET['kode'])) && (! empty($_GET['kode'])) ) ? urlencode(xss_clean($_GET['kode'])) : '';
		$start = ( (isset($_GET['start'])) && (! empty($_GET['start'])) ) ? urlencode(xss_clean($_GET['start'])) : 0;
		$limit = ( (isset($_GET['limit'])) && (! empty($_GET['limit'])) ) ? urlencode(xss_clean($_GET['limit'])) : 50;

		$result = get_faskes($kode, $start, $limit);
		
		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => '800',
					'message' => 'Tidak dapat menghubungi server BPJS'
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => '200',
					'message' => 'OK'
				),
				'response' => $result
			);

		}
		
		echo json_encode($result);
	break;

	case 'get_peserta':
		$search_by = ( (isset($_GET['search_by'])) && (! empty($_GET['search_by'])) ) ? urlencode(xss_clean($_GET['search_by'])) : null;
		$s = ( (isset($_GET['s'])) && (! empty($_GET['s'])) ) ? urlencode(xss_clean($_GET['s'])) : null;

		$result = get_peserta($search_by, $s);

		if ( $result === FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => '800',
					'message' => 'Tidak dapat menghubungi server BPJS'
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => '200',
					'message' => 'OK'
				),
				'response' => $result
			);

		}

		echo json_encode($response);
	break;

	case 'insert_sep':
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			unset($data);
			$data['no_kartu']          = ((isset($_POST['no_kartu'])) && (! empty($_POST['no_kartu']))) ? xss_clean($_POST['no_kartu']) : null;
			$data['tgl_sep']           = ((isset($_POST['tgl_sep'])) && (! empty($_POST['tgl_sep']))) ? xss_clean($_POST['tgl_sep']) : date('Y-m-d H:i:s');
			$data['tgl_rujukan']       = ((isset($_POST['tgl_rujukan'])) && (! empty($_POST['tgl_rujukan']))) ? xss_clean($_POST['tgl_rujukan']) : null;
			$data['no_rujukan']        = ((isset($_POST['no_rujukan'])) && (! empty($_POST['no_rujukan']))) ? xss_clean($_POST['no_rujukan']) : null;
			$data['ppk_rujukan']       = ((isset($_POST['ppk_rujukan'])) && (! empty($_POST['ppk_rujukan']))) ? xss_clean($_POST['ppk_rujukan']) : null;
			$data['ppk_pelayanan']     = ((isset($_POST['ppk_pelayanan'])) && (! empty($_POST['ppk_pelayanan']))) ? xss_clean($_POST['ppk_pelayanan']) : null;
			$data['kd_jns_pelayanan']  = ((isset($_POST['kd_jns_pelayanan'])) && (! empty($_POST['kd_jns_pelayanan']))) ? xss_clean($_POST['kd_jns_pelayanan']) : null;
			$data['catatan']           = ((isset($_POST['catatan'])) && (! empty($_POST['catatan']))) ? xss_clean($_POST['catatan']) : null;
			$data['kd_diagnosa_awal']  = ((isset($_POST['kd_diagnosa_awal'])) && (! empty($_POST['kd_diagnosa_awal']))) ? xss_clean($_POST['kd_diagnosa_awal']) : null;
			$data['kd_poli_tujuan']    = ((isset($_POST['kd_poli_tujuan'])) && (! empty($_POST['kd_poli_tujuan']))) ? xss_clean($_POST['kd_poli_tujuan']) : null;
			$data['kd_kelas_rawat']    = ((isset($_POST['kd_kelas_rawat'])) && (! empty($_POST['kd_kelas_rawat']))) ? xss_clean($_POST['kd_kelas_rawat']) : null;
			$data['kd_lakalantas']     = ((isset($_POST['kd_lakalantas'])) && (! empty($_POST['kd_lakalantas']))) ? xss_clean($_POST['kd_lakalantas']) : null;
			$data['lokasi_lakalantas'] = ((isset($_POST['lokasi_lakalantas'])) && (! empty($_POST['lokasi_lakalantas']))) ? xss_clean($_POST['lokasi_lakalantas']) : null;
			$data['username']          = ((isset($_POST['username'])) && (! empty($_POST['username']))) ? xss_clean($_POST['username']) : null;
			$data['no_mr']             = ((isset($_POST['no_mr'])) && (! empty($_POST['no_mr']))) ? xss_clean($_POST['no_mr']) : null;

			$result = insert_sep($data);
			if ( $result == false ) {
				echo 'error';
			}
			else {
				echo json_encode($result);
			}
		}
		else {
			http_response_code(404);
		}
	break;

	case 'update_sep':
		if ( $_SERVER['REQUEST_METHOD'] === 'PUT' ) {
			$put = array();
			parse_str(file_get_contents('php://input'), $put);

			unset($data);

			$data['noSep']        = ((isset($put['no_sep'])) && (! empty($put['no_sep']))) ? xss_clean($put['no_sep']) : '';
			$data['noKartu']      = ((isset($put['no_kartu'])) && (! empty($put['no_kartu']))) ? xss_clean($put['no_kartu']) : '';
			$data['tglSep']       = ((isset($put['tgl_sep'])) && (! empty($put['tgl_sep']))) ? xss_clean($put['tgl_sep']) : '';
			$data['tglRujukan']   = ((isset($put['tgl_rujukan'])) && (! empty($put['tgl_rujukan']))) ? xss_clean($put['tgl_rujukan']) : '';
			$data['noRujukan']    = ((isset($put['no_rujukan'])) && (! empty($put['no_rujukan']))) ? xss_clean($put['no_rujukan']) : '';
			$data['ppkRujukan']   = ((isset($put['ppk_rujukan'])) && (! empty($put['ppk_rujukan']))) ? xss_clean($put['ppk_rujukan']) : '';
			$data['ppkPelayanan'] = ((isset($put['ppk_pelayanan'])) && (! empty($put['ppk_pelayanan']))) ? xss_clean($put['ppk_pelayanan']) : '';
			$data['jnsPelayanan'] = ((isset($put['kd_jns_pelayanan'])) && (! empty($put['kd_jns_pelayanan']))) ? xss_clean($put['kd_jns_pelayanan']) : '';
			$data['catatan']      = ((isset($put['catatan'])) && (! empty($put['catatan']))) ? xss_clean($put['catatan']) : '';
			$data['diagAwal']     = ((isset($put['kd_diagnosa_awal'])) && (! empty($put['kd_diagnosa_awal']))) ? xss_clean($put['kd_diagnosa_awal']) : '';
			$data['poliTujuan']   = ((isset($put['kd_poli_tujuan'])) && (! empty($put['kd_poli_tujuan']))) ? xss_clean($put['kd_poli_tujuan']) : '';
			$data['klsRawat']     = ((isset($put['kd_kelas_rawat'])) && (! empty($put['kd_kelas_rawat']))) ? xss_clean($put['kd_kelas_rawat']) : '';
			$data['lakaLantas']   = ((isset($put['kd_lakalantas'])) && (! empty($put['kd_lakalantas']))) ? xss_clean($put['kd_lakalantas']) : '';
			$data['lokasiLaka']   = ((isset($put['lokasi_lakalantas'])) && (! empty($put['lokasi_lakalantas']))) ? xss_clean($put['lokasi_lakalantas']) : '';
			$data['user']         = ((isset($put['username'])) && (! empty($put['username']))) ? xss_clean($put['username']) : '';
			$data['noMr']         = ((isset($put['no_mr'])) && (! empty($put['no_mr']))) ? xss_clean($put['no_mr']) : '';

			$result = update_sep($data);
			if ( $result == false ) {
				echo 'error';
			}
			else {
				echo json_encode($result);
			}
		}
		else {
			http_response_code(404);
		}
	break;

	case 'detail_sep':
		$no_sep = ( (isset($_GET['no_sep'])) && (! empty($_GET['no_sep'])) ) ? urlencode(xss_clean($_GET['no_sep'])) : null;

		$result = detail_sep($no_sep);
		if ( $result == false ) {
			echo 'error';
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'delete_sep':
		if ( $_SERVER['REQUEST_METHOD'] === 'DELETE' ) {
			$delete = array();
			parse_str(file_get_contents('php://input'), $delete);

			unset($data);
			$data['no_sep'] = ((isset($delete['no_sep'])) && (! empty($delete['no_sep']))) ? xss_clean($delete['no_sep']) : null;
			$data['ppk_pelayanan'] = ((isset($delete['ppk_pelayanan'])) && (! empty($delete['ppk_pelayanan']))) ? xss_clean($delete['ppk_pelayanan']) : null;

			$result = delete_sep($data);
			if ( $result == false ) {
				echo 'error';
			}
			else {
				echo json_encode($result);
			}
		}
		else {
			http_response_code(404);
		}
	break;

	case 'update_tgl_pulang':
		if ( $_SERVER['REQUEST_METHOD'] === 'PUT' ) {
			$put = array();
			parse_str(file_get_contents('php://input'), $put);

			unset($data);
			$data['no_sep'] = ((isset($put['no_sep'])) && (! empty($put['no_sep']))) ? xss_clean($put['no_sep']) : null;
			$data['tgl_pulang'] = ((isset($put['tgl_pulang'])) && (! empty($put['tgl_pulang']))) ? xss_clean($put['tgl_pulang']) : null;
			$data['ppk_pelayanan'] = ((isset($put['ppk_pelayanan'])) && (! empty($put['ppk_pelayanan']))) ? xss_clean($put['ppk_pelayanan']) : null;

			$result = update_tgl_pulang($data);

			if ( $result === FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => '800',
						'message' => 'Tidak dapat menghubungi server BPJS'
					),
					'response' => NULL
				);

			}
			else {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => '200',
						'message' => 'OK'
					),
					'response' => $result
				);

			}

			echo json_encode($response);
		}
		else {
			http_response_code(404);
		}
	break;

	case 'data_kunjungan_peserta':
		$no_sep = ( (isset($_GET['no_sep'])) && (! empty($_GET['no_sep'])) ) ? urlencode(xss_clean($_GET['no_sep'])) : null;

		$result = data_kunjungan_peserta($no_sep);
		if ( $result == false ) {
			echo 'error';
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'riwayat_pelayanan_peserta':
		$no_kartu = ( (isset($_GET['no_kartu'])) && (! empty($_GET['no_kartu'])) ) ? urlencode(xss_clean($_GET['no_kartu'])) : null;

		$result = riwayat_pelayanan_peserta($no_kartu);
		if ( $result == false ) {
			echo 'error';
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'get_rujukan':
		$faskes = ( (isset($_GET['faskes'])) && (! empty($_GET['faskes'])) ) ? urlencode(xss_clean($_GET['faskes'])) : null;
		$search_by = ( (isset($_GET['search_by'])) && (! empty($_GET['search_by'])) ) ? urlencode(xss_clean($_GET['search_by'])) : null;
		$s = ( (isset($_GET['s'])) && (! empty($_GET['s'])) ) ? urlencode(xss_clean($_GET['s'])) : null;

		$result = get_rujukan($faskes, $search_by, $s);
		if ( $result == false ) {
			$response = (object) array (
				'metadata' => (object) array (
					'code' => '800',
					'message' => 'Tidak dapat menghubungi server BPJS'
				),
				'response' => NULL
			);
		}
		else {
			$response = (object) array (
				'metadata' => (object) array (
					'code' => '200',
					'message' => 'OK'
				),
				'response' => $result
			);
		}

		echo json_encode($response);
	break;
	
	default:
		http_response_code(404);
	break;
}