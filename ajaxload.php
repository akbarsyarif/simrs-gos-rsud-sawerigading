<?php
require 'include/connect.php';
require 'include/function.php';
require 'include/security_helper.php';

$p        = xss_clean($_GET['p']); // page
$a        = "Controller\\".xss_clean($_GET['a']); // action
$filepath = "ajaxload/{$p}.php";

if ( file_exists( $filepath ) ) {
	require $filepath;

	if ( function_exists( $a ) ) {
		$a();
	}
	else {
		http_response_code(404);
		echo "Action not found";
	}
}
else {
	http_response_code(404);
	echo "Page not found";
}