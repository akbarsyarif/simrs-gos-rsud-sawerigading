<?php
require '../include/connect.php';
require BASEPATH . 'models/m_barang_group.php';
?>

<?php if ( ! empty($_GET['gudang']) ) : ?>
	<select class="text" name="grpbarang" id="grpbarang"  onchange="javascript: MyAjaxRequest('ruang_x','orderbarang/changegroup.php?ruang_x=' + this.value); MyAjaxRequest('ruang_h','orderbarang/changegroup.php?ruang_h=' + this.value); return false;" >
		<?php
		$groups = Models\BarangGroup\get_barang_group(array(
			'query' => "WHERE farmasi = 1"
		)); ?>

		<?php if ( $groups == false ) : ?>
			<option value="">-- Grup barang tidak ditemukan --</option>
		<?php else : ?>
			<?php foreach ( $groups as $group ) : ?>
				<option value="<?= $group['group_barang'] ?>"><?= $group['nama_group'] ?></option>
			<?php endforeach; ?>
		<?php endif; ?>
	</select>
<?php elseif ( ! empty($_GET['logistik']) ) : ?>
	<select class="text" name="grpbarang" id="grpbarang">
		<?php
		$groups = Models\BarangGroup\get_barang_group(array(
			'query' => "WHERE farmasi = 0"
		)); ?>

		<?php if ( $groups == false ) : ?>
			<option value="">-- Grup barang tidak ditemukan --</option>
		<?php else : ?>
			<?php foreach ( $groups as $group ) : ?>
				<option value="<?= $group['group_barang'] ?>"><?= $group['nama_group'] ?></option>
			<?php endforeach; ?>
		<?php endif; ?>
	</select>
<?php endif; ?>

<?php if ( ! empty($_GET['ruang_x']) ) : ?>
	<?php if ( $_GET['ruang_x'] == "7" ) : ?>
		<?php
			$sql_ruang = "SELECT
							m_ruang_gas.no,
							m_ruang_gas.nama
						  FROM
							m_ruang_gas
						  ORDER BY m_ruang_gas.nama";
			$get_ruang = mysql_query($sql_ruang);
		?>
		<select class="text" name="ruang" id="ruang" >
			<?php while ( $data_ruang = mysql_fetch_array($get_ruang) ) : ?>
				<option value="<?= $data_ruang['no'] ?>" ><?= $data_ruang['nama'] ?></option>
			<?php endwhile; ?>
		</select>
	<?php else : ?>
		&nbsp;
	<?php endif; ?> 
	
	<?php if ( ! empty($_GET['ruang_h']) ) : ?>
		<?php if ( $_GET['ruang_h'] == "4" ) : ?>
			Ruang
		<?php else : ?>
			&nbsp;
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>