<?php
namespace Models\RekapKunjungan;

require_once 'include/connect.php';

/**
 * menampilkan rekapitulasi kunjungan pasien berdasarkan poli
 * @param  [type] $in_jenis_poly [description]
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function rawatJalanByPoly($in_jenis_poly, $in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "SELECT
		kode AS kode_poly,
		nama,
		(
			SELECT COUNT(t_pendaftaran.NOMR) 
			FROM t_pendaftaran 
			JOIN m_poly ON m_poly.kode = t_pendaftaran.KDPOLY
			JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
			JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
			WHERE t_pendaftaran.KDPOLY = kode_poly 
				AND (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
				AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL) 
				AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		) AS total_kunjungan,
		(
			SELECT COUNT(t_pendaftaran.NOMR) 
			FROM t_pendaftaran 
			JOIN m_poly ON m_poly.kode = t_pendaftaran.KDPOLY
			JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
			JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
			WHERE m_poly.jenispoly = {$in_jenis_poly} 
				AND (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
				AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL) 
				AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		) AS total_keseluruhan
	FROM m_poly
	WHERE jenispoly = '{$in_jenis_poly}'
	GROUP BY kode_poly";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

/**
 * menampilkan rekapitulasi kunjungan pasien berdasarkan jenis kunjungan
 * kunjungan baru/lama
 * @param  [type] $in_jenis_poly [description]
 * @param  [type] $in_begin_date [description]
 * @return [type]                [description]
 */
function rawatJalanByJenisKunjungan($in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "SELECT
		CASE
			WHEN t_pendaftaran.PASIENBARU = 0 THEN 'Lama'
			ELSE 'Baru'
		END AS jenis_kunjungan,
		COUNT(t_pendaftaran.NOMR) AS total_kunjungan,
		(
			SELECT COUNT(t_pendaftaran.NOMR)
			FROM t_pendaftaran 
			JOIN m_poly ON m_poly.kode = t_pendaftaran.KDPOLY
			JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
			JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
			WHERE (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
				AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL) 
				AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		) AS total_keseluruhan
	FROM t_pendaftaran 
	JOIN m_poly ON m_poly.kode = t_pendaftaran.KDPOLY
	JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
	JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
	WHERE (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
		AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL) 
		AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
	GROUP BY jenis_kunjungan
	ORDER BY jenis_kunjungan ASC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

/**
 * menampilkan rekapitulasi kunjungan pasien berdasarkan cara bayar
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function rawatJalanByCaraBayar($in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "SELECT
		KODE AS kode_carabayar,
		NAMA AS nama,
		JMKS AS jmks,
		(
			SELECT COUNT(t_pendaftaran.NOMR)
			FROM t_pendaftaran
			JOIN m_poly ON m_poly.kode = t_pendaftaran.KDPOLY
			JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
			JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
			WHERE t_pendaftaran.KDCARABAYAR = kode_carabayar 
				AND (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
				AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL) 
				AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		) AS total_kunjungan,
		(
			SELECT COUNT(t_pendaftaran.NOMR)
			FROM t_pendaftaran 
			JOIN m_poly ON m_poly.kode = t_pendaftaran.KDPOLY
			JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
			JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
			WHERE (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
				AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL) 
				AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		) AS total_keseluruhan
	FROM m_carabayar
	GROUP BY m_carabayar.KODE
	ORDER BY m_carabayar.KODE ASC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

/**
 * rekap kunjungan pasien JKN per kategori
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function rekapKunjunganJKN($in_begin_date, $in_end_date)
{
	$sql = "SELECT
		CASE
			WHEN t_pendaftaran.NM_JENIS_PESERTA = '' OR t_pendaftaran.NM_JENIS_PESERTA IS NULL THEN 'Lainnya'
			ELSE t_pendaftaran.NM_JENIS_PESERTA
		END AS nama,
		COUNT(t_pendaftaran.NOMR) AS total_kunjungan
	FROM t_pendaftaran
	JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
	JOIN m_pasien ON m_pasien.NOMR = t_pendaftaran.NOMR
	WHERE t_pendaftaran.KDCARABAYAR = 2 
		AND (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
		AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL)
		AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
	GROUP BY t_pendaftaran.NM_JENIS_PESERTA
	ORDER BY t_pendaftaran.NM_JENIS_PESERTA ASC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}