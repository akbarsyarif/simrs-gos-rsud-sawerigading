<?php
namespace Models\Auth;

if ( ! function_exists('is_supervisor') )
{
	/**
	 * mengecek apakah user adalah supervisor
	 * @param  [type]  $username [description]
	 * @param  [type]  $password [description]
	 * @return boolean           [description]
	 */
	function is_supervisor($username, $password)
	{
		$sql = "SELECT * FROM m_login WHERE NIP = '{$username}' AND PWD = '{$password}' AND ROLES = '99'";
		$result = mysql_query($sql) OR die(mysql_error());

		if ( mysql_num_rows($result) === 0 ) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
}

if ( ! function_exists('getDepartemen') )
{
	/**
	 * mengecek nama departemen user berdasarkan nip/username
	 * @param  [type] $nip [description]
	 * @return [type]      [description]
	 */
	function getDepartemen($nip=NULL)
	{
		$nip = (strlen($nip) > 0) ? $nip : $_SESSION['NIP'];
		$sql = "SELECT DEPARTEMEN AS departemen FROM m_login WHERE NIP = '{$nip}' LIMIT 1";
		$result = mysql_query($sql) OR die(mysql_error());

		if ( mysql_num_rows($result) > 0 ) {
			$data = mysql_fetch_assoc($result);
			return $data['DEPARTEMEN'];
		}

		return FALSE;
	}
}

if ( ! function_exists('getUserLogin') )
{
	function getUserLogin()
	{
		if ( isset($_SESSION['SES_REG']) && !empty($_SESSION['SES_REG']) && isset($_SESSION['NIP']) && !empty($_SESSION['NIP'])) {
			return $_SESSION['NIP'];
		}

		return FALSE;
	}
}