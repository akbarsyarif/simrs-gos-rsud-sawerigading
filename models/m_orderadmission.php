<?php
namespace Models\OrderAdmission;

//---------------------------------------------------------------------------------

function get($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : NULL;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : NULL;

	$sql = "SELECT * FROM t_orderadmission {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY IDXORDER DESC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function row($config=array())
{
	$result = get($config);

	if ( $result !== FALSE ) {
		return $result[0];
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function count($config=array())
{
	$query = (isset($config['query'])) ? $config['query'] : NULL;

	$sql = "SELECT COUNT(*) AS numrows FROM t_orderadmission {$query}";
	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}

//---------------------------------------------------------------------------------

function insert($data=array())
{

	if ( is_array($data) === FALSE ) {
		
		throw new Exception("Parameter pertama harus bertipe array");
	
	}
	else if ( count($data) == 0 ) {

		throw new Exception("Parameter pertama harus berisi setidaknya 1 indeks");
		
	}
	else {

		// parsing "set" statement
		$set = '';
		foreach ( $data as $field => $value ) {
			if ( $value != "" ) {
				$set .= ",{$field} = '{$value}'";
			}
		}

		$set = ltrim($set, ',');

		$sql = "INSERT INTO t_orderadmission SET {$set}";
		$result = mysql_query($sql);

		if ( $result == FALSE ) {

			throw new Exception("Internal server error. Gagal menambahkan data.");

		}
		else {

			return TRUE;

		}

	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function update($data=array(), $filter=array())
{

	if ( is_array($data) === FALSE ) {
		
		throw new Exception("Parameter pertama harus bertipe array");
	
	}
	else if ( count($data) == 0 ) {

		throw new Exception("Parameter pertama harus berisi setidaknya 1 indeks");
		
	}
	else if ( is_array($filter) === FALSE ) {
	
		throw new Exception("Parameter kedua harus bertipe array");
	
	}
	else if ( count($filter) == 0 ) {

		throw new Exception("Parameter kedua harus berisi setidaknya 1 indeks");
		
	}
	else {

		// parsing "set" statement
		$set = '';
		foreach ( $data as $field => $value ) {
			if ( $value != "" ) {
				$set .= ",{$field} = '{$value}'";
			}
		}

		$set = ltrim($set, ',');

		// parsing "where" statement
		$where = '';
		$i = 0;
		foreach ( $filter as $field => $value ) {
			if ( $value != "" ) {
				if ( $i == 0 ) {
					$where .= "{$field} = '{$value}'";
				}
				else {
					$where .= " AND {$field} = '{$value}'";
				}
			}
			$i++;
		}

		$sql = "UPDATE t_orderadmission SET {$set} WHERE {$where}";
		$result = mysql_query($sql);

		if ( $result == FALSE ) {

			throw new Exception("Internal server error. Gagal mengupdate data.");

		}
		else {

			return TRUE;

		}

	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function delete($filter=array())
{

	if ( is_array($filter) === FALSE ) {
	
		throw new Exception("Parameter kedua harus bertipe array");
	
	}
	else if ( count($filter) == 0 ) {

		throw new Exception("Parameter kedua harus berisi setidaknya 1 indeks");
		
	}
	else {

		// parsing "where" statement
		$where = '';
		$i = 0;
		foreach ( $filter as $field => $value ) {
			if ( $value != "" ) {
				if ( $i == 0 ) {
					$where .= "{$field} = '{$value}'";
				}
				else {
					$where .= " AND {$field} = '{$value}'";
				}
			}
			$i++;
		}

		$sql = "DELETE FROM t_orderadmission WHERE {$where}";
		$result = mysql_query($sql);

		if ( $result == FALSE ) {

			throw new Exception("Internal server error. Gagal menghapus data.");

		}
		else {

			return TRUE;

		}

	}

	return FALSE;

}

//---------------------------------------------------------------------------------