<?php
namespace Models;

require_once 'include/connect.php';

//---------------------------------------------------------------------------------

function get_pasien($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT m1.id, m1.NOMR, m1.TITLE,
					TRIM(LEFT(TRIM(m1.NAMA),
						CASE 
							WHEN LOCATE(',', TRIM(m1.NAMA)) <= 0 THEN LENGTH(TRIM(m1.NAMA))
							ELSE LOCATE(',', TRIM(m1.NAMA)) - 1
						END
					)) AS NAMA,
					m1.TEMPAT,
					-- tgl. lahir pasien
					m1.TGLLAHIR, DATE_FORMAT(m1.TGLLAHIR, '%d/%m/%Y') AS TGLLAHIR_CAL,
					m1.JENISKELAMIN, m1.ALAMAT, m1.KELURAHAN, m1.KDKECAMATAN, m1.KOTA, m1.KDPROVINSI,
					m1.NOTELP, m1.NOKTP, m1.SUAMI_ORTU, m1.PEKERJAAN,
					m1.NAMA_AYAH, m1.PEKERJAAN_AYAH, m1.NAMA_IBU, m1.PEKERJAAN_IBU, m1.NAMA_PASANGAN, m1.PEKERJAAN_PASANGAN,
					-- menghitung umur pasien
					CASE WHEN YEAR(CURDATE()) - YEAR(m1.TGLLAHIR) > 0
					THEN
						CONCAT(YEAR(CURDATE()) - YEAR(m1.TGLLAHIR), ' Thn')
					ELSE
						CASE WHEN PERIOD_DIFF(DATE_FORMAT(CURDATE(), '%Y%m'), DATE_FORMAT(m1.TGLLAHIR, '%Y%m')) > 0
						THEN
							CONCAT(PERIOD_DIFF(DATE_FORMAT(CURDATE(), '%Y%m'), DATE_FORMAT(m1.TGLLAHIR, '%Y%m')), ' Bln')
						ELSE
							CONCAT(DATEDIFF(CURDATE(), m1.TGLLAHIR), ' Hr')
						END
					END AS UMUR_PASIEN,
					-- status perkawinan pasien
					m1.STATUS,
					CASE
						WHEN m1.STATUS = 3 THEN 'Janda/Duda'
						WHEN m1.STATUS = 2 THEN 'Kawin'
						ELSE 'Belum Kawin'
					END AS STATUS_PERKAWINAN_LABEL,
					-- agama
					m1.AGAMA,
					CASE
						WHEN m1.AGAMA = 1 THEN 'Islam'
						WHEN m1.AGAMA = 2 THEN 'Kristen Protestan'
						WHEN m1.AGAMA = 3 THEN 'Khatolik'
						WHEN m1.AGAMA = 4 THEN 'Hindu'
						WHEN m1.AGAMA = 5 THEN 'Budha'
						ELSE 'Lain - Lain'
					END AS AGAMA_LABEL,
					-- pendidikan terakhir
					m1.PENDIDIKAN,
					CASE
						WHEN m1.PENDIDIKAN = 1 THEN 'SD'
						WHEN m1.PENDIDIKAN = 2 THEN 'SLTP'
						WHEN m1.PENDIDIKAN = 3 THEN 'SMU'
						WHEN m1.PENDIDIKAN = 4 THEN 'D3/Akademik'
						WHEN m1.PENDIDIKAN = 5 THEN 'Universitas'
						ELSE 'Belum Sekolah'
					END AS PENDIDIKAN_LABEL,
					m1.NIP,
					-- tgl. awal daftar
					m1.TGLDAFTAR, DATE_FORMAT(m1.TGLDAFTAR, '%d/%m/%Y') AS TGLDAFTAR_CAL,
					m1.ALAMAT_KTP, m1.PARENT_NOMR, m1.PENANGGUNGJAWAB_NAMA, m1.PENANGGUNGJAWAB_HUBUNGAN, m1.PENANGGUNGJAWAB_ALAMAT,
					m1.PENANGGUNGJAWAB_PHONE, m1.KDCARABAYAR, m1.NO_KARTU,
					m2.namakelurahan,
					m3.namakecamatan,
					m4.namakota,
					m5.namaprovinsi,
					m6.NAMA as CARABAYAR, m6.JMKS,
					m7.ROLES, m7.KDUNIT
			FROM m_pasien m1
			LEFT JOIN m_kelurahan m2 ON m2.idkelurahan = m1.KELURAHAN
			LEFT JOIN m_kecamatan m3 ON m3.idkecamatan = m2.idkecamatan
			LEFT JOIN m_kota m4 ON m4.idkota = m3.idkota
			LEFT JOIN m_provinsi m5 ON m5.idprovinsi = m4.idprovinsi
			LEFT JOIN m_carabayar m6 ON m6.KODE = m1.KDCARABAYAR
			LEFT JOIN m_login m7 ON m7.NIP = m1.NIP
			{$query}";

	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY m1.NOMR ASC";
	
	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}

	$result = mysql_query($sql);

	if ( mysql_num_rows($result) > 0 ) {
		$counter = (isset($offset)) ? $offset + 1 : 1;
		while ( $row = mysql_fetch_assoc($result) ) {
			$row['COUNTER'] = $counter;
			$data[] = $row;

			$counter++;
		}

		return $data;
	}

	return false;
}

//---------------------------------------------------------------------------------

function row_pasien($config=array())
{

	$result = get_pasien($config);

	if ( is_array($result) ) {

		return $result[0];

	}

	return FALSE;

}

//---------------------------------------------------------------------------------

function count_pasien($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;

	$sql = "SELECT COUNT(*) AS numrows
			FROM m_pasien m1
			LEFT JOIN m_kelurahan m2 ON m2.idkelurahan = m1.KELURAHAN
			LEFT JOIN m_kecamatan m3 ON m3.idkecamatan = m2.idkecamatan
			LEFT JOIN m_kota m4 ON m4.idkota = m3.idkota
			LEFT JOIN m_provinsi m5 ON m5.idprovinsi = m4.idprovinsi
			LEFT JOIN m_carabayar m6 ON m6.KODE = m1.KDCARABAYAR
			LEFT JOIN m_login m7 ON m7.NIP = m1.NIP
			{$query}";
	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}

//---------------------------------------------------------------------------------

if ( ! function_exists('insert_pasien') )
{

	function insert_pasien($data=array())
	{

		if ( is_array($data) === FALSE ) {
		
			throw new \Exception("Parameter pertama harus bertipe array");
		
		}
		else if ( count($data) == 0 ) {

			throw new \Exception("Parameter pertama harus berisi setidaknya 1 indeks");
			
		}
		else {

			// parsing "set" statement
			$set = '';
			foreach ( $data as $field => $value ) {
				if ( $value != "" ) {
					$set .= ",{$field} = '{$value}'";
				}
			}

			$set = substr($set, 1);

			if ( strlen($set) > 0 ) {

				$sql = "INSERT INTO m_pasien SET {$set}";

				$result = mysql_query($sql);

				if ( $result == FALSE ) {

					throw new \Exception("Error 500. Tidak dapat menyimpan data");

				}
				else {

					return TRUE;

				}

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('update_pasien') )
{

	function update_pasien($data=array(), $filter=array())
	{

		if ( is_array($data) === FALSE ) {
		
			throw new \Exception("Parameter pertama harus bertipe array");
		
		}
		else if ( count($data) == 0 ) {

			throw new \Exception("Parameter pertama harus berisi setidaknya 1 indeks");
			
		}
		else if ( is_array($filter) === FALSE ) {
		
			throw new \Exception("Parameter kedua harus bertipe array");
		
		}
		else if ( count($filter) == 0 ) {

			throw new \Exception("Parameter kedua harus berisi setidaknya 1 indeks");
			
		}
		else {

			// parsing "set" statement
			$set = '';
			foreach ( $data as $field => $value ) {
				if ( $value != "" ) {
					$set .= ",{$field} = '{$value}'";
				}
			}

			$set = substr($set, 1);

			// parsing "where" statement
			$where = '';
			$i = 0;
			foreach ( $filter as $field => $value ) {
				if ( $value != "" ) {
					if ( $i == 0 ) {
						$where .= "{$field} = '{$value}'";
					}
					else {
						$where .= " AND {$field} = '{$value}'";
					}
				}
				$i++;
			}

			if ( strlen($set) > 0 ) {

				$sql = "UPDATE m_pasien SET {$set} WHERE {$where}";

				$result = mysql_query($sql);

				if ( $result == FALSE ) {

					throw new \Exception("Error 500. Tidak dapat mengupdate data");

				}
				else {

					return TRUE;

				}

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('updateNoMr') )
{

	function updateNoMr($newNoMr=NULL, $oldNoMr=NULL)
	{

		if ( is_null($newNoMr) ) {
		
			throw new \Exception("No. Rekam Medis baru tidak boleh kosong");
		
		}
		else if ( is_null($oldNoMr) ) {
		
			throw new \Exception("No. Rekam Medis lama tidak boleh kosong");
		
		}
		else {

			$sql = "CALL pr_update_nomr('{$newNoMr}', '{$oldNoMr}')";
			$result = mysql_query($sql);
			$data = mysql_fetch_assoc($result);

			if ( $data['result'] == TRUE ) {

				return TRUE;

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('deletePasien') ) 
{

	function deletePasien($nomr=NULL) {

		if ( is_null($nomr) || empty($nomr) ) {

			throw new \Exception("Nomor rekam medis tidak boleh kosong");

		}
		else {

			$sql = "CALL pr_delete_pasien('{$nomr}')";
			$result = mysql_query($sql);
			$data = mysql_fetch_assoc($result);

			if ( $data['result'] == TRUE ) {

				return TRUE;

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('merger_pasien') )
{

	function merger_pasien($nomr_a=NULL, $nomr_b=NULL, $nomr_rujukan=NULL)
	{

		// validation
		// nomr_a tidak boleh kosong
		if ( is_null($nomr_a) || empty($nomr_a) ) {

			throw new \Exception("Nomor rekam medis Pasien A tidak boleh kosong");
		
		}
		else if ( is_null($nomr_b) || empty($nomr_b) ) {

			throw new \Exception("Nomor rekam medis Pasien B tidak boleh kosong");

		}
		else if ( is_null($nomr_rujukan) || empty($nomr_rujukan) ) {

			throw new \Exception("Nomor rekam medis rujukan tidak boleh kosong");

		}
		else {

			$sql = "CALL pr_merge_pasien('{$nomr_a}', '{$nomr_b}', '{$nomr_rujukan}')";
			$result = mysql_query($sql);
			$data = mysql_fetch_assoc($result);

			if ( $data['result'] == TRUE ) {

				return TRUE;

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------