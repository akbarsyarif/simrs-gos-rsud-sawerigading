<?php
namespace Models\Reminder;

//---------------------------------------------------------------------------------

function get($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT * FROM reminder {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY id DESC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}

//---------------------------------------------------------------------------------

function row($config=array())
{
	$result = get($config);

	if ( $result !== false ) {
		return $result[0];
	}

	return false;
}

//---------------------------------------------------------------------------------

function count($config=array())
{
	$query = (isset($config['query'])) ? $config['query'] : null;

	$sql = "SELECT COUNT('id') AS numrows FROM reminder {$query}";
	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}

//---------------------------------------------------------------------------------

function insert($data=array())
{
	if ( is_array($data) && count($data) > 0 ) {
		$params = '';
		foreach ( $data as $key => $value ) {
			$params .= ",{$key} = '{$value}'";
		}

		$params = ltrim($params, ',');

		$sql = "INSERT INTO reminder SET {$params}";
		$result = mysql_query($sql);

		return $result;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function update($data=array(), $pk)
{
	if ( is_array($data) && count($data) > 0 ) {
		$params = '';
		foreach ( $data as $key => $value ) {
			$params .= ",{$key} = '{$value}'";
		}

		$params = ltrim($params, ',');

		$sql = "UPDATE reminder SET {$params} WHERE id = '{$pk}'";
		$result = mysql_query($sql);

		return $result;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function delete($pk)
{

	$sql = "DELETE FROM reminder WHERE id = '{$pk}'";
	$result = mysql_query($sql);

	return $result;

}

//---------------------------------------------------------------------------------