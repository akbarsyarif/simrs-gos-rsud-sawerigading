<?php
namespace Models\BarangGroup;

function get_barang_group($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT * FROM m_barang_group {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY farmasi ASC, nama_group ASC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}

function count($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;

	$sql = "SELECT COUNT(*) AS numrows FROM m_barang_group {$query}";
	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}