<?php
namespace Models\Ranap;

require BASEPATH . 'include/security_helper.php';

//---------------------------------------------------------------------------------

function getPasienLunas($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 0;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT a.nomr, a.id_admission, a.masukrs, a.keluarrs, c.NAMA AS nama_pasien, d.nama As namaruang
            FROM t_admission a
            JOIN t_bayarranap b ON a.nomr = b.NOMR 
            JOIN m_pasien c ON c.nomr = a.nomr
            JOIN m_ruang d ON d.no = a.noruang
            WHERE b.status = 'LUNAS' AND a.keluarrs IS NULL
            GROUP BY id_admission
	        {$query}";
	
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY a.id_admission ASC";

	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}
	
	// var_dump($sql);
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}

//---------------------------------------------------------------------------------

function rowPasienLunas($config=array())
{
	$result = getPasienLunas($config);

	if ( $result !== false ) {
		return $result[0];
	}

	return false;
}

//---------------------------------------------------------------------------------

function countPasienLunas($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 0;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT COUNT(a.id_admission) AS total_data, a.nomr, a.id_admission, a.masukrs, a.keluarrs, c.NAMA, d.nama As namaruang
            FROM t_admission a
            JOIN t_bayarranap b ON a.nomr = b.NOMR 
            JOIN m_pasien c ON c.nomr = a.nomr
            JOIN m_ruang d ON d.no = a.noruang
            WHERE b.status = 'LUNAS' AND a.keluarrs IS NULL
            GROUP BY id_admission
	        {$query}";
	
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY t1.IDXDAFTAR ASC";

	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}
	
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		$data = mysql_fetch_assoc($result);
		return $data['total_data'];
	}

	return 0;
}

//---------------------------------------------------------------------------------