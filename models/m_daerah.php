<?php
namespace Models;

require_once 'include/connect.php';

/**
 * menampilkan daftar kabupaten/kota berdasarkan
 * @return [type] [description]
 */
function get_kota($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT idkota,idprovinsi,namakota,namaprovinsi FROM view_kota {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY idkota ASC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}

/**
 * menampilkan daftar kecamatan
 * @param  array  $config [description]
 * @return [type]         [description]
 */
function get_kecamatan($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT idkecamatan,idkota,namakecamatan FROM m_kecamatan {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY idkecamatan ASC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}

/**
 * menampilkan daftar kelurahan
 * @param  array  $config [description]
 * @return [type]         [description]
 */
function get_kelurahan($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT idkelurahan,idkecamatan,namakelurahan FROM m_kelurahan {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY idkelurahan ASC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}