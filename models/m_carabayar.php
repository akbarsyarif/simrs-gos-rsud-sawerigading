<?php
namespace Models\CaraBayar;

require_once 'include/connect.php';

//---------------------------------------------------------------------------------

function get($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT * FROM m_carabayar {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY KODE ASC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function row($config=array())
{
	$result = getPoly($config);

	if ( $result === FALSE ) {
		return FALSE;
	}
	else {
		return $result[0];
	}
}

//---------------------------------------------------------------------------------

function count($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;

	$sql = "SELECT COUNT(*) AS numrows FROM m_carabayar {$query}";
	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}

//---------------------------------------------------------------------------------

function insert($data=array())
{
	if ( is_array($data) === FALSE ) {		
		throw new \Exception("Parameter pertama harus bertipe array");
	} else {
		$set = '';
		foreach ( $data as $field => $value ) {
			if ( $value != "" ) {
				$set .= ",{$field} = '{$value}'";
			}
		}

		$set = substr($set, 1);

		if ( strlen($set) > 0 ) {

			$sql = "INSERT INTO m_carabayar SET {$set}";
			$result = mysql_query($sql);

			if ( $result == FALSE ) {
				throw new \Exception("Error 500. Tidak dapat menyimpan data");
			}
			else {
				return TRUE;
			}
		}
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function update($data=array(), $pk=NULL)
{
	if ( is_array($data) === FALSE ) {
		throw new \Exception("Parameter pertama harus bertipe array");
	} else if ( $pk == "" ) {
		throw new \Exception("Primary key tidak boleh kosong");
	} else {
		$set = '';
		foreach ( $data as $field => $value ) {
			if ( $value != "" ) {
				$set .= ",{$field} = '{$value}'";
			}
		}

		$set = substr($set, 1);

		if ( strlen($set) > 0 ) {
			$sql = "UPDATE m_carabayar SET {$set} WHERE KODE = '{$pk}'";
			$result = mysql_query($sql);

			if ( $result == FALSE ) {
				throw new \Exception("Error 500. Tidak dapat mengupdate data");
			} else {
				if ( $pk == NULL ) {
					throw new \Exception('Error 400. Kode transaksi tidak ditemukan');
				} else {
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function isJmks($kode)
{
	$sql = "SELECT JMKS FROM m_carabayar WHERE KODE = '{$kode}'";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		$data = mysql_fetch_assoc($result);
		return $data['JMKS'];
	}

	return FALSE;
}

//---------------------------------------------------------------------------------