<?php
namespace Models\Admission;

//---------------------------------------------------------------------------------

function get($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : NULL;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : NULL;

	$sql = "SELECT admission.id_admission, admission.nomr, admission.parent_nomr, admission.dokterpengirim, admission.statusbayar, admission.kirimdari, 
					admission.keluargadekat, admission.panggungjawab, admission.masukrs, admission.noruang, admission.nott, admission.deposit,
					admission.keluarrs, admission.icd_masuk, admission.icd_keluar, admission.NIP, admission.noruang_asal, admission.nott_asal,
					admission.tgl_pindah, admission.kd_rujuk, admission.st_bayar, admission.dokter_penanggungjawab, admission.perawat,
					pasien.TITLE,
					TRIM(LEFT(TRIM(pasien.NAMA),
						CASE 
							WHEN LOCATE(',', TRIM(pasien.NAMA)) <= 0 THEN LENGTH(TRIM(pasien.NAMA))
							ELSE LOCATE(',', TRIM(pasien.NAMA)) - 1
						END
					)) AS NAMA,
					pasien.TEMPAT,
					-- tgl. lahir pasien
					pasien.TGLLAHIR, DATE_FORMAT(pasien.TGLLAHIR, '%d/%m/%Y') AS TGLLAHIR_CAL,
					pasien.JENISKELAMIN, pasien.ALAMAT, pasien.KELURAHAN, pasien.KDKECAMATAN, pasien.KOTA, pasien.KDPROVINSI,
					pasien.NOTELP, pasien.NOKTP, pasien.SUAMI_ORTU, pasien.PEKERJAAN,
					-- status perkawinan pasien
					pasien.STATUS,
					CASE
						WHEN pasien.STATUS = 3 THEN 'Janda/Duda'
						WHEN pasien.STATUS = 2 THEN 'Kawin'
						ELSE 'Belum Kawin'
					END AS STATUS_PERKAWINAN_LABEL,
					-- agama
					pasien.AGAMA,
					CASE
						WHEN pasien.AGAMA = 1 THEN 'Islam'
						WHEN pasien.AGAMA = 2 THEN 'Kristen Protestan'
						WHEN pasien.AGAMA = 3 THEN 'Khatolik'
						WHEN pasien.AGAMA = 4 THEN 'Hindu'
						WHEN pasien.AGAMA = 5 THEN 'Budha'
						ELSE 'Lain - Lain'
					END AS AGAMA_LABEL,
					-- pendidikan terakhir
					pasien.PENDIDIKAN,
					CASE
						WHEN pasien.PENDIDIKAN = 1 THEN 'SD'
						WHEN pasien.PENDIDIKAN = 2 THEN 'SLTP'
						WHEN pasien.PENDIDIKAN = 3 THEN 'SMU'
						WHEN pasien.PENDIDIKAN = 4 THEN 'D3/Akademik'
						WHEN pasien.PENDIDIKAN = 5 THEN 'Universitas'
						ELSE 'Belum Sekolah'
					END AS PENDIDIKAN_LABEL,
					pasien.NIP, pasien.ALAMAT_KTP,
					pendaftaran.PENANGGUNGJAWAB_NAMA, pendaftaran.PENANGGUNGJAWAB_HUBUNGAN, pendaftaran.PENANGGUNGJAWAB_ALAMAT, pendaftaran.PENANGGUNGJAWAB_PHONE,
					pendaftaran.KDCARABAYAR,
					kelurahan.namakelurahan,
					kecamatan.namakecamatan,
					kota.namakota,
					provinsi.namaprovinsi,
					carabayar.NAMA as CARABAYAR, carabayar.JMKS,
					login.ROLES, login.KDUNIT
			FROM t_admission admission
			INNER JOIN t_pendaftaran pendaftaran ON pendaftaran.IDXDAFTAR = admission.id_admission
			INNER JOIN m_pasien pasien ON pasien.NOMR = pendaftaran.NOMR
			INNER JOIN m_pasien orangtua ON orangtua.NOMR = pendaftaran.PARENT_NOMR
			LEFT JOIN m_kelurahan kelurahan ON kelurahan.idkelurahan = pendaftaran.KELURAHAN
			LEFT JOIN m_kecamatan kecamatan ON kecamatan.idkecamatan = kelurahan.idkecamatan
			LEFT JOIN m_kota kota ON kota.idkota = kecamatan.idkota
			LEFT JOIN m_provinsi provinsi ON provinsi.idprovinsi = kota.idprovinsi
			LEFT JOIN m_carabayar carabayar ON carabayar.KODE = pendaftaran.KDCARABAYAR
			LEFT JOIN m_login login ON login.NIP = pendaftaran.NIP
			{$query}";

	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY admission.id_admission DESC";
	
	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function row($config=array())
{
	$result = get($config);

	if ( $result !== FALSE ) {
		return $result[0];
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function count($config=array())
{
	$query = (isset($config['query'])) ? $config['query'] : NULL;

	$sql = "SELECT COUNT(*) AS numrows
			FROM t_admission admission
			INNER JOIN t_pendaftaran pendaftaran ON pendaftaran.IDXDAFTAR = admission.id_admission
			INNER JOIN m_pasien pasien ON pasien.NOMR = pendaftaran.NOMR
			INNER JOIN m_pasien orangtua ON orangtua.NOMR = pendaftaran.PARENT_NOMR
			LEFT JOIN m_kelurahan kelurahan ON kelurahan.idkelurahan = pendaftaran.KELURAHAN
			LEFT JOIN m_kecamatan kecamatan ON kecamatan.idkecamatan = kelurahan.idkecamatan
			LEFT JOIN m_kota kota ON kota.idkota = kecamatan.idkota
			LEFT JOIN m_provinsi provinsi ON provinsi.idprovinsi = kota.idprovinsi
			LEFT JOIN m_carabayar carabayar ON carabayar.KODE = pendaftaran.KDCARABAYAR
			LEFT JOIN m_login login ON login.NIP = pendaftaran.NIP
			{$query}";

	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}

//---------------------------------------------------------------------------------

function insert($data=array())
{

	if ( is_array($data) === FALSE ) {
		
		throw new Exception("Parameter pertama harus bertipe array");
	
	}
	else if ( count($data) == 0 ) {

		throw new Exception("Parameter pertama harus berisi setidaknya 1 indeks");
		
	}
	else {

		// parsing "set" statement
		$set = '';
		foreach ( $data as $field => $value ) {
			if ( $value != "" ) {
				$set .= ",{$field} = '{$value}'";
			}
		}

		$set = ltrim($set, ',');

		$sql = "INSERT INTO t_admission SET {$set}";
		$result = mysql_query($sql);

		if ( $result == FALSE ) {

			throw new Exception("Internal server error. Gagal menambahkan data.");

		}
		else {

			return TRUE;

		}

	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function update($data=array(), $filter=array())
{

	if ( is_array($data) === FALSE ) {
		
		throw new Exception("Parameter pertama harus bertipe array");
	
	}
	else if ( count($data) == 0 ) {

		throw new Exception("Parameter pertama harus berisi setidaknya 1 indeks");
		
	}
	else if ( is_array($filter) === FALSE ) {
	
		throw new Exception("Parameter kedua harus bertipe array");
	
	}
	else if ( count($filter) == 0 ) {

		throw new Exception("Parameter kedua harus berisi setidaknya 1 indeks");
		
	}
	else {

		// parsing "set" statement
		$set = '';
		foreach ( $data as $field => $value ) {
			if ( $value != "" ) {
				$set .= ",{$field} = '{$value}'";
			}
		}

		$set = ltrim($set, ',');

		// parsing "where" statement
		$where = '';
		$i = 0;
		foreach ( $filter as $field => $value ) {
			if ( $value != "" ) {
				if ( $i == 0 ) {
					$where .= "{$field} = '{$value}'";
				}
				else {
					$where .= " AND {$field} = '{$value}'";
				}
			}
			$i++;
		}

		$sql = "UPDATE t_admission SET {$set} WHERE {$where}";
		$result = mysql_query($sql);

		if ( $result == FALSE ) {

			throw new Exception("Internal server error. Gagal mengupdate data.");

		}
		else {

			return TRUE;

		}

	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function delete($filter=array())
{

	if ( is_array($filter) === FALSE ) {
	
		throw new Exception("Parameter kedua harus bertipe array");
	
	}
	else if ( count($filter) == 0 ) {

		throw new Exception("Parameter kedua harus berisi setidaknya 1 indeks");
		
	}
	else {

		// parsing "where" statement
		$where = '';
		$i = 0;
		foreach ( $filter as $field => $value ) {
			if ( $value != "" ) {
				if ( $i == 0 ) {
					$where .= "{$field} = '{$value}'";
				}
				else {
					$where .= " AND {$field} = '{$value}'";
				}
			}
			$i++;
		}

		$sql = "DELETE FROM t_admission WHERE {$where}";
		$result = mysql_query($sql);

		if ( $result == FALSE ) {

			throw new Exception("Internal server error. Gagal menghapus data.");

		}
		else {

			return TRUE;

		}

	}

	return FALSE;

}

//---------------------------------------------------------------------------------