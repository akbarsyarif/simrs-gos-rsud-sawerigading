<?php
namespace Models\Demografi;

require_once 'include/connect.php';

/**
 * menampilkan kunjungan pasien berdasarkan kabupaten/kota
 * @param  [type] $kd_poly [description]
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function kabupaten($in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "
		SELECT
			IDKOTA,
			NAMA_KOTA AS NAMA_KOTA_1,
			COUNT(NAMA_KOTA) AS JUMLAH,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'L'
				AND NAMA_KOTA = NAMA_KOTA_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS LAKILAKI,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'P'
				AND NAMA_KOTA = NAMA_KOTA_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS PEREMPUAN
		FROM
			view_kunjungan_pasien
		WHERE
			NAMA_KOTA IS NOT NULL
			AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		GROUP BY
			NAMA_KOTA
		ORDER BY
			JUMLAH DESC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}

function kecamatan($kabupaten=NULL, $in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "
		SELECT
			NAMA_KECAMATAN AS NAMA_KECAMATAN_1,
			COUNT(NAMA_KECAMATAN) AS JUMLAH,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'L'
				AND NAMA_KECAMATAN = NAMA_KECAMATAN_1
				AND (
					DATE(TGLREG) BETWEEN '{$in_begin_date}'
					AND '{$in_end_date}'
				)
			) AS LAKILAKI,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'P'
				AND NAMA_KECAMATAN = NAMA_KECAMATAN_1
				AND (
					DATE(TGLREG) BETWEEN '{$in_begin_date}'
					AND '{$in_end_date}'
				)
			) AS PEREMPUAN
		FROM
			view_kunjungan_pasien
		WHERE
			NAMA_KOTA IS NOT NULL
		AND (
			DATE(TGLREG) BETWEEN '{$in_begin_date}'
			AND '{$in_end_date}'
		)
		AND (IDKOTA = '{$kabupaten}')
		GROUP BY
			NAMA_KECAMATAN
		ORDER BY
			JUMLAH DESC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}

/**
 * menampilkan kunjungan pasien berdasarkan jenjang pendidikan
 * @param  [type] $kd_poly [description]
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function pendidikan($in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "
		SELECT
			PENDIDIKAN AS PENDIDIKAN_1,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'L'
				AND PENDIDIKAN = PENDIDIKAN_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS LAKILAKI,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'P'
				AND PENDIDIKAN = PENDIDIKAN_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS PEREMPUAN,
			COUNT(PENDIDIKAN) AS JUMLAH
		FROM
			view_kunjungan_pasien
		WHERE
			PENDIDIKAN IS NOT NULL
			AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		GROUP BY
			PENDIDIKAN
		ORDER BY
			JUMLAH DESC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}

/**
 * menampilkan kunjungan pasien berdasarkan agama
 * @param  [type] $kd_poly [description]
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function agama($in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "
		SELECT
			AGAMA AS AGAMA_1,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'L'
				AND AGAMA = AGAMA_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS LAKILAKI,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'P'
				AND AGAMA = AGAMA_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS PEREMPUAN,
			COUNT(PENDIDIKAN) AS JUMLAH
		FROM
			view_kunjungan_pasien
		WHERE
			AGAMA IS NOT NULL
			AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		GROUP BY
			AGAMA
		ORDER BY
			JUMLAH DESC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}

/**
 * menampilkan kunjungan pasien kelompok usia
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function kelompokUsia($in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "
		SELECT
			KELOMPOK_USIA AS KELOMPOK_USIA_1,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'L'
				AND TGLLAHIR <> '0000-00-00'
				AND KELOMPOK_USIA = KELOMPOK_USIA_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS LAKILAKI,
			(
				SELECT
					COUNT(*)
				FROM
					view_kunjungan_pasien
				WHERE
					JENISKELAMIN = 'P'
				AND TGLLAHIR <> '0000-00-00'
				AND KELOMPOK_USIA = KELOMPOK_USIA_1
				AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
			) AS PEREMPUAN,
			COUNT(KELOMPOK_USIA) AS JUMLAH
		FROM
			view_kunjungan_pasien
		WHERE
			TGLLAHIR <> '0000-00-00'
			AND (DATE(TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
		GROUP BY
			KELOMPOK_USIA
		ORDER BY
			JUMLAH DESC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}