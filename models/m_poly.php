<?php
namespace Models\Poly;

require_once 'include/connect.php';

//---------------------------------------------------------------------------------

function getPoly($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 10;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT * FROM m_poly {$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY kode ASC";
	$sql .= " LIMIT {$offset},{$limit}";
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function rowPoly($config=array())
{

	$result = getPoly($config);

	if ( $result === FALSE ) {
		return FALSE;
	}
	else {
		return $result[0];
	}

}

//---------------------------------------------------------------------------------

function count($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;

	$sql = "SELECT COUNT(*) AS numrows FROM m_poly {$query}";
	$result = mysql_query($sql);
	$data = mysql_fetch_assoc($result);

	return $data['numrows'];
}

//---------------------------------------------------------------------------------