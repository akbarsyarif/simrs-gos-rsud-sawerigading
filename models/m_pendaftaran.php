<?php
namespace Models;

require BASEPATH . 'include/security_helper.php';

//---------------------------------------------------------------------------------

function get_pendaftaran($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 0;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT t1.IDXDAFTAR, t1.NOMR, t1.TGLREG, t1.KDDOKTER, t1.KDPOLY, t1.SHIFT, t1.STATUS, t1.KETERANGAN_STATUS, t1.PASIENBARU, t1.NIP, t1.MASUKPOLY, t1.KELUARPOLY,
				   t1.KDCARABAYAR, t1.PENANGGUNGJAWAB_NAMA, t1.PENANGGUNGJAWAB_HUBUNGAN, t1.PENANGGUNGJAWAB_ALAMAT, t1.PENANGGUNGJAWAB_PHONE, t1.JAMREG, t1.MINTA_RUJUKAN,
				   t1.BATAL, t1.NO_SJP, t1.LAKALANTAS, t1.LOKASI_LAKALANTAS, t1.NO_PESERTA, t1.NO_KARTU, t1.KD_JENIS_PESERTA, t1.NM_JENIS_PESERTA,
				   t1.KD_RUJUK, t1.NO_RUJUKAN, t1.TGL_RUJUKAN AS TIMESTAMP_RUJUKAN, DATE_FORMAT(t1.TGL_RUJUKAN, '%Y-%m-%d') AS TGL_RUJUKAN, t1.KD_PPK_RUJUKAN, t1.NM_PPK_RUJUKAN,
				   t1.KD_DIAGNOSA, t1.NM_DIAGNOSA, t1.KD_KELAS, t1.NO_SEP, t1.CATATAN, t1.JUMLAH_CETAK, t1.TGL_CETAK_TERAKHIR AS DATETIME_CETAK_TERAKHIR, DATE_FORMAT(t1.TGL_CETAK_TERAKHIR, '%Y-%m-%d') AS TGL_CETAK_TERAKHIR,
				   t2.id_admission,t2.keluarrs AS RANAP_KELUARRS,
				   DATE_FORMAT(t1.TGL_CETAK_TERAKHIR, '%H:%i:%s') AS JAM_CETAK_TERAKHIR,
				   m1.id, m1.TITLE, m1.NAMA AS NAMA_PASIEN, m1.TEMPAT AS TMP_LAHIR_PASIEN, m1.TGLLAHIR AS TGL_LAHIR_PASIEN,
				   m1.TGLLAHIR, DATE_FORMAT(m1.TGLLAHIR, '%d/%m/%Y') AS TGLLAHIR_CAL,
				   m1.NO_KARTU AS NO_KARTU_HAK,
				   m1.KD_JENIS_PESERTA AS KD_JENIS_PESERTA_HAK, m1.NM_JENIS_PESERTA AS NM_JENIS_PESERTA_HAK,
				   m1.KD_PROVIDER AS KD_PROVIDER_HAK, m1.NM_PROVIDER AS NM_PROVIDER_HAK,
				   m1.KD_KELAS AS KD_KELAS_HAK, m1.NM_KELAS AS NM_KELAS_HAK,
				    -- nama pasien without title
				    SUBSTRING_INDEX(m1.NAMA, ',', 1) AS NAMA_PASIEN_RAW,
				    -- tanggal pulang/keluar rs
				    CASE
				    	WHEN t2.id_admission <> '' THEN t2.keluarrs
				    	ELSE CONCAT(t1.TGLREG,' ',t1.KELUARPOLY)
				    END AS TGL_PULANG,
				    -- jenis kunjungan
				    CASE
				    	WHEN t1.PASIENBARU = 1 THEN 'Baru'
				    	ELSE 'Lama'
				    END AS JENIS_KUNJUNGAN,
				    -- kd jenis pelayanan
				    CASE
				    	WHEN t2.id_admission <> '' THEN 1
				    	ELSE 2
				    END AS KD_JENIS_PELAYANAN,
				    -- label jenis pelayanan
				    CASE
				    	WHEN t2.id_admission <> '' THEN 'Rawat Inap'
				    	ELSE 'Rawat Jalan'
				    END AS NM_JENIS_PELAYANAN,
				    -- kelas
				   	CASE
				   		WHEN t1.KD_KELAS = 1 THEN 'Kelas I'
				   		WHEN t1.KD_KELAS = 2 THEN 'Kelas II'
				   		WHEN t1.KD_KELAS = 3 THEN 'Kelas III'
				   		ELSE ''
				   	END AS KELAS_LABEL,
				    -- lakalantas
				    CASE
				    	WHEN t1.LAKALANTAS = 1 THEN 'Iya'
				    	ELSE 'Bukan'
				    END AS LAKALANTAS_LABEL,
				    -- kd_lakalantas bpjs
				    CASE
				    	WHEN t1.LAKALANTAS = 1 THEN 1
				    	ELSE 2
				    END AS LAKALANTAS_BPJS,
				    -- shift petugas
				    CASE
				    	WHEN t1.SHIFT = 1 THEN 'Pagi'
				    	WHEN t1.SHIFT = 2 THEN 'Siang'
				    	ELSE 'Sore'
				    END AS SHIFT_LABEL,
				    -- profil rumah sakit
				    (SELECT kdproviderbpjs FROM profil) AS KD_PROV_PELAYANAN,
				  	-- menghitung umur pasien
					CASE WHEN YEAR(CURDATE()) - YEAR(m1.TGLLAHIR) > 0
					THEN
						CONCAT(YEAR(CURDATE()) - YEAR(m1.TGLLAHIR), ' Thn')
					ELSE
						CASE WHEN PERIOD_DIFF(DATE_FORMAT(CURDATE(), '%Y%m'), DATE_FORMAT(m1.TGLLAHIR, '%Y%m')) > 0
						THEN
							CONCAT(PERIOD_DIFF(DATE_FORMAT(CURDATE(), '%Y%m'), DATE_FORMAT(m1.TGLLAHIR, '%Y%m')), ' Bln')
						ELSE
							CONCAT(DATEDIFF(CURDATE(), m1.TGLLAHIR), ' Hr')
						END
					END AS UMUR_PASIEN,
					-- status perkawinan pasien
					m1.STATUS AS STATUS_PERKAWINAN,
					CASE
						WHEN m1.STATUS = 3 THEN 'Janda/Duda'
						WHEN m1.STATUS = 2 THEN 'Kawin'
						ELSE 'Belum Kawin'
					END AS STATUS_PERKAWINAN_LABEL,
					-- Agama
					m1.AGAMA,
					CASE
						WHEN m1.AGAMA = 1 THEN 'Islam'
						WHEN m1.AGAMA = 2 THEN 'Kristen Protestan'
						WHEN m1.AGAMA = 3 THEN 'Khatolik'
						WHEN m1.AGAMA = 4 THEN 'Hindu'
						WHEN m1.AGAMA = 5 THEN 'Budha'
						ELSE 'Lain - Lain'
					END AS AGAMA_LABEL,
					-- Pendidikan terakhir
					m1.PENDIDIKAN,
					CASE
						WHEN m1.PENDIDIKAN = 1 THEN 'SD'
						WHEN m1.PENDIDIKAN = 2 THEN 'SLTP'
						WHEN m1.PENDIDIKAN = 3 THEN 'SMU'
						WHEN m1.PENDIDIKAN = 4 THEN 'D3/Akademik'
						WHEN m1.PENDIDIKAN = 5 THEN 'Universitas'
						ELSE 'Belum Sekolah'
					END AS PENDIDIKAN_LABEL,
					-- waktu registrasi
					DATE_FORMAT(t1.JAMREG, '%Y-%m-%d') AS TGLREG,
					DATE_FORMAT(t1.JAMREG, '%H:%i:%s') AS JAMREG,
					DATE_FORMAT(t1.JAMREG, '%Y-%m-%d %H:%i:%s') AS JAMREGRAW,
					DATE_FORMAT(t1.JAMREG, '%d/%m/%Y') AS TGLREG_CAL,
				   m1.JENISKELAMIN, m1.ALAMAT, m1.KELURAHAN AS KD_KELURAHAN, m1.KDKECAMATAN AS KD_KECAMATAN, m1.KOTA AS KD_KOTA, m1.KDPROVINSI AS KD_PROVINSI, m1.NOTELP, m1.NOKTP, m1.SUAMI_ORTU, m1.PEKERJAAN, m1.TGLDAFTAR, m1.ALAMAT_KTP, m1.PARENT_NOMR,
				   m1.NAMA_AYAH, m1.PEKERJAAN_AYAH, m1.NAMA_IBU, m1.PEKERJAAN_IBU, m1.NAMA_PASANGAN, m1.PEKERJAAN_PASANGAN,
				   m2.nama AS NAMA_POLY, m2.kd_bpjs AS KD_POLY_BPJS, m2.jenispoly,
				   m3.NAMADOKTER, m3.KDPROFESI, m3.NAMAPROFESI, m3.st_aktif,
				   m4.namakelurahan,
				   m5.namakecamatan,
				   m6.namakota,
				   m7.namaprovinsi,
				   m8.NAMA as CARABAYAR, m8.JMKS,
				   m9.NAMA as RUJUKAN,
				   m10.ROLES, m10.KDUNIT
			FROM t_pendaftaran t1
			LEFT JOIN t_admission t2 ON t2.id_admission = t1.IDXDAFTAR
	        INNER JOIN m_pasien m1 ON t1.NOMR = m1.NOMR
	        LEFT JOIN m_poly m2 ON t1.KDPOLY = m2.kode
	        LEFT JOIN m_dokter m3 ON t1.KDDOKTER = m3.KDDOKTER
	        LEFT JOIN m_kelurahan m4 ON m1.KELURAHAN = m4.idkelurahan
	        LEFT JOIN m_kecamatan m5 ON m4.idkecamatan = m5.idkecamatan
	        LEFT JOIN m_kota m6 ON m5.idkota = m6.idkota
	        LEFT JOIN m_provinsi m7 ON m6.idprovinsi = m7.idprovinsi
	        LEFT JOIN m_carabayar m8 ON t1.KDCARABAYAR = m8.KODE
	        LEFT JOIN m_rujukan m9 ON t1.KD_RUJUK = m9.KODE
	        LEFT JOIN m_login m10 ON m10.NIP = t1.NIP
	        {$query}";
	
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY t1.IDXDAFTAR ASC";

	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}
	
	// var_dump($sql);
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return false;
}

//---------------------------------------------------------------------------------

function row_pendaftaran($config=array())
{
	$result = get_pendaftaran($config);

	if ( $result !== false ) {
		return $result[0];
	}

	return false;
}

//---------------------------------------------------------------------------------

function count_pendaftaran($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : null;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 0;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : null;

	$sql = "SELECT COUNT(*) AS total_data
			FROM t_pendaftaran t1
			LEFT JOIN t_admission t2 ON t2.id_admission = t1.IDXDAFTAR
	        INNER JOIN m_pasien m1 ON t1.NOMR = m1.NOMR
	        LEFT JOIN m_poly m2 ON t1.KDPOLY = m2.kode
	        LEFT JOIN m_dokter m3 ON t1.KDDOKTER = m3.KDDOKTER
	        LEFT JOIN m_kelurahan m4 ON m1.KELURAHAN = m4.idkelurahan
	        LEFT JOIN m_kecamatan m5 ON m4.idkecamatan = m5.idkecamatan
	        LEFT JOIN m_kota m6 ON m5.idkota = m6.idkota
	        LEFT JOIN m_provinsi m7 ON m6.idprovinsi = m7.idprovinsi
	        LEFT JOIN m_carabayar m8 ON t1.KDCARABAYAR = m8.KODE
	        LEFT JOIN m_rujukan m9 ON t1.KD_RUJUK = m9.KODE
	        LEFT JOIN m_login m10 ON m10.NIP = t1.NIP
	        {$query}";
	
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY t1.IDXDAFTAR ASC";

	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}
	
	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		$data = mysql_fetch_assoc($result);
		return $data['total_data'];
	}

	return 0;
}

//---------------------------------------------------------------------------------

function has_sep($idxdaftar)
{
	$result = row_pendaftaran(array(
		'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
	));

	if ( $result !== false ) {
		if ( (! is_null($result['NO_SEP'])) && (! empty($result['NO_SEP'])) ) {
			return true;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------

function update_t_pendaftaran($data, $pk)
{
	$str = '';

	$sql = "UPDATE t_pendaftaran SET ";
	foreach ( $data as $col => $val ) {
		$str .= ",{$col}='{$val}'";
	}

	$sql .= substr($str, 1);

	$sql .= " WHERE IDXDAFTAR = '{$pk}'";

	// exit($sql);

	$result = mysql_query($sql);

	return $result;
}

//---------------------------------------------------------------------------------

if ( ! function_exists('incJumlahCetak') )
{

	function incJumlahCetak($idxdaftar)
	{

		$check = row_pendaftaran( array (
			'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
		) );

		if ( $check !== FALSE ) {

			$sql = "UPDATE t_pendaftaran SET JUMLAH_CETAK = JUMLAH_CETAK+1, TGL_CETAK_TERAKHIR = NOW() WHERE IDXDAFTAR = '{$idxdaftar}'";

			$result = mysql_query($sql);

			if ( mysql_affected_rows() > 0 ) {

				$info_cetak = row_pendaftaran( array (
					'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
				) );

				return $info_cetak;

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('resetJumlahCetak') )
{

	function resetJumlahCetak($idxdaftar)
	{

		$check = row_pendaftaran( array (
			'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
		) );

		if ( $check !== FALSE ) {

			$sql = "UPDATE t_pendaftaran SET JUMLAH_CETAK = 0, TGL_CETAK_TERAKHIR = NULL WHERE IDXDAFTAR = '{$idxdaftar}'";

			$result = mysql_query($sql);

			if ( mysql_affected_rows() > 0 ) {

				return TRUE;

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('insert_pendaftaran') )
{

	function insert_pendaftaran($data=array())
	{

		if ( is_array($data) === FALSE ) {
		
			throw new \Exception("Parameter pertama harus bertipe array");
		
		}
		else {

			$set = '';
			foreach ( $data as $field => $value ) {
				if ( $value != "" ) {
					$set .= ",{$field} = '{$value}'";
				}
			}

			$set = substr($set, 1);

			if ( strlen($set) > 0 ) {

				$sql = "INSERT INTO t_pendaftaran SET {$set}";

				$result = mysql_query($sql);

				if ( $result == FALSE ) {

					throw new \Exception("Error 500. Tidak dapat menyimpan data");

				}
				else {

					return TRUE;

				}

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('update_pendaftaran') )
{

	function update_pendaftaran($data=array(), $pk=NULL)
	{

		if ( is_array($data) === FALSE ) {
		
			throw new \Exception("Parameter pertama harus bertipe array");
		
		}
		else if ( $pk == "" ) {
		
			throw new \Exception("Primary key tidak boleh kosong");
		
		}
		else {

			$set = '';
			foreach ( $data as $field => $value ) {
				if ( $value != "" ) {
					$set .= ",{$field} = '{$value}'";
				}
			}

			$set = substr($set, 1);

			if ( strlen($set) > 0 ) {

				$sql = "UPDATE t_pendaftaran SET {$set} WHERE IDXDAFTAR = '{$pk}'";

				$result = mysql_query($sql);

				if ( $result == FALSE ) {

					throw new \Exception("Error 500. Tidak dapat mengupdate data");

				}
				else {

					if ( $pk == NULL ) {

						throw new \Exception('Error 400. Kode transaksi tidak ditemukan');

					}
					else {

						return TRUE;

					}

				}

			}

		}

		return FALSE;

	}

}

//---------------------------------------------------------------------------------

// menghapus data kunjungan dan relasinya
if ( ! function_exists('deleteKunjungan') )
{

	function deleteKunjungan($idxdaftar)
	{

		// start transaction
		mysql_query("START TRANSACTION");
		
		// delete t_pendaftaran and it's relations
		$a = mysql_query("DELETE FROM t_pendaftaran WHERE IDXDAFTAR = '{$idxdaftar}'");
		$c = mysql_query("DELETE FROM t_pendaftaran_iso WHERE idxdaftar = '{$idxdaftar}'");
		$b = mysql_query("DELETE FROM t_billrajal WHERE IDXDAFTAR = '{$idxdaftar}'");
		$c = mysql_query("DELETE FROM t_bayarrajal WHERE IDXDAFTAR = '{$idxdaftar}'");

		if ( $a == TRUE && $b == TRUE && $c == TRUE ) {

			$result = TRUE;

			// commit change
			mysql_query("COMMIT");

		}
		else {

			$result = FALSE;

			// rollback change if any error found
			mysql_query("ROLLBACK");

		}

		return $result;

	}

}

//---------------------------------------------------------------------------------