<?php
namespace Models\LapRawatJalan;

require_once 'include/connect.php';

/**
 * menampilkan kunjungan pasien rawat jalan berdasarkan poli
 * @param  [type] $kd_poly [description]
 * @param  [type] $in_begin_date [description]
 * @param  [type] $in_end_date   [description]
 * @return [type]                [description]
 */
function dataPasien($kd_poly=NULL, $kd_carabayar=NULL, $jenis_kepesertaan=NULL, $in_begin_date=NULL, $in_end_date=NULL)
{
	$sql = "SELECT
		m_pasien.NOMR,
		m_pasien.NAMA AS NAMA_PASIEN,
		m_kelurahan.namakelurahan AS NAMA_KELURAHAN,
		m_kecamatan.namakecamatan AS NAMA_KECAMATAN,
		m_kota.namakota AS NAMA_KOTA,
		m_provinsi.namaprovinsi AS NAMA_PROVINSI,
		t_pendaftaran.IDXDAFTAR,
		DATE(t_pendaftaran.TGLREG) AS TGLREG,
		DATE_FORMAT(t_pendaftaran.TGLREG, '%d/%m/%Y') AS TGLREG_INDONESIA,
		TIME(t_pendaftaran.JAMREG) AS JAMREG,
		m_carabayar.NAMA AS CARABAYAR,
		t_diagnosadanterapi.DIAGNOSA,
		t_diagnosadanterapi.TERAPI,
		m_poly.nama AS NAMA_POLY,
		m_dokter.NAMADOKTER AS NAMA_DOKTER
	FROM
		t_pendaftaran
	INNER JOIN m_pasien ON t_pendaftaran.NOMR = m_pasien.NOMR
    LEFT JOIN m_poly ON t_pendaftaran.KDPOLY = m_poly.kode
    LEFT JOIN m_dokter ON t_pendaftaran.KDDOKTER = m_dokter.KDDOKTER
    LEFT JOIN m_kelurahan ON m_pasien.KELURAHAN = m_kelurahan.idkelurahan
    LEFT JOIN m_kecamatan ON m_kelurahan.idkecamatan = m_kecamatan.idkecamatan
    LEFT JOIN m_kota ON m_kecamatan.idkota = m_kota.idkota
    LEFT JOIN m_provinsi ON m_kota.idprovinsi = m_provinsi.idprovinsi
    LEFT JOIN m_carabayar ON t_pendaftaran.KDCARABAYAR = m_carabayar.KODE
    LEFT JOIN m_rujukan ON t_pendaftaran.KD_RUJUK = m_rujukan.KODE
    LEFT JOIN m_login ON m_login.NIP = t_pendaftaran.NIP
	LEFT JOIN t_diagnosadanterapi ON t_diagnosadanterapi.IDXDAFTAR = t_pendaftaran.IDXDAFTAR
	WHERE (m_login.ROLES = 1 OR m_login.ROLES = NULL) 
		AND (t_pendaftaran.TGLREG <> '' AND t_pendaftaran.TGLREG IS NOT NULL)
		AND (t_pendaftaran.MASUKPOLY <> '' AND t_pendaftaran.MASUKPOLY IS NOT NULL)
		AND (m_poly.kode <> 9)
		AND (m_poly.kode <> 10)";

	if ( !is_null($kd_poly) && !empty($kd_poly) ) {
		$sql .= "AND (m_poly.kode = '{$kd_poly}')";
	}

	if ( !is_null($kd_carabayar) && !empty($kd_carabayar) ) {
		$sql .= "AND (t_pendaftaran.KDCARABAYAR = '{$kd_carabayar}')";
	}

	if ( !is_null($jenis_kepesertaan) && !empty($jenis_kepesertaan) ) {
		if ($jenis_kepesertaan == "pbi") {
			$sql .= "AND (t_pendaftaran.NM_JENIS_PESERTA LIKE '%PBI%')";
		} else if ($jenis_kepesertaan == "non_pbi") {
			$sql .= "AND (t_pendaftaran.NM_JENIS_PESERTA NOT LIKE '%PBI%')";
		}
	}
		
	$sql .= "
		AND (DATE(t_pendaftaran.TGLREG) BETWEEN '{$in_begin_date}' AND '{$in_end_date}')
	ORDER BY m_poly.nama ASC, DATE(t_pendaftaran.TGLREG) ASC";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$row['TINDAKAN'] = getTindakan($row['IDXDAFTAR']);
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}

function getTindakan($idxdaftar)
{
	$sql = "
		SELECT
			t_billrajal.IDXDAFTAR,
			t_billrajal.KODETARIF,
			m_tarif2012.nama_tindakan AS NAMA_TINDAKAN
		FROM
			t_billrajal
		INNER JOIN m_tarif2012 ON m_tarif2012.kode_tindakan = t_billrajal.KODETARIF
		WHERE t_billrajal.IDXDAFTAR = '{$idxdaftar}'
	";

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$rows[] = $row;
		}

		$data = array(
			'count' => count($rows),
			'rows' => $rows
		);

		return $data;
	}

	return FALSE;
}