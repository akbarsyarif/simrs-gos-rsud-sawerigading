<?php 
session_start();
require '../include/connect.php';
require BASEPATH . 'vendor/autoload.php';
require BASEPATH . 'include/function.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'include/pasien_helper.php';
require BASEPATH . 'tracer/action_tracer.php';

$_error_msg = "";
$rm_manual  = ($_POST['rm_manual'] == 1) ? 1 : 0;
$status		= $_POST['STATUSPASIEN'];
$ketemu 	= FALSE;

// username petugas
$user_login = $_SESSION['NIP'];

// data medis
$nomr              = "";
$kd_rujuk          = xss_clean($_POST['KDRUJUK']);
$kd_carabayar      = xss_clean($_POST['KDCARABAYAR']);
$kd_poly           = xss_clean($_POST['KDPOLY']);
$kd_dokter         = xss_clean($_POST['KDDOKTER']);
$minta_rujukan     = xss_clean($_POST['minta_rujukan']);
$tgl_reg           = xss_clean($_POST['TGLREG']);
$lakalantas        = (!empty($_POST['LAKALANTAS'])) ? xss_clean($_POST['LAKALANTAS']) : 2;
$lokasi_lakalantas = ($lakalantas == 1) ? xss_clean($_POST['LOKASI_LAKALANTAS']) : NULL;
$catatan           = xss_clean($_POST['catatan']);
$shift             = xss_clean($_POST['SHIFT']);

// data JKN/BPJS
$no_kartu         = (isset($_POST['no_kartu']) && ! empty($_POST['no_kartu'])) ? xss_clean($_POST['no_kartu']) : NULL;
$kd_jenis_peserta = (isset($_POST['kd_jenis_peserta']) && ! empty($_POST['kd_jenis_peserta'])) ? xss_clean($_POST['kd_jenis_peserta']) : NULL;
$nm_jenis_peserta = (isset($_POST['nm_jenis_peserta']) && ! empty($_POST['nm_jenis_peserta'])) ? xss_clean($_POST['nm_jenis_peserta']) : NULL;
$kd_kelas         = (isset($_POST['kd_kelas']) && ! empty($_POST['kd_kelas'])) ? xss_clean($_POST['kd_kelas']) : NULL;
$nm_kelas         = (isset($_POST['nm_kelas']) && ! empty($_POST['nm_kelas'])) ? xss_clean($_POST['nm_kelas']) : NULL;
$no_rujukan       = (isset($_POST['no_rujukan']) && ! empty($_POST['no_rujukan'])) ? xss_clean($_POST['no_rujukan']) : NULL;
$tgl_rujukan      = (isset($_POST['tgl_rujukan']) && ! empty($_POST['tgl_rujukan'])) ? xss_clean($_POST['tgl_rujukan']) : NULL;
$kd_ppk_rujukan   = (isset($_POST['kd_ppk_rujukan']) && ! empty($_POST['kd_ppk_rujukan'])) ? xss_clean($_POST['kd_ppk_rujukan']) : NULL;
$nm_ppk_rujukan   = (isset($_POST['nm_ppk_rujukan']) && ! empty($_POST['nm_ppk_rujukan'])) ? xss_clean($_POST['nm_ppk_rujukan']) : NULL;
$kd_diagnosa      = (isset($_POST['kd_diagnosa']) && ! empty($_POST['kd_diagnosa'])) ? xss_clean($_POST['kd_diagnosa']) : NULL;
$nm_diagnosa      = (isset($_POST['nm_diagnosa']) && ! empty($_POST['nm_diagnosa'])) ? xss_clean($_POST['nm_diagnosa']) : NULL;

// data pasien
$nama_pasien              = xss_clean($_POST['NAMA']);
$alias                    = xss_clean($_POST['CALLER']);
$tmp_lahir                = xss_clean($_POST['TEMPAT']);
$tgl_lahir                = xss_clean($_POST['TGLLAHIR']);
$umur                     = xss_clean($_POST['umur']);
$alamat_sekarang          = xss_clean($_POST['ALAMAT']);
$alamat_ktp               = xss_clean($_POST['ALAMAT_KTP']);
$kd_provinsi              = xss_clean($_POST['KDPROVINSI']);
$kd_kota                  = xss_clean($_POST['KOTA']);
$kd_kecamatan             = xss_clean($_POST['KDKECAMATAN']);
$kd_kelurahan             = xss_clean($_POST['KELURAHAN']);
$no_telp                  = xss_clean($_POST['NOTELP']);
$no_ktp                   = xss_clean($_POST['NOKTP']);
$nama_suami_ortu          = xss_clean($_POST['SUAMI_ORTU']);
$pekerjaan                = xss_clean($_POST['PEKERJAAN']);
$nama_penanggungjawab     = xss_clean($_POST['nama_penanggungjawab']);
$hubungan_penanggungjawab = xss_clean($_POST['hubungan_penanggungjawab']);
$alamat_penanggungjawab   = xss_clean($_POST['alamat_penanggungjawab']);
$phone_penanggungjawab    = xss_clean($_POST['phone_penanggungjawab']);
$jenis_kelamin            = xss_clean($_POST['JENISKELAMIN']);
$status_perkawinan        = xss_clean($_POST['STATUS']);
$pendidikan               = xss_clean($_POST['PENDIDIKAN']);
$agama                    = xss_clean($_POST['AGAMA']);

// cek apakah pasien merupakan pasien baru atau pasien lama
// kode 1 untuk pasien baru, kode 0 untuk pasien lama
if ( $status == 1 ) {
	if ( $rm_manual ) {
		// nomor rekam medik manual
		$nomr = xss_clean($_POST['NOMR']);
		if ( is_pasien_exist($nomr) ) $_error_msg .= "Nomor {$nomr} sudah digunakan, ";
	}
	else {
		// nomor rekam medik otomatis
		$nomr = create_nomr();
		while ( is_pasien_exist($nomr) ) {
			$nomr = create_nomr();
		}
	}
}
else {
	// pasien lama
	// cek apakah nomor rekam medik terdaftar
	$nomr = xss_clean($_POST['NOMR']);
	if ( is_pasien_exist($nomr) === FALSE ) {
		$_error_msg .= "Nomor rekam medis pasien {$nomr} tidak ditemukan, ";
	}
	else {
		$ketemu = TRUE;
	}
}

// proses validasi form
if ( strlen( $nomr ) != 6 ) $_error_msg .= "No rekam medik harus 6 digit, ";
if ( $kd_carabayar == "" ) $_error_msg .= "Cara Bayar Belum Dipilih, ";
if ( $_POST['KDRUJUK']=="" ) $_error_msg .= "Rujukan Pasien Belum Dipilih, ";
if ( $_POST['SHIFT']=="" ) $_error_msg .= "Shift Belum Dipilih, ";
if ( $_POST['NAMA']=="" ) $_error_msg .= "Nama Pasien Belum Diisi, ";
if ( $_POST['TEMPAT']=="" ) $_error_msg .= "Tempat Lahir Belum Lengkap, ";
if ( $_POST['TGLLAHIR']=="" ) $_error_msg .= "Tanggal Lahir Belum Lengkap, ";
if ( $_POST['JENISKELAMIN']=="" ) $_error_msg .= "Jenis Kelamin Belum Dipilih, ";
if ( $_POST['ALAMAT']=="" ) $_error_msg .= "Alamat Belum Lengkap, ";
if ( $_POST['KELURAHAN']=="" ) $_error_msg .= "Kelurahan Belum Dipilih, ";
if ( $_POST['KDKECAMATAN']=="0" ) $_error_msg .= "Kecamatan Belum Dipilih, ";
if ( $_POST['KOTA']=="" ) $_error_msg .= "Kota Belum Lengkap, ";
if ( $_POST['KDPROVINSI']=="" ) $_error_msg .= "Provinsi Belum Lengkap, ";
if ( $_POST['KDPOLY']=="0" ) $_error_msg .= "Poli Belum Dipilih, ";
if ( $_POST['KDDOKTER']=="") $_error_msg .= "Dokter Jaga Belum Ada, ";

if ( strlen($_error_msg) > 0 ) {
	$_error_msg = substr($_error_msg, 0, strlen($_error_msg)-2).".";

	$response = (object) array (
		'metadata' => (object) array (
			'code' => "800",
			'message' => $_error_msg
		),
		'response' => NULL
	);
}
else {
	if ( ! empty($_POST['KDDOKTER']) ) {
		$dokter = trim($_POST['KDDOKTER']);
	} else {
		$dokter = "NULL";
	}

	if ( ! empty($_POST['KDCARABAYAR']) ) {
		$KDCARABAYAR = trim($_POST['KDCARABAYAR']);
	} else {
		$KDCARABAYAR = 1;
	}
	
	if ( empty($_POST['PENDIDIKAN']) ) {
		$pendidikan = "NULL";
	} else {
		$pendidikan = $_POST['PENDIDIKAN'];
	}

	if ( empty($_POST['AGAMA']) ) {
		$agama = "NULL";
	} else {
		$agama = $_POST['AGAMA'];
	}

	if ( !isset($_POST['STATUS']) OR empty($_POST['STATUS']) ) {
		$status_perkawinan = "0";
	} else {
		$status_perkawinan = $_POST['STATUS'];
	}

	if ( ! empty($_POST['CALLER']) ) {
		$NAMADATA = str_replace(',',' ',$_POST['NAMA']).', '.$_REQUEST['CALLER'];
	} else {
		$NAMADATA = str_replace(',',' ',$_POST['NAMA']);
	}
	$nama_pasien = xss_clean($NAMADATA);

	if ( empty($minta_rujukan) ) {
		$minta_rujukan = "0";
	} else {
		$minta_rujukan = "1";
	}
	
	$tmpTGLLAHIR = date('Y-m-d', strtotime(str_replace('/','-',$_POST['TGLLAHIR'])));
	$tgl_lahir = trim($tmpTGLLAHIR);
	
	if ( $ketemu == TRUE ) {
		$sqlupdate_pasien = "UPDATE m_pasien 
		                     SET NAMA = '{$nama_pasien}', 
								 TITLE = '{$alias}',
								 TEMPAT  = '{$tmp_lahir}', 
								 TGLLAHIR = '{$tgl_lahir}', 
								 JENISKELAMIN = '{$jenis_kelamin}', 
								 ALAMAT = '{$alamat_sekarang}', 
								 KELURAHAN = '{$kd_kelurahan}', 
								 KDKECAMATAN = '{$kd_kecamatan}', 
								 KOTA = '{$kd_kota}', 
								 KDPROVINSI = '{$kd_provinsi}', 
								 NOTELP = '{$no_telp}', 
								 NOKTP = '{$no_ktp}', 
								 SUAMI_ORTU = '{$nama_suami_ortu}', 
								 PEKERJAAN = '{$pekerjaan}', 
								 STATUS = '{$status_perkawinan}', 
								 AGAMA = '{$agama}', 
								 PENDIDIKAN = '{$pendidikan}', 
								 KDCARABAYAR = '{$kd_carabayar}', 
								 NIP = '{$user_login}', 
								 ALAMAT_KTP = '{$alamat_ktp}', 
								 PENANGGUNGJAWAB_NAMA = '{$nama_penanggungjawab}', 
								 PENANGGUNGJAWAB_HUBUNGAN = '{$hubungan_penanggungjawab}', 
								 PENANGGUNGJAWAB_ALAMAT = '{$alamat_penanggungjawab}', 
								 PENANGGUNGJAWAB_PHONE = '{$phone_penanggungjawab}', 
								 NO_KARTU = '{$no_kartu}', 
								 NM_JENIS_PESERTA = '{$nm_jenis_peserta}' 
							 WHERE NOMR = '{$nomr}'";
		mysql_query($sqlupdate_pasien) or die(mysql_error());
	}
	else {
		$sqlinsert_pasien = "INSERT INTO m_pasien 
							 SET NOMR = '{$nomr}',
								 NAMA = '{$nama_pasien}',
								 TITLE = '{$alias}',
								 TEMPAT = '{$tmp_lahir}',
								 TGLLAHIR = '{$tgl_lahir}',
								 JENISKELAMIN = '{$jenis_kelamin}',
								 ALAMAT = '{$alamat_sekarang}',
								 KELURAHAN = '{$kd_kelurahan}',
								 KDKECAMATAN = '{$kd_kecamatan}',
								 KOTA = '{$kd_kota}',
								 KDPROVINSI = '{$kd_provinsi}',
								 NOTELP = '{$no_telp}',
								 NOKTP = '{$no_ktp}',
								 SUAMI_ORTU = '{$nama_suami_ortu}',
								 PEKERJAAN = '{$pekerjaan}',
								 STATUS = '{$status_perkawinan}',
								 AGAMA = '{$agama}',
								 PENDIDIKAN = '{$pendidikan}',
								 KDCARABAYAR = '{$kd_carabayar}',
								 NIP = '{$user_login}',
								 TGLDAFTAR = '{$tgl_reg}',
								 ALAMAT_KTP = '{$alamat_ktp}',
								 PENANGGUNGJAWAB_NAMA = '{$nama_penanggungjawab}',
								 PENANGGUNGJAWAB_HUBUNGAN = '{$hubungan_penanggungjawab}',
								 PENANGGUNGJAWAB_ALAMAT = '{$alamat_penanggungjawab}',
								 PENANGGUNGJAWAB_PHONE = '{$phone_penanggungjawab}',
								 NO_KARTU = '{$no_kartu}',
								 NM_JENIS_PESERTA = '{$nm_jenis_peserta}'";
		mysql_query($sqlinsert_pasien) or die(mysql_error());		
	}

	if ( $_POST['KDPOLY'] == "9" || $_POST['KDPOLY'] == "10" ) {
		$sqlinsert_pendaftaran = "INSERT INTO t_pendaftaran
								  SET NOMR = '{$nomr}',
									  TGLREG = '{$tgl_reg}',
									  KDPOLY = '{$kd_poly}',
									  KDDOKTER = '{$kd_dokter}',
									  KDRUJUK = '{$kd_rujuk}',
									  KDCARABAYAR = '{$kd_carabayar}',
									  SHIFT = '{$shift}',
									  JAMREG = '{$tgl_reg}',
									  MASUKPOLY = current_time(),
									  MINTA_RUJUKAN = '{$minta_rujukan}',
									  STATUS = 0,
									  PASIENBARU = '{$status}',
									  NIP = '{$user_login}',
									  PENANGGUNGJAWAB_NAMA = '{$nama_penanggungjawab}',
									  PENANGGUNGJAWAB_HUBUNGAN = '{$hubungan_penanggungjawab}',
									  PENANGGUNGJAWAB_ALAMAT = '{$alamat_penanggungjawab}',
									  PENANGGUNGJAWAB_PHONE = '{$phone_penanggungjawab}',
									  LAKALANTAS = '{$lakalantas}',
									  LOKASI_LAKALANTAS = '{$lokasi_lakalantas}',
									  CATATAN = '{$catatan}',
									  NO_KARTU = '{$no_kartu}',
									  KD_JENIS_PESERTA = '{$kd_jenis_peserta}',
									  NM_JENIS_PESERTA = '{$nm_jenis_peserta}',
									  KD_RUJUK = '{$kd_rujuk}',
									  NO_RUJUKAN = '{$no_rujukan}',
									  TGL_RUJUKAN = '{$tgl_rujukan}',
									  KD_PPK_RUJUKAN = '{$kd_ppk_rujukan}',
									  NM_PPK_RUJUKAN = '{$nm_ppk_rujukan}',
									  KD_DIAGNOSA = '{$kd_diagnosa}',
									  NM_DIAGNOSA = '{$nm_diagnosa}',
									  KD_KELAS = '{$kd_kelas}'";
	}
	else {
		$sqlinsert_pendaftaran = "INSERT INTO t_pendaftaran
								  SET NOMR = '{$nomr}',
									  TGLREG = '{$tgl_reg}',
									  KDPOLY = '{$kd_poly}',
									  KDDOKTER = '{$kd_dokter}',
									  KDRUJUK = '{$kd_rujuk}',
									  KDCARABAYAR = '{$kd_carabayar}',
									  SHIFT = '{$shift}',
									  JAMREG = '{$tgl_reg}',
									  MINTA_RUJUKAN = '{$minta_rujukan}',
									  STATUS = 0,
									  PASIENBARU = '{$status}',
									  NIP = '{$user_login}',
									  PENANGGUNGJAWAB_NAMA = '{$nama_penanggungjawab}',
									  PENANGGUNGJAWAB_HUBUNGAN = '{$hubungan_penanggungjawab}',
									  PENANGGUNGJAWAB_ALAMAT = '{$alamat_penanggungjawab}',
									  PENANGGUNGJAWAB_PHONE = '{$phone_penanggungjawab}',
									  LAKALANTAS = '{$lakalantas}',
									  LOKASI_LAKALANTAS = '{$lokasi_lakalantas}',
									  CATATAN = '{$catatan}',
									  NO_KARTU = '{$no_kartu}',
									  KD_JENIS_PESERTA = '{$kd_jenis_peserta}',
									  NM_JENIS_PESERTA = '{$nm_jenis_peserta}',
									  KD_RUJUK = '{$kd_rujuk}',
									  NO_RUJUKAN = '{$no_rujukan}',
									  TGL_RUJUKAN = '{$tgl_rujukan}',
									  KD_PPK_RUJUKAN = '{$kd_ppk_rujukan}',
									  NM_PPK_RUJUKAN = '{$nm_ppk_rujukan}',
									  KD_DIAGNOSA = '{$kd_diagnosa}',
									  NM_DIAGNOSA = '{$nm_diagnosa}',
									  KD_KELAS = '{$kd_kelas}'";
	}

	mysql_query($sqlinsert_pendaftaran) or die(mysql_error());

	// get last idxdaftar
	$last_idxdaftar = mysql_insert_id();

	if ( $_POST['KDPOLY'] == "51" ) {
		$sql_idx_daftar = "SELECT IDXDAFTAR FROM t_pendaftaran WHERE NOMR = '{$nomr}' ORDER BY IDXDAFTAR DESC LIMIT 1";
		$query_idx_daftar = mysql_query($sql_idx_daftar);
		$data_idx_daftar = mysql_fetch_assoc($query_idx_daftar);
		$idx_daftar = $data_idx_daftar['IDXDAFTAR'];

		$ins_operasi = "INSERT INTO t_operasi (nomr, KDUNIT, IDXDAFTAR, RAJAL, NIP, TGLORDER) VALUES ('".$nomr."', ".$_SESSION['KDUNIT'].", ".$idx_daftar.", 2, '".$_SESSION['NIP']."', CURDATE())";
		mysql_query($ins_operasi) or die(mysql_error());
	}
	
	if ( ! empty($_POST['start_daftar']) && ! empty($_POST['stop_daftar']) ) {
		$sql_last_daftar = "SELECT IDXDAFTAR, NOMR FROM t_pendaftaran ORDER BY IDXDAFTAR DESC LIMIT 1";
		$query_last_daftar = mysql_query($sql_last_daftar);
		$data_last_daftar = mysql_fetch_assoc($query_last_daftar);
		$idx_daftar = $data_last_daftar['IDXDAFTAR'];
		$nomr_last = $data_last_daftar['NOMR'];
		$start_daftar = $_POST['start_daftar'];
		$stop_daftar = $_POST['stop_daftar'];

		$sql_insert_time_daftar = "INSERT INTO t_pendaftaran_iso (idxdaftar, NOMR, start_daftar, stop_daftar) VALUES ('{$idx_daftar}', '{$nomr_last}', '{$start_daftar}', '{$stop_daftar}')";
		mysql_query($sql_insert_time_daftar) or die(mysql_error());
	}

	$jenispoly    = $_POST['KDPOLY'];
	$kdprofesi    = getProfesiDoktor($_POST['KDDOKTER']);
	$kodetarif    = getKodePendaftaran($jenispoly, $kdprofesi);
	$tarif_daftar = getTarifPendaftaran($kodetarif);
	$last_bill    = getLastNoBILL(1);
	$qty          = 1;

	$ip    = getRealIpAddr();
	$tarif = getTarif($kodetarif);
	
	mysql_query('INSERT INTO tmp_cartbayar SET KODETARIF = "'.$kodetarif.'", QTY = 1, IP = "'.$ip.'", ID = "'.$kodetarif.'", POLY = "'.$_REQUEST['KDPOLY'].'", KDDOKTER='.$_REQUEST['KDDOKTER'].',TARIF = "'.$tarif['tarif'].'", TOTTARIF = '.$tarif['tarif'].', JASA_PELAYANAN = '.$tarif['jasa_pelayanan'].', JASA_SARANA = '.$tarif['jasa_sarana'].', UNIT = '.$_REQUEST['KDPOLY']);

	if ( $_POST['KDCARABAYAR'] > 1 ) {
		$sql_bill = 'INSERT INTO t_billrajal SET KODETARIF = "'.$kodetarif.'",
												 NOMR = "'.$nomr.'",
												 KDPOLY = "'.$kd_poly.'",
												 TANGGAL = CURDATE(),
												 SHIFT = '.$shift.',
												 NIP = "'.$user_login.'",
												 QTY = '.$qty.',
												 IDXDAFTAR = '.$last_idxdaftar.',
												 NOBILL = '.$last_bill.',
												 ASKES = 0,
												 COSTSHARING = 0,
												 KETERANGAN = "-",
												 KDDOKTER = '.$dokter.',
												 STATUS = "SELESAI",
												 CARABAYAR = '.$kd_carabayar.',
												 APS = 0,
												 JASA_SARANA = '.$tarif_daftar[0].',
												 JASA_PELAYANAN = '.$tarif_daftar[1].',
												 UNIT='.$kd_poly.',
												 TARIFRS = '.$tarif_daftar[2];
		mysql_query($sql_bill) or die(mysql_error());

		$sql_bayar = 'INSERT INTO t_bayarrajal SET NOMR = "'.$nomr.'",
												   IDXDAFTAR = '.$last_idxdaftar.',
												   NOBILL = '.$last_bill.',
												   TOTTARIFRS = '.$tarif_daftar[2] * $qty.',
												   TOTJASA_SARANA = '.$tarif_daftar[0] * $qty.',
												   TOTJASA_PELAYANAN = '.$tarif_daftar[1] * $qty.',
												   APS = 0,
												   CARABAYAR = '.$kd_carabayar.',
												   TGLBAYAR=CURDATE(),
												   JAMBAYAR=CURTIME(),
												   JMBAYAR="'.$tarif_daftar[2] * $qty.'",
												   NIP = "'.$user_login.'",
												   SHIFT="'.$shift.'",
												   TBP="0",
												   UNIT='.$kd_poly.',
												   LUNAS = 1,
												   STATUS = "LUNAS"';
		mysql_query($sql_bayar) or die(mysql_error());
	}
	else {
		$sql_bill = 'INSERT INTO t_billrajal SET KODETARIF = "'.$kodetarif.'",
												 NOMR = "'.$nomr.'",
												 KDPOLY = "'.$kd_poly.'",
												 TANGGAL = CURDATE(),
												 SHIFT = '.$shift.',
												 NIP = "'.$user_login.'",
												 QTY = '.$qty.',
												 IDXDAFTAR = '.$last_idxdaftar.',
												 NOBILL = '.$last_bill.',
												 ASKES = 0,
												 COSTSHARING = 0,
												 KETERANGAN = "-",
												 KDDOKTER = '.$dokter.',
												 STATUS = "SELESAI",
												 CARABAYAR = '.$kd_carabayar.',
												 APS = 0,
												 JASA_SARANA = '.$tarif_daftar[0].',
												 JASA_PELAYANAN = '.$tarif_daftar[1].',
												 UNIT='.$kd_poly.',
												 TARIFRS = '.$tarif_daftar[2];
		mysql_query($sql_bill) or die(mysql_error());

		$sql_bayar = 'INSERT INTO t_bayarrajal SET NOMR = "'.$nomr.'",
												   IDXDAFTAR = '.$last_idxdaftar.',
												   NOBILL = '.$last_bill.',
												   TOTTARIFRS = '.$tarif_daftar[2] * $qty.',
												   UNIT='.$kd_poly.',
												   TOTJASA_SARANA = '.$tarif_daftar[0] * $qty.',
												   TOTJASA_PELAYANAN = '.$tarif_daftar[1] * $qty.',
												   APS = 0,
												   CARABAYAR = '.$kd_carabayar;
		mysql_query($sql_bayar) or die(mysql_error());
	}

	# update maxnomr
	if ( $status == 1 && $rm_manual == FALSE ) update_last_nomr($nomr);

	# update maxnobill 
	mysql_query("UPDATE m_maxnobill SET nomor = '{$last_bill}'") or die(mysql_error());

	# print tracer
	try {
		printTracer($last_idxdaftar, FALSE);
	} catch (Exception $e) {
		show_exception_error($e);
	}

	$response = (object) array (
		'metadata' => (object) array (
			'code' => "200",
			'message' => "OK"
		),
		'response' => (object) array (
			'idxDaftar' => $last_idxdaftar,
			'noMr' => $nomr,
			'namaPasien' => $nama_pasien
		)
	);
}

echo json_encode($response);