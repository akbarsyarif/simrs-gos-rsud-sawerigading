<?php
namespace Models\DokterJaga;

//---------------------------------------------------------------------------------

function get($config=array())
{
	$query  = (isset($config['query'])) ? $config['query'] : NULL;
	$limit  = (isset($config['limit'])) ? $config['limit'] : 0;
	$offset = (isset($config['offset'])) ? $config['offset'] : 0;
	$order  = (isset($config['order'])) ? $config['order'] : NULL;

	$sql = "SELECT *
			FROM view_dokterjaga
			{$query}";
	$sql .= (! is_null($order)) ? " ORDER BY {$order}" : " ORDER BY id ASC";
	
	if ( $limit > 0 ) {
		$sql .= " LIMIT {$offset},{$limit}";
	}

	$result = mysql_query($sql);
	if ( mysql_num_rows($result) > 0 ) {
		while ( $row = mysql_fetch_assoc($result) ) {
			$data[] = $row;
		}

		return $data;
	}

	return FALSE;
}

//---------------------------------------------------------------------------------

function row($config=array())
{
	$result = get($config);

	if ( $result !== FALSE ) {
		return $result[0];
	}

	return FALSE;
}

//---------------------------------------------------------------------------------