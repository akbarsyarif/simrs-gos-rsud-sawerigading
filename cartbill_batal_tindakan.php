<?php
session_start();

require_once 'include/connect.php';
require_once BASEPATH . 'include/function.php';
require_once BASEPATH . 'include/security_helper.php';

// get session
$nip = $_SESSION['NIP'];

// get query string
$idxdaftar = (isset($_REQUEST['idxdaftar'])) ? xss_clean($_REQUEST['idxdaftar']) : NULL;
$kode = (isset($_REQUEST['kode'])) ? xss_clean($_REQUEST['kode']) : NULL;
$nobill = (isset($_REQUEST['nobill'])) ? xss_clean($_REQUEST['nobill']) : NULL;
$alasan = (isset($_REQUEST['alasan'])) ? xss_clean($_REQUEST['alasan']) : NULL;

if ( (substr($kode, 0, 5) == '01.01') OR (substr($kode, 0, 5) == '02.01') OR (substr($kode, 0, 5) == '05.01') ) {

	$result1 = mysql_query("UPDATE t_pendaftaran SET STATUS = '11' WHERE IDXDAFTAR = '{$idxdaftar}'");

}

$sql = "INSERT INTO m_batal (KODETARIF, NOMR, TGLBATAL, SHIFT, CARABAYAR, NIP, TARIF, QTY, KETERANGAN, KDPOLY, KDDOKTER, IDBAYAR)
		SELECT a.KODETARIF, a.NOMR, CURRENT_TIMESTAMP(), a.SHIFT, a.CARABAYAR, '{$nip}', TARIFRS, QTY, '{$alasan}', KDPOLY, KDDOKTER, b.IDXBAYAR
		FROM t_billrajal a, t_bayarrajal b
		WHERE a.NOBILL = b.NOBILL AND a.IDXBILL = '{$nobill}'";
$result2 = mysql_query($sql);

if ( $result2 ) {

	$result3 = mysql_query("SELECT nomor FROM m_maxnobill");
	$data3 = mysql_fetch_array($result3);
	$new_nobill = $data3['nomor'] + 1;

	// increment maxnobill
	$result4 = mysql_query("UPDATE m_maxnobill SET nomor = '{$new_nobill}'");
	$result5 = mysql_query("SELECT * FROM t_billrajal WHERE idxbill = '{$nobill}'");

	$sql = "INSERT INTO t_bayarrajal (NOMR, IDXDAFTAR, TGLBAYAR, JAMBAYAR, SHIFT, NIP, NOBILL, TOTASKES, TOTCOSTSHARING, TOTTARIFRS, LUNAS, TBP, CARABAYAR, RETRIBUSI, STATUS, APS, TOTJASA_SARANA, TOTJASA_PELAYANAN, UNIT, ALASAN_KERINGANAN, JMBAYAR)
			SELECT NOMR, IDXDAFTAR, CURDATE(), CURTIME(), SHIFT, '{$nip}', '{$new_nobill}', TOTASKES * -1, TOTCOSTSHARING * -1, TOTTARIFRS * -1, 1, TBP, CARABAYAR, RETRIBUSI, 'BATAL', APS, TOTJASA_SARANA * -1, TOTJASA_PELAYANAN * -1, UNIT, ALASAN_KERINGANAN, JMBAYAR * -1
			FROM t_bayarrajal WHERE nobill = '{$nobill}'";
	$result6 = mysql_query($sql);
	
	$result7 = mysql_query("UPDATE t_bayarrajal SET LUNAS = 1, STATUS = 'BATAL' WHERE nobill = '{$nobill}'");

}

$result8 = mysql_query("DELETE FROM t_billrajal WHERE idxbill = '{$nobill}'");