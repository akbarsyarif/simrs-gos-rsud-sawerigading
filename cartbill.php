<script language="javascript" type="text/javascript" src="js/jquery.PrintArea.js"></script>
<script language="javascript">
	function printIt()
	{
		content=document.getElementById('print_selection');
		w=window.open('about:blank');
		w.document.writeln("<? session_start(); ?>");
		w.document.writeln("<link href='dq_sirs.css' type='text/css' rel='stylesheet' />");
		w.document.write( content.innerHTML );
		w.document.writeln("<? $var = $_SESSION['cetak']; ?>");
		w.document.writeln("<script>");
		w.document.writeln("window.print()");
		w.document.writeln("</"+"script>");
	}
</script>
<script language="javascript" type="text/javascript">
	function jumpTo (link)
	{
		var new_url=link;
		if (  (new_url != "")  &&  (new_url != null)  ) window.location=new_url;
	}

	function terbilangnya(bilangan) {
	bilangan    = String(bilangan);
	var angka   = new Array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
	var kata    = new Array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan');
	var tingkat = new Array('','Ribu','Juta','Milyar','Triliun');

	var panjang_bilangan = bilangan.length;

	/* pengujian panjang bilangan */
	if (panjang_bilangan > 15) {
		kaLimat = "Diluar Batas";
		return kaLimat;
	}

	/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
	for (i = 1; i <= panjang_bilangan; i++) {
		angka[i] = bilangan.substr(-(i),1);
	}

	i = 1;
	j = 0;
	kaLimat = "";

	/* mulai proses iterasi terhadap array angka */
	while (i <= panjang_bilangan) {
		subkaLimat = "";
		kata1 = "";
		kata2 = "";
		kata3 = "";

		/* untuk Ratusan */
		if (angka[i+2] != "0") {
			if (angka[i+2] == "1") {
				kata1 = "Seratus";
			} else {
				kata1 = kata[angka[i+2]] + " Ratus";
			}
		}

		/* untuk Puluhan atau Belasan */
		if (angka[i+1] != "0") {
			if (angka[i+1] == "1") {
				if (angka[i] == "0") {
					kata2 = "Sepuluh";
				} else if (angka[i] == "1") {
					kata2 = "Sebelas";
				} else {
					kata2 = kata[angka[i]] + " Belas";
				}
			} else {
				kata2 = kata[angka[i+1]] + " Puluh";
			}
		}

		/* untuk Satuan */
		if (angka[i] != "0") {
			if (angka[i+1] != "1") {
				kata3 = kata[angka[i]];
			}
		}

		/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
		if ((angka[i] != "0") || (angka[i+1] != "0") || (angka[i+2] != "0")) {
			subkaLimat = kata1+" "+kata2+" "+kata3+" "+tingkat[j]+" ";
		}

		/* gabungkan variabe sub kaLimat (untuk Satu blok 3 angka) ke variabel kaLimat */
		kaLimat = subkaLimat + kaLimat;
		i = i + 3;
		j = j + 1;

		}

		/* mengganti Satu Ribu jadi Seribu jika diperlukan */
		if ((angka[5] == "0") && (angka[6] == "0")) {
			kaLimat = kaLimat.replace("Satu Ribu","Seribu");
		}

		return kaLimat + "Rupiah";
	}
		
	function isibayar(kondisi){
		if (kondisi) {
			document.getElementById("jumlah_dibayar").value=document.getElementById("tanda_terimah").value;				
			document.getElementById('terbilang').value=terbilangnya(document.getElementById("tanda_terimah").value);
		}
		else {
			document.getElementById("jumlah_dibayar").value="";
			document.getElementById('terbilang').value="";
		}
	}
	function isiterbilangnya(s){
		document.getElementById('terbilang').value=terbilangnya(s);
	}
</script>
<script type="text/javascript">
	function popUp(URL) {
		day = new Date();
		id = 'keringanan';
		eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1000,height=400,left=50,top=50');");
	}

	jQuery(document).ready(function() {
		jQuery('.bayar').click(function() {
			var idxdaftar	= jQuery(this).attr('rel');
			var idxbayar	= jQuery(this).attr('svn');
			var shif		= jQuery('#shift_'+idxdaftar).val();
			var tbp			= jQuery('#tbp_'+idxdaftar).val();
			var total		= jQuery('#hiden_total_bayar_'+idxdaftar).val();
			var carabayar	= jQuery('#carabayar').val();
			var keringanan	= jQuery('#hiden_keringanan_'+idxdaftar).val();
			var alasan		= jQuery('#hiden_alasan_'+idxdaftar).val();
		
			if ( shif == '' ) {
				alert('Shift belum dipilih');
				return false;
			}

			jQuery.post('<?php echo _BASE_; ?>include/process.php?idxb='+idxbayar+'&idxdaftar='+idxdaftar,{SHIFT:shif,tbp:tbp,total:total,carabayar:carabayar,alasan:alasan,keringanan:keringanan},function(data){
				if ( data == 'ok' ) {
					jQuery('#bayar_'+idxdaftar).hide();
					jQuery('#print_'+idxdaftar).css({'display':'inline','float':'right'});
					jQuery('#shift_'+idxdaftar).hide();
					jQuery('#cancel_'+idxdaftar).hide();
					jQuery('#tbp_'+idxdaftar).hide();
					jQuery('#text_shift_'+idxdaftar).empty().append(shif);
					jQuery('#text_tbp_'+idxdaftar).empty().append(tbp);
				}
				else {
					alert('Lost Connection');
					return false;
				}
			});
		});

		jQuery('.print').click(function(){
			var idxdaftar	= jQuery(this).attr('rel');
			var idxbayar	= jQuery(this).attr('svn');
			jQuery.get('<?php echo _BASE_; ?>print_pembayaran.php?idxb='+idxbayar+'&idxdaftar='+idxdaftar+'&nomr=<?php echo $_REQUEST['nomr']; ?>',function(data){
				jQuery('#tmp_print').empty().html(data);
				w=window.open();
				w.document.write(jQuery('#tmp_print').html());
				w.print();
			});
		});

		jQuery('#retribusi').change(function(){
			if(jQuery(this).val() != ''){
				var nomr		= jQuery('#nomr').val();
				var idxdaftar	= jQuery('#idxdaftar').val();
				var carabayar	= jQuery('#carabayar').val();
				var poly		= jQuery('#poly').val();
				popUp('daftar_tindakan_poly.php?nomr='+nomr+'&idx='+idxdaftar+'&carabayar='+carabayar+'&poly='+poly);
			}
		});

		jQuery('.tindakan').click(function(){
			var nomr		= jQuery('#nomr').val();
			var idxdaftar	= jQuery('#idxdaftar').val();
			var carabayar	= jQuery('#carabayar').val();
			var poly		= jQuery(this).attr('id');
			popUp('daftar_tindakan_poly.php?nomr='+nomr+'&idx='+idxdaftar+'&carabayar='+carabayar+'&poly='+poly);
		});

		// pembatalan pembayaran
		jQuery('.cancel').click(function() {
			var nobill    = jQuery(this).attr('svn');
			var nomr      = jQuery('#nomr').val();
			var kode      = jQuery(this).attr('kode');
			var idxdaftar = jQuery(this).attr('idxdaftar');

			swal({
				title: "Hapus Data?",
				type: "warning",
				text: "Anda akan menghapus item pembayaran",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak"
			}).then(function() {
				jQuery.ajax({
					url : '<?= _BASE_ . "cartbill_pembayaran_batal.php" ?>',
					method : 'post',
					data : {
						nobill : nobill,
						nomr : nomr,
						kode : kode,
						idxdaftar : idxdaftar
					},
					success : function(r) {
						swal('Berhasil !', 'Item pembayaran berhasil dihapus.', 'success').then(function() {
							location.reload();
						});
					},
					error : function(e) {
						swal('Error !', 'Error 400. Tidak dapat menghubungi server.', 'error');
						console.log(e);
					}
				});
			}, function(dismiss) {

			})
		});
		
		jQuery('.link_detail').click(function(){
			var bill = jQuery(this).attr('id');
			var tarif = jQuery(this).attr('detail');
			var active = jQuery('#d'+bill).attr('show');
			if(active == 'show'){
				jQuery('#d'+bill).empty().attr('show','hide');
			}else{
				jQuery.post('<?php echo _BASE_ ?>include/ajaxload.php',{nobill:bill,view_detail_bill:true,kode_tarif:tarif},function(data){
					jQuery('#d'+bill).empty().html(data).attr('show','show');
				});
			}
		});

		jQuery('.form_keringanan').click(function(){
			var nobill = jQuery(this).attr('id');
			popUp('form_keringanan_rajal.php?nobill='+nobill);
		});
		
		jQuery('#masuk').click(function(){
			var nomr = jQuery('#nomr').val();
			var idxdaftar = jQuery('#idxdaftar').val();
			var dokter	= jQuery('#dokterpelaksana').val();
			jQuery.post('<?php echo _BASE_;?>rajal/valid_keluar-masuk.php',{Masuk:'Masuk', NOMR:nomr, IDXDAFTAR2:idxdaftar, dokter:dokter},function(data){
			alert(data);																																			});
		});
	});
</script>

<style type="text/css">
	.detail_billing ul{
		list-style:none; padding-left:10px;
	}
	.detail_billing li{padding:3px;}
	.link_detail{ color:#0D5875; cursor:pointer;}
</style>

<style type="text/css" media="screen">
	#tmp_print{display:none;}
</style>

<style type="text/css" media="print">
	#tmp_print{display:block;}
</style>

<div id="frame">
	<div id="frame_title"><h3>Cart Bayar Rawat Jalan</h3></div>
	<div class="frame_body">
		<?php
		$q_pasien = "select a.NOMR, b.NAMA, b.ALAMAT, b.JENISKELAMIN, b.TGLLAHIR, c.NAMA as CARABAYAR, a.KDCARABAYAR, a.IDXDAFTAR, a.KDPOLY, d.nama as nama_poly,a.KDDOKTER from t_pendaftaran a, m_pasien b, m_carabayar c, m_poly d 
					where a.NOMR = b.NOMR and a.KDCARABAYAR = c.KODE and a.KDPOLY = d.kode and a.NOMR = '".$_REQUEST['nomr']."' and a.IDXDAFTAR = '".$_REQUEST['idxdaftar']."'
					UNION 
					select a.NOMR, b.NAMA, b.ALAMAT, b.JENISKELAMIN, b.TGLLAHIR, c.NAMA as CARABAYAR, a.KDCARABAYAR, a.IDXDAFTAR, a.KDPOLY, d.nama as nama_poly,a.KDDOKTER 
					from t_pendaftaran a, m_pasien b, m_carabayar c, m_poly d 
					where a.NOMR = b.NOMR and a.KDCARABAYAR = c.KODE and a.KDPOLY = d.kode and a.NOMR = '".$_REQUEST['nomr']."'
					";
		$get = mysql_query ($q_pasien)or die(mysql_error());
		$userdata = mysql_fetch_assoc($get); ?>

		<table class="table table-horizontal-highlight">
			<tr><th width="200">No. RM</th><td width="10">:</td><td><?= $userdata['NOMR'] ?></td></tr>
			<tr><th>Nama Lengkap Pasien</th><td>:</td><td><?= $userdata['NAMA'];?></td></tr>
			<tr><th>Alamat Pasien</th><td>:</td><td><?= $userdata['ALAMAT'];?></td></tr>
			<tr>
				<th>Jenis Kelamin</th>
				<td>:</td>
				<td><?= $userdata['JENISKELAMIN'] ?></td>
			</tr>
			<tr><th>Tanggal Lahir</th><td>:</td><td><?php echo $userdata['TGLLAHIR'];?>          </td></tr>
			<tr><th>Umur</th><td>:</td><td><?php $a = datediff($userdata['TGLLAHIR'], date("Y-m-d")); echo $a[years]." tahun ".$a[months]." bulan ".$a[days]." hari"; ?></td></tr>
			<tr><th>Nama Poly</th><td>:</td><td><?php echo $userdata['nama_poly']; ?></td></tr>
			<tr><th>Cara Bayar</th><td>:</td><td><?php echo $userdata['CARABAYAR'];?></td></tr>
		</table>
	</div>
</div>

<div id="tmp_print"></div>

<div class="frame">
	<div id="frame_title"><h3>List Pembayaran</h3></div>
	<div class="frame_body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<?php if ( $_SESSION['ROLES'] == 26 ) : ?>
						<?php
						$sqll = 'SELECT STATUS FROM t_pendaftaran WHERE NOMR = "'.$_REQUEST['nomr'].'" AND IDXDAFTAR = "'.$_REQUEST['idxdaftar'].'"';
						$sqll = mysql_query($sqll);
						$qrtl = mysql_fetch_array($sqll); ?>
						<?php if ( $qrtl['STATUS'] < 1 ) : ?>
							<button type="button" name="tindakan_poly" class="tindakan btn btn-default btn-sm" id="<?= $_REQUEST['poly'] ?>"><i class="fa fa-fw fa-plus-circle"></i> Jasa Tindakan Poly</button>
							<button type="button" name="tindakan_poly" class="tindakan btn btn-default btn-sm" id="0"><i class="fa fa-fw fa-plus-circle"></i> Jasa Tindakan Lain</button>
						<?php else : ?>
							<div class="alert alert-warning">
								Pasien telah dipulangkan.
							</div>
						<?php endif; ?>
					<?php else : ?>
						<button type="button" name="tindakan_poly" class="tindakan btn btn-default btn-sm" id="0"><i class="fa fa-fw fa-plus-circle"></i> Jasa Tindakan Lain</button>
					<?php endif; ?>

					<input type="hidden" name="nomr" id="nomr" value="<?= $userdata['NOMR'] ?>" />
					<input type="hidden" name="idxdaftar" id="idxdaftar" value="<?= $userdata['IDXDAFTAR'] ?>" />
					<input type="hidden" name="carabayar" id="carabayar" value="<?= $userdata['KDCARABAYAR'] ?>" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form name="byr1" action="include/process.php" method="post" id="byr1">
					<table width="100%" border="1" cellpadding="0" cellspacing="0" class="table">
						<thead>
							<tr>
								<th>Nama Jasa</th>
								<?php if ( $_SESSION['ROLES'] == 2 ) : ?>
									<th width="140">Keringanan</th>
								<?php endif; ?>
								<th width="150">Tarif</th>
								<th width="120">Shift</th>
								<?php if ( $_SESSION['ROLES'] == 2 ) : ?>
									<th width="10">Aksi</th>
								<?php endif;?>
								<?php if ( $_SESSION['ROLES'] == 26 ) : ?>
									<th>Status</th>
								<?php endif; ?>
							</tr>
						</thead>
						<tbody class="list_billrajal">
							<?php
							$sql = 'SELECT a.NOMR, b.NOBILL, c.nama_gruptindakan,c.kode_tindakan, c.nama_tindakan AS nama_jasa, c.tarif AS harga, b.QTY,a.TBP,a.SHIFT, SUM(b.tarifrs * b.QTY) AS subtotal, 
									a.TGLBAYAR , b.IDXDAFTAR, b.CARABAYAR 
									FROM t_bayarrajal a 
									JOIN t_billrajal b ON a.NOBILL = b.NOBILL
									JOIN m_tarif2012 c ON c.kode_tindakan = b.KODETARIF
									WHERE a.NOMR = "'.$_REQUEST['nomr'].'" AND a.STATUS !="BATAL"  AND b.IDXDAFTAR = "'.$_REQUEST['idxdaftar'].'" 
									AND b.KODETARIF not like "07%"
									AND b.KODETARIF not like "01.01.18%"
									GROUP BY 
									a.NOMR, b.NOBILL,
									a.TGLBAYAR , b.IDXDAFTAR';

							$qry = mysql_query($sql) or die(mysql_error());

							while ( $data = mysql_fetch_array($qry) ) {
								$j = substr($data['kode_tindakan'], 0, 5);
								$js = getGroupName($j);

								if ( $j == '02.03' ) {
									$jasa = $js['nama_gruptindakan'];
								}
								else {
									$jasa = $js['nama_tindakan'];
								}

								if ( ($data['TGLBAYAR'] == '') OR ($data['TGLBAYAR'] == '0000-00-00') ) { ?>
									<tr>
										<td style="padding-top:7px;" valign="top"><span class="link_detail" detail="<?php echo $data['kode_tindakan']?>" id="<?php echo $data['NOBILL']?>"><?php echo $jasa;?> </span><div class="detail_billing" id="d<?php echo $data['NOBILL']; ?>"></div></td>
										<?php if($_SESSION['ROLES'] == 2) : ?>
										<td style="text-align:center;">
											<a href="javascript:void(0)" class="form_keringanan" id="<?php echo $data['NOBILL']?>">Form Keringanan</a>
										</td>
										<?php endif;?>
										<td align="right" valign="top" style="padding-top:7px;"><span id="tarif_<?php echo $data['NOBILL']?>"><? echo "Rp. ".curformat($data['subtotal'],2); ?></span></td>
										<td valign="top" style="padding-top:7px;">
											<?php if($_SESSION['ROLES'] == 2): ?>
												<select name="shift" id="shift_<?php echo $data['NOBILL']; ?>" class="text">
													<option value=""> Pilih Shift </option><option value="1"> 1 </option><option value="2"> 2 </option><option value="3"> 3 </option>
												</select>
												<input type="hidden" name="total" id="hiden_total_bayar_<?php echo $data['NOBILL']; ?>" value="<?php echo $data['subtotal']; ?>" />
												<input type="hidden" name="keringanan" id="hiden_keringanan_<?php echo $data['NOBILL']; ?>" value="0" />
												<input type="hidden" name="alasan" id="hiden_alasan_<?php echo $data['NOBILL']; ?>" value="" />
												<span id="text_shift_<?php echo $data['NOBILL'];?>"></span>
											<?php endif; ?>
										</td>
										<?php if ( $_SESSION['ROLES'] == 2 ) : ?>
										<td valign="top" style="padding-top:7px;">
											<input type="button" name="Submit" value="Bayar" class="text bayar" id="bayar_<?php echo $data['NOBILL']; ?>" rel="<?php echo $data['NOBILL']; ?>" svn="<?php echo $data['NOBILL']; ?>" />
											<input type="button" name="Cancel" value="Batal" class="text cancel" idxdaftar="<?php echo $userdata['IDXDAFTAR'];?>" kode="<?php echo $data['kode_tindakan'];?>" id="cancel_<?php echo $data['NOBILL']; ?>" rel="<?php echo $data['NOBILL']; ?>" svn="<?php echo $data['NOBILL']; ?>" />
										</td>
										<?php endif; ?>
										<?php if ( $_SESSION['ROLES'] == 26 ) : ?>
										<td style="width:150px; text-align:center; padding-top:7px;" valign="top">Belum di Bayar</td>
										<?php endif; ?>
									</tr>
								<?php
								}
								else { ?>
									<tr>
										<td>
											<span class="link_detail"
												  detail="<?= $data['kode_tindakan'] ?>"
												  id="<?= $data['NOBILL'] ?>">
												  <?= $jasa ?>
											</span>
											<div class="detail_billing" show="hide" id="d<?= $data['NOBILL'] ?>"></div>
										</td>
										<td>
											<?php
											$cara_bayar = $data['CARABAYAR'];
											$sql_carabayar = mysql_query("SELECT NAMA FROM m_carabayar WHERE KODE = '$cara_bayar' ");
											$cb = mysql_fetch_array($sql_carabayar);
											echo $cb['NAMA']; ?>
										</td>
										<td>
											<div class="row">
												<div class="col-xs-3">Rp.</div>
												<div class="col-xs-9 text-right"><?= curformat( $data['subtotal'] ) ?></div>
											</div>
										</td>
										<td><?= $data['SHIFT'] ?></td>
										<?php if ( ($_SESSION['ROLES'] == 2) ) : ?>
											<td valign="top">
												<button type="button"
														name="Cancel"
														class="btn btn-default btn-xs btn-block cancel"
														idxdaftar="<?= $userdata['IDXDAFTAR'] ?>"
														kode="<?= $data['kode_tindakan'] ?>"
														id="cancel_<?= $data['NOBILL'] ?>"
														rel="<?= $data['NOBILL'] ?>"
														svn="<?= $data['NOBILL'] ?>">
														Batal
												</button>
											</td>
										<?php endif; ?>
										<?php if ( $_SESSION['ROLES'] == 26 ) : ?>
											<td align="center">Lunas : <?php echo $data['NOBILL']; ?></td>
										<?php endif; ?>
									</tr>
								<?php
								}
							} ?>

							<?php
							$sql3 = 'SELECT a.NOMR, b.NOBILL
									FROM t_bayarrajal a
									INNER JOIN t_billrajal b ON a.NOBILL = b.NOBILL 
									WHERE a.NOMR = "'.$_REQUEST['nomr'].'" AND a.STATUS !="BATAL" AND b.IDXDAFTAR = "'.$_REQUEST['idxdaftar'].'" 
									AND b.KODETARIF not like "07%"
									AND b.KODETARIF not like "01.01.18%"
									GROUP BY 
									a.NOMR, b.NOBILL,
									a.TGLBAYAR , b.IDXDAFTAR';
							$qry3 = mysql_query($sql3)or die(mysql_error());
							$data2 = mysql_fetch_array($qry3); ?>

							<tr style="border:1px;">
								<td colspan="4" style="vertical-align: middle">
									No. Tagihan : <?= $data2['NOBILL']; ?>
								</td>
								<td>
									<button type="button" name="print" class="btn btn-info btn-sm btn-block print" id="print_<?= $data2['NOBILL']; ?>" rel="<?= $userdata['IDXDAFTAR']; ?>" svn="<?= $data2['NOBILL']; ?>">
										<i class="fa fa-fw fa-print"></i> Print
									</button>
									<div id="callback_<?= $data2['NOBILL']; ?>"></div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>