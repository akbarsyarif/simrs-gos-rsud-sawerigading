<?php
require '../include/connect.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'pdfb/pdfb/pdfb.php';
require BASEPATH . 'pdfb/includes/BarcodeBase.php';
require BASEPATH . 'pdfb/includes/Code39.php';
require BASEPATH . 'models/m_pendaftaran.php';

$error_msg = array();

// url params
$idx_daftar = (isset($_GET['idxdaftar'])) ? xss_clean($_GET['idxdaftar']) : NULL;

// data transaksi
$transaksi = Models\row_pendaftaran(array(
	'query' => " WHERE t1.IDXDAFTAR = '{$idx_daftar}'"
));

// idxdaftar kosong
if ( is_null($idx_daftar) ) {
	$error_msg[] = "IDXDAFTAR tidak boleh kosong.";
}

// transaksi tidak ditemukan
if ( $transaksi === FALSE ) {
	$error_msg[] = "Data transaksi {$idx_daftar} tidak ditemukan.";
}

// check errors
if ( count($error_msg) > 0 ) {
	
	show_error($error_msg);

}
else {

	// barcode config
	$bcode['c39'] = array(
		'name' => 'Code39',
		'obj' => new emberlabs\Barcode\Code39()
	);

	// pdfb instance
	class PDF extends PDFB {
		function Header() {}
		function Footer() {}
	}

	// Create a PDF object and set up the properties
	$pdf = new PDF("p", "mm", array(85,54));
	$pdf->SetAuthor("PDFB Library");
	$pdf->SetTitle("Kartu Pasien");

	$pdf->SetFont("Arial", "", 8);
	$pdf->SetAutoPageBreak(false);

	// Load the base PDF into template
	$pdf->setSourceFile("kartu_pasien_swg.pdf");
	$tplidx = $pdf->ImportPage(1);

	// Add new page & use the base PDF as template
	$pdf->AddPage();
	$pdf->useTemplate($tplidx);

	// $pdf->Text(32, 23.5, $data['NOMR']);
	$pdf->Text(32, 33, $transaksi['NAMA_PASIEN']);
	$pdf->Text(32, 37, $transaksi['TGL_LAHIR_PASIEN']);
	$pdf->Text(32, 41, $transaksi['ALAMAT']);
	$pdf->Text(32, 35.5, $transaksi['JENISKELAMIN']);
	// $pdf->SetXY(32,40);
	// $pdf->MultiCell(0,2.5,$data['ALAMAT'],0,"L");

	$pdf->Output();
	$pdf->closeParsers();

}