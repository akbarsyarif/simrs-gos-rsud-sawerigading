<?php
error_reporting(E_ALL);

$tmpl = "form_pasien.rtf";
$targ = "form_pasien2.rtf";

$f = fopen($tmpl, "r");
$isi = fread($f, filesize($tmpl));
fclose($f);

$isi = str_replace('=NamaLengkap=', 'Akbar Syarif', $isi);
$isi = str_replace('=AlamatLengkap=', 'Jl. Akasia, No.11 Lemo-Lemo Indah. RT 001/RW 002', $isi);
$isi = str_replace('=Kelurahan=', 'Balandai', $isi);
$isi = str_replace('=Kecamatan=', 'Bara', $isi);
$isi = str_replace('=Kota=', 'Palopo', $isi);
$isi = str_replace('=Telephone=', '082395297500', $isi);
$isi = str_replace('=TempatLahir=', 'Palopo', $isi);
$isi = str_replace('=TglLahir=', '03-02-1993', $isi);
$isi = str_replace('=Umur=', '24', $isi);
$isi = str_replace('=Sex=', 'L', $isi);
$isi = str_replace('=Status=', 'Belum Menikah', $isi);
$isi = str_replace('=Agama=', 'Islam', $isi);
$isi = str_replace('=Pdk=', 'Universitas', $isi);
$isi = str_replace('=Pekerjaan=', 'Seniman', $isi);
$isi = str_replace('=NamaOrangtua=', 'Herpin', $isi);
$isi = str_replace('=PekerjaanOrangtua=', 'IRT', $isi);
$isi = str_replace('=NamaSuami=', '-', $isi);
$isi = str_replace('=PekerjaanSuami=', '-', $isi);
$isi = str_replace('=NamaProvider=', 'Puskesmas Wara Utara', $isi);
$isi = str_replace('=CaraBayar=', 'JKN', $isi);
$isi = str_replace('=NoJaminan=', '12345678910123', $isi);
$isi = str_replace('=StatusKepesertaan=', 'PENERIMA PENSIUN PNS', $isi);
$isi = str_replace('=TglKunjungan=', '17-11-2016', $isi);
$isi = str_replace('=JamKunjungan=', '17:00:00', $isi);
$isi = str_replace('=PolyTujuan=', 'Poly Mata', $isi);

// tulis
$f = fopen($targ, "w");
fwrite($f, $isi);
fclose($f);


// force download file
$path = $targ;

if (headers_sent()) {
    echo 'HTTP header already sent';
} else {
    if (!is_file($path)) {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        echo 'File not found';
    } else if (!is_readable($path)) {
        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
        echo 'File not readable';
    } else {
        header("Content-Disposition: attachment; filename=\"".basename($path)."\"");
        readfile($path);
    }
}