<?php
require '../include/connect.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';

$error_msg = array();

// url params
$idx_daftar = (isset($_GET['idxdaftar'])) ? xss_clean($_GET['idxdaftar']) : NULL;

$config['query'] = " WHERE t1.IDXDAFTAR = '{$idx_daftar}'";
$config['limit'] = 1;

$result = Models\row_pendaftaran($config);

// idxdaftar kosong
if ( is_null($idx_daftar) ) {
	$error_msg[] = "IDXDAFTAR tidak boleh kosong.";
};

// data transaksi tidak ditemukan
if ( $result === FALSE ) {
	$error_msg[] = "Data transaksi {$idx_daftar} tidak ditemukan.";
};

if ( count($error_msg) > 0 ) {

	show_error($error_msg);

}
else {
	// fetch data
	$data = $result;

	$jamregraw = str_replace(' ', '', $data['JAMREGRAW']);
	$jamregraw = str_replace(':', '', $jamregraw);

	// form template *.rtf
	$tmpl = "form_pasien.rtf";
	$targ = "form_pasien/".$data['NOMR']."-".$jamregraw.".rtf";

	// remove old file
	if ( file_exists("./{$targ}") ) {
		unlink("./{$targ}");
	}

	$f = fopen($tmpl, "r");
	$isi = fread($f, filesize($tmpl));
	fclose($f);

	// split nomr
	$split = str_split($data['NOMR'], 1);

	$isi = str_replace('=0=', $split[0], $isi);
	$isi = str_replace('=1=', $split[1], $isi);
	$isi = str_replace('=2=', $split[2], $isi);
	$isi = str_replace('=3=', $split[3], $isi);
	$isi = str_replace('=4=', $split[4], $isi);
	$isi = str_replace('=5=', $split[5], $isi);
	$isi = str_replace('=NamaLengkap=', $data['NAMA_PASIEN'], $isi);
	$isi = str_replace('=AlamatLengkap=', $data['ALAMAT'], $isi);
	$isi = str_replace('=Kelurahan=', $data['namakelurahan'], $isi);
	$isi = str_replace('=Kecamatan=', $data['namakecamatan'], $isi);
	$isi = str_replace('=Kota=', $data['namakota'], $isi);
	$isi = str_replace('=Telephone=', $data['PENANGGUNGJAWAB_PHONE'], $isi);
	$isi = str_replace('=TempatLahir=', $data['TMP_LAHIR_PASIEN'], $isi);
	$isi = str_replace('=TglLahir=', $data['TGL_LAHIR_PASIEN'], $isi);
	$isi = str_replace('=Umur=', $data['UMUR_PASIEN'], $isi);
	$isi = str_replace('=Sex=', $data['JENISKELAMIN'], $isi);
	$isi = str_replace('=Status=', $data['STATUS_PERKAWINAN_LABEL'], $isi);
	$isi = str_replace('=Agama=', $data['AGAMA_LABEL'], $isi);
	$isi = str_replace('=Pdk=', $data['PENDIDIKAN_LABEL'], $isi);
	$isi = str_replace('=Pekerjaan=', $data['PEKERJAAN'], $isi);
	$isi = str_replace('=NamaOrangtua=', $data['SUAMI_ORTU'], $isi);
	$isi = str_replace('=PekerjaanOrangtua=', $data['PEKERJAAN'], $isi);
	$isi = str_replace('=NamaSuami=', '-', $isi);
	$isi = str_replace('=PekerjaanSuami=', '-', $isi);
	$isi = str_replace('=NamaProvider=', $data['NM_PPK_RUJUKAN'], $isi);
	$isi = str_replace('=CaraBayar=', $data['CARABAYAR'], $isi);
	$isi = str_replace('=NoJaminan=', $data['NO_KARTU'], $isi);
	$isi = str_replace('=StatusKepesertaan=', $data['NM_JENIS_PESERTA'], $isi);
	$isi = str_replace('=TglKunjungan=', $data['TGLREG'], $isi);
	$isi = str_replace('=JamKunjungan=', $data['JAMREG'], $isi);
	$isi = str_replace('=PolyTujuan=', $data['NAMA_POLY'], $isi);

	// tulis
	$f = fopen($targ, "w");
	fwrite($f, $isi);
	fclose($f);

	// force download
	header('Content-Type: application/doc');
    header("Content-Disposition: attachment; filename=\"$targ\"");
    readfile($targ);
}