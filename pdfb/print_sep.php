<?php
require '../include/connect.php';

require BASEPATH . 'pdfb/pdfb/pdfb.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'include/bpjs_api.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('__downloadSepPdf') )
{

	function __downloadSepPdf($data_sep, $info_cetak)
	{

		// merubah format tanggal sep
		$tgl_sep = date_format(date_create($data_sep->tglSep), 'd/m/Y');

		// merubah format tanggal lahir
		$tgl_lahir = date_format(date_create($data_sep->peserta->tglLahir), 'd/m/Y');

		// catatan
		if ( $data_sep->lakaLantas->status != "0" ) {
			$catatan = 'Kecelakaan Lalulintas. ' . $data_sep->catatan;
		}
		else {
			$catatan = $data_sep->catatan;
		}

		// download SEP dalam format PDF
		class PDF extends PDFB {
			function Header() {}
			function Footer() {}
		}

		// Create a PDF object and set up the properties
		$pdf = new PDF("p", "mm", array(209.97, 95));
		$pdf->SetAuthor("RSUD Sawerigading Palopo");
		$pdf->SetTitle("Surat Elegibilitas Peserta");

		$pdf->SetFont("Arial", "", 10);
		$pdf->SetAutoPageBreak(false);

		// Load the base PDF into template
		$pdf->setSourceFile("sep.pdf");
		$tplidx = $pdf->ImportPage(1);

		// Add new page & use the base PDF as template
		$pdf->AddPage();
		$pdf->useTemplate($tplidx);

		$pdf->Text(48, 26, $data_sep->noSep);
		$pdf->Text(48, 30.5, $tgl_sep);
		$pdf->Text(48, 35.3, $data_sep->peserta->noKartu);
		$pdf->Text(48, 40, $data_sep->peserta->nama);
		$pdf->Text(48, 44.6, $tgl_lahir);
		$pdf->Text(48, 49.2, $data_sep->peserta->sex);
		$pdf->Text(48, 53.9, $data_sep->poliTujuan->nmPoli);
		$pdf->Text(48, 58.6, $data_sep->provRujukan->nmProvider);

		$pdf->SetFont("Arial", "", 8);
		$pdf->Text(48, 63.2, $data_sep->diagAwal->nmDiag);
		$pdf->Text(48, 67.9, $catatan);

		$pdf->SetFont("Arial", "", 10);
		$pdf->Text(99, 35.3, $data_sep->peserta->noMr);
		$pdf->Text(155.5, 35.3, $data_sep->peserta->jenisPeserta->nmJenisPeserta);
		$pdf->Text(155.5, 44.6, $data_sep->statusCOB->namaCOB);
		$pdf->Text(155.5, 49.2, $data_sep->jnsPelayanan);
		$pdf->Text(155.5, 53.9, $data_sep->klsRawat->nmKelas);

		$jumlah_cetak = $info_cetak['jumlah_cetak'];
		$tgl_cetak = date_format(date_create($info_cetak['tgl_cetak']), 'd/m/Y H:i:s');

		$pdf->SetFont("Arial", "", 8);
		$pdf->Text(28, 84.2, "{$jumlah_cetak} - {$tgl_cetak}");

		$pdf->Output();
		$pdf->closeParsers();

	}

}

//---------------------------------------------------------------------------------

$idxdaftar = xss_clean($_GET['idxdaftar']);

// data transaksi
$transaksi = Models\row_pendaftaran(array(
	'query' => "WHERE t1.IDXDAFTAR = '{$idxdaftar}'"
));

if ( $transaksi === FALSE ) {
	$error_msg[] = 'Index pendaftaran tidak ditemukan.';
}

// cek apakah ada error
if ( isset($error_msg) && count($error_msg) > 0 ) {
	
	$response = (object) array (
		'metadata' => (object) array (
			'code' => "201",
			'message' => show_error($error_msg, TRUE)
		),
		'response' => NULL
	);

}
else {

	$no_sep = $transaksi['NO_SEP'];

	// cek detail sep di server bpjs
	$result_sep = detail_sep($no_sep);

	if ( $result_sep === FALSE ) {

		$response = (object) array (
			'metadata' => (object) array (
				'code' => "800",
				'message' => "Tidak dapat menghubungi server BPJS. Silahkan coba beberapa saat lagi."
			),
			'response' => NULL
		);

	}
	else {

		// validasi sep
		if ( $result_sep->metadata->code == "200" ) {

			$data_sep = $result_sep->response;

			// increment kolom cetakan dan tanggal cetakan terakhir
			$info_cetak = array (
				'jumlah_cetak' => $transaksi['JUMLAH_CETAK'],
				'tgl_cetak' => $transaksi['DATETIME_CETAK_TERAKHIR']
			);

			__downloadSepPdf($data_sep, $info_cetak);

		}

	}

}

//---------------------------------------------------------------------------------