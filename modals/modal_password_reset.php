<div class="modal fade" id="modal-password-reset">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-key"></i> Reset Password</h4>
			</div>
			<div class="modal-body">
				Mohon maaf untuk saat ini kata sandi hanya bisa direset oleh administrator. Untuk mendapatkan kata sandi baru silahkan hubungi administrator SIMRS pada kontak yang tersedia 
				atau datang langsung ke ruang SIMRS.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>