<!-- modal search rujukan bpjs -->
<div class="modal fade" id="modal-rujukan-bpjs">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Cari Data Rujukan BPJS</h4>
			</div>
			<div class="modal-body">
				<form action="" method="get" name="form-search-rujukan-bpjs" id="form-search-rujukan-bpjs">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Cari Berdasarkan</label>
								<select name="search_by" class="form-control">
									<option value="norujukan">No.Rujukan</option>
									<option value="nokartu">No.Kartu</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Fasilitas Kesehatan</label>
								<select name="faskes" class="form-control">
									<option value="pcare">PCare</option>
									<option value="rs">Rumah Sakit</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>No.Rujukan / No.Kartu</label>
								<input type="text" name="s" class="form-control" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!-- <div class="form-group"> -->
								<button class="btn btn-block btn-primary" type="submit" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> Sedang mencari..."><i class="fa fa-fw fa-search"></i> Cari</button>
							<!-- </div> -->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#form-search-rujukan-bpjs').submit(function(e) {
			$modal = jQuery('#modal-rujukan-bpjs');
			$form = jQuery(this);

			var search_by = $form.find('[name="search_by"]').val();
			var faskes = $form.find('[name="faskes"]').val();
			var s = $form.find('[name="s"]').val();

			$form.find('[type="submit"]').button('loading');
			
			getRujukan({
				search_by : search_by,
				faskes : faskes,
				s : s
			})
			.done(function(r) {
				if ( r.metadata.code != '200' ) {
					swal('Error', r.metadata.message, 'error');
					console.log(r);
				}
				else {
					if ( r.response.metadata.code != '200' ) {
						swal('Server BPJS Response', r.response.metadata.message, 'error');
						console.log(r);
					}
					else {
						// rujukan ditemukan
						var d = r.response.response.item;

						jQuery('.jkn_container').find('[name="no_kartu"]').val(d.peserta.noKartu);
						jQuery('.jkn_container').find('[name="kd_jenis_peserta"]').val(d.peserta.jenisPeserta.kdJenisPeserta);
						jQuery('.jkn_container').find('[name="nm_jenis_peserta"]').val(d.peserta.jenisPeserta.nmJenisPeserta);
						jQuery('.jkn_container').find('[name="kd_kelas"]').val(d.peserta.kelasTanggungan.kdKelas);
						jQuery('.jkn_container').find('[name="nm_kelas"]').val(d.peserta.kelasTanggungan.nmKelas);
						jQuery('.jkn_container').find('[name="no_rujukan"]').val(d.noKunjungan);
						jQuery('.jkn_container').find('[name="tgl_rujukan"]').val(d.tglKunjungan);
						jQuery('.jkn_container').find('[name="kd_ppk_rujukan"]').val(d.provKunjungan.kdProvider);
						jQuery('.jkn_container').find('[name="nm_ppk_rujukan"]').val(d.provKunjungan.nmProvider);
						jQuery('.jkn_container').find('[name="kd_diagnosa"]').val(d.diagnosa.kdDiag);
						jQuery('.jkn_container').find('[name="nm_diagnosa"]').val(d.diagnosa.nmDiag);
						jQuery('[name="catatan"]').val(d.catatan);
						jQuery('[name="NAMA"]').val(d.peserta.nama);
						jQuery('[name="NOKTP"]').val(d.peserta.nik);
						jQuery('[name="JENISKELAMIN"][value="'+d.peserta.sex+'"]').trigger('click');

						// merubah format tgl.lahir sesuai
						// format aplikasi simrs (why i should do this thing? f*ck!!!!!)
						var tgl_lahir = formatTglSimrsDirjenBukKemenkesFuck(d.peserta.tglLahir);
						jQuery('[name="TGLLAHIR"]').val(tgl_lahir);

						// mapping poli tujuan
						var kd_poly_bpjs = d.poliRujukan.kdPoli;
						mappingPoly(kd_poly_bpjs)
							.done(function(r) {
								if ( r.metadata.code != '200' ) {
									alertify(r.metadata.message);
									console.log(r);
								}
								else {
									var kd_poly = r.response;
									jQuery('[name="KDPOLY"]').val(kd_poly).change();
								}
							})
							.fail(function(e) {
								alertify('Terjadi error ketika mencoba melakukan mapping poli tujuan');
								console.log(e);
							});

						// close modal
						$modal.modal('hide');
					}
				}
			})
			.fail(function(e) {
				swal('Error', 'Tidak dapat menghubungi server', 'error');
				console.log(e);
			})
			.always(function() {
				$form.find('[type="submit"]').button('reset');
			});

			return false;
		});

		jQuery('#data-diagnosa tbody').on('click', 'a.choose', function() {
			var kodeDiagnosa = jQuery(this).data('kode');
			jQuery('#diagnosa').val(kodeDiagnosa);
			jQuery('#modal-rujukan-bpjs').modal('hide');
		});
	});
</script>