<!-- modal search provider bpjs -->
<div class="modal fade" id="modal-search-peserta-bpjs">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Cari Peserta BPJS</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<form action="" id="form-search-peserta-bpjs">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<label>Cari Berdasarkan</label>
										<select name="search_by" class="form-control">
											<option value="nokartu">No. Peserta</option>
											<option value="nik">NIK</option>
										</select>
									</div>
									<div class="col-md-6">
										<label>NIK/Nomor Peserta</label>
										<input type="text" placeholder="Masukkan NIK/Nomor Peserta" name="s" class="form-control">
									</div>
								</div>
							</div>
						</form>
						<div class="form-group">
							<div>
								<table class="table result" id="data-peserta-bpjs">
									<thead>
										<tr>
											<th>NIK</th>
											<th>No. Peserta</th>
											<th>Nama</th>
											<th>No.MR</th>
											<th>Pilih</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="5" class="text-center">Peserta tidak ditemukan</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<input type="hidden" name="resultTemp">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {

		var modal = jQuery('#modal-search-peserta-bpjs');
		var form = jQuery('#form-search-peserta-bpjs');
		
		//------------------------------------------------------------------------

		form.submit(function(e) {
			// search parameter
			var searchBy = form.find('[name="search_by"]').val();
			var s = form.find('[name="s"]').val();
			
			if ( s.length >= 1 ) {
				jQuery.ajax({
					url : '<?= _BASE_ ?>' + 'bridging_proses.php',
					type : 'GET',
					dataType : 'json',
					data : {
						reqdata : 'get_peserta',
						search_by : searchBy,
						s : s
					},
					beforeSend : function() {
						form.find('input,textarea,select').attr('readonly', 'true');
						modal.find('table.result tbody').html('<tr><td colspan="5" class="text-center">Sedang memuat...</td></tr>');
					},
					success : function(r) {
						if ( r.metadata.code != "200" ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);

							modal.find('table.result tbody').html('<tr><td colspan="5" class="text-center">Peserta tidak ditemukan</td></tr>');
						}
						else {
							if ( r.response.metadata.code != "200" ) {
								swal('Error', r.response.metadata.message, 'error');
								console.log(r);
								modal.find('table.result tbody').html('<tr><td colspan="5" class="text-center">Peserta tidak ditemukan</td></tr>');
							}
							else {
								html = '';
								var peserta = r.response.response.peserta;

								// menyimpan data ke hidden textinput sementara
								modal.find('[name="resultTemp"]').val(JSON.stringify(peserta));

								html += '<tr>';
									html += '<td>' + peserta.nik + '</td>';
									html += '<td>' + peserta.noKartu + '</td>';
									html += '<td>' + peserta.nama + '</td>';
									html += '<td>' + peserta.noMr + '</td>';
									html += '<td class="no-gap"><a href="javascript:void(0)" class="choose btn btn-xs btn-primary"><i class="fa fa-fw fa-hand-pointer-o"></i></a></td>';
								html += '</tr>';

								modal.find('table.result tbody').html(html);
							}
						}
					},
					error : function(error) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(error);
						modal.find('table.result tbody').html('<tr><td colspan="5" class="text-center">Peserta tidak ditemukan</td></tr>');
					},
					complete: function() {
						form.find('input,textarea,select').removeAttr('readonly');
					}
				});
			}

			return false;
		});

		//------------------------------------------------------------------------

	});
</script>