<div class="modal fade" id="modal-register">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-user"></i> Buat Akun Baru</h4>
			</div>
			<div class="modal-body">
				Mohon maaf untuk saat ini akun baru hanya bisa ditambahkan oleh administrator. Untuk membuat akun baru silahkan hubungi administrator SIMRS pada kontak yang tersedia 
				atau datang langsung ke ruang SIMRS.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>