<!-- modal search pasien -->
<div class="modal fade" id="modal-search-pasien">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-fw fa-search"></i> Cari Pasien</h4>
			</div>
			<div class="modal-body">
				<form id="form-search-pasien">
					<div class="row sm-margin">
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" placeholder="Nomor Rekam Medik" class="form-control" data-column="0">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" placeholder="Nama Pasien" class="form-control" data-column="1">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" placeholder="YYYY-MM-DD" class="form-control" data-column="2">
							</div>
						</div>
					</div>
					<div class="row sm-margin">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" placeholder="NIK" class="form-control" data-column="3">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" placeholder="Nomor Peserta JKN" class="form-control" data-column="4">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<button type="button" id="clear" class="btn btn-warning">Clear Filter</button> 
							<button type="submit" class="btn btn-primary">Cari Pasien</button>
						</div>
					</div>
				</form>
			</div>
			<hr />
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-bordered" id="data-pasien">
							<thead>
								<tr>
									<th class="no-gap">No.MR</th>
									<th>Nama</th>
									<th>Tempat & Tgl.Lahir</th>
									<th>NIK</th>
									<th>No.JKN</th>
									<th class="no-gap"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		var table = jQuery('#data-pasien').DataTable({
			ordering : true,
			order : [[0, "asc"]],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_."ajaxload.php?p=pasien&a=data_table" ?>',
			columns : [
				{
					data : "NOMR",
					className : 'text-center'
				},
				{
					data : "NAMA",
					mRender: function(data, type, row) {
						if ( row.TITLE == "" || row.TITLE == null ) {
							return data;
						}
						else {
							return data + ', ' + row.TITLE;
						}
					}
				},
				{
					data : "TGLLAHIR",
					mRender : function(data, type, row) {
						var TEMPAT = '';
						if ( row.TEMPAT != '' ) {
							TEMPAT = row.TEMPAT+', ';
						}
						return TEMPAT+row.TGLLAHIR;
					},
					orderable : false
				},
				{ data : "NOKTP" },
				{ data : "NO_KARTU" },
				{
					data : null,
					orderable : false,
					className : 'text-center',
					mRender : function(data, type, row) {
						return '<a class="choose btn btn-xs btn-block btn-primary" href="javascript:void(0)" data-nomr="'+row.NOMR+'"><i class="fa fa-fw fa-hand-pointer-o"></i></a>'
					},
				}
			]
		});

		// Apply the search
		table.columns().every( function () {
			var that = this;

			jQuery( 'input', this.footer() ).on( 'keyup change', function () {
				jQuery( '#data-pasien tfoot input.text' ).each(function() {
					var i = jQuery(this).attr('data-column');

					table.column(i).search(
						jQuery(this).val()
					).draw();
				});
			});
		});

		// klik tombol pilih
		jQuery('#data-pasien tbody').on('click', 'a.choose', function() {
			var nomr = jQuery(this).data('nomr');
			jQuery('#NOMR').val(nomr);
			jQuery('#NOMR').focus();
			setTimeout(function() {
				jQuery('#kdpoly').focus();
			}, 10);
			jQuery('#modal-search-pasien').modal('hide');
		});

		// search pasien
		jQuery('#form-search-pasien').on('submit', function() {
			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#form-search-pasien #clear').on('click', function() {
			jQuery('#form-search-pasien').resetForm();

			jQuery('#form-search-pasien .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});
	});
</script>