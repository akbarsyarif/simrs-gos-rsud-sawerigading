<div align="center">
    <div id="frame">
    <div id="frame_title"><h3>Laporan Harian</h3></div>

<fieldset >
<legend>Filter Laporan Harian</legend>
<form name="formsearch" id="filterlap" method="get" >
<input type="hidden" name="link" value="g01" />
<table class="table" align="left">
         <tr>
         <td>Tanggal</td>
          <td><input type="text" name="tgl_pesan" id="tgl_pesan" class="text"
			value = "<?php if($_REQUEST['tgl_pesan'] !=""): echo $_REQUEST['tgl_pesan']; else: echo date('Y/m/d'); endif; ?>"
			/><a href="javascript:showCal('Calendar3')"><button type="button" class="text"><i class="fa fa-fw fa-calendar"></i></button></a></td>
</tr>

 <tr>
   <td>Group Barang</td>
   <td><select name="group" id="group" class="text">
<? if($_SESSION['KDUNIT']=="12") : ?>
      <?php
      $groups = Models\BarangGroup\get_barang_group(array(
        'query' => "WHERE farmasi = 1"
      )); ?>

      <?php if ( $groups == false ) : ?>
        <option value="">-- Grup barang tidak ditemukan --</option>
      <?php else : ?>
        <?php foreach ( $groups as $group ) : ?>
          <option value="<?= $group['group_barang'] ?>"><?= $group['nama_group'] ?></option>
        <?php endforeach; ?>
      <?php endif; ?>
   <?php elseif ($_SESSION['KDUNIT']=="13") : ?>
      <?php
      $groups = Models\BarangGroup\get_barang_group(array(
        'query' => "WHERE farmasi = 0"
      )); ?>

      <?php if ( $groups == false ) : ?>
        <option value="">-- Grup barang tidak ditemukan --</option>
      <?php else : ?>
        <?php foreach ( $groups as $group ) : ?>
          <option value="<?= $group['group_barang'] ?>"><?= $group['nama_group'] ?></option>
        <?php endforeach; ?>
      <?php endif; ?>
  <?php endif; ?>
   </select></td>
 </tr>
 <tr>
   <td>Nama Barang</td>
   <td><input type="text" name="nm_barang" class="text" /></td>
 </tr>
  <tr>
   <td>&nbsp;</td>
   <td><input type="submit" value="Open" class="text" /></td>
 </tr>
</table>
</form>
</fieldset >
</div></div>