<?php

$link = isset($_GET['link']) ? xss_clean($_GET['link']) : '';

switch ( $link ) {
	case 'public':
		require_once BASEPATH . 'themes/public.php';
		break;
	
	default:
		require_once BASEPATH . 'themes/default.php';
		break;
}