<?php
require BASEPATH . 'signature-vclaim.php';

/**
 * Referensi kamar
 * @return return array/object if success or false if error found
 */
function ref_kamar() {
	$url = BASE_API . "aplicaresws/rest/ref/kelas";

	$result = _curl($url);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * Ketersediaan kamar
 * @return return array/object if success or false if error found
 */
function get_kamar($start=1, $limit=1) {
	$url = BASE_API . "aplicaresws/rest/bed/read/" . KD_PPK . "/{$start}/{$limit}";

	$result = _curl($url);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * Menambahkan ruangan baru
 * @return return array/object if success or false if error found
 */
function add_kamar($data) {
	$url = BASE_API . "aplicaresws/rest/bed/create/" . KD_PPK;

	unset($prepare_data);
	$prepare_data = (object) array (
		"kodekelas"          => $data['kodekelas'],
		"koderuang"          => $data['koderuang'],
		"namaruang"          => $data['namaruang'],
		"kapasitas"          => $data['kapasitas'],
		"tersedia"           => $data['tersedia'],
		"tersediapria"       => $data['tersediapria'],
		"tersediawanita"     => $data['tersediawanita'],
		"tersediapriawanita" => $data['tersediapriawanita']
	);

	$data_encode = json_encode($prepare_data);

	$result = _curl_post($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * Mengupdate ketersediaan tempat tidur
 * @return return array/object if success or false if error found
 */
function update_kamar($data) {
	$url = BASE_API . "aplicaresws/rest/bed/update/" . KD_PPK;

	unset($prepare_data);
	$prepare_data = (object) array (
		"kodekelas"          => $data['kodekelas'],
		"koderuang"          => $data['koderuang'],
		"namaruang"          => $data['namaruang'],
		"kapasitas"          => $data['kapasitas'],
		"tersedia"           => $data['tersedia'],
		"tersediapria"       => $data['tersediapria'],
		"tersediawanita"     => $data['tersediawanita'],
		"tersediapriawanita" => $data['tersediapriawanita']
	);

	$data_encode = json_encode($prepare_data);

	$result = _curl_post($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * Menghapus kamar
 * @return return array/object if success or false if error found
 */
function delete_kamar($data) {
	$url = BASE_API . "aplicaresws/rest/bed/delete/" . KD_PPK;

	unset($prepare_data);
	$prepare_data = (object) array (
		"kodekelas" => $data['kodekelas'],
		"koderuang" => $data['koderuang']
	);

	$data_encode = json_encode($prepare_data);

	$result = _curl_post($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

function _curl($url, $content_type="application/json") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $content_type\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}

function _curl_post($url, $data, $content_type="application/json") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	// curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $content_type\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	// curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}

function _curl_put($url, $data, $content_type="application/json") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	// curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $content_type\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}

function _curl_delete($url, $data, $content_type="application/json") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	// curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $content_type\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}