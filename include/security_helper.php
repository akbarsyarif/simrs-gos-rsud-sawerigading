<?php
if ( ! function_exists('xss_clean') ) {
	/**
	 * xss clean
	 * filter input value
	 * @param  [type] $str [description]
	 * @return [type]      [description]
	 */
	function xss_clean($str) {
		$str = trim($str);
		$str = mysql_escape_string($str);
		$str = htmlspecialchars($str);

		return $str;
	}
}