<?php
require BASEPATH . 'signature.php';

/**
 * get list of poli
 * @return return array/object if success or false if error found
 */
function get_poli() {
	$url = BASE_API."poli/ref/poli?limit=10";

	$result    = _curl($url);
	$response  = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * get list of diagnosa
 * @return return array/object if success or false if error found
 */
function get_diagnosa($kode=null) {
	$filter = substr($kode, -1);
	$kode = ( $filter == '.' ) ? rtrim($kode, '.') : $kode;

	$url = BASE_API."diagnosa/ref/diagnosa/{$kode}";

	$result    = _curl($url);
	$response  = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * get list of faskes
 * @return return array/object if success or false if error found
 */
function get_faskes($kode='', $start=0, $limit=10) {
	$url = BASE_API."provider/ref/provider/query?nama={$kode}&start={$start}&limit={$limit}";

	$result   = _curl($url);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * get peserta info
 * @return return array/object if success or false if error found
 */
function get_peserta($search_by='nokartu', $s=null) {
	$search_by = (!is_null($search_by)) ? $search_by : 'nokartu';
	$url = ( $search_by == 'nik' ) ? BASE_API."Peserta/Peserta/nik/{$s}" : BASE_API."Peserta/Peserta/{$s}";

	$result = _curl($url);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * create no_sep
 * @return return array/object if success or false if error found
 */
function insert_sep($data) {
	$url = BASE_API."SEP/insert";

	unset($prepare_data);
	$prepare_data = (object) array (
		"request" => (object) array (
			"t_sep" => (object) array (
				"noKartu" => $data['no_kartu'],
				"tglSep" => $data['tgl_sep'],
				"tglRujukan" => $data['tgl_rujukan'],
				"noRujukan" => $data['no_rujukan'],
				"ppkRujukan" => $data['ppk_rujukan'],
				"ppkPelayanan" => $data['ppk_pelayanan'],
				"jnsPelayanan" => $data['kd_jns_pelayanan'],
				"catatan" => $data['catatan'],
				"diagAwal" => $data['kd_diagnosa_awal'],
				"poliTujuan" => $data['kd_poli_tujuan'],
				"klsRawat" => $data['kd_kelas_rawat'],
				"lakaLantas" => $data['kd_lakalantas'],
				"lokasiLaka" => $data['lokasi_lakalantas'],
				"user" => $data['username'],
				"noMr" => $data['no_mr']
			),
		),
	);

	$data_encode = json_encode($prepare_data);

	// exit($data_encode);

	$result = _curl_post($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * update sep
 * @return return array/object if success or false if error found
 */
function update_sep($data) {
	$url = BASE_API."SEP/update";

	unset($prepare_data);
	$prepare_data = (object) array(
		"request" => (object) array(
			"t_sep" => (object) $data
		),
	);

	$data_encode = json_encode($prepare_data);

	$result = _curl_put($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * detail no_sep
 * @return return array/object if success or false if error found
 */
function detail_sep($no_sep="") {
	$no_sep = (!empty($no_sep)) ? $no_sep : "0";

	$url = BASE_API."SEP/{$no_sep}";

	$result = _curl($url, "Application/x-www-form-urlencoded");
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

function delete_sep($data) {
	$url = BASE_API . "SEP/delete";

	$req = (object) array(
		"request" => (object) array(
			"t_sep" => (object) array(
				"noSep" => $data['no_sep'],
				"ppkPelayanan" => $data['ppk_pelayanan']
			),
		),
	);

	$data_encode = json_encode($req);

	$result = _curl_delete($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

function update_tgl_pulang($data) {
	$url = BASE_API . "Sep/updtglplg";

	$req = (object) array(
		"request" => (object) array(
			"t_sep" => (object) array(
				"noSep" => $data['noSep'],
				"tglPlg" => $data['tglPulang'],
				"ppkPelayanan" => $data['ppkPelayanan']
			),
		),
	);

	$data_encode = json_encode($req);

	$result = _curl_put($url, $data_encode);
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * data kunjungan peserta
 * @return return array/object if success or false if error found
 */
function data_kunjungan_peserta($no_sep) {
	$url = BASE_API."sep/integrated/Kunjungan/sep/{$no_sep}";

	$result = _curl($url, "Application/x-www-form-urlencoded");
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * riwayat pelayanan peserta
 * @return return array/object if success or false if error found
 */
function riwayat_pelayanan_peserta($no_kartu) {
	$url = BASE_API."sep/peserta/{$no_kartu}";

	$result = _curl($url, "Application/x-www-form-urlencoded");
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

/**
 * data kunjungan pasien pcare
 * @return array/object if success or false if error found
 */
function get_rujukan($faskes='pcare', $search_by='norujukan', $s=null) {
	$s = (!is_null($s) || !empty($s)) ? $s : 0;

	if ( $faskes == 'pcare' ) {
		if ( $search_by == 'nokartu' ) {
			$url = BASE_API."Rujukan/Peserta/{$s}";
		}
		else {
			$url = BASE_API."Rujukan/{$s}";
		}
	}
	else {
		if ( $search_by == 'nokartu' ) {
			$url = BASE_API."Rujukan/RS/Peserta/{$s}";
		}
		else {
			$url = BASE_API."Rujukan/RS/{$s}";
		}
	}

	$result = _curl($url, "application/json; charset=utf-8");
	$response = json_decode($result);

	if ( is_object($response) ) {
		return $response;
	}

	return false;
}

function _curl($url, $content_type="application/json") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $content_type\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}

function _curl_post($url, $data, $content_type="Application/x-www-form-urlencoded") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	// curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: Application/x-www-form-urlencoded\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	// curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}

function _curl_put($url, $data, $content_type="Application/x-www-form-urlencoded") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	// curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: Application/x-www-form-urlencoded\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}

function _curl_delete($url, $data, $content_type="Application/x-www-form-urlencoded") {
	global $cons_id;
	global $tStamp;
	global $encodedSignature;

	$ch = curl_init($url);
	// curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: Application/x-www-form-urlencoded\r\n" . "X-cons-id: $cons_id\r\n" . "X-Timestamp: $tStamp\r\n" . "X-Signature: $encodedSignature"));
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result    = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);

	return $result;
}