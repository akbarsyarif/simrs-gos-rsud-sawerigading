<?php 
//-----------------------------------------------------------------------------------

if ( ! function_exists('Terbilang') ) {
	function Terbilang($x)
	{
	  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	  if ($x < 12)
		return " " . $abil[$x];
	  elseif ($x < 20)
		return Terbilang($x - 10) . "belas";
	  elseif ($x < 100)
		return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
	  elseif ($x < 200)
		return " seratus" . Terbilang($x - 100);
	  elseif ($x < 1000)
		return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
	  elseif ($x < 2000)
		return " seribu" . Terbilang($x - 1000);
	  elseif ($x < 1000000)
		return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
	  elseif ($x < 1000000000)
		return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('nomr') ) {
	function nomr($p='0'){
		$sql = "select LPAD(nomor,6,'0') as nomor from m_maxnomr";
		$query = mysql_query($sql);
		$data = mysql_fetch_assoc($query);
		$v = $data['nomor'] + $p;
		$vl = strlen($v);
		if ( $vl < 6 ) {
			$x = 6 - $vl;
			$val = '';
			for ( $i=1; $i<=$vl; $i++ ) :
				$val = $val +0;
			endfor;
			echo "$val"."$v";
		}
		else {
			echo $v;
		}
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getLastNoM') ) {
	function getLastNoM($p=0){
		$sql="select last2 as nomor from m_maxnomr where status=1";
		$query=mysql_query($sql);
		$data=mysql_fetch_assoc($query);
		$v = $data['nomor'];
		$vl=strlen($v);
		if($v < 10){ $value = '0'.$v;
		}else if($v < 100){ $value =$v;
		
		}
		return $value;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getLastMR') ) {
	function getLastMR($p=0){
		$sql="select last1 as nomor from m_maxnomr where status=1";
		$query=mysql_query($sql);
		$data=mysql_fetch_assoc($query);
		$v = $data['nomor'];
		$vl=strlen($v);
		if($v < 10){ $value = '0'.$v;
		}else if($v < 100){ $value =$v;
		
		}
		return $value;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('datediff') ) {
	function datediff($d1, $d2){
		$diff 	= abs(strtotime($d2) - strtotime($d1));
		$a	= array();
		$a['years'] 	= floor($diff / (365*60*60*24));
		$a['months'] = floor(($diff - $a['years'] * 365*60*60*24) / (30*60*60*24));
		$a['days'] 	= floor(($diff - $a['years'] * 365*60*60*24 - $a['months']*30*60*60*24)/ (60*60*24));
		return $a;
		#printf("%d years, %d months, %d days\n", $years, $months, $days);
		/*
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);
		$diff_secs = abs($d1 - $d2);
		$base_year = min(date("Y", $d1), date("Y", $d2));
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
		return array(
		"years" => date("Y", $diff) - $base_year,
		"months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
		"months" => date("n", $diff) - 1,
		"days_total" => floor($diff_secs / (3600 * 24)),
		"days" => date("j", $diff) - 1,
		"hours_total" => floor($diff_secs / 3600),
		"hours" => date("G", $diff),
		"minutes_total" => floor($diff_secs / 60),
		"minutes" => (int) date("i", $diff),
		"seconds_total" => $diff_secs,
		"seconds" => (int) date("s", $diff)
		
		);*/
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('datediff2') ) {
	function datediff2($d1,$d2){
		
	}
}

if ( ! function_exists('CurFormat') ) {
	function CurFormat($value,$dec=0){
		$res = number_format ($value,$dec,",",".");
		return $res;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getRealIpAddr') ) {
	function getRealIpAddr() {
		if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
		  $ip=$_SERVER['HTTP_CLIENT_IP']; // share internet
		} elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; // pass from proxy
		} else {
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
	  return $ip;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('jeniskelamin') ) {
	function jeniskelamin($P){
		if($P == 'P'){
			$v = "Perempuan (P)";
		}else{
			$v = "Laki - Laki (L)";
		}
		return $v;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getDokterName') ) {
	function getDokterName($kddokter){
		include("connect.php");
		$sql = mysql_query('select NAMADOKTER from m_dokter where KDDOKTER = '.$kddokter);
		if(mysql_num_rows($sql) > 0){
			$data = mysql_fetch_array($sql);
			$val = $data['NAMADOKTER'];
		}else{
			$val = '';
		}
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getJenisPoly') ) {
	function getJenisPoly($kdpoly){
		include("connect.php");
		$sql = mysql_query('select * from m_poly where kode = '.$kdpoly);
		if(mysql_num_rows($sql) > 0){
			$data = mysql_fetch_array($sql);
			$val = $data['jenispoly'];
		}else{
			$val = '';
		}
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getProfesiDoktor') ) {
	function getProfesiDoktor($kddokter){
		include("connect.php");
		$sql = mysql_query('select * from m_dokter where KDDOKTER = '.$kddokter);
		if(mysql_num_rows($sql) > 0) {
			$data = mysql_fetch_array($sql);
			$val = $data['KDPROFESI'];
		} else {
			$val = '';
		}
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getKodePendaftaran') ) {
	function getKodePendaftaran($jenispoly,$kdprofesi) {
		include("connect.php");
		$sql = 'select * from m_tarif2012 where kode_unit = "'.$jenispoly.'" and kode_profesi = "'.$kdprofesi.'"';
		$sql = mysql_query($sql);
		if ( mysql_num_rows($sql) > 0 ) {
			$data = mysql_fetch_array($sql);
			$val = $data['kode_tindakan'];
		}
		else{
			$val = '';
		}
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getTarifPendaftaran') ) {
	function getTarifPendaftaran($kodetarif){
		include("connect.php");
		$sql = 'select * from m_tarif2012 where kode_tindakan = "'.$kodetarif.'"';
		$sql = mysql_query($sql);
		if(mysql_num_rows($sql) > 0){
			$data = mysql_fetch_array($sql);
			$val[] = $data['jasa_sarana'];
			$val[] = $data['jasa_pelayanan'];
			$val[] = $data['tarif'];
		}else{
			$val[] = '';
		}
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getLastNoBILL') ) {
	function getLastNoBILL($p=0){
		include("connect.php");
		$sql = mysql_query('SELECT nomor FROM M_MAXNOBILL');
		if(mysql_num_rows($sql) > 0):
			$d	= mysql_fetch_array($sql);
			$no	= $d['nomor'] + $p;
		else:
			$no = 1;
		endif;
		return $no;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getLastIDXDAFTAR') ) {
	function getLastIDXDAFTAR($p=0){
		include("connect.php");
		$sql = mysql_query('select IDXDAFTAR from t_pendaftaran order by IDXDAFTAR desc limit 1');
		if(mysql_num_rows($sql) > 0):
			$d	= mysql_fetch_array($sql);
			$no	= $d['IDXDAFTAR'] + $p;
		else:
			$no = 1;
		endif;
		return $no;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getLastIDXOBATtmp') ) {
	function getLastIDXOBATtmp($p=0){
		include("connect.php");
		$sql = mysql_query('select IDXOBAT from tmp_cartresep order by IDXOBAT desc limit 1');
		if(mysql_num_rows($sql) > 0):
			$d	= mysql_fetch_array($sql);
			$no	= $d['IDXOBAT'] + $p;
		else:
			$no = 1;
		endif;
		return $no;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getNamaPoly') ) {
	function getNamaPoly($kdpoly){
		include 'connect.php';
		$sql 	= mysql_query('select nama from m_poly where kode = '.$kdpoly);
		if(mysql_num_rows($sql)){
			$data	= mysql_fetch_array($sql);
			$v	= 'Daftar Tindakan Poliklinik '.$data['nama'];
		}else{
			$v	= 'Daftar Tindakan Lain Lain';
		}
		return $v;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getPoly') ) {
	function getPoly($kdpoly){
		include 'connect.php';
		$sql 	= mysql_query('select nama from m_poly where kode = '.$kdpoly);
		if(mysql_num_rows($sql)){
			$data	= mysql_fetch_array($sql);
			$v	= $data['nama'];
		}else{
			$v	= '';
		}
		return $v;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getGroupUnit') ) {
	function getGroupUnit($val){
		include 'connect.php';
		$sql	= mysql_query('select * from m_unit where kode_unit = '.$val);
		if(mysql_num_rows($sql) > 0):
			$data	= mysql_fetch_array($sql);
			$v	= $data['grup_unit'];
		else:
			$v	= 1;
		endif;
		return $v;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('CheckLastLevel') ) {
	function CheckLastLevel($kode_tindakan,$kelas=''){
		include 'connect.php';
		$kelas_q = '';
		if($kelas != ''){
			$kelas_q 	= 'and kelas = "'.$kelas.'"';
		}
		$sql = mysql_query('select * from m_tarif2012 where kode_tindakan like "'.$kode_tindakan.'.%" '.$kelas_q);
		$val = mysql_num_rows($sql);
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getLastLevel') ) {
	function getLastLevel($kode_tindakan){
		include 'connect.php';
		$sql = mysql_query('select * from m_tarif2012 where kode_tindakan like "'.$kode_tindakan.'.%"');
		return $sql;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getGroupName') ) {
	function getGroupName($kode){
		include 'connect.php';
		$sql = mysql_query('select * from m_tarif2012 where kode_tindakan like "'.$kode.'%" order by kode_tindakan asc limit 1');
		$data= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('check_t_admission') ) {
	function check_t_admission($nomr,$idxdaftar,$kondisi=''){
		include 'connect.php';
		if($kondisi != ''){
			$kondisi = 'and '.$kondisi;
		}
		$sql	= 'select * from t_admission where id_admission = "'.$idxdaftar.'" and nomr = "'.$nomr.'"';
		$sql 	= mysql_query($sql);
		$data	= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('check_t_bayarrajal') ) {
	function check_t_bayarrajal($nomr,$idxdaftar){
		include 'connect.php';
		$sql	= 'select * from t_bayarrajal where IDXDAFTAR = "'.$idxdaftar.'" and NOMR = "'.$nomr.'" ORDER BY NOBILL DESC LIMIT 1';
		$sql 	= mysql_query($sql);
		$data	= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('check_t_bayarranap') ) {
	function check_t_bayarranap($nomr,$idxdaftar){
		include 'connect.php';
		$sql	= 'select * from t_bayarranap where IDXDAFTAR = "'.$idxdaftar.'" and NOMR = "'.$nomr.'"';
		$sql 	= mysql_query($sql);
		$data	= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('check_rajal_ranap_status_operasi') ) {
	function check_rajal_ranap_status_operasi($select="*",$id) {
		include 'connect.php';
		$sql	= mysql_query('select '.$select.' from t_operasi where id_operasi = '.$id);
		$data	= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getNamaDokter') ) {
	function getNamaDokter($kddokter) {
		include 'connect.php';
		$sql 	= mysql_query('select * from m_dokter where KDDOKTER = "'.$kddokter.'"');
		$data	= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getTarif') ) {
	function getTarif($kode){
		include 'connect.php';
		$sql = mysql_query('select jasa_sarana, jasa_pelayanan, tarif from m_tarif2012 where kode_tindakan ="'.$kode.'"');
		$data	= mysql_fetch_array($sql);
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('TrimArray') ) {
	function TrimArray($Input){ 
		if (!is_array($Input))
			return trim($Input);
		return array_map('TrimArray', $Input);
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getKecamatanName') ) {
	function getKecamatanName($id){
		include("connect.php");
		$sql = mysql_query('select * from m_kecamatan where idkecamatan = '.$id);
		if(mysql_num_rows($sql) > 0){
			$data = mysql_fetch_array($sql);
			$val = $data['namakecamatan'];
		}else{
			$val = '';
		}
		return $val;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getdaftar') ) {
	function getdaftar($nomr) {
		include 'connect.php';
		$sql 	= mysql_query('select IDXDAFTAR from t_pendaftaran where KDPOLY ="10" and NOMR = "'.$nomr.'" order by IDXDAFTAR desc limit 1');
		$data2	= mysql_fetch_array($sql);
		$data = $data2['IDXDAFTAR'];
		return $data;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('redirect') ) {
	function redirect($url) {
		header("Location: $url");
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('http_response_code') )
{
	function http_response_code($newcode = NULL) {
		static $code = 200;
		if($newcode !== NULL)
		{
			header('X-PHP-Response-Code: '.$newcode, true, $newcode);
			if(!headers_sent())
				$code = $newcode;
		}
		return $code;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('getProfil') )
{

	function getProfil()
	{

		$profil = mysql_fetch_array(mysql_query("SELECT * FROM profil"));

		return $profil;

	}

}

//-----------------------------------------------------------------------------------

if ( ! function_exists('dinamycDropdown') )
{

	function dinamycDropdown( $config=array() )
	{

		$query = array();
		$html = '';

		$name        = (isset($config['name'])) ? $config['name'] : '';
		$table       = (isset($config['table'])) ? $config['table'] : '';
		$select      = (isset($config['select'])) ? $config['select'] : '*';
		$key         = (isset($config['key'])) ? $config['key'] : '';
		$value       = (isset($config['value'])) ? $config['value'] : '';
		$selected    = (isset($config['selected'])) ? $config['selected'] : '';
		$where       = (isset($config['where'])) ? $config['where'] : '';
		$order_by    = (isset($config['order_by'])) ? $config['order_by'] : '';
		$empty_first = (isset($config['empty_first'])) ? $config['empty_first'] : '';
		$first_value = (isset($config['first_value'])) ? $config['first_value'] : '-- Pilih --';
		$attr        = (isset($config['attr'])) ? $config['attr'] : '';

		// join table
		$join_table = (isset($config['join'])) ? $config['join'] : '';
		$reference_key = (isset($config['reference_key'])) ? $config['reference_key'] : '';
		$source_key = (isset($config['source_key'])) ? $config['source_key'] : '';

		$join = "";

		if ( $join_table != "" ) {

			$join .= "INNER JOIN {$join_table} ON {$join_table}.{$reference_key} = {$table}.{$source_key}";

		}

		$sql = "SELECT {$select} FROM $table {$join}";
		$sql .= ($where != '') ? " WHERE {$where}" : "";
		$sql .= ($order_by != '') ? " ORDER BY {$order_by}" : " ORDER BY {$key} ASC";
		$result = mysql_query($sql);

		$html .= "<select name='{$name}' {$attr}>";

		if ( $empty_first === TRUE ) {
			$html .= "<option value=''>" . $first_value . "</option>";
		}

		while ( $row = mysql_fetch_assoc($result) ) {
			$is_selected = ($selected == $row["{$key}"]) ? 'selected' : '';
			$html .= "<option {$is_selected} value='" . $row["{$key}"] . "'>" . $row["{$value}"] . "</option>";
		}
		$html .= "</select>";

		return $html;

	}

}

//-----------------------------------------------------------------------------------

if ( ! function_exists('formDropdown') )
{

	function formDropdown( $config=array() )
	{

		$name     = (isset($config['name'])) ? $config['name'] : '';
		$options  = (isset($config['options'])) ? $config['options'] : array();
		$selected = (isset($config['selected'])) ? $config['selected'] : '';
		$attr     = (isset($config['attr'])) ? $config['attr'] : '';

		$html = '';

		$html .= "<select name='{$name}' {$attr}>";
			foreach ( $options as $value => $label ) {
				$is_selected = ($selected == $value) ? 'selected' : '';
				$html .= "<option {$is_selected} value='" . $value . "'>" . $label . "</option>";
			}
		$html .= "</select>";

		return $html;

	}

}

//-----------------------------------------------------------------------------------

if ( ! function_exists('formRadio') )
{

	function formRadio( $config=array() )
	{

		$name     = (isset($config['name'])) ? $config['name'] : '';
		$options  = (isset($config['options'])) ? $config['options'] : array();
		$selected = (isset($config['selected'])) ? $config['selected'] : NULL;
		$attr     = (isset($config['attr'])) ? $config['attr'] : '';

		$html = '';

		foreach ( $options as $value => $label ) {
			if ( is_null($selected) ) {
				$is_selected = '';
			} else {
				$is_selected = ($selected == $value) ? 'checked' : '';
			}

			$html .= "<div>";
				$html .= "<label class='radio-inline'>";
					$html .= "<input type='radio' name='{$name}' {$attr} value='{$value}' $is_selected /> {$label}";
				$html .= "</label>";
			$html .= "</div>";
		}

		return $html;

	}

}

//-----------------------------------------------------------------------------------

if ( ! function_exists('mappingPolyBpjs') )
{

	function mappingPolyBpjs($kd_poly)
	{

		require_once BASEPATH . 'models/m_poly.php';

		$result = Models\Poly\rowPoly( array (
			'query' => "WHERE kode='{$kd_poly}'"
		) );

		if ( $result === FALSE ) {

			$kd_poly_bpjs = NULL;

		}
		else {

			$kd_poly_bpjs = $result['kd_bpjs'];

		}

		return $kd_poly_bpjs;

	}

}

//-----------------------------------------------------------------------------------

if ( ! function_exists('tgl_indonesia'))
{
	function tgl_indonesia($tgl)
	{
		$pecah_timestamp = explode(" ", $tgl);
		$waktu = '';
		if ( count($pecah_timestamp > 0) ) {
			$tgl = $pecah_timestamp[0];
			$waktu = $pecah_timestamp[1];
		}

		$ubah    = gmdate($tgl, time()+60*60*8);
		$pecah   = explode("-",$ubah);
		$tanggal = $pecah[2];
		$bulan   = $pecah[1];
		$tahun   = $pecah[0];
		$hari    = nama_hari($tgl);

		return $hari.', '.$tanggal.'/'.$bulan.'/'.$tahun.' '.$waktu;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('nama_hari'))
{
	function nama_hari($tanggal)
	{
		$ubah = gmdate($tanggal, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tgl = $pecah[2];
		$bln = $pecah[1];
		$thn = $pecah[0];

		$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
		$nama_hari = "";
		if($nama=="Sunday") {$nama_hari="Minggu";}
		else if($nama=="Monday") {$nama_hari="Senin";}
		else if($nama=="Tuesday") {$nama_hari="Selasa";}
		else if($nama=="Wednesday") {$nama_hari="Rabu";}
		else if($nama=="Thursday") {$nama_hari="Kamis";}
		else if($nama=="Friday") {$nama_hari="Jumat";}
		else if($nama=="Saturday") {$nama_hari="Sabtu";}

		return $nama_hari;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('countTimeDiff') )
{
	function countTimeDiff($from_time, $to_time)
	{
		$from_time = strtotime($from_time);
		$to_time = strtotime($to_time);
		$result = round(abs($to_time - $from_time) / 60,2). " menit";

		return $result;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('clearDiagnosaStr') )
{
	function clearDiagnosaStr($str)
	{
		$str = trim($str);
		$str = strip_tags($str, "<br>");
		$str = str_replace('&nbsp;', '', $str);
		$str = str_replace('<br>', ', ', $str);
		$str = str_replace(' ,', ',', $str);
		$str = trim($str);
		$str = rtrim($str, ',');

		return (string) $str;
	}
}

//-----------------------------------------------------------------------------------

if ( ! function_exists('arrTindakanToStr') )
{
	function arrTindakanToStr($arr)
	{
		$str = "";

		if ( $arr !== FALSE ) {
			foreach ($arr['rows'] as $row) {
				$str .= $row['NAMA_TINDAKAN'] . ', ';
			}
		}

		$str = rtrim($str, ', ');

		return $str;
	}
}

//-----------------------------------------------------------------------------------