<?php
require_once 'connect.php';

if ( ! function_exists('create_nomr') ) {
	/**
	 * membuat nomor rekam medik otomatis
	 * @return string
	 */
	function create_nomr() {
		$sql = "SELECT * FROM m_maxnomr WHERE status='1'";
		$get_max_nomr = mysql_query($sql);
		$data = mysql_fetch_array($get_max_nomr);
		$last_no_mr = $data['last2']+1;
		$new_no_mr = str_pad($last_no_mr,6,"0",STR_PAD_LEFT);

		return $new_no_mr;
	}
}

if ( ! function_exists('update_last_nomr') ) {
	/**
	 * memperbahrui nomor rekam medik terakhir
	 * @return boolean
	 */
	function update_last_nomr($nomr) {
		$sql = "UPDATE m_maxnomr SET nomor='{$nomr}', last2='{$nomr}' WHERE status='1'";
		$update_last_nomr = mysql_query($sql);
		if ( $update_last_nomr ) {
			return TRUE;
		}

		return FALSE;
	}
}

if ( ! function_exists('is_pasien_exist') ) {
	/**
	 * mengecek pasien berdasarkan nomor rekam medik
	 * @param  [type]  $nomr [description]
	 * @return boolean       [description]
	 */
	function is_pasien_exist($nomr) {
		$sql = "SELECT COUNT(*) AS numrows FROM m_pasien WHERE NOMR = '{$nomr}'";
		$result = mysql_query($sql);
		$data = mysql_fetch_assoc($result);
		if ( $data['numrows'] > 0 ) {
			return TRUE;
		}

		return FALSE;
	}
}

if ( ! function_exists('isNoMrExists') ) {
	/**
	 * mengecek apakah nomor rekam medis sudah digunakan oleh pasien lain
	 * @param  [type]  $nomr [nomor rekam medis yang ingin dicari]
	 * @param  [type]  $id   [id pasien yang akan dikecualikan dalam pencarian]
	 * @return array       [data pasien]
	 */
	function isNoMrExists($nomr, $id) {
		$sql = "SELECT COUNT(*) AS numrows, NOMR, NAMA FROM m_pasien WHERE NOMR = '{$nomr}' AND id <> '{$id}' LIMIT 1";
		$result = mysql_query($sql);
		$data = mysql_fetch_assoc($result);
		if ( $data['numrows'] > 0 ) {
			return array (
				'nomr' => $data['NOMR'],
				'nama' => $data['NAMA']
			);
		}

		return FALSE;
	}
}