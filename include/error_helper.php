<?php

if ( ! function_exists('show_error') ) {
	/**
	 * menampilkan pesan error
	 * @param  string  $error_msg [description]
	 * @param  boolean $return    [description]
	 * @return [type]             [description]
	 */
	function show_error($error_msg='', $return=FALSE) {
		if ( is_array($error_msg) ) {
			$html = '<div>';
			foreach( $error_msg as $error ) {
				$html .= "<p>".$error."</p>";
			}
			$html .= '</div>';
		}
		else {
			$html = "<p>".$error_msg."</p>";
		}

		if ( $return === FALSE ) {
			exit($html);
		}
		else {
			return $html;
		}
	}
}

if ( ! function_exists('show_exception_error') ) {
	/**
	 * menampilkan error dari exeption object
	 * @param  [type] $e [description]
	 * @return [type]    [description]
	 */
	function show_exception_error($e, $return=FALSE) {
		$html = '';
		if ( is_object($e) ) {
			$html .= '<pre class="error-exception">';
				$html .= '<strong>' . $e->getMessage() . '. ' . $e->getFile() . ' on line ' . $e->getLine() . '</strong>';
			$html .= '</pre>';
		}

		if ( $return === TRUE ) {
			return $html;
		} else {
			die($html);
		}
	}
}