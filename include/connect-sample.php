<?php
ini_set('memory_limit' , '128M');

// set timezone
date_default_timezone_set("Asia/Makassar");

// set environtment
define('ENV', 'development');

switch (ENV) {
	case 'development':
		# development
		// error_reporting(E_ALL);
		error_reporting(E_ALL ^ E_NOTICE || E_WARNING);
		break;

	case 'testing':
		error_reporting(E_DEPRECATED);
		break;

	default:
		error_reporting(0);
		break;
}

define ('BASEPATH', __DIR__ . "/../");
define ('_BASE_', 'http://'.$_SERVER['HTTP_HOST'].'/simrs/');
define ('_POPUPTIME_', '50000');
define ('DIRSEPARATOR', '/');
define ('FILEEXT', '.php');

// database settings
$hostname = 'localhost';
$database = 'simrs-20170112145500';
$username = 'root';
$password = '';

// connect to database server
$connect = mysql_connect($hostname, $username, $password,true,65536) or die(mysql_error());
mysql_select_db($database, $connect)or die(mysql_error());

$profil = mysql_fetch_array(mysql_query("SELECT * FROM profil"));

$rstitle        = $profil['rstitle'];
$singrstitl     = $profil['singrstitl'];
$singhead1      = $profil['singhead1'];
$singsurat      = $profil['singsurat'];
$header1        = $profil['header1'];
$header2        = $profil['header2'];
$header3        = $profil['header3'];
$header4        = $profil['header4'];
$KDRS           = $profil['kdrs'];
$KelasRS        = $profil['kelasrs'];
$NamaRS         = $profil['namars'];
$KDTarifINACBG  = $profil['kdtarifnacbg'];
$kdproviderbpjs = $profil['kdproviderbpjs'];