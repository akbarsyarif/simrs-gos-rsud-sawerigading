<script language="JavaScript">
	jQuery(document).ready(function() {
		// dropdown kota
		jQuery('#segment-data-pasien').on('change', '#KDPROVINSI', function() {
			var id_prov = jQuery(this).val();
			getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : id_prov, target : '#KOTA', selected : '' });
			jQuery('#KDKECAMATAN').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#KELURAHAN').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kecamatan
		jQuery('#segment-data-pasien').on('change', '#KOTA', function() {
			var id_kota = jQuery(this).val();
			getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : id_kota, target : '#KDKECAMATAN', selected : '' });
			jQuery('#KELURAHAN').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kelurahan
		jQuery('#segment-data-pasien').on('change', '#KDKECAMATAN', function() {
			var id_kec = jQuery(this).val();
			getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : id_kec, target : '#KELURAHAN', selected : '' });
		});
	});
</script>

<div id='msg'></div>
<div id="list_data"></div>

<fieldset class="fieldset" id="segment-data-pasien">
	<legend>Data Pasien</legend>

	<table width="100%">
		<tr>
			<td width="70%">
				<table width="100%" border="0" id="tablebody" cellpadding="0" cellspacing="0" title=" From Ini Berfungsi Sebagai Form Entry Data Pasien Baru." style="float:left;">
					<tr>
						<td width="30%">Nama Lengkap Pasien</td>
						<td>
							<span id="nam"><input class="text required" title="Tidak boleh kosong"  type="text" name="NAMA" value="" id="NAMA" /></span>
							<select name="CALLER" class="text required" title="Tidak boleh kosong"  id="CALLER">
								<option selected value="">- Alias -</option>
								<option value="Tn" <? if($_GET['CALLER']=="Tn") echo "selected=selected"; ?>> Tn </option>
								<option value="Ny" <? if($_GET['CALLER']=="Ny") echo "selected=selected"; ?>> Ny </option>
								<option value="Nn" <? if($_GET['CALLER']=="Nn") echo "selected=selected"; ?>> Nn </option>
								<option value="An" <? if($_GET['CALLER']=="An") echo "selected=selected"; ?>> An </option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Tempat Tanggal Lahir</td>
						<td>
							<input placeholder="Tempat" type="text" value="<? if(!empty($_GET['TEMPAT'])){ echo $_GET['TEMPAT']; }else{ echo $m_pasien->TEMPAT;} ?>" class="text required" title="Tidak boleh kosong"  name="TEMPAT" size="15" id="TEMPAT" />
							<span class="relative-container">
								<input onblur="calage1(this.value,'umur');" placeholder="Tanggal lahir" type="text" class="text required datepicker" title="Tidak boleh kosong" data-date-format="DD/MM/YYYY" value="<?= (isset($_GET['TGLLAHIR']) && !empty($_GET['TGLLAHIR'])) ? date('d/m/Y', strtotime($_GET['TGLLAHIR'])) : date('d/m/Y', strtotime($m_pasien->TGLLAHIR)) ?>" name="TGLLAHIR" id="TGLLAHIR" size="20" />
							</span>
							<!-- <a href="javascript:showCal1('Calendar1')"><button type="button" class="text"><i class="fa fa-fw fa-calendar"></i></button></a> -->
						</td>
					</tr>
					<tr>
						<td>Umur</td>
						<td>
							<?php 
							if ( $m_pasien->TGLLAHIR == "" ) {
								$a = datediff(date("Y/m/d"), date("Y/m/d"));
							}
							else {
								 $a = datediff($m_pasien->TGLLAHIR, date("Y/m/d"));
							} ?>
							<span id="umurc"><input class="text" type="text" value="" name="umur" id="umur" size="45" /></span>
						</td>
					</tr>
					<tr>
						<td valign="top">Alamat Sekarang</td>
						<td colspan="1"><input name="ALAMAT" id="ALAMAT" class="text required" title="Tidak boleh kosong"  type="text" value="<? if(!empty($_GET['ALAMAT'])){ echo $_GET['ALAMAT']; } ?>" size="45" /></td>
					</tr>
					<tr>
						<td>Alamat KTP</td>
						<td><input name="ALAMAT_KTP" class="text required" type="text" value="<? if(!empty($_GET['ALAMAT_KTP'])){ echo $_GET['ALAMAT_KTP']; } ?>" size="45" id="ALAMAT_KTP" /></td>
					</tr>
					<tr>
						<td>Provinsi</td>
						<td>
							<?= dinamycDropdown(array(
								'name' => 'KDPROVINSI',
								'table' => 'm_provinsi',
								'key' => 'idprovinsi',
								'value' => 'namaprovinsi',
								'selected' => '',
								'empty_first' => TRUE,
								'first_value' => '- Pilih Provinsi -',
								'order_by' => 'namaprovinsi asc',
								'attr' => 'class="text required" title="Tidak boleh kosong" id="KDPROVINSI"',
							)) ?>
							<input class="text" value="<?=$_GET['KOTA']?>" type="hidden" name="KOTAHIDDEN" id="KOTAHIDDEN" />
							<input class="text" value="<?=$_GET['KECAMATAN']?>" type="hidden" name="KECAMATANHIDDEN" id="KECAMATANHIDDEN" />
							<input class="text" value="<?=$_GET['KELURAHAN']?>" type="hidden" name="KELURAHANHIDDEN" id="KELURAHANHIDDEN" />
						</td>
					</tr>
					<tr>
						<td>Kabupaten / Kota</td>
						<td>
							<div id="kotapilih">
								<select name="KOTA" class="text required" title="Tidak boleh kosong"  id="KOTA">
									<option value=""> - Pilih Kota - </option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Kecamatan</td>
						<td>
							<div id="kecamatanpilih">
								<select name="KDKECAMATAN" class="text required" title="Tidak boleh kosong"  id="KDKECAMATAN">
									<option value=""> - Pilih Kecamatan - </option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Kelurahan</td>
						<td>
							<div id="kelurahanpilih">
								<select name="KELURAHAN" class="text required" title="Tidak boleh kosong"  id="KELURAHAN">
									<option value=""> - Pilih Kelurahan - </option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>No Telepon / HP </td>
						<td><input  class="text required" value="<? if(!empty($_GET['NOTELP'])){ echo $_GET['NOTELP']; }else{ echo $m_pasien->NOTELP;} ?>" type="text" name="NOTELP" id="notelp" /></td>
					</tr>
					<tr>
						<td>No KTP </td>
						<td><input  class="text required" value="<? if(!empty($_GET['NOKTP'])){ echo $_GET['NOKTP']; }else{ echo $m_pasien->NOKTP;} ?>" type="text" name="NOKTP" id="NOKTP" /></td>
						</tr>
					<tr>
						<td>Nama Suami / Orang Tua </td>
						<td><input class="text required" title="Tidak boleh kosong"  type="text" value="<? if(!empty($_GET['SUAMI_ORTU'])){ echo $_GET['SUAMI_ORTU']; }else{ echo $m_pasien->SUAMI_ORTU;} ?>" name="SUAMI_ORTU" id="SUAMI_ORTU" /></td>
						</tr>
					<tr>
						<td>Pekerjaan Pasien / Orang Tua</td>
						<td><input class="text required" type="text" value="<? if(!empty($_GET['PEKERJAAN'])){ echo $_GET['PEKERJAAN']; }else{ echo $m_pasien->PEKERJAAN;} ?>" name="PEKERJAAN" id="PEKERJAAN" /></td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td valign="top">Nama Penanggung Jawab</td>
						<td valign="top"><input class="text required" title="Tidak boleh kosong"  type="text" name="nama_penanggungjawab" value="<? if(!empty($_GET['nama_penanggungjawab'])){ echo $_GET['nama_penanggungjawab']; } ?>" id="nama_penanggungjawab"  /></td>
					</tr>
					<tr>
						<td valign="top">Hubungan Dengan Pasien</td>
						<td valign="top"><input class="text required" title="Tidak boleh kosong"  type="text" name="hubungan_penanggungjawab" value="<? if(!empty($_GET['hubungan_penanggungjawab'])){ echo $_GET['hubungan_penanggungjawab']; } ?>" id="hubungan_penanggungjawab" /></td>
					</tr>
					<tr>
						<td valign="top">Alamat</td>
						<td valign="top"><input name="alamat_penanggungjawab" class="text required" type="text" value="<? if(!empty($_GET['alamat_penanggungjawab'])){ echo $_GET['alamat_penanggungjawab']; } ?>" id="alamat_penanggungjawab" /></td>
					</tr>
					<tr>
						<td valign="top">No Telepon / HP</td>
						<td valign="top"><input  class="text required" type="text" name="phone_penanggungjawab" value="<? if(!empty($_GET['phone_penanggungjawab'])){ echo $_GET['phone_penanggungjawab']; } ?>" id="phone_penanggungjawab" /></td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td colspan="2"><input type='hidden' name='stop_daftar' id='stop_daftar' /></td>
					</tr>
				</table>
			</td>
			<td width="30%">
				<table width="100%">
					<tr>
						<td width="40%">Jenis Kelamin</td>
						<td>
							<div><label class="radio-inline"><input type="radio" name="JENISKELAMIN" id="JENISKELAMIN_L" class="required" title="Tidak boleh kosong"  value="L" /> Laki-laki</label></div>
							<div><label class="radio-inline"><input type="radio" name="JENISKELAMIN" id="JENISKELAMIN_P" class="required" title="Tidak boleh kosong"  value="P" /> Perempuan</label></div>
						</td>
					</tr>
					<tr>
						<td>Status Perkawinan</td>
						<td>
							<div><label class="radio-inline"><input type="radio" name="STATUS" id="status_1" class="required" title="Tidak boleh kosong"  value="1" <? if($m_pasien->STATUS=="1" || $_GET['STATUS']=="1") echo "checked";?>/> Belum Kawin</label></div>
							<div><label class="radio-inline"><input type="radio" name="STATUS" id="status_2" class="required" title="Tidak boleh kosong"  value="2" <? if($m_pasien->STATUS=="2" || $_GET['STATUS']=="2") echo "checked";?> /> Kawin</label></div>
							<div><label class="radio-inline"><input type="radio" name="STATUS" id="status_3" class="required" title="Tidak boleh kosong"  value="3" <? if($m_pasien->STATUS=="3" || $_GET['STATUS']=="3") echo "checked";?>/> Janda / Duda</label></div>
						</td>
					</tr>
					<tr>
						<td>Pendidikan Terakhir</td>
						<td>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_0" class="required" title="Tidak boleh kosong"  value="0" <? if($m_pasien->PENDIDIKAN=="0" || $_GET['PENDIDIKAN']=="0") echo "checked";?> /> Belum Sekolah</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_1" class="required" title="Tidak boleh kosong"  value="1" <? if($m_pasien->PENDIDIKAN=="1" || $_GET['PENDIDIKAN']=="1") echo "checked";?> /> SD</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_2" class="required" title="Tidak boleh kosong"  value="2" <? if($m_pasien->PENDIDIKAN=="2" || $_GET['PENDIDIKAN']=="2") echo "checked";?> /> SLTP</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_3" class="required" title="Tidak boleh kosong"  value="3" <? if($m_pasien->PENDIDIKAN=="3" || $_GET['PENDIDIKAN']=="3") echo "checked";?> /> SMU</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_4" class="required" title="Tidak boleh kosong"  value="4" <? if($m_pasien->PENDIDIKAN=="4" || $_GET['PENDIDIKAN']=="4") echo "checked";?> /> D3/Akademik</label></div>
							<div><label class="radio-inline"><input type="radio" name="PENDIDIKAN" id="PENDIDIKAN_5" class="required" title="Tidak boleh kosong"  value="5" <? if($m_pasien->PENDIDIKAN=="5" || $_GET['PENDIDIKAN']=="5") echo "checked";?> /> Universitas</label></div>
						</td>
					</tr>
					<tr>
						<td>Agama</td>
						<td>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_1" class="required" title="Tidak boleh kosong"  value="1" <? if($m_pasien->AGAMA=="1" || $_GET['AGAMA']=="1") echo "checked";?> /> Islam</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_2" class="required" title="Tidak boleh kosong"  value="2" <? if($m_pasien->AGAMA=="2" || $_GET['AGAMA']=="2") echo "checked";?>/> Kristen Protestan</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_3" class="required" title="Tidak boleh kosong"  value="3" <? if($m_pasien->AGAMA=="3" || $_GET['AGAMA']=="3") echo "checked";?>/> Katholik</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_4" class="required" title="Tidak boleh kosong"  value="4" <? if($m_pasien->AGAMA=="4" || $_GET['AGAMA']=="4") echo "checked";?>/> Hindu</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_5" class="required" title="Tidak boleh kosong"  value="5" <? if($m_pasien->AGAMA=="5" || $_GET['AGAMA']=="5") echo "checked";?>/> Budha</label></div>
							<div><label class="radio-inline"><input type="radio" name="AGAMA" id="AGAMA_6" class="required" title="Tidak boleh kosong"  value="6" <? if($m_pasien->AGAMA=="6" || $_GET['AGAMA']=="6") echo "checked";?>/> Lain - lain</label></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<div class="text-center">
		<button type="submit" name="daftar" class="btn btn-primary btn-sm" onclick="stopjam()" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> Sedang Menyimpan"><i class="fa fa-fw fa-save"></i> Simpan</button>
	</div>

	<input type="text" id="msgid" name="msgid" style="border:1px #FFF solid; width:0px; height:0px;">
</fieldset>

<?php if ( ! empty($_GET['TGLLAHIR']) ) : ?>
	<script language="javascript" >
		calage1('<?=$_GET['TGLLAHIR']?>','umur');
	</script>
<?php endif; ?>

<script language="javascript" type="text/javascript">
	function cetak() {
		var nomr = document.getElementById('NOMR').value;
		var nama = document.getElementById('NAMA').value;
		var alamat = document.getElementById('ALAMAT').value;
		var TGLLAHIR = document.getElementById('TGLLAHIR').value;
		var JENISKELAMIN = document.getElementById('JENISKELAMIN_L').value;

		window.open("pdfb/kartupasien.php?NOMR="+ nomr +"&NAMA="+ nama +"&ALAMAT="+ alamat+"&TGLLAHIR="+ TGLLAHIR+"&JENISKELAMIN="+ JENISKELAMIN,"mywindow");
		
		return false;
	}
</script>