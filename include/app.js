// show progress when ajax start
jQuery(document).ajaxStart(function() {
	NProgress.start();
	NProgress.set(0.3);
	NProgress.inc();
});

// stop progress when ajax stop
jQuery(document).ajaxStop(function() {
	NProgress.done();
});

jQuery(document).ready(function() {
	// select 2
	jQuery('.select2-single').select2();

	// trigger select2 of first page load
	jQuery('.select2-single').trigger('change');

	// eonasdan date time picker
	jQuery('input.datepicker, .input-group.datepicker').datetimepicker({
		format: "YYYY-MM-DD"
	});

	jQuery('input.timepicker, .input-group.timepicker').datetimepicker({
		format: "HH:mm:ss"
	});

	jQuery('input.datetimepicker, .input-group.datetimepicker').datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss"
	});

	// add bootstrap table class
	jQuery('table.tb').addClass('table');
	// jQuery('[type="text"],[type="password"],select').addClass('text');

	/** input mask integer */
	jQuery('.input-mask-integer').inputmask('integer', {
		groupSeparator : '.',
		autoGroup      : true,
		digits         : 0
	});

	/** input mask numeric */
	jQuery('.input-mask-numeric').inputmask('numeric');

	/** input mask only number */
	jQuery('.input-mask-only-number').inputmask('integer', {
		autoGroup : true,
		digits : 0
	});

	/** input mask date */
	jQuery('.input-mask-date').inputmask('99/99/9999');

	// Settings object that controls default parameters for library methods:
	accounting.settings = {
		currency: {
			symbol : "Rp",   // default currency symbol is '$'
			format : "%s %v", // controls output: %s = symbol, %v = value/number (can be object: see below)
			decimal : ".",  // decimal point separator
			thousand : ",",  // thousands separator
			precision : 2   // decimal places
		},
		number: {
			precision : 0,  // default precision on numbers is 0
			thousand : ",",
			decimal : "."
		}
	}
});

// reinitialize select2
function reinitializeSelect2Single($target) {
	jQuery($target).select2('destroy');
	jQuery($target).select2();
}

// get and set kab/kota when provinsi changed
function getKota(obj) {
	if (typeof obj.beforeSend != 'undefined') {
		obj.beforeSend();
	}

	// validate parameter
	// id provinsi
	if (obj.id_prov == '' || typeof obj.id_prov == 'undefined') {
		console.log('Error : ID provinsi tidak ditemukan.');
	}
	else {
		jQuery.ajax({
			url : obj.url,
			method : 'get',
			data : { p : 'daerah', a : 'get_kota', id_prov : obj.id_prov },
			dataType : 'json',
			success : function(response) {
				if (response.result == false) {
					alert(response.message);
				}
				else {
					html = '<option value="">- Pilih Kota -</option>';
					jQuery(obj.target).html(function() {
						for (i=0; i<=response.data.length - 1; i++) {
							var current_option = response.data[i];

							if (typeof obj.selected != 'undefined') {
								if (obj.selected == current_option.idkota) {
									html += '<option selected value="'+current_option.idkota+'">'+current_option.namakota+'</option>';
								}
								else {
									html += '<option value="'+current_option.idkota+'">'+current_option.namakota+'</option>';
								}
							}
							else {
								html += '<option value="'+current_option.idkota+'">'+current_option.namakota+'</option>';
							}
						}

						return html;
					});
				}
			},
			error : function(error) {
				alert('Terjadi kesalahan. Tidak dapat menghubungi server.');
				console.log(error);
			},
			complete : function() {
				if (typeof obj.complete != 'undefined') {
					obj.complete();
				}
			}
		})
	}
}


// get and set kecamatan when kabupaten changed
function getKecamatan(obj) {
	if (typeof obj.beforeSend != 'undefined') {
		obj.beforeSend();
	}

	// validate parameter
	// id kabupaten
	if (obj.id_kota == '' || typeof obj.id_kota == 'undefined') {
		console.log('Error : ID kota tidak ditemukan.');
	}
	else {
		jQuery.ajax({
			url : obj.url,
			method : 'get',
			data : { p : 'daerah', a : 'get_kecamatan', id_kota : obj.id_kota },
			dataType : 'json',
			success : function(response) {
				if (response.result == false) {
					alert(response.message);
				}
				else {
					html = '<option value="">- Pilih Kecamatan -</option>';
					jQuery(obj.target).html(function() {
						for (i=0; i<=response.data.length - 1; i++) {
							var current_option = response.data[i];

							if (typeof obj.selected != 'undefined') {
								if (obj.selected == current_option.idkecamatan) {
									html += '<option selected value="'+current_option.idkecamatan+'">'+current_option.namakecamatan+'</option>';
								}
								else {
									html += '<option value="'+current_option.idkecamatan+'">'+current_option.namakecamatan+'</option>';
								}
							}
							else {
								html += '<option value="'+current_option.idkecamatan+'">'+current_option.namakecamatan+'</option>';
							}
						}

						return html;
					});
				}
			},
			error : function(error) {
				console.log(error);
			},
			complete : function() {
				if (typeof obj.complete != 'undefined') {
					obj.complete();
				}
			}
		})
	}
}


// get and set kelurahan when kecamatan changed
function getKelurahan(obj) {
	if (typeof obj.beforeSend != 'undefined') {
		obj.beforeSend();
	}

	// validate parameter
	// id kecamatan
	if (obj.id_kec == '' || typeof obj.id_kec == 'undefined') {
		console.log('Error : ID Kabupaten tidak ditemukan.');
	}
	else {
		jQuery.ajax({
			url : obj.url,
			method : 'get',
			data : { p : 'daerah', a : 'get_kelurahan', id_kec : obj.id_kec },
			dataType : 'json',
			success : function(response) {
				if (response.result == false) {
					alert(response.message);
				}
				else {
					html = '<option value="">- Pilih Kelurahan -</option>';
					jQuery(obj.target).html(function() {
						for (i=0; i<=response.data.length - 1; i++) {
							var current_option = response.data[i];

							if (typeof obj.selected != 'undefined') {
								if (obj.selected == current_option.idkelurahan) {
									html += '<option selected value="'+current_option.idkelurahan+'">'+current_option.namakelurahan+'</option>';
								}
								else {
									html += '<option value="'+current_option.idkelurahan+'">'+current_option.namakelurahan+'</option>';
								}
							}
							else {
								html += '<option value="'+current_option.idkelurahan+'">'+current_option.namakelurahan+'</option>';
							}
						}

						return html;
					});
				}
			},
			error : function(error) {
				console.log(error);
			},
			complete : function() {
				if (typeof obj.complete != 'undefined') {
					obj.complete();
				}
			}
		})
	}
}

// dropdown dokter jaga
function getDokterJaga(obj) {
	if (typeof obj.beforeSend != 'undefined') {
		obj.beforeSend();
	}

	// validate parameter
	if (obj.kd_poly == '' || typeof obj.kd_poly == 'undefined') {
		console.log('Error : Kode poli tidak ditemukan.');
	}
	else {
		jQuery.ajax({
			url : obj.url,
			method : 'get',
			data : { p : 'dokterjaga', a : 'getdokterjaga', kd_poly : obj.kd_poly },
			dataType : 'json',
			success : function(r) {
				if (r.metadata.code != 200) {
					alert(r.metadata.message);
				}
				else {
					html = '<option value="">- Pilih Dokter -</option>';
					jQuery(obj.target).html(function() {
						for (i=0; i<=r.response.length - 1; i++) {
							var current_option = r.response[i];

							if (typeof obj.selected != 'undefined') {
								if (obj.selected == current_option.kddokter) {
									html += '<option selected value="'+current_option.kddokter+'">'+current_option.namadokter+'</option>';
								}
								else {
									html += '<option value="'+current_option.kddokter+'">'+current_option.namadokter+'</option>';
								}
							}
							else {
								html += '<option value="'+current_option.kddokter+'">'+current_option.namadokter+'</option>';
							}
						}

						return html;
					});
				}
			},
			error : function(error) {
				alert('Terjadi kesalahan. Tidak dapat menghubungi server.');
				console.log(error);
			},
			complete : function() {
				if (typeof obj.complete != 'undefined') {
					obj.complete();
				}
			}
		})
	}
}

function addToSelectBox(selector, value, label) {
	var option = '';
	option += '<option value="'+ value +'">';
	option += label;
	option += '</option>';
	jQuery(selector).append(option);
}

/**
 * set dropdown option
 * @param object. Dropdown selector
 */
function set_dropdown_option(data, default_val) {
	var html;
	for (var i = 0; i <= data.length - 1; i++) {
		if (data[i][0] == default_val) {
			html += "<option value='"+ data[i][0] +"' selected='true'>"+ data[i][1] +"</option>";
		} else {
			html += "<option value='"+ data[i][0] +"'>"+ data[i][1] +"</option>";
		}
	};

	return html;
};