<?php

require BASEPATH . 'models/m_ruang.php';
require BASEPATH . 'models/m_detail_tempat_tidur.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('datatable') )
{

	function datatable()
	{

		$query = array();
		$query['query'] = '';

		// columns index
		$column_index = array('kd_kamar', 'nama', 'kelas_rs', 'kelas_bpjs', 'ruang', 'kapasitas', 'tersedia', 'tersedia_perempuan', 'tersedia_lakilaki', 'tersedia_lakilaki_perempuan');

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['query'] .= " WHERE " . $column_index[1] . " LIKE '%{$search_value}%'";

		}
		else {

			$i = 0;

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $i == 0 ) {
						$query['query'] .= " WHERE ";
					}
					else {
						$query['query'] .= " AND ";
					}

					if ( $column_key == 0 ) {
						$query['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
					}
					else {
						$query['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
					}

					$i++;

				}

			}

		}

		$records_total = Models\Ruang\count($query);
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order'] = $column_index[$order_column] . " " . $order_dir;
			}
		}

		// limit
		$query['limit'] = $_GET['length'];
		$query['offset'] = $_GET['start'];

		$data = Models\Ruang\get($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('get') )
{

	function get()
	{

		$kd_kamar = xss_clean($_GET['kd_kamar']);

		$result = Models\Ruang\row( array (
			'query' => "WHERE kd_kamar = '{$kd_kamar}'"
		) );

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "204",
					'message' => "Data tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $result
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------