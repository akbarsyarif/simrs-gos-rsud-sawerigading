<script type="text/javascript">
	jQuery(document).ready(function() {
		var table = jQuery('#datatable-ketersediaan-kamar').DataTable({
			displayLength: 100,
			ordering : true,
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_ . "index2.php?link=aplicares/rest&c=aplicares&a=datatable" ?>',
			columns : [
				{
					className : "text-center",
					data : "kd_kamar",
				},
				{
					data : "nama",
				},
				{
					data : "kelas_rs",
				},
				{
					data : "kelas_bpjs",
				},
				{
					data : "ruang",
				},
				{
					data : "kapasitas",
				},
				{
					data : "tersedia",
				},
				{
					data : "tersedia_perempuan",
				},
				{
					data : "tersedia_lakilaki",
				},
				{
					data : "tersedia_lakilaki_perempuan",
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button data-kd_kamar="'+ row.kd_kamar +'" class="insert-aplicares btn btn-xs btn-block btn-success"><i class="fa fa-fw fa-plus"></i></button>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button data-kd_kamar="'+ row.kd_kamar +'" class="update-aplicares btn btn-xs btn-block btn-info"><i class="fa fa-fw fa-refresh"></i></button>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button data-kd_kamar="'+ row.kd_kamar +'" class="delete-aplicares btn btn-xs btn-block btn-danger"><i class="fa fa-fw fa-trash"></i></button>';
					}
				},
			],
		});

		// menambahkan kamar ke aplicares
		jQuery('#datatable-ketersediaan-kamar tbody').on('click', '.insert-aplicares', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url: '<?= _BASE_ . "bridging_proses_ketersediaan_kamar.php?reqdata=add_kamar" ?>',
				method: 'post',
				dataType: 'json',
				data: {
					kodekelas: data.kelas_bpjs,
					koderuang: data.kd_kamar,
					namaruang: data.nama,
					kapasitas: data.kapasitas,
					tersedia: data.tersedia,
					tersediapria: data.tersedia_lakilaki,
					tersediawanita: data.tersedia_perempuan,
					tersediapriawanita: data.tersedia_lakilaki_perempuan
				},
				beforeSend: function() {
					jQuery('body').waitMe({
						effect: 'rotation',
						text: 'Tunggu...',
						bg: 'rgba(255,255,255,0.7)',
						color: '#000',
						waitTime: -1,
						textPos: 'vertical'
					});
				},
				success: function(xhr) {
					try {
						if ( xhr.metadata.code != "200" ) {
							swal('Error', xhr.metadata.message, 'error');
							console.log(xhr);
						}
						else {
							swal('Berhasil', 'Data kamar berhasil ditambahkan ke aplicares', 'success');
						}
					} catch(e) {
						swal('Error', e, 'error');
						console.log(e);
					}
				},
				error: function(xhr) {
					try {
						let error = JSON.parse(xhr.responseText);
						swal('Error', error.metadata.message, 'error');
					} catch(e) {
						swal('Error', xhr, 'error');
						console.log(e);
					}
				},
				complete: function() {
					jQuery('body').waitMe('hide');
				}
			});
		});

		// memperbahrui informasi ketersediaan bed ke aplicares
		jQuery('#datatable-ketersediaan-kamar tbody').on('click', '.update-aplicares', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url: '<?= _BASE_ . "bridging_proses_ketersediaan_kamar.php?reqdata=update_kamar" ?>',
				method: 'post',
				dataType: 'json',
				data: {
					kodekelas: data.kelas_bpjs,
					koderuang: data.kd_kamar,
					namaruang: data.nama,
					kapasitas: data.kapasitas,
					tersedia: data.tersedia,
					tersediapria: data.tersedia_lakilaki,
					tersediawanita: data.tersedia_perempuan,
					tersediapriawanita: data.tersedia_lakilaki_perempuan
				},
				beforeSend: function() {
					jQuery('body').waitMe({
						effect: 'rotation',
						text: 'Tunggu...',
						bg: 'rgba(255,255,255,0.7)',
						color: '#000',
						waitTime: -1,
						textPos: 'vertical'
					});
				},
				success: function(xhr) {
					try {
						if ( xhr.metadata.code != "200" ) {
							swal('Error', xhr.metadata.message, 'error');
							console.log(xhr);
						}
						else {
							swal('Berhasil', 'Data kamar pada aplicares berhasil diperbahrui', 'success');
						}
					} catch(e) {
						swal('Error', e, 'error');
						console.log(e);
					}
				},
				error: function(xhr) {
					try {
						let error = JSON.parse(xhr.responseText);
						swal('Error', error.metadata.message, 'error');
					} catch(e) {
						swal('Error', xhr, 'error');
						console.log(e);
					}
				},
				complete: function() {
					jQuery('body').waitMe('hide');
				}
			});
		});

		// menghapus data kamar di aplicares
		jQuery('#datatable-ketersediaan-kamar tbody').on('click', '.delete-aplicares', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url: '<?= _BASE_ . "bridging_proses_ketersediaan_kamar.php?reqdata=delete_kamar" ?>',
				method: 'post',
				dataType: 'json',
				data: {
					kodekelas: data.kelas_bpjs,
					koderuang: data.kd_kamar
				},
				beforeSend: function() {
					jQuery('body').waitMe({
						effect: 'rotation',
						text: 'Tunggu...',
						bg: 'rgba(255,255,255,0.7)',
						color: '#000',
						waitTime: -1,
						textPos: 'vertical'
					});
				},
				success: function(xhr) {
					try {
						if ( xhr.metadata.code != "200" ) {
							swal('Error', xhr.metadata.message, 'error');
							console.log(xhr);
						}
						else {
							swal('Berhasil', 'Data kamar pada aplicares berhasil dihapus', 'success');
						}
					} catch(e) {
						swal('Error', e, 'error');
						console.log(e);
					}
				},
				error: function(xhr) {
					try {
						let error = JSON.parse(xhr.responseText);
						swal('Error', error.metadata.message, 'error');
					} catch(e) {
						swal('Error', xhr, 'error');
						console.log(e);
					}
				},
				complete: function() {
					jQuery('body').waitMe('hide');
				}
			});
		});
	});
</script>