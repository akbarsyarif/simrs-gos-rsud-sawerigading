<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>SINKRONISASI APLICARES BPJS</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered" id="datatable-ketersediaan-kamar">
					<thead>
						<th width="50">Kode</th>
						<th>Nama Kamar</th>
						<th>Kelas RS</th>
						<th>Kelas BPJS</th>
						<th>Ruang</th>
						<th>Kapasitas</th>
						<th>Tersedia</th>
						<th>Tersedia (L)</th>
						<th>Tersedia (P)</th>
						<th>Tersedia (L/P)</th>
						<th width="10"></th>
						<th width="10"></th>
						<th width="10"></th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- javascript -->
<?php require 'includes/js/index.js.php' ?>