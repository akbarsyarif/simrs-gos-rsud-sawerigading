<?php
require BASEPATH . 'models/m_pasien.php';
$id = (isset($_GET['id']) && !empty($_GET['id'])) ? xss_clean($_GET['id']) : NULL;
$pasien = Models\row_pasien( array (
	'query' => "WHERE m1.id = '{$id}'"
) );
?>

<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>EDIT DATA PASIEN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<?php if ( $pasien == FALSE ) : ?>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="alert alert-warning text-center">
						Data pasien dengan ID <b><?= $id ?></b> tidak ditemukan. Kembali ke halaman <a href="<?= _BASE_ . 'index.php?link=pasien&c=list_pasien' ?>">list data pasien</a>
					</div>
				</div>
			</div>
		<?php else : ?>
			<form id="form-edit-pasien" class="form-horizontal" method="post" action="<?= _BASE_ . 'index2.php?link=pasien&c=action_list_pasien&a=updatePasien' ?>">
				<input type="hidden" name="id" value="<?= $pasien['id'] ?>" />
				<input type="hidden" name="old_nomr" value="<?= $pasien['NOMR'] ?>" />
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<!-- identitas pasien -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-user"></i> Identitas Pasien</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nomr">Nomor Rekam Medis</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NOMR'] ?>" id="nomr" name="nomr" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nama">Nama Pasien</label>
									<div class="col-md-5">
										<input type="text" value="<?= $pasien['NAMA'] ?>" id="nama" name="nama" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
									<label class="col-md-1 control-label" for="title">Title</label>
									<div class="col-md-2">
										<?= formDropdown( array (
											'name' => 'title',
											'options' => array (
												'' => '- Pilih -',
												'Tn' => 'Tn',
												'Ny' => 'Ny',
												'Nn' => 'Nn',
												'An' => 'An',
											),
											'selected' => $pasien['TITLE'],
											'attr' => 'class="form-control input-sm required" id="title" title="Tidak boleh kosong"'
										) ) ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tmp_lahir">Tempat & Tgl. Lahir</label>
									<div class="col-md-4">
										<input type="text" value="<?= $pasien['TEMPAT'] ?>" id="tmp_lahir" name="tmp_lahir" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
									<div class="col-md-4">
										<input type="text" value="<?= $pasien['TGLLAHIR'] ?>" id="tgl_lahir" name="tgl_lahir" class="form-control input-sm datepicker required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nik">No. Induk Kependudukan</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NOKTP'] ?>" id="nik" name="nik" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="notelp">No. Telephone / HP</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NOTELP'] ?>" id="notelp" name="notelp" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="pekerjaan_pasien">Pekerjaan Pasien</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PEKERJAAN'] ?>" id="pekerjaan_pasien" name="pekerjaan_pasien" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
							</fieldset>

							<!-- cara bayar -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-briefcase"></i> Cara Bayar</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="kd_carabayar">Cara Bayar</label>
									<div class="col-md-8">
										<?= dinamycDropdown(array(
											'name' => 'kd_carabayar',
											'table' => 'm_carabayar',
											'key' => 'KODE',
											'value' => 'NAMA',
											'selected' => $pasien['KDCARABAYAR'],
											'empty_first' => TRUE,
											'first_value' => '- Pilih Cara Bayar -',
											'order_by' => 'ORDERS asc',
											'attr' => 'class="form-control input-sm required" title="Tidak boleh kosong" id="kd_carabayar"',
										)) ?>
										<p class="help-block">Metode pembayaran ini akan digunakan sebagai metode pembayaran default setiap kali pasien berobat (Masih dapat dirubah oleh petugas pendaftaran).</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="no_kartu">No. Kartu <i>(Jika ada)</i></label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NO_KARTU'] ?>" id="no_kartu" name="no_kartu" class="form-control input-sm" />
									</div>
								</div>
							</fieldset>

							<!-- alamat -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-home"></i> Alamat Pasien</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="alamat">Alamat Pasien</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['ALAMAT'] ?>" id="alamat" name="alamat" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="alamat_ktp">Alamat <i>(Sesuai KTP)</i></label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['ALAMAT_KTP'] ?>" id="alamat_ktp" name="alamat_ktp" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="id_provinsi">Provinsi</label>
									<div class="col-md-8">
										<?= dinamycDropdown(array(
											'name' => 'id_provinsi',
											'table' => 'm_provinsi',
											'key' => 'idprovinsi',
											'value' => 'namaprovinsi',
											'selected' => $pasien['KDPROVINSI'],
											'empty_first' => TRUE,
											'first_value' => '- Pilih Provinsi -',
											'order_by' => 'namaprovinsi asc',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_provinsi"',
										)) ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="id_kota">Kabupaten/Kota</label>
									<div class="col-md-8">
										<?= dinamycDropdown(array(
											'name' => 'id_kota',
											'table' => 'm_kota',
											'where' => "idprovinsi = '" . $pasien['KDPROVINSI'] . "'",
											'key' => 'idkota',
											'value' => 'namakota',
											'selected' => $pasien['KOTA'],
											'empty_first' => TRUE,
											'first_value' => '- Pilih Kota -',
											'order_by' => 'namakota asc',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_kota"',
										)) ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="id_kecamatan">Kecamatan</label>
									<div class="col-md-8">
										<?= dinamycDropdown(array(
											'name' => 'id_kecamatan',
											'table' => 'm_kecamatan',
											'where' => "idkota = '" . $pasien['KOTA'] . "'",
											'key' => 'idkecamatan',
											'value' => 'namakecamatan',
											'selected' => $pasien['KDKECAMATAN'],
											'empty_first' => TRUE,
											'first_value' => '- Pilih Kecamatan -',
											'order_by' => 'namakecamatan asc',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_kecamatan"',
										)) ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="id_kelurahan">Desa/Kelurahan</label>
									<div class="col-md-8">
										<?= dinamycDropdown(array(
											'name' => 'id_kelurahan',
											'table' => 'm_kelurahan',
											'where' => "idkecamatan = '" . $pasien['KDKECAMATAN'] . "'",
											'key' => 'idkelurahan',
											'value' => 'namakelurahan',
											'selected' => $pasien['KELURAHAN'],
											'empty_first' => TRUE,
											'first_value' => '- Pilih Kelurahan -',
											'order_by' => 'namakelurahan asc',
											'attr' => 'class="form-control input-sm required select2-single" title="Tidak boleh kosong" id="id_kelurahan"',
										)) ?>
									</div>
								</div>
							</fieldset>

							<!-- data orangtua -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-user-plus"></i> Orangtua</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nama_ayah">Nama Ayah</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NAMA_AYAH'] ?>" id="nama_ayah" name="nama_ayah" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="pekerjaan_ayah">Pekerjaan Ayah</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PEKERJAAN_AYAH'] ?>" id="pekerjaan_ayah" name="pekerjaan_ayah" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nama_ibu">Nama Ibu</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NAMA_IBU'] ?>" id="nama_ibu" name="nama_ibu" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="pekerjaan_ibu">Pekerjaan Ibu</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PEKERJAAN_IBU'] ?>" id="pekerjaan_ibu" name="pekerjaan_ibu" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
							</fieldset>

							<!-- data pasangan -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-user-plus"></i> Pasangan</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nama_pasangan">Nama Suami/Istri</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['NAMA_PASANGAN'] ?>" id="nama_pasangan" name="nama_pasangan" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="pekerjaan_pasangan">Pekerjaan Suami/Istri</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PEKERJAAN_PASANGAN'] ?>" id="pekerjaan_pasangan" name="pekerjaan_pasangan" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Jenis Kelamin</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'jenis_kelamin',
										'options' => array (
											'L' => 'Laki - Laki',
											'P' => 'Perempuan',
										),
										'selected' => $pasien['JENISKELAMIN'],
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Status Perkawinan</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'status',
										'options' => array (
											'1' => 'Belum Kawin',
											'2' => 'Kawin',
											'3' => 'Janda / Duda',
										),
										'selected' => $pasien['STATUS'],
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Pendidikan Terakhir</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'pendidikan',
										'options' => array (
											'0' => 'Belum Sekolah',
											'1' => 'SD',
											'2' => 'SLTP',
											'3' => 'SMU',
											'4' => 'D3/Akademik',
											'5' => 'Universitas',
										),
										'selected' => $pasien['PENDIDIKAN'],
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Agama</label>
								<div class="col-md-8">
									<?= formRadio( array (
										'name' => 'agama',
										'options' => array (
											'1' => 'Islam',
											'2' => 'Kristen Protestan',
											'3' => 'Khatolik',
											'4' => 'Hindu',
											'5' => 'Budha',
											'6' => 'Lain - Lain',
										),
										'selected' => $pasien['AGAMA'],
										'attr' => 'class="required" title="Tidak boleh kosong"'
									) ) ?>
								</div>
							</div>

							<!-- penanggung jawab -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-user-plus"></i> Penanggung Jawab</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="penanggungjawab_nama">Nama Penanggung Jawab</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PENANGGUNGJAWAB_NAMA'] ?>" id="penanggungjawab_nama" name="penanggungjawab_nama" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="penanggungjawab_hubungan">Hubungan Dengan Pasien</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PENANGGUNGJAWAB_HUBUNGAN'] ?>" id="penanggungjawab_hubungan" name="penanggungjawab_hubungan" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="penanggungjawab_alamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PENANGGUNGJAWAB_ALAMAT'] ?>" id="penanggungjawab_alamat" name="penanggungjawab_alamat" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="penanggungjawab_phone">No. Telephone / HP</label>
									<div class="col-md-8">
										<input type="text" value="<?= $pasien['PENANGGUNGJAWAB_PHONE'] ?>" id="penanggungjawab_phone" name="penanggungjawab_phone" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
							</fieldset>

							<!-- otoritas -->
							<fieldset class="lumen">
								<legend><i class="fa fa-fw fa-user-secret"></i> Otoritas Pengawas</legend>
								<div class="form-group">
									<label class="col-md-4 control-label" for="username_spv">Username SPV</label>
									<div class="col-md-8">
										<input type="text" id="username_spv" name="username_spv" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="password_spv">Password SPV</label>
									<div class="col-md-8">
										<input type="password" id="password_spv" name="password_spv" class="form-control input-sm required" title="Tidak boleh kosong" />
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="text-center">
								<button type="submit" class="btn btn-sm btn-primary">Simpan Perubahan</button>
								<a href="<?= _BASE_ . 'index.php?link=pasien&c=list_pasien' ?>" class="btn btn-sm btn-default">Batal</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		<?php endif; ?>
	</div>
</div>

<!-- javascript -->
<?php require 'includes/js/edit_pasien.js.php' ?>