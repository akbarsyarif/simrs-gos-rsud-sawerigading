<div class="modal fade" id="modal-merger-pasien">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="form-merger" action="<?= _BASE_ . 'index2.php?link=pasien&c=action_list_pasien&a=mergerPasien' ?>">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-users"></i> Merger Data Pasien</h4>
				</div>
				<div class="modal-body" style="padding:0">
					<div class="row">
						<div class="col-sm-12">
							<div style="padding:10px; border-bottom:solid 1px #DDDDDD">
								<div class="alert alert-warning" style="margin-bottom:0">
									<h4>Peringatan!</h4>
									Fasilitas <i>MERGER DATA PASIEN</i> digunakan untuk menggabungkan beberapa riwayat rekam medis pasien menjadi satu. Hati - hati menggunakan fitur ini, pastikan anda tahu apa yang anda lakukan. Penggabungan data riwayat rekam medis pasien hanya boleh dilakukan di bawah pengawasan otoritas instalasi rekam medis. Jika anda tidak sengaja membuka window ini, silahkan tutup kembali dengan mengklik tombol batal.
								</div>
							</div>
						</div>
					</div>
					<div class="row no-margin" style="display:flex">
						<div class="col-md-6" style="align-items:stretch">
							<div id="pasien-a-result">
								<table class="table table-striped table-horizontal">
									<tr>
										<th colspan="3">
											<div class="row row-sm">
												<div class="col-sm-10">
													<input type="text" class="form-control required" title="Pasien A tidak boleh kosong" name="nomr_a" placeholder="Nomor rekam medis Pasien A" />
												</div>
												<div class="col-sm-2">
													<button type="button" class="btn btn-block btn-default" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i>" id="btn-search-a"><i class="fa fa-fw fa-search"></i></button>
												</div>
											</div>
										</th>
									</tr>
									<tr>
										<th style="width:150px !important">Nama Pasien</th><td width="10">:</td><td class="nama"></td>
									</tr>
									<tr>
										<th>Tempat & Tgl. Lahir</th><td>:</td><td class="ttl"></td>
									</tr>
									<tr>
										<th>NIK</th><td>:</td><td class="nik"></td>
									</tr>
									<tr>
										<th>No. JKN</th><td>:</td><td class="nojkn"></td>
									</tr>
									<tr>
										<th>No. Telephone / HP</th><td>:</td><td class="notelp"></td>
									</tr>
									<tr>
										<th>Jenis Kelamin</th><td>:</td><td class="jk"></td>
									</tr>
									<tr>
										<th>Status Perkawinan</th><td>:</td><td class="status"></td>
									</tr>
									<tr>
										<th>Pendidikan Terakhir</th><td>:</td><td class="pendidikan"></td>
									</tr>
									<tr>
										<th>Agama</th><td>:</td><td class="agama"></td>
									</tr>
									<tr>
										<th colspan="3"><center>.:: RIWAYAT PELAYANAN ::.</center></th>
									</tr>
								</table>
								<div class="riwayat_pelayanan"></div>
							</div>
						</div>
						<div class="col-md-6" style="align-items:stretch; border-left:solid 1px #DDDDDD">
							<div id="pasien-b-result">
								<table class="table table-striped table-horizontal">
									<tr>
										<th colspan="3">
											<div class="row row-sm">
												<div class="col-sm-10">
													<input type="text" class="form-control required" title="Pasien B tidak boleh kosong" name="nomr_b" placeholder="Nomor rekam medis Pasien B" />
												</div>
												<div class="col-sm-2">
													<button type="button" class="btn btn-block btn-default" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i>" id="btn-search-b"><i class="fa fa-fw fa-search"></i></button>
												</div>
											</div>
										</th>
									</tr>
									<tr>
										<th style="width:150px !important">Nama Pasien</th><td width="10">:</td><td class="nama"></td>
									</tr>
									<tr>
										<th>Tempat & Tgl. Lahir</th><td>:</td><td class="ttl"></td>
									</tr>
									<tr>
										<th>NIK</th><td>:</td><td class="nik"></td>
									</tr>
									<tr>
										<th>No. JKN</th><td>:</td><td class="nojkn"></td>
									</tr>
									<tr>
										<th>No. Telephone / HP</th><td>:</td><td class="notelp"></td>
									</tr>
									<tr>
										<th>Jenis Kelamin</th><td>:</td><td class="jk"></td>
									</tr>
									<tr>
										<th>Status Perkawinan</th><td>:</td><td class="status"></td>
									</tr>
									<tr>
										<th>Pendidikan Terakhir</th><td>:</td><td class="pendidikan"></td>
									</tr>
									<tr>
										<th>Agama</th><td>:</td><td class="agama"></td>
									</tr>
									<tr>
										<th colspan="3"><center>.:: RIWAYAT PELAYANAN ::.</center></th>
									</tr>
								</table>
								<div class="riwayat_pelayanan"></div>
							</div>
						</div>
					</div>
					<div class="row no-margin" style="display:flex">
						<div class="col-md-6" style="align-items:stretch">
							<div class="form-group">
								<div class="text-center">
									<?= formRadio( array (
										'name' => 'rujukan',
										'options' => array (
											'pasien_a' => 'Rujukan Utama'
										),
										'selected' => '',
										'attr' => 'class="required" title="Pilih salah satu"'
									) ); ?>
									<p class="help-block">Pilih jika data Pasien A yang akan dipertahankan</p>
								</div>
							</div>
						</div>
						<div class="col-md-6" style="align-items:stretch; border-left:solid 1px #DDDDDD">
							<div class="form-group">
								<div class="text-center">
									<?= formRadio( array (
										'name' => 'rujukan',
										'options' => array (
											'pasien_b' => 'Rujukan Utama'
										),
										'selected' => '',
										'attr' => 'class="required" title="Pilih salah satu"'
									) ); ?>
									<p class="help-block">Pilih jika data Pasien B yang akan dipertahankan</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-sm btn-success">Merger</button>
				</div>
			</form>
		</div>
	</div>
</div>