<div class="modal fade" id="modal-search-pasien">
	<div class="modal-dialog">
		<form id="form-search-pasien" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-search-plus"></i> Pencarian Lanjutan</h4>
				</div>
				<div class="modal-body">
					<div class="row row-sm">
						<div class="col-sm-6">
							<div class="form-group">
								<label>No. Rekam Medis</label>
								<input type="text" class="form-control" data-column="2" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama Pasien</label>
								<input type="text" class="form-control" data-column="3" />
							</div>
						</div>
					</div>
					<div class="row row-sm">
						<div class="col-sm-6">
							<div class="form-group">
								<label>L/P</label>
								<select class="form-control" data-column="4">
									<option value="" selected>-</option>
									<option value="L">L</option>
									<option value="P">P</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Tgl. Lahir</label>
								<input type="text" class="form-control" data-column="5" placeholder="YYYY-MM-DD" />
							</div>
						</div>
					</div>
					<div class="row row-sm">
						<div class="col-sm-6">
							<div class="form-group">
								<label>No. Induk Kependudukan</label>
								<input type="text" class="form-control" data-column="6" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>No. Peserta JKN</label>
								<input type="text" class="form-control" data-column="7" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning btn-sm" data-dismiss="modal" id="clear">Bersihkan</button>
					<button type="submit" class="btn btn-primary btn-sm">Cari</button>
				</div>
			</div>
		</form>
	</div>
</div>