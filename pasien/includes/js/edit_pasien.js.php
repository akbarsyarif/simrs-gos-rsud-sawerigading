<script type="text/javascript">
	jQuery(document).ready(function() {
		var formEditPasien = jQuery('#form-edit-pasien');

		// dropdown kota
		jQuery('#form-edit-pasien').on('change', '#id_provinsi', function() {
			var id_prov = jQuery(this).val();
			getKota({ url : '<?= _BASE_ ?>ajaxload.php', id_prov : id_prov, target : '#id_kota', selected : '' });
			jQuery('#id_kecamatan').html('<option value="" selected>- Pilih Kecamatan -</option>');
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kecamatan
		jQuery('#form-edit-pasien').on('change', '#id_kota', function() {
			var id_kota = jQuery(this).val();
			getKecamatan({ url : '<?= _BASE_ ?>ajaxload.php', id_kota : id_kota, target : '#id_kecamatan', selected : '' });
			jQuery('#id_kelurahan').html('<option value="" selected>- Pilih Kelurahan -</option>');
		});

		// dropdown kelurahan
		jQuery('#form-edit-pasien').on('change', '#id_kecamatan', function() {
			var id_kec = jQuery(this).val();
			getKelurahan({ url : '<?= _BASE_ ?>ajaxload.php', id_kec : id_kec, target : '#id_kelurahan', selected : '' });
		});

		formEditPasien.validate({
			submitHandler : function(form) {
				jQuery(form).ajaxSubmit({
					dataType : 'json',
					method : 'post',
					beforeSubmit : function(att, $form, options) {
						formEditPasien.find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( typeof r.metadata.code == 'undefined' ) {
							swal('Error', r, 'error');
							console.log(r);
						}
						else if ( r.metadata.code != 200 ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							swal('Success!', 'Informasi pasien berhasil diperbahrui', 'success').then(function() {
								location.reload();
							}, function(dismiss) {
								location.reload();
							});
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						formEditPasien.find('[type="submit"]').button('reset');
					}
				});
			}
		});
	});
</script>