<script type="text/javascript">
	function setResult(data, targetId) {
		var target = jQuery(targetId);

		target.find('.nama').html(data == null ? '' : data.dataPasien.NAMA);
		target.find('.ttl').html(data == null ? '' : data.dataPasien.TEMPAT + ', ' + moment(data.dataPasien.TGLLAHIR).format('DD-MM-YYYY'));
		target.find('.nik').html(data == null ? '' : data.dataPasien.NOKTP);
		target.find('.nojkn').html(data == null ? '' : data.dataPasien.NO_KARTU);
		target.find('.notelp').html(data == null ? '' : data.dataPasien.NOTELP);
		target.find('.jk').html(data == null ? '' : data.dataPasien.JENISKELAMIN);
		target.find('.status').html(data == null ? '' : data.dataPasien.STATUS_PERKAWINAN_LABEL);
		target.find('.pendidikan').html(data == null ? '' : data.dataPasien.PENDIDIKAN_LABEL);
		target.find('.agama').html(data == null ? '' : data.dataPasien.AGAMA_LABEL);
		
		// menampilkan riwayat pelayanan
		target.find('.riwayat_pelayanan').html(data == null ? '' : function() {
			html = '<table class="table table-striped table-horizontal">';
				html += '<tr>';
					html += '<th style="width:70px !important">Tgl. Register</th>';
					html += '<th style="width:80px !important">Poly</th>';
					html += '<th>Dokter</th>';
				html += '</tr>';
				for (var i = 0; i < data.riwayatPelayanan.length; i++) {
					html += '<tr>';
						html += '<td>' + moment(data.riwayatPelayanan[i].TGLREG).format('DD/MM/YYYY') + '</td>';
						html += '<td>' + data.riwayatPelayanan[i].NAMA_POLY + '</td>';
						html += '<td>' + data.riwayatPelayanan[i].NAMADOKTER + '</td>';
					html += '</tr>';
				};
			html += '</table>';

			return html;
		});
	}

	jQuery(document).ready(function() {
		var formMerger = jQuery('#form-merger');
		var modalMerger = jQuery('#modal-merger-pasien');
		
		function format ( d ) {
			// `d` is the original data object for the row
			var html = '';

			html += '<div class="row">';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100" valign="top">Alamat</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.ALAMAT+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Kelurahan</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.namakelurahan+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Kecamatan</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.namakecamatan+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Kabupaten/Kota</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.namakota+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Provinsi</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.namaprovinsi+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">Status</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.STATUS_PERKAWINAN_LABEL+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Agama</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.AGAMA_LABEL+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Pendidikan</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.PENDIDIKAN_LABEL+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Cara Bayar</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.CARABAYAR+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">No. JKN</th>';
							html += '<td width="10">:</td>';
							html += '<td>';
								if ( d.KDCARABAYAR == 2 ) {
									if ( d.NO_KARTU == "" ) {
										nokartu = "<i class='text-danger'>Belum Diisi</i>";
									}
									else {
										nokartu = d.NO_KARTU;
									}
								}
								else {
									nokartu = "<i class='text-info'>Bukan Pasien JKN</i>";
								}
								html += nokartu;
							html += '</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">Penanggung Jawab</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.PENANGGUNGJAWAB_NAMA+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Hubungan</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.PENANGGUNGJAWAB_HUBUNGAN+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">Alamat</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.PENANGGUNGJAWAB_ALAMAT+'</td>';
						html += '</tr>';
						html += '<tr>';
							html += '<th width="100">No. Telephone</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.PENANGGUNGJAWAB_PHONE+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
				html += '<div class="col-md-3">';
					html += '<table width="100%" class="padding">';
						html += '<tr>';
							html += '<th width="100">Petugas Pendaftaran</th>';
							html += '<td width="10">:</td>';
							html += '<td>'+d.NIP+'</td>';
						html += '</tr>';
					html += '</table>';
				html += '</div>';
			html += '</div>';

			return html;
		};

		var table = jQuery('#datatable-pasien').DataTable({
			displayLength: 10,
			ordering : true,
			order : [[2, 'desc']],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_ . "index2.php?link=pasien&c=action_list_pasien&a=datatable" ?>',
			columns : [
				{
					className : 'details-control',
					orderable : false,
					data : '',
					defaultContent : '<i class="fa fa-fw fa-plus-circle"></i> <i class="fa fa-fw fa-minus-circle"></i>'
				},
				{
					className : 'text-center',
					data : "TGLDAFTAR",
					mRender: function(data, type, row) {
						return moment(data).format('DD/MM/YYYY');
					}
				},
				{
					className : 'text-center',
					data : "NOMR"
				},
				{
					data : "NAMA",
					mRender: function(data, type, row) {
						if ( row.TITLE == "" || row.TITLE == null ) {
							return data;
						}
						else {
							return data + ', ' + row.TITLE;
						}
					}
				},
				{
					className : 'text-center',
					data : "JENISKELAMIN"
				},
				{
					data : "TGLLAHIR",
					mRender: function(data, type, row) {
						return row.TEMPAT + ', ' + moment(data).format('DD-MM-YYYY');
					}
				},
				{
					data : "NOKTP",
					className : "text-center",
				},
				{
					data : "NO_KARTU",
					className : "text-center",
					mRender: function(data, type, row) {
						if ( row.KDCARABAYAR == 2 ) {
							if ( row.NO_KARTU == "" ) {
								html = "<i class='text-danger'>Belum Diisi</i>";
							}
							else {
								html = row.NO_KARTU;
							}
						}
						else {
							html = "<i class='text-info'>Bukan Pasien JKN</i>";
						}

						return html;
					}
				},
				{
					data : "NOTELP",
					className : "text-center"
				},
				{ data : "SUAMI_ORTU" },
				{
					orderable: false,
					data: "",
					mRender: function(data, type, row) {
						let html = '';
						html += '<div class="dropdown">';
							html += '<button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdown-printout" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
								html += '<i class="fa fa-fw fa-print"></i> <span class="caret"></span>';
							html += '</button>';
							html += '<ul class="dropdown-menu" aria-labelledby="dropdown-printout">';
								html += '<li><a href="<?= _BASE_ . "index2.php?link=pasien&c=laporan&a=mr1&nomr='+row.NOMR+'" ?>" target="_blank">MR.1</a></li>';
							html += '</ul>';
						html += '</div>';
						return html;
					}
				},
				<?php if ( $_SESSION['ROLES'] == 1 ) : ?>
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<a href="<?= _BASE_ ?>index.php?link=pasien&c=edit_pasien&id='+row.id+'" class="edit btn btn-xs btn-warning btn-block"><i class="fa fa-fw fa-pencil"></i></a>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button class="delete btn btn-xs btn-danger btn-block"><i class="fa fa-fw fa-trash"></i></button>';
					}
				}
				<?php endif; ?>
			],
		});

		// Add event listener for opening and closing details
		jQuery('#datatable-pasien').on('click', 'td.details-control', function () {
			var tr = jQuery(this).closest('tr');
			var row = table.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		});

		// search
		jQuery('#form-search-pasien').on('submit', function() {
			jQuery('#modal-search-pasien').modal('hide');

			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#clear').on('click', function() {
			jQuery('#form-search-pasien').resetForm();
			jQuery('#modal-search-pasien').modal('hide');

			jQuery('#form-search-pasien .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});

		// delete data pasien
		jQuery('#datatable-pasien tbody').on('click', '.delete', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			swal({
				title: "Hapus Data?",
				type: "warning",
				html: 
					"Anda akan menghapus data pasien dengan nomor rekam medis <b class='text-danger'>" + data.NOMR + "</b> a/n <b class='text-danger'>" + data.NAMA + "</b>" +
					"<p>" +
					"<div class='form-group'><input type='text' id='username-spv' class='form-control input-lg' placeholder='Username SPV' style='text-align:center' /></div>" +
					"<div class='form-group'><input type='password' id='password-spv' class='form-control input-lg' placeholder='Password SPV' style='text-align:center' /></div>" +
					"</p>",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				preConfirm: function() {
					return new Promise(function (resolve, reject) {
						var usernameSpv = jQuery('#username-spv').val();
						var passwordSpv = jQuery('#password-spv').val();

						if ( usernameSpv === "" ) {
							swal.showValidationError('Username SPV tidak boleh kosong');
							reject();
						}
						else if ( passwordSpv === "" ) {
							swal.showValidationError('Password SPV tidak boleh kosong');
							reject();
						}
						else {
							jQuery.ajax({
								url: '<?= _BASE_ . "index2.php?link=pasien&c=action_list_pasien&a=deletepasien" ?>',
								method: 'get',
								dataType: 'json',
								data: {
									usernameSpv : usernameSpv,
									passwordSpv : passwordSpv,
									id : data.id,
									nomr : data.NOMR
								},
								success: function(r) {
									if ( r.metadata.code != "200" ) {
										swal('Eror', r.metadata.message, 'error');
										console.log(r);
									}
									else {
										swal('Success', 'Data berhasil dihapus', 'success');
										table.ajax.reload(null, false);
									}
								},
								error: function(e) {
									swal('Error', 'Error 400. Bad request', 'error');
									console.log(e);
								}
							});
						}
					});
				},
			}).then(function(result) {
				swal('Success!', 'Berhasil', 'success');
			}, function(dismiss) {});
		});

		// get pasien a
		jQuery('#btn-search-a').on('click', function(e) {
			var nomr_a = jQuery('[name="nomr_a"]').val();
			var button = jQuery(this);
			var targetId = '#pasien-a-result';

			jQuery.ajax({
				url: "<?= _BASE_ . 'index2.php?link=pasien&c=action_list_pasien&a=getRiwayatPelayanan' ?>",
				method: 'get',
				dataType: 'json',
				data: {
					nomr: nomr_a
				},
				beforeSend: function() {
					jQuery('[name="nomr_a"]').attr('readonly', 'true');
					button.button('loading');
				},
				success: function(r) {
					if ( r.metadata.code != "200" ) {
						console.log(r);
						swal('Error', r.metadata.message, 'error');
						setResult(r.response, targetId);
					}
					else {
						setResult(r.response, targetId);
					}
				},
				error: function(e) {
					swal('Error', 'Error 400. Bad request', 'error');
					console.log(e);
				},
				complete: function() {
					jQuery('[name="nomr_a"]').removeAttr('readonly');
					button.button('reset');
				}
			});
		});

		jQuery('[name="nomr_a"]').keypress(function(e) {
			if (e.which == 13) {
				e.preventDefault();
				jQuery('#btn-search-a').trigger('click');
			}
		});

		// get pasien b
		jQuery('#btn-search-b').on('click', function(e) {
			var nomr_a = jQuery('[name="nomr_b"]').val();
			var button = jQuery(this);
			var targetId = '#pasien-b-result';

			jQuery.ajax({
				url: "<?= _BASE_ . 'index2.php?link=pasien&c=action_list_pasien&a=getRiwayatPelayanan' ?>",
				method: 'get',
				dataType: 'json',
				data: {
					nomr: nomr_a
				},
				beforeSend: function() {
					jQuery('[name="nomr_b"]').attr('readonly', 'true');
					button.button('loading');
				},
				success: function(r) {
					if ( r.metadata.code != "200" ) {
						console.log(r);
						swal('Error', r.metadata.message, 'error');
						setResult(r.response, targetId);
					}
					else {
						setResult(r.response, targetId);
					}
				},
				error: function(e) {
					swal('Error', 'Error 400. Bad request', 'error');
					console.log(e);
				},
				complete: function() {
					jQuery('[name="nomr_b"]').removeAttr('readonly');
					button.button('reset');
				}
			});
		});

		jQuery('[name="nomr_b"]').keypress(function(e) {
			if (e.which == 13) {
				e.preventDefault();
				jQuery('#btn-search-b').trigger('click');
			}
		});

		// submit form merger pasien
		formMerger.find('[type="submit"]').on('click', function(e) {
			// e.preventDefault();

			// data pasien a
			var pasienA = {
				nomr: formMerger.find('#pasien-a-result [name="nomr_a"]').val(),
				nama: formMerger.find('#pasien-a-result .nama').html()
			}

			// data pasien b
			var pasienB = {
				nomr: formMerger.find('#pasien-b-result [name="nomr_b"]').val(),
				nama: formMerger.find('#pasien-b-result .nama').html()
			}

			// rujukan utama
			var rujukan = formMerger.find('[name="rujukan"]:checked').val();

			// form validation
			formMerger.validate({
				submitHandler: function(form) {
					// data pasien a
					pasienA = {
						nomr: formMerger.find('#pasien-a-result [name="nomr_a"]').val(),
						nama: formMerger.find('#pasien-a-result .nama').html()
					}

					// data pasien b
					pasienB = {
						nomr: formMerger.find('#pasien-b-result [name="nomr_b"]').val(),
						nama: formMerger.find('#pasien-b-result .nama').html()
					}

					// rujukan utama
					rujukanUtama = formMerger.find('[name="rujukan"]:checked').val();

					// form validation
					if ( pasienA.nomr == pasienB.nomr ) {
						swal('Validation Error', 'Tidak dapat menggabungkan data pasien yang sama. Silahkan pilih pasien yang berbeda.', 'error');
					} else {
						swal({
							title: "Merger Data Pasien?",
							type: "warning",
							html: 
								"Anda akan menggabungkan data rekam medis pasien <b class='text-danger'>" + pasienA.nomr + "</b> a/n <b class='text-danger'>" + pasienA.nama + "</b> dengan pasien <b class='text-danger'>" + pasienB.nomr + "</b> a/n <b class='text-danger'>" + pasienB.nama + "</b>" +
								"<p>" +
								"<div class='form-group'><input type='text' id='username-spv' class='form-control input-lg' placeholder='Username SPV' style='text-align:center' /></div>" +
								"<div class='form-group'><input type='password' id='password-spv' class='form-control input-lg' placeholder='Password SPV' style='text-align:center' /></div>" +
								"</p>",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Ya",
							cancelButtonText: "Tidak",
							preConfirm: function() {
								return new Promise(function (resolve, reject) {
									var usernameSpv = jQuery('#username-spv').val();
									var passwordSpv = jQuery('#password-spv').val();

									if ( usernameSpv === "" ) {
										swal.showValidationError('Username SPV tidak boleh kosong');
										reject();
									}
									else if ( passwordSpv === "" ) {
										swal.showValidationError('Password SPV tidak boleh kosong');
										reject();
									}
									else {
										jQuery.ajax({
											url: '<?= _BASE_ . "index2.php?link=pasien&c=action_list_pasien&a=mergerpasien" ?>',
											method: 'post',
											dataType: 'json',
											data: {
												username_spv : usernameSpv,
												password_spv : passwordSpv,
												nomr_a : pasienA.nomr,
												nomr_b : pasienB.nomr,
												rujukan : rujukanUtama
											},
											success: function(r) {
												if ( r.metadata.code != "200" ) {
													swal('Eror', r.metadata.message, 'error');
													console.log(r);
												}
												else {
													swal('Success', 'Data rekam medis pasien berhasil digabungkan', 'success');
													formMerger.resetForm();
													setResult(null, '#pasien-a-result');
													setResult(null, '#pasien-b-result');
													modalMerger.modal('hide');
													table.ajax.reload(null, false);
												}
											},
											error: function(e) {
												swal('Error', 'Error 400. Bad request', 'error');
												console.log(e);
											}
										});
									}
								});
							},
						}).then(function(result) {}, function(dismiss) {});
					}
				}
			});
		});
	});
</script>