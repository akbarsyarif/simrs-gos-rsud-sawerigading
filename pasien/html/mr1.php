<!DOCTYPE html>
<html>
<head>
	<title>MR.1</title>
	<style type="text/css">
		@media screen {
			.frame {
				margin: 0 auto;
				width: 210mm;
				height: 297mm;
				border: solid 1px #000000;
				-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
				-moz-box-sizing: border-box; /* Firefox, other Gecko */
				box-sizing: border-box; /* Opera/IE 8+ */
			}
		}
		@media print {
			.hide-print {
				display: none;
			}
			.frame {
				margin: 0 auto;
				width: 210mm;
				height: 295mm;
				border: none;
				-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
				-moz-box-sizing: border-box; /* Firefox, other Gecko */
				box-sizing: border-box; /* Opera/IE 8+ */
			}
		}
		@media all {
			body {
				font-family: 'Calibri';
				font-size: 11pt;
			}

			.frame {
				padding: 1cm 1cm 1cm 1.8cm;
				position: relative;
			}

			.text-center {
				text-align: center;
			}

			.footer {
				position: absolute;
				bottom: 1cm;
				font-weight: bold;
				font-style: italic;
			}

			.header .nama-rs {
				font-weight: bold;
				font-size: 12pt;
				color: #00B050;
			}
			.header .kontak-rs {
				font-size: 8pt;
				color: #7030A0;
			}
			.header .nama-dokumen {
				font-weight: bold;
				font-size: 12pt;
				color: #00B050;
			}
			table {
				border-collapse: collapse;
				width: 100% !important;
			}
			td {
				font-weight: normal;
				vertical-align: top;
				padding: 0;
				margin: 0;
			}
			h3 {
				font-size: 12pt;
				margin: 0 !important;
			}
			th.wrap-text,
			td.wrap-text {
				white-space: normal;
			}
			small {
				font-size: 8pt;
			}
			hr {
				border: none;
				border-bottom: solid 1px #000000;
			}

			.table-content {
				margin-bottom: 80px;
			}

			.table-content table {
				border-collapse: collapse;
			}
			.table-content table td {
				padding: 10px 10px;
				border-color: #000000;
			}

			.table-content .judul {
				font-size: 16pt;
				color: #0070C0;
				font-weight: bold;
			}

			.table-content .nomr td {
				text-align: center;
			}

			.table-content .table-info {
				border: solid 1px #000000;
				border-top: none;
			}

			.table-content .table-info tr td {
				border-top: none !important;
			}

			.table-content .field {
				width: 130px !important;
			}
			.table-content .equal {
				width: 10px !important;
				padding-left: 5px !important;
				padding-right: 5px !important;
				text-align: center !important;
			}

			table.no-padding td {
				padding: 0;
			}

			.no-padding {
				padding: 0;
			}

			.group {
				margin-bottom: 10px;
			}
		}
	</style>
</head>
<body>
	<div class="frame">
		<table style="margin-bottom:0.9cm" class="header">
			<tr>
				<td width="70%">
					<table>
						<tr>
							<td rowspan="2" width="60">
								<span class="logo"><img src="assets/img/logo-rsud2.gif" width="50"></span>
							</td>
							<td style="vertical-align:bottom">
								<span class="nama-rs">RSUD SAWERIGADING KOTA PALOPO</span>
							</td>
						</tr>
						<tr>
							<td style="vertical-align:top">
								<span class="kontak-rs">Jl. Dr. Ratulangi Km.7 Rampoang Telp. (0471) 3312133 Fax. (0471) 3312200</span>
							</td>
						</tr>
					</table>
				</td>
				<td width="30%" align="right">
					<span class="nama-dokumen">MR.1/RJ/B/2017</span>
				</td>
			</tr>
		</table>
		<table class="table-content">
			<tr>
				<td>
					<table class="nomr" border="1">
						<tr>
							<td width="70%" rowspan="2" style="vertical-align:middle"><span class="judul">IDENTITAS PASIEN</span></td>
							<td colspan="6">Nomor Rekam Medis</td>
						</tr>
						<tr>
							<td><?= $nomrSplit[0] ?></td>
							<td><?= $nomrSplit[1] ?></td>
							<td><?= $nomrSplit[2] ?></td>
							<td><?= $nomrSplit[3] ?></td>
							<td><?= $nomrSplit[4] ?></td>
							<td><?= $nomrSplit[5] ?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Nama Lengkap</td>
						<td class="equal">:</td>
						<td class="value"><?= $data['NAMA'] ?>, <?= $data['TITLE'] ?></td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Alamat Lengkap</td>
						<td class="equal">:</td>
						<td class="value">
							<table class="no-padding">
								<tr>
									<td colspan="6"><div class="group"><?= $data['ALAMAT'] ?></div></td>
								</tr>
								<tr>
									<td width="60"><div class="group">Desa/Kel</div></td><td class="equal">:</td><td width="100"><?= $data['namakelurahan'] ?></td>
									<td width="10">Kec</td><td class="equal">:</td><td><?= $data['namakecamatan'] ?></td>
								</tr>
								<tr>
									<td><div class="group">Kab/Kota</div></td><td class="equal">:</td><td><?= $data['namakota'] ?></td>
									<td><div class="group">No.Telp/Hp</div></td><td class="equal">:</td><td><?= $data['NOTELP'] ?></td>
								</tr>
							</table>
						</td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info" border="1">
						<td>
							<div class="group">Tempat Lahir :</div>
							<?= $data['TEMPAT'] ?>
						</td>
						<td>
							<div class="group">Tgl.Lahir :</div>
							<?= $data['TGLLAHIR_CAL'] ?>
						</td>
						<td>
							<div class="group">Umur :</div>
							<?= $data['UMUR_PASIEN'] ?>
						</td>
						<td>
							<div class="group">Sex :</div>
							<?= $data['JENISKELAMIN'] ?>
						</td>
						<td>
							<div class="group">Status :</div>
							<?= $data['STATUS_PERKAWINAN_LABEL'] ?>
						</td>
						<td>
							<div class="group">Agama :</div>
							<?= $data['AGAMA_LABEL'] ?>
						</td>
						<td>
							<div class="group">Pendidikan :</div>
							<?= $data['PENDIDIKAN_LABEL'] ?>
						</td>
						<td>
							<div class="group">Pekerjaan :</div>
							<?= $data['PEKERJAAN'] ?>
						</td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Nama Ayah/Ibu</td>
						<td class="equal">:</td>
						<td class="value" width="180"><?= $nama_orangtua ?></td>
						<td class="field" style="width:80px !important">Pekerjaan</td>
						<td class="equal">:</td>
						<td class="value"><?= $pekerjaan_orangtua ?></td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Nama Suami/Istri</td>
						<td class="equal">:</td>
						<td class="value" width="180"><?= $data['NAMA_PASANGAN'] ?></td>
						<td class="field" style="width:80px !important">Pekerjaan</td>
						<td class="equal">:</td>
						<td class="value"><?= $data['PEKERJAAN_PASANGAN'] ?></td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Rujukan/Dikirim dari</td>
						<td class="equal">:</td>
						<td class="value"><?= $nama_provider ?></td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Cara Bayar</td>
						<td class="equal">:</td>
						<td class="value" width="180"><?= $data['CARABAYAR'] ?></td>
						<td class="field" style="width:80px !important">No.Jaminan</td>
						<td class="equal">:</td>
						<td class="value"><?= $data['NO_KARTU'] ?></td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Status Kepesertaan</td>
						<td class="equal">:</td>
						<td class="value"><?= $jenis_peserta ?></td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Tanggal Kunjungan</td>
						<td class="equal">:</td>
						<td class="value" width="180"><?= $tgl_kunjungan ?></td>
						<td class="field" style="width:80px !important">Jam</td>
						<td class="equal">:</td>
						<td class="value"><?= $jam_kunjungan ?> WITA</td>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="table-info">
						<td class="field">Poliklinik Yang Dituju</td>
						<td class="equal">:</td>
						<td class="value"><?= $poli_tujuan ?></td>
					</table>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td></td>
				<td width="300">
					<div class="text-center">Petugas Pendaftaran,</div>
					<div style="height:80px"></div>
					<div class="text-center">( ............................................ )</div>
					<div class="text-center"><small><i>Nama Jelas dan Tanda Tangan</i></small></div>
				</td>
			</tr>
		</table>
		<div class="footer">
			SIMRS RSUD Sawerigading Palopo
		</div>
	</div>
	<p align="center" class="hide-print">
		<button class="print" onclick="window.print()">Cetak</button>
	</p>
</body>
</html>