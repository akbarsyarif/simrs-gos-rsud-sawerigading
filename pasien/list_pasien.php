<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>DATA PASIEN</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<?php if ( $_SESSION['ROLES'] == 1 ) : ?>
					<a href="<?= _BASE_ . 'index.php?link=2' ?>" class="btn btn-sm btn-primary" title="Pendaftaran pasien baru">Pendaftaran Pasien</a>
					<button href="#modal-merger-pasien" data-toggle="modal" class="btn btn-sm btn-success" title="Menggabungkan dua riwayat pasien">Merger Data Pasien</button>
				<?php endif; ?>
				<button href="#modal-search-pasien" data-toggle="modal" class="btn btn-sm btn-default">Cari Data Pasien</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered" id="datatable-pasien">
					<thead>
						<th width="10"></th>
						<th width="70">Awal Daftar</th>
						<th width="50">No. RM</th>
						<th>Nama</th>
						<th width="10">L/P</th>
						<th>Tgl. Lahir</th>
						<th width="120">NIK</th>
						<th width="100">No. JKN</th>
						<th width="100">No. Telephone</th>
						<th>Suami/Keluarga</th>
						<th width="10"><!-- print out --></th>
						<?php if ( $_SESSION['ROLES'] == 1 ) : ?>
							<th width="10"></th>
							<th width="10"></th>
						<?php endif; ?>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modals/modal_search_pasien.php' ?>
<?php require 'includes/modals/modal_merger_pasien.php' ?>

<!-- javascript -->
<?php require 'includes/js/list_pasien.js.php' ?>