<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'include/pasien_helper.php';
require BASEPATH . 'models/m_pasien.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'include/bpjs_api.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('mr1') ) {

	function mr1()
	{

		$nomr = (isset($_GET['nomr'])) ? xss_clean($_GET['nomr']) : NULL;

		if ( $nomr === NULL ) {
			$error_msg[] = 'Nomor rekam medis tidak boleh kosong.';
		}

		// data pelayanan
		$data = Models\row_pasien(array(
			'query' => "WHERE m1.NOMR = '{$nomr}'"
		));

		if ( $data === FALSE ) {
			$error_msg[] = 'Data pasien tidak ditemukan.';
		}

		if ( isset($error_msg) && count($error_msg) > 0 ) {
			show_error($error_msg, TRUE);
		} else {
			// split nomor rekam medis
			$nomrSplit = str_split($data['NOMR']);

			// nama orangtua
			$nama_orangtua = (!empty($data['NAMA_AYAH']) && $data['NAMA_AYAH'] != '-') ? $data['NAMA_AYAH'] : $data['NAMA_IBU'];
			// pekerjaan orangtua
			$pekerjaan_orangtua = (!empty($data['NAMA_AYAH']) && $data['NAMA_AYAH'] != '-') ? $data['PEKERJAAN_AYAH'] : $data['PEKERJAAN_IBU'];

			$jenis_peserta = '-';
			$nama_provider = '-';

			if ( $data['KDCARABAYAR'] == 2 ) {
				// data kepesertaan bpjs
				$bpjs = get_peserta('nokartu', $data['NO_KARTU']);
				if ( $bpjs !== FALSE && $bpjs->metadata->code == 200 ) {
					$jenis_peserta = $bpjs->response->peserta->jenisPeserta->nmJenisPeserta;
					$nama_provider = $bpjs->response->peserta->provUmum->nmProvider;
				}
			}

			// data pelayanan pertama
			$pelayanan = Models\row_pendaftaran(array(
				'limit' => 1,
				'order' => "t1.IDXDAFTAR ASC",
				'query' => "WHERE t1.NOMR = '{$nomr}'"
			));

			$tgl_kunjungan = '-';
			$jam_kunjungan = '-';
			$poli_tujuan = '-';

			if ( $pelayanan !== FALSE ) {
				$tgl_kunjungan = $pelayanan['TGLREG_CAL'];
				// jam kunjungan
				$jam_tmp = explode(':', $pelayanan['JAMREG']);
				$jam_kunjungan = $jam_tmp[0].':'.$jam_tmp[1];
				$poli_tujuan = $pelayanan['NAMA_POLY'];
			}

			require BASEPATH . 'pasien/html/mr1.php';
		}

	}

}

//---------------------------------------------------------------------------------