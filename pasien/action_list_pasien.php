<?php
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';
require BASEPATH . 'include/pasien_helper.php';
require BASEPATH . 'models/m_pasien.php';
require BASEPATH . 'models/m_pendaftaran.php';
require BASEPATH . 'models/m_auth.php';
require BASEPATH . 'thirdparty/GUMP-master/gump.class.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('dataTable') ) 
{

	function dataTable()
	{

		$query = array();
		$query['query'] = '';

		// columns index
		$column_index = array(NULL, 'm1.TGLDAFTAR', 'm1.NOMR', 'm1.NAMA', 'm1.JENISKELAMIN', 'm1.TGLLAHIR', 'm1.NOKTP', 'm1.NO_KARTU', 'm1.NOTELP', 'm1.SUAMI_ORTU');

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['query'] .= "WHERE " . $column_index[2] . " LIKE '%$search_value%'";

		}
		else {

			$i = 0;

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $i == 0 ) {
						$query['query'] .= " WHERE ";
					}
					else {
						$query['query'] .= " AND ";
					}

					if ( $column_key == 2 || $column_key == 4 ) {
						$query['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
					}
					else {
						$query['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
					}

					$i++;

				}

			}

		}

		$records_total = Models\count_pasien($query);
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order'] = $column_index[$order_column] . " " . $order_dir;
			}
		}

		// limit
		$query['limit'] = $_GET['length'];
		$query['offset'] = $_GET['start'];

		$data = Models\get_pasien($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('getPasien') )
{

	function getPasien()
	{

		// get query string
		$id = (isset($_GET['id']) && !empty($_GET['id'])) ? xss_clean($_GET['id']) : NULL;

		$result = Models\row_pasien( array (
			'query' => "WHERE m1.id = '{$id}'"
		) );

		if ( $id == NULL OR $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "Data tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $result
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('updatePasien') )
{

	function updatePasien()
	{

		// form validation
		$gump = new GUMP();
		$validation_rules = array ( 
			'id' => 'required',
			'old_nomr' => 'required',
			'nomr' => 'required',
			'nama' => 'required',
			'title' => 'required',
			'tmp_lahir' => 'required',
			'tgl_lahir' => 'required',
			'nik' => 'required',
			'notelp' => 'required',
			'pekerjaan_pasien' => 'required',
			'kd_carabayar' => 'required',
			'alamat' => 'required',
			'alamat_ktp' => 'required',
			'id_provinsi' => 'required',
			'id_kota' => 'required',
			'id_kecamatan' => 'required',
			'id_kelurahan' => 'required',
			'nama_ayah' => 'required',
			'pekerjaan_ayah' => 'required',
			'nama_ibu' => 'required',
			'pekerjaan_ibu' => 'required',
			'nama_pasangan' => 'required',
			'pekerjaan_pasangan' => 'required',
			'jenis_kelamin' => 'required',
			'status' => 'required',
			'pendidikan' => 'required',
			'agama' => 'required',
			'penanggungjawab_nama' => 'required',
			'penanggungjawab_hubungan' => 'required',
			'penanggungjawab_alamat' => 'required',
			'penanggungjawab_phone' => 'required',
			'username_spv' => 'required',
			'password_spv' => 'required'
		);

		// conditional validation
		// jika metode pembayaran menggunakan JKN
		if ( $_POST['kdcarabayar'] == 2 ) {
			$validation_rules['no_kartu'] = 'required';
		}

		$gump->validation_rules($validation_rules);

		if ( $gump->run($_POST) === FALSE ) {
			
			$response = (object) array (
				'metadata' => (object) array (
					'code' => 400,
					'message' => $gump->get_readable_errors(true)
				)
			);

		} else {

			$username_spv = (isset($_POST['username_spv'])) ? xss_clean($_POST['username_spv']) : '';
			$password_spv = (isset($_POST['password_spv'])) ? xss_clean($_POST['password_spv']) : '';

			$id_pasien = (isset($_POST['id'])) ? xss_clean($_POST['id']) : '';
			$old_nomr = (isset($_POST['old_nomr'])) ? xss_clean($_POST['old_nomr']) : '';
			$nomr = (isset($_POST['nomr'])) ? xss_clean($_POST['nomr']) : '';
			
			// data pasien
			$data = array (
				'NAMA'                     => (isset($_POST['nama'])) ? xss_clean($_POST['nama']) : '',
				'TITLE'                    => (isset($_POST['title'])) ? xss_clean($_POST['title']) : '',
				'TEMPAT'                   => (isset($_POST['tmp_lahir'])) ? xss_clean($_POST['tmp_lahir']) : '',
				'TGLLAHIR'                 => (isset($_POST['tgl_lahir'])) ? xss_clean($_POST['tgl_lahir']) : '',
				'NOKTP'                    => (isset($_POST['nik'])) ? xss_clean($_POST['nik']) : '',
				'NOTELP'                   => (isset($_POST['notelp'])) ? xss_clean($_POST['notelp']) : '',
				'PEKERJAAN'                => (isset($_POST['pekerjaan_pasien'])) ? xss_clean($_POST['pekerjaan_pasien']) : '',
				'ALAMAT'                   => (isset($_POST['alamat'])) ? xss_clean($_POST['alamat']) : '',
				'ALAMAT_KTP'               => (isset($_POST['alamat_ktp'])) ? xss_clean($_POST['alamat_ktp']) : '',
				'KDPROVINSI'               => (isset($_POST['id_provinsi'])) ? xss_clean($_POST['id_provinsi']) : '',
				'KOTA'                     => (isset($_POST['id_kota'])) ? xss_clean($_POST['id_kota']) : '',
				'KDKECAMATAN'              => (isset($_POST['id_kecamatan'])) ? xss_clean($_POST['id_kecamatan']) : '',
				'KELURAHAN'                => (isset($_POST['id_kelurahan'])) ? xss_clean($_POST['id_kelurahan']) : '',
				'NAMA_AYAH'                => (isset($_POST['nama_ayah'])) ? xss_clean($_POST['nama_ayah']) : '',
				'PEKERJAAN_AYAH'           => (isset($_POST['pekerjaan_ayah'])) ? xss_clean($_POST['pekerjaan_ayah']) : '',
				'NAMA_IBU'                 => (isset($_POST['nama_ibu'])) ? xss_clean($_POST['nama_ibu']) : '',
				'PEKERJAAN_IBU'            => (isset($_POST['pekerjaan_ibu'])) ? xss_clean($_POST['pekerjaan_ibu']) : '',
				'NAMA_PASANGAN'            => (isset($_POST['nama_pasangan'])) ? xss_clean($_POST['nama_pasangan']) : '',
				'PEKERJAAN_PASANGAN'       => (isset($_POST['pekerjaan_pasangan'])) ? xss_clean($_POST['pekerjaan_pasangan']) : '',
				'PENANGGUNGJAWAB_NAMA'     => (isset($_POST['penanggungjawab_nama'])) ? xss_clean($_POST['penanggungjawab_nama']) : '',
				'PENANGGUNGJAWAB_HUBUNGAN' => (isset($_POST['penanggungjawab_hubungan'])) ? xss_clean($_POST['penanggungjawab_hubungan']) : '',
				'PENANGGUNGJAWAB_ALAMAT'   => (isset($_POST['penanggungjawab_alamat'])) ? xss_clean($_POST['penanggungjawab_alamat']) : '',
				'PENANGGUNGJAWAB_PHONE'    => (isset($_POST['penanggungjawab_phone'])) ? xss_clean($_POST['penanggungjawab_phone']) : '',
				'JENISKELAMIN'             => (isset($_POST['jenis_kelamin'])) ? xss_clean($_POST['jenis_kelamin']) : '',
				'STATUS'                   => (isset($_POST['status'])) ? xss_clean($_POST['status']) : '',
				'PENDIDIKAN'               => (isset($_POST['pendidikan'])) ? xss_clean($_POST['pendidikan']) : '',
				'AGAMA'                    => (isset($_POST['agama'])) ? xss_clean($_POST['agama']) : '',
				'KDCARABAYAR'              => (isset($_POST['kd_carabayar'])) ? xss_clean($_POST['kd_carabayar']) : ''
			);

			// jika kode cara bayar sama dengan 2 (JKN) maka isi informasi nomor kartu
			if ( $data['KDCARABAYAR'] == 2 ) $data['NO_KARTU'] = xss_clean($_POST['no_kartu']);

			// cek otoritas supervisor
			$is_spv = Models\Auth\is_supervisor($username_spv, $password_spv);
			if ( $is_spv == FALSE ) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "403",
						'message' => "Kombinasi username dan password salah. Silahkan hubungi supervisor."
					),
					'response' => NULL
				);

			}
			else {

				// update data pasien
				$result1 = Models\update_pasien($data, array ( 'id' => $id_pasien ));

				// update nomor rekam medis
				// jika nomor rekam medis berubah
				if ( $old_nomr != $nomr ) {

					// cek apakah nomor rekam medis baru sudah digunakan oleh pasien lain
					$noMrExists = isNoMrExists($nomr, $id_pasien);
					if ( $noMrExists !== FALSE ) {

						$response = (object) array (
							'metadata' => (object) array (
								'code' => "422",
								'message' => "Nomor rekam medis <b class='text-danger'>" . $noMrExists['nomr'] . "</b> sudah digunakan oleh pasien a/n <b class='text-danger'>" . $noMrExists['nama'] ."</b>"
							),
							'response' => NULL
						);

						exit(json_encode($response));

					}
					else {

						try {
							
							$result2 = Models\updateNoMr($nomr, $old_nomr);
						
						} catch (Exception $e) {

							$response = (object) array (
								'metadata' => (object) array (
									'code' => "500",
									'message' => $e->getMessage()
								),
								'response' => NULL
							);

							exit(json_encode($response));
						}
						
					}

				}

				if ( $result1 == FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "500",
							'message' => "Internal Server Error. Gagal mengupdate data pasien."
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}

			}

		}

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('deletePasien') )
{

	function deletePasien()
	{

		$username_spv = (isset($_GET['usernameSpv'])) ? xss_clean($_GET['usernameSpv']) : '';
		$password_spv = (isset($_GET['passwordSpv'])) ? xss_clean($_GET['passwordSpv']) : '';

		$id = (isset($_GET['id'])) ? xss_clean($_GET['id']) : '';
		$nomr = (isset($_GET['nomr'])) ? xss_clean($_GET['nomr']) : '';

		// cek otoritas supervisor
		$is_spv = Models\Auth\is_supervisor($username_spv, $password_spv);
		if ( $is_spv == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "403",
					'message' => "Kombinasi username dan password salah. Silahkan hubungi supervisor."
				),
				'response' => NULL
			);

		}
		else {

			try {

				$result = Models\deletePasien($nomr);

				if ( $result == FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "500",
							'message' => "Internal server error. Gagal menghapus data."
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}
				
			} catch (Exception $e) {
				
				$response = (object) array (
					'metadata' => (object) array (
						'code' => "500",
						'message' => $e->getMessage()
					),
					'response' => NULL
				);

			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('getRiwayatPelayanan') )
{

	function getRiwayatPelayanan()
	{

		// get query string
		$nomr = (isset($_GET['nomr']) && !empty($_GET['nomr'])) ? xss_clean($_GET['nomr']) : NULL;

		// data pasien
		$pasien = Models\row_pasien( array (
			'query' => "WHERE m1.NOMR = '{$nomr}'"
		) );

		if ( $nomr == NULL OR $pasien == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "201",
					'message' => "Data pasien tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			// riwayat pelayanan
			$riwayat_pelayanan = Models\get_pendaftaran( array (
				'query' => "WHERE m1.NOMR = '{$nomr}'",
				'limit' => 10,
				'order' => "JAMREGRAW DESC"
			) );

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => (object) array (
					'dataPasien' => $pasien,
					'riwayatPelayanan' => $riwayat_pelayanan
				)
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('mergerPasien') )
{

	function mergerPasien()
	{

		$username_spv = (isset($_POST['username_spv'])) ? xss_clean($_POST['username_spv']) : '';
		$password_spv = (isset($_POST['password_spv'])) ? xss_clean($_POST['password_spv']) : '';

		// cek otoritas supervisor
		$is_spv = Models\Auth\is_supervisor($username_spv, $password_spv);

		if ( $is_spv == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "403",
					'message' => "Kombinasi username dan password salah. Silahkan hubungi supervisor."
				),
				'response' => NULL
			);

		}
		else {

			$nomr_a = (isset($_POST['nomr_a'])) ? xss_clean($_POST['nomr_a']) : NULL;
			$nomr_b = (isset($_POST['nomr_b'])) ? xss_clean($_POST['nomr_b']) : NULL;

			// rujukan utama
			// jika nilai rujukan utama adalah pasien_a maka set $rujukan sama dengan $nomr_a
			// selain itu set $rujukan sama dengan $nomr_b
			$rujukan = (isset($_POST['rujukan'])) ? xss_clean($_POST['rujukan']) : NULL;
			if ( $rujukan == "pasien_a" ) {
				$nomr_rujukan = $nomr_a;
			}
			else if ( $rujukan == "pasien_b" ) {
				$nomr_rujukan = $nomr_b;
			}
			else {
				$nomr_rujukan = NULL;
			}

			try {

				$result = Models\merger_pasien($nomr_a, $nomr_b, $nomr_rujukan);

				if ( $result == FALSE ) {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "500",
							'message' => "Error 500. Gagal menggabungkan data pasien"
						),
						'response' => NULL
					);

				}
				else {

					$response = (object) array (
						'metadata' => (object) array (
							'code' => "200",
							'message' => "OK"
						),
						'response' => NULL
					);

				}
				
			} catch (Exception $e) {

				$response = (object) array (
					'metadata' => (object) array (
						'code' => "201",
						'message' => $e->getMessage()
					),
					'response' => NULL
				);
				
			}

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------