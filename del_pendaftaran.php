<?php
session_start();

require 'include/connect.php';
require BASEPATH . 'include/function.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'include/error_helper.php';

// load models
require BASEPATH . 'models/m_auth.php';
require BASEPATH . 'models/m_pasien.php';
require BASEPATH . 'models/m_pendaftaran.php';

$userspv = (isset($_POST['userspv'])) ? xss_clean($_POST['userspv']) : NULL;
$passspv = (isset($_POST['pwdspv'])) ? xss_clean($_POST['pwdspv']) : NULL;

// cek otorisasi supervisor
$result_spv = Models\Auth\is_supervisor($userspv, $passspv);

$error_msg = array();

if ( is_null($userspv) OR is_null($passspv) OR $result_spv === FALSE ) {
	$error_msg[] = 'Kombinasi user dan password salah. Silahkan hubungi penyelia anda.';
}

if ( count($error_msg) > 0 ) {

	show_error($error_msg);

}
else {

	$action = (isset($_POST['action'])) ? xss_clean($_POST['action']) : NULL;

	switch ( $action ) {
		case 'update':

			$idxdaftar     = (isset($_POST['IDXDAFTAR'])) ? xss_clean($_POST['IDXDAFTAR']) : NULL;
			$nomr          = (isset($_POST['NOMR'])) ? xss_clean($_POST['NOMR']) : NULL;
			$nobill        = (isset($_POST['nobill'])) ? xss_clean($_POST['nobill']) : NULL;
			$old_carabayar = (isset($_POST['old_carabayar'])) ? xss_clean($_POST['old_carabayar']) : NULL;
			
			// data administrasi baru
			$kdrujuk           = (isset($_POST['KDRUJUK'])) ? xss_clean($_POST['KDRUJUK']) : NULL;
			$kdcarabayar       = (isset($_POST['KDCARABAYAR'])) ? xss_clean($_POST['KDCARABAYAR']) : NULL;
			$kdpoly            = (isset($_POST['KDPOLY'])) ? xss_clean($_POST['KDPOLY']) : NULL;
			$kddokter          = (isset($_POST['KDDOKTER'])) ? xss_clean($_POST['KDDOKTER']) : NULL;
			$minta_rujukan     = (isset($_POST['MINTA_RUJUKAN'])) ? xss_clean($_POST['MINTA_RUJUKAN']) : NULL;
			$tglreg            = (isset($_POST['TGLREG'])) ? xss_clean($_POST['TGLREG']) : NULL;
			$lakalantas        = (isset($_POST['LAKALANTAS'])) ? xss_clean($_POST['LAKALANTAS']) : NULL;
			$lokasi_lakalantas = (isset($_POST['LOKASI_LAKALANTAS'])) ? xss_clean($_POST['LOKASI_LAKALANTAS']) : NULL;
			$catatan           = (isset($_POST['CATATAN'])) ? xss_clean($_POST['CATATAN']) : NULL;
			$shift             = (isset($_POST['SHIFT'])) ? xss_clean($_POST['SHIFT']) : NULL;

			// data peserta bpjs
			$no_kartu         = (isset($_POST['no_kartu'])) ? xss_clean($_POST['no_kartu']) : NULL;
			$kd_jenis_peserta = (isset($_POST['kd_jenis_peserta'])) ? xss_clean($_POST['kd_jenis_peserta']) : NULL;
			$nm_jenis_peserta = (isset($_POST['nm_jenis_peserta'])) ? xss_clean($_POST['nm_jenis_peserta']) : NULL;
			$kd_kelas         = (isset($_POST['kd_kelas'])) ? xss_clean($_POST['kd_kelas']) : NULL;
			$nm_kelas         = (isset($_POST['nm_kelas'])) ? xss_clean($_POST['nm_kelas']) : NULL;
			$no_rujukan       = (isset($_POST['no_rujukan'])) ? xss_clean($_POST['no_rujukan']) : NULL;
			$tgl_rujukan      = (isset($_POST['tgl_rujukan'])) ? xss_clean($_POST['tgl_rujukan']) : NULL;
			$kd_ppk_rujukan   = (isset($_POST['kd_ppk_rujukan'])) ? xss_clean($_POST['kd_ppk_rujukan']) : NULL;
			$nm_ppk_rujukan   = (isset($_POST['nm_ppk_rujukan'])) ? xss_clean($_POST['nm_ppk_rujukan']) : NULL;
			$kd_diagnosa      = (isset($_POST['kd_diagnosa'])) ? xss_clean($_POST['kd_diagnosa']) : NULL;
			$nm_diagnosa      = (isset($_POST['nm_diagnosa'])) ? xss_clean($_POST['nm_diagnosa']) : NULL;

			if ( $nobill != '' ) {
				$ss = mysql_query("SELECT * FROM t_bayarrajal WHERE nobill = '{$nobill}'");
				if ( mysql_num_rows($ss) > 0 ) {
					$val = mysql_fetch_array($ss);
					$status	= $val['status'];
				}
				else{
					show_error('No. Billing tidak terdaftar');
				}				
			}

			$jenispoly = $kdpoly;
			$kdprofesi = getProfesiDoktor($kddokter);
			$kodetarif = getKodePendaftaran($jenispoly, $kdprofesi);
			$tarif     = getTarif($kodetarif);

			$tarifrs            = $tarif['tarif'];
			$tarifjasasarana    = $tarif['jasa_sarana'];
			$tarifjasapelayanan = $tarif['jasa_pelayanan'];

			try {

				// update carabayar pasien
				$update_pasien = Models\update_pasien(
					array ( 'KDCARABAYAR' => $kdcarabayar ),
					array ( 'NOMR' => $nomr )
				);

				$data_pendaftaran = array (
					'KD_RUJUK' => $kdrujuk,
					'KDCARABAYAR' => $kdcarabayar,
					'KDPOLY' => $kdpoly,
					'KDDOKTER' => $kddokter,
					'MINTA_RUJUKAN' => $minta_rujukan,
					'TGLREG' => $tglreg,
					'JAMREG' => $tglreg,
					'LAKALANTAS' => $lakalantas,
					'LOKASI_LAKALANTAS' => $lokasi_lakalantas,
					'CATATAN' => $catatan,
					'SHIFT' => $shift
				);

				// jika carabayar menggunakan JKN/BPJS
				// update data peserta BPJS pada tabel pendaftaran
				if ( $kdcarabayar == "2" ) {

					$data_pendaftaran['NO_KARTU'] = $no_kartu;
					$data_pendaftaran['KD_JENIS_PESERTA'] = $kd_jenis_peserta;
					$data_pendaftaran['NM_JENIS_PESERTA'] = $nm_jenis_peserta;
					$data_pendaftaran['KD_RUJUK'] = $kd_rujuk;
					$data_pendaftaran['NO_RUJUKAN'] = $no_rujukan;
					$data_pendaftaran['TGL_RUJUKAN'] = $tgl_rujukan;
					$data_pendaftaran['KD_PPK_RUJUKAN'] = $kd_ppk_rujukan;
					$data_pendaftaran['NM_PPK_RUJUKAN'] = $nm_ppk_rujukan;
					$data_pendaftaran['KD_DIAGNOSA'] = $kd_diagnosa;
					$data_pendaftaran['NM_DIAGNOSA'] = $nm_diagnosa;
					$data_pendaftaran['KD_KELAS'] = $kd_kelas;

				}

				// update data pendaftaran
				$update_pendaftaran = Models\update_pendaftaran( $data_pendaftaran, $idxdaftar );
			
			} catch (Exception $e) {
			
				show_error($e->getMessage());

			}

			mysql_query("UPDATE t_bayarrajal
				         SET UNIT = '{$kdpoly}'
				         WHERE IDXDAFTAR = '{$idxdaftar}'
				         AND NOBILL = '{$nobill}'");
			
			mysql_query("UPDATE t_billrajal
				         SET KDPOLY = '{$kdpoly}',
				             UNIT = '{$kdpoly}',
				             KDDOKTER = '{$kddokter}',
				             CARABAYAR = '{$kdcarabayar}',
				             TARIFRS = '{$tarifrs}',
				             JASA_SARANA = '{$tarifjasasarana}',
				             JASA_PELAYANAN = '{$tarifjasapelayanan}'
				         WHERE IDXDAFTAR = '{$idxdaftar}'
				         AND NOBILL = '{$nobill}'");
			
			if ( $kdcarabayar > 1 ) {

				mysql_query("UPDATE t_bayarrajal
					         SET CARABAYAR = '{$kdcarabayar}',
					             UNIT = '{$kdpoly}',
					             JMBAYAR = 0,
					             TGLBAYAR = CURDATE(),
					             JAMBAYAR = CURTIME(),
					             SHIFT = '{$shift}',
					             LUNAS = 1,
					             STATUS = 'LUNAS'
					         WHERE IDXDAFTAR = '{$idxdaftar}'");

			}
			else {

				if ( $old_carabayar > 1 ) {

					mysql_query("UPDATE t_bayarrajal
						         SET UNIT = '{$kdpoly}',
						             CARABAYAR = '{$kdcarabayar}',
						             JMBAYAR = 0,
						             TGLBAYAR = '0000-00-00',
						             JAMBAYAR = '00:00:00',
						             SHIFT = '{$shift}',
						             LUNAS = 0,
						             STATUS = 'TRX',
						             TARIFRS = '{$tarifrs}',
						             JASA_SARANA = '{$tarifjasasarana}',
						             JASA_PELAYANAN = '{$tarifjasapelayanan}'
						         WHERE IDXDAFTAR = '{$idxdaftar}'
						         AND NOBILL = '{$nobill}'");

				}
				else {

					if ( $status == 'LUNAS' ) {
						
						mysql_query("UPDATE t_bayarrajal
							         SET UNIT = '{$kdpoly}',
							             CARABAYAR = '{$kdcarabayar}',
							             JMBAYAR = '{$tarifrs}',
							             TARIFRS = '{$tarifrs}',
							             JASA_SARANA = '{$tarifjasasarana}',
							             JASA_PELAYANAN = '{$tarifjasapelayanan}'
							         WHERE IDXDAFTAR = '{$idxdaftar}'
							         AND NOBILL = '{$nobill}'");
					
					}
					else {

						mysql_query("UPDATE t_bayarrajal
						             SET UNIT = '{$kdpoly}',
						                 CARABAYAR = '{$kdcarabayar}',
						                 JMBAYAR = 0,
						                 TARIFRS = '{$tarifrs}',
						                 JASA_SARANA = '{$tarifjasasarana}',
						                 JASA_PELAYANAN = '{$tarifjasapelayanan}'
						             WHERE IDXDAFTAR = '{$idxdaftar}'
						             AND NOBILL = '{$nobill}'");

					}

				}

			}

			redirect( _BASE_ . 'index.php?link=22' );

			break;

		case 'delete':
			
			// $sql = "DELETE FROM m_pasien WHERE NOMR = '{$nomr}'";
			// $row = mysql_query($sql) or die(mysql_error());
			
			$sql = "DELETE FROM t_pendaftaran WHERE IDXDAFTAR = '{$idxdaftar}'";
			$row = mysql_query($sql) or die(mysql_error());
			
			$sql = "DELETE FROM t_billrajal WHERE IDXDAFTAR = '{$idxdaftar}'";
			$row = mysql_query($sql) or die(mysql_error());	
		
			$sql = "DELETE FROM t_bayarrajal WHERE IDXDAFTAR = '{$idxdaftar}'";
			$row = mysql_query($sql) or die(mysql_error());	

			break;
		
		default:
			show_error('Aksi tidak diketahui');
			break;
	}

}