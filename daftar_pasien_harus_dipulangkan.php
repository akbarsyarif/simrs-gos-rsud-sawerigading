<?php
use Models;

require 'include/connect.php';
require BASEPATH . 'include/security_helper.php';
require BASEPATH . 'models/m_ranap.php';

$config = NULL;

if ( isset($_GET['limit']) ) {
	$config['limit'] = xss_clean($_GET['limit']);
}

$pasien = Models\Ranap\getPasienLunas($config);
$jumlah = count($pasien);

if ( $pasien == FALSE ) {

	$response = (object) array (
		'metadata' => (object) array (
			'code' => "204",
			'message' => "Data tidak ditemukan"
		),
		'response' => NULL
	);

}
else {

	$response = (object) array (
		'metadata' => (object) array (
			'code' => "200",
			'message' => "OK"
		),
		'response' => (object) array (
			'count' => $jumlah,
			'list' => $pasien
		)
	);

}

echo json_encode($response);