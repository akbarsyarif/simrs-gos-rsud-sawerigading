<script type="text/javascript">
	jQuery(document).ready(function() {
		var addModal = jQuery('#modal-add-icd9');
		var addForm = jQuery('#form-add-icd9');
		var editModal = jQuery('#modal-edit-icd9');
		var editForm = jQuery('#form-edit-icd9');

		var table = jQuery('#datatable-icd9').DataTable({
			displayLength: 10,
			ordering : true,
			order : [[0, 'asc']],
			processing : true,
			serverSide : true,
			ajax : '<?= _BASE_ . "index2.php?link=icd9&c=icd9&a=datatableIcd9" ?>',
			columns : [
				{
					className : "text-center",
					data : "kode",
				},
				{
					data : "keterangan",
				},
				{
					data : "keterangan_local",
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button data-kode="'+ row.kode +'" class="edit btn btn-xs btn-block btn-warning"><i class="fa fa-fw fa-pencil"></i></button>';
					}
				},
				{
					orderable : false,
					data : "",
					mRender : function(data, type, row) {
						return '<button data-kode="'+ row.kode +'" class="delete btn btn-xs btn-block btn-danger"><i class="fa fa-fw fa-trash"></i></button>';
					}
				},
			],
		});

		// search
		jQuery('#form-search-icd9').on('submit', function() {
			jQuery('#modal-search-icd9').modal('hide');

			jQuery(this).find('.form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();

			return false;
		});

		// clear search filter
		jQuery('#clear').on('click', function() {
			jQuery('#form-search-icd9').resetForm();
			jQuery('#modal-search-icd9').modal('hide');

			jQuery('#form-search-icd9 .form-control').each(function() {
				var i = jQuery(this).attr('data-column');

				table.column(i).search(jQuery(this).val());
			});

			table.draw();
		});

		// submit add icd9 form
		addForm.validate({
			submitHandler : function(form) {
				jQuery(form).ajaxSubmit({
					dataType : 'json',
					beforeSubmit : function(arr, $form, options) {
						addForm.find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( r.metadata.code != "200" ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							swal('Success', 'Data berhasil ditambahkan', 'success');
							addModal.modal('hide');
							table.ajax.reload(null, false);
							$form.resetForm();
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						addForm.find('[type="submit"]').button('reset');
					}
				});
			}
		});

		// edit data icd9
		jQuery('#datatable-icd9 tbody').on('click', '.edit', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			jQuery.ajax({
				url: '<?= _BASE_ . "index2.php?link=icd9&c=icd9&a=getIcd9" ?>',
				method: 'get',
				dataType: 'json',
				data: {kode : data.kode},
				success: function(r) {
					if ( r.metadata.code != "200" ) {
						swal('Eror', r.metadata.message, 'error');
						console.log(r);
					}
					else {
						var d = r.response;

						editForm.find('[name="pk"]').val(d.kode);
						editForm.find('[name="kode"]').val(d.kode);
						editForm.find('[name="keterangan"]').val(d.keterangan);
						editForm.find('[name="keterangan_local"]').val(d.keterangan_local);

						editModal.modal('show');
					}
				},
				error: function(e) {
					swal('Error', 'Error 400. Bad request', 'error');
					console.log(e);
				}
			});
		});

		// submit edit icd9 form
		editForm.validate({
			submitHandler : function(form) {
				jQuery(form).ajaxSubmit({
					dataType : 'json',
					beforeSubmit : function(arr, $form, options) {
						editForm.find('[type="submit"]').button('loading');
					},
					success : function(r, statusText, xhr, $form) {
						if ( r.metadata.code != "200" ) {
							swal('Error', r.metadata.message, 'error');
							console.log(r);
						}
						else {
							swal('Success', 'Data berhasil disimpan', 'success');
							editModal.modal('hide');
							table.ajax.reload(null, false);
							$form.resetForm();
						}
					},
					error : function(e) {
						swal('Error', 'Tidak dapat menghubungi server', 'error');
						console.log(e);
					},
					complete : function() {
						editForm.find('[type="submit"]').button('reset');
					}
				});
			}
		});

		// delete icd9
		jQuery('#datatable-icd9 tbody').on('click', '.delete', function(e) {
			var row = jQuery(this).closest('tr');
			var data = table.row(row).data();

			swal({
				title: "Hapus Data?",
				text: "Anda akan menghapus data dengan kode " + data.kode,
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				closeOnConfirm: false
			},
			function(isConfirm) {
				if ( isConfirm == true ) {
					jQuery.ajax({
						url: '<?= _BASE_ . "index2.php?link=icd9&c=icd9&a=delete" ?>',
						method: 'get',
						dataType: 'json',
						data: {kode : data.kode},
						success: function(r) {
							if ( r.metadata.code != "200" ) {
								swal('Eror', r.metadata.message, 'error');
								console.log(r);
							}
							else {
								swal('Success', 'Data berhasil dihapus', 'success');
								table.ajax.reload(null, false);
							}
						},
						error: function(e) {
							swal('Error', 'Error 400. Bad request', 'error');
							console.log(e);
						}
					});
				}
			});
		});
	});
</script>