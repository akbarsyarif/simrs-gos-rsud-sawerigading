<div class="modal fade" id="modal-add-icd9">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-add-icd9" action="<?= _BASE_ . 'index2.php?link=icd9&c=icd9&a=insert' ?>" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-plus-circle"></i> Tambah Data ICD 9</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Kode</label>
						<input type="text" name="kode" class="form-control required" />
					</div>
					<div class="form-group">
						<label>Deskripsi</label>
						<input type="text" name="keterangan" class="form-control required" />
					</div>
					<div class="form-group">
						<label>Deksripsi Lokal</label>
						<input type="text" name="keterangan_local" class="form-control" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> Sedang memproses...">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>