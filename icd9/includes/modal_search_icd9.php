<div class="modal fade" id="modal-search-icd9">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST" id="form-search-icd9" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-fw fa-search-plus"></i> Pencarian Lanjutan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Kode</label>
						<input type="text" name="kode" class="form-control" data-column="0" />
					</div>
					<div class="form-group">
						<label>Deskripsi</label>
						<input type="text" name="keterangan" class="form-control" data-column="1" />
					</div>
					<div class="form-group">
						<label>Deskripsi Lokal</label>
						<input type="text" name="keterangan_local" class="form-control" data-column="2" />
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="clear" class="btn btn-warning btn-sm" data-dismiss="modal">Bersihkan</button>
					<button type="submit" class="btn btn-primary btn-sm">Cari</button>
				</div>
			</form>
		</div>
	</div>
</div>