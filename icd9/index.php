<div class="frame">
	<div id="frame_title">
		<div class="row">
			<div class="col-md-12">
				<h3>Master ICD9 CM</h3>
			</div>
		</div>
	</div>
	<div class="frame_body">
		<div class="row form-group">
			<div class="col-md-6">
				<a href="#modal-add-icd9" data-toggle="modal" class="btn btn-sm btn-primary">Tambah</a>
				<a href="#modal-search-icd9" data-toggle="modal" class="btn btn-sm btn-default">Cari Data ICD</a>
			</div>
			<div class="col-md-6"></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered" id="datatable-icd9">
					<thead>
						<th width="50">Kode</th>
						<th>Deskripsi</th>
						<th>Deskripsi Lokal</th>
						<th width="10"></th>
						<th width="10"></th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- modals -->
<?php require 'includes/modal_search_icd9.php' ?>
<?php require 'includes/modal_add_icd9.php' ?>
<?php require 'includes/modal_edit_icd9.php' ?>

<!-- javascript -->
<?php require 'includes/js/index.js.php' ?>