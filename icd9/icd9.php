<?php

require BASEPATH . 'models/m_icd9.php';

//---------------------------------------------------------------------------------

if ( ! function_exists('datatableIcd9') )
{

	function datatableIcd9()
	{

		$query = array();
		$query['query'] = '';

		// columns index
		$column_index = array('icd_cm.kode', 'icd_cm.keterangan', 'icd_cm.keterangan_local');

		// filter column handler / search
		$columns = $_GET['columns'];

		if ( ! empty($_GET['search']['value']) ) {

			$search_value = xss_clean($_GET['search']['value']);

			// global search
			$query['query'] .= " WHERE " . $column_index[0] . " = '{$search_value}'";

		}
		else {

			$i = 0;

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $i == 0 ) {
						$query['query'] .= " WHERE ";
					}
					else {
						$query['query'] .= " AND ";
					}

					if ( $column_key == 0 ) {
						$query['query'] .= $column_index[$column_key]." = '".$column_val['search']['value']."'";
					}
					else {
						$query['query'] .= $column_index[$column_key]." LIKE '%".$column_val['search']['value']."%'";
					}

					$i++;

				}

			}

		}

		$records_total = Models\ICD9\count($query);
		$records_filtered = $records_total;

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $_GET['order'] as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order'] = $column_index[$order_column] . " " . $order_dir;
			}
		}

		// limit
		$query['limit'] = $_GET['length'];
		$query['offset'] = $_GET['start'];

		$data = Models\ICD9\get($query);

		$response = (object) array (
			'draw' => $_GET['draw'],
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_filtered,
			'data' => $data
		);

		echo json_encode($response);
		
	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('getIcd9') )
{

	function getIcd9()
	{

		$kode = xss_clean($_GET['kode']);

		$result = Models\ICD9\row( array (
			'query' => "WHERE kode = '{$kode}'"
		) );

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "204",
					'message' => "Data tidak ditemukan"
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $result
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('insert') )
{

	function insert()
	{

		$data = array (
			'kode' => xss_clean($_POST['kode']),
			'keterangan' => xss_clean($_POST['keterangan']),
			'keterangan_local' => xss_clean($_POST['keterangan_local'])
		);

		$result = Models\ICD9\insert($data);

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "422",
					'message' => "Server error. Gagal menyimpan data."
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $data
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('update') )
{

	function update()
	{

		$pk = xss_clean($_POST['pk']);

		$data = array (
			'kode' => xss_clean($_POST['kode']),
			'keterangan' => xss_clean($_POST['keterangan']),
			'keterangan_local' => xss_clean($_POST['keterangan_local'])
		);

		$result = Models\ICD9\update($data, $pk);

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "422",
					'message' => "Server error. Gagal menyimpan data."
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $data
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------

if ( ! function_exists('delete') )
{

	function delete()
	{

		$kode = xss_clean($_GET['kode']);

		$result = Models\ICD9\delete($kode);

		if ( $result == FALSE ) {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "422",
					'message' => "Server error. Gagal menghapus data."
				),
				'response' => NULL
			);

		}
		else {

			$response = (object) array (
				'metadata' => (object) array (
					'code' => "200",
					'message' => "OK"
				),
				'response' => $kode
			);

		}

		echo json_encode($response);

	}

}

//---------------------------------------------------------------------------------