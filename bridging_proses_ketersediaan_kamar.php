<?php
include 'include/connect.php';

include BASEPATH . 'include/bpjs_api_ketersediaan_kamar.php';
include BASEPATH . 'include/function.php';
include BASEPATH . 'include/security_helper.php';

$reqdata = xss_clean($_GET['reqdata']);

switch ( $reqdata ) {
	case 'ref_kamar':
		$result = ref_kamar();
		if ( $result == false ) {
			echo json_encode(array(
				'metadata' => array(
					'code' => 500,
					'message' => 'Server error'
				)
			));
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'get_kamar':
		$start = (isset($_GET['start'])) ? xss_clean($_GET['start']) : 1;
		$limit = (isset($_GET['limit'])) ? xss_clean($_GET['limit']) : 1;

		$result = get_kamar($start, $limit);
		if ( $result == false ) {
			echo json_encode(array(
				'metadata' => array(
					'code' => 500,
					'message' => 'Server error'
				)
			));
		}
		else {
			echo json_encode($result);
		}
	break;

	case 'add_kamar':
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			unset($data);
			$data['kodekelas']          = ((isset($_POST['kodekelas'])) && (! empty($_POST['kodekelas']))) ? xss_clean($_POST['kodekelas']) : null;
			$data['koderuang']          = ((isset($_POST['koderuang'])) && (! empty($_POST['koderuang']))) ? xss_clean($_POST['koderuang']) : null;
			$data['namaruang']          = ((isset($_POST['namaruang'])) && (! empty($_POST['namaruang']))) ? xss_clean($_POST['namaruang']) : null;
			$data['kapasitas']          = ((isset($_POST['kapasitas'])) && (! empty($_POST['kapasitas']))) ? xss_clean($_POST['kapasitas']) : null;
			$data['tersedia']           = ((isset($_POST['tersedia'])) && (! empty($_POST['tersedia']))) ? xss_clean($_POST['tersedia']) : 0;
			$data['tersediapria']       = ((isset($_POST['tersediapria'])) && (! empty($_POST['tersediapria']))) ? xss_clean($_POST['tersediapria']) : 0;
			$data['tersediawanita']     = ((isset($_POST['tersediawanita'])) && (! empty($_POST['tersediawanita']))) ? xss_clean($_POST['tersediawanita']) : 0;
			$data['tersediapriawanita'] = ((isset($_POST['tersediapriawanita'])) && (! empty($_POST['tersediapriawanita']))) ? xss_clean($_POST['tersediapriawanita']) : 0;

			$result = add_kamar($data);
			if ( $result == false ) {
				echo json_encode(array(
					'metadata' => array(
						'code' => 500,
						'message' => 'Server error'
					)
				));
			}
			else {
				echo json_encode($result);
			}
		}
		else {
			http_response_code(404);
		}
	break;

	case 'update_kamar':
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			unset($data);
			$data['kodekelas']          = ((isset($_POST['kodekelas'])) && (! empty($_POST['kodekelas']))) ? xss_clean($_POST['kodekelas']) : null;
			$data['koderuang']          = ((isset($_POST['koderuang'])) && (! empty($_POST['koderuang']))) ? xss_clean($_POST['koderuang']) : null;
			$data['namaruang']          = ((isset($_POST['namaruang'])) && (! empty($_POST['namaruang']))) ? xss_clean($_POST['namaruang']) : null;
			$data['kapasitas']          = ((isset($_POST['kapasitas'])) && (! empty($_POST['kapasitas']))) ? xss_clean($_POST['kapasitas']) : null;
			$data['tersedia']           = ((isset($_POST['tersedia'])) && (! empty($_POST['tersedia']))) ? xss_clean($_POST['tersedia']) : 0;
			$data['tersediapria']       = ((isset($_POST['tersediapria'])) && (! empty($_POST['tersediapria']))) ? xss_clean($_POST['tersediapria']) : 0;
			$data['tersediawanita']     = ((isset($_POST['tersediawanita'])) && (! empty($_POST['tersediawanita']))) ? xss_clean($_POST['tersediawanita']) : 0;
			$data['tersediapriawanita'] = ((isset($_POST['tersediapriawanita'])) && (! empty($_POST['tersediapriawanita']))) ? xss_clean($_POST['tersediapriawanita']) : 0;

			$result = update_kamar($data);
			if ( $result == false ) {
				echo json_encode(array(
					'metadata' => array(
						'code' => 500,
						'message' => 'Server error'
					)
				));
			}
			else {
				echo json_encode($result);
			}
		}
		else {
			http_response_code(404);
		}
	break;

	case 'delete_kamar':
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
			unset($data);
			$data['kodekelas'] = ((isset($_POST['kodekelas'])) && (! empty($_POST['kodekelas']))) ? xss_clean($_POST['kodekelas']) : null;
			$data['koderuang'] = ((isset($_POST['koderuang'])) && (! empty($_POST['koderuang']))) ? xss_clean($_POST['koderuang']) : null;

			$result = delete_kamar($data);
			if ( $result == false ) {
				echo json_encode(array(
					'metadata' => array(
						'code' => 500,
						'message' => 'Server error'
					)
				));
			}
			else {
				echo json_encode($result);
			}
		}
		else {
			http_response_code(404);
		}
	break;
	
	default:
		http_response_code(404);
	break;
}