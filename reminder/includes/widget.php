<?php
require BASEPATH . 'models/m_reminder.php';

$result = Models\Reminder\get( array (
	'query' => "WHERE DATE(start_at) <= CURDATE() AND DATE(end_at) > CURDATE()"
) );

if ( $result !== FALSE ) {

	foreach ( $result as $data ) {

		echo '<div class="reminder-widget">';
			echo '<h3>' . $data['title'] . '</h3>';
			echo '<p>' . $data['content'] . '</p>';
		echo '</div>';

	}

}